/** @type {import('next').NextConfig} */
const nextConfig = {
  reactStrictMode: false,
  i18n: {
    locales: ["mn", "en"],
    defaultLocale: "mn",
    localeDetection: false,
  },
  webpack: function (config, options) {
    config.experiments = { ...config.experiments, asyncWebAssembly: true };
    return config;
  },
  // compiler: {
  // removeConsole: true,
  // },
  trailingSlash: true,
  output: "standalone",
};

module.exports = nextConfig;

const withBundleAnalyzer = require("@next/bundle-analyzer")({
  enabled: process.env.ANALYZE === "true",
});
module.exports = withBundleAnalyzer(nextConfig);
