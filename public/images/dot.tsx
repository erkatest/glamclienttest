const DotSvg = ({ color }: any) => {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      width="10"
      height="11"
      style={{ fill: color }}
      viewBox="0 0 10 11"
      fill="none"
    >
      <circle cx="5" cy="5.42773" r="5" fill="#FCC77B" />
    </svg>
  );
};
export default DotSvg;
