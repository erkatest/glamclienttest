FROM node:18.18.2-slim

WORKDIR /app

COPY package*.json .

RUN npm install

COPY . .

RUN npm run build


EXPOSE 3030

CMD ["npm", "run", "dev"]