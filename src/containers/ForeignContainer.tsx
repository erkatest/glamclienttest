import * as React from "react";
import { Formik, Form, Field, ErrorMessage } from "formik";
import * as Yup from "yup";
import Link from "next/link";
import useMessage from "~/hooks/useMessage";
import LoadingOverly from "~/components/libs/LoadingOverly";
import { useUser } from "~/context/auth";
import { toast } from "react-toastify";

const SignUpContainer = () => {
  const f = useMessage();
  const { signup, verify, resend } = useUser();
  const [loading, setLoading] = React.useState(false);
  const [data, setData] = React.useState<any>();
  const [verifyCod, setVerify] = React.useState("");
  const [done, setDone] = React.useState(false);
  const [show, setShow] = React.useState(false);

  return (
    <div className="container mx-auto px-8 my-20 max-w-7xl flex justify-center flex-col">
      <div className="flex items-center lg:space-x-10">
        <div className="flex-1 hidden lg:block relative">
          <div className="absolute top-0 left-0 right-0 bottom-0 flex items-center justify-center">
            <img src="/images/bg-404.png" />
          </div>
          <img src={"/images/phone.png"} className="relative w-1/2 mx-auto" />
        </div>
        <div className="w-[400px] max-w-full mx-auto">
          <div className="border mx-auto p-[20px] relative border-[#E6EAF4] border-t-[4px] border-t-c5">
            <LoadingOverly visible={loading} />
            <h3 className="text-[28px] mb-[24px] font-bold text-center text-c4">
              {f({ id: "signup-title" })}
            </h3>
            <p className="text-c4 text-center mb-5 text-[14px]">
              {done
                ? f(
                    { id: "signup-success" },
                    {
                      email: data?.email || "-----",
                      strong: (value: any) => {
                        return (
                          <span className="font-bold text-c6">{value[0]}</span>
                        );
                      },
                      a: (value: any) => {
                        return (
                          <button
                            className="underline text-blue-600"
                            onClick={async () => {
                              setLoading(true);
                              await resend(data?.email);
                              setLoading(false);
                            }}
                          >
                            {value[0]}
                          </button>
                        );
                      },
                    }
                  )
                : f({ id: "signup-desc" })}
            </p>

            {done ? (
              <>
                <img className="block mb-5" src="/images/signup.svg" />
                <input
                  className="w-full mb-6 p-2.5 border rounded-md"
                  placeholder="Баталгаажуулах код оруулах"
                  onChange={(e) => {
                    setVerify(e.target.value);
                  }}
                />
                <button
                  className="w-full bg-c5 hover:bg-c5/80 active:bg-c5 text-white p-2 rounded-[5px]"
                  onClick={async () => {
                    await verify(data?.email, verifyCod);
                  }}
                >
                  {f({ id: "signin-btn" })}
                </button>
              </>
            ) : (
              <Formik
                initialValues={{
                  username: "",
                  email: "",
                  password: "",
                  passwordConfirm: "",
                  firstname: "",
                  lastname: "",
                  agreement: false,
                  foreign: true,
                }}
                validationSchema={Yup.object({
                  username: Yup.string().required(
                    ` ${f({
                      id: "form-username-error",
                    })}`
                  ),
                  firstname: Yup.string().required(
                    `${f({ id: "form-firstname-error" })}`
                  ),
                  lastname: Yup.string().required(
                    `${f({ id: "form-lastname-error" })}`
                  ),
                  email: Yup.string()
                    .email(f({ id: "form-email-error" }))
                    .required(
                      `${f({ id: "form-email" })} ${f({ id: "form-required" })}`
                    ),
                  password: Yup.string().required(
                    `${f({ id: "form-password" })} ${f({
                      id: "form-required",
                    })}`
                  ),
                  passwordConfirm: Yup.string().required(
                    `${f({ id: "form-password" })} ${f({
                      id: "form-required",
                    })}`
                  ),
                  agreement: Yup.boolean().oneOf(
                    [true],
                    f({ id: "form-agreement-error" })
                  ),
                })}
                onSubmit={(values, { setSubmitting }) => {
                  setLoading(true);
                  setTimeout(async () => {
                    const res = await signup(
                      values.email,
                      values.password,
                      values.firstname,
                      values.lastname,
                    );

                    setData(values);
                    if (res.success === true) {
                      setDone(true);
                    } else {
                      toast.error(res.e);
                    }
                    setSubmitting(false);
                    setLoading(false);
                  }, 1000);
                }}
              >
                {({ isSubmitting }) => (
                  <Form autoComplete="off">
                    <div className="relative z-0">
                      <Field
                        type="username"
                        name="username"
                        id="username"
                        className="block py-2.5 px-0 w-full text-sm text-gray-900 bg-transparent border-0 border-b-2 border-gray-300 appearance-none dark:text-white dark:border-gray-600 dark:focus:border-c5 focus:outline-none focus:ring-0 focus:border-c5 peer"
                        placeholder=" "
                      />
                      <label
                        htmlFor="username"
                        className="absolute text-sm text-gray-500 dark:text-gray-400 duration-300 transform -translate-y-6 scale-75 top-3 -z-10 origin-[0] peer-focus:left-0 peer-focus:text-c5 peer-focus:dark:text-c5 peer-placeholder-shown:scale-100 peer-placeholder-shown:translate-y-0 peer-focus:scale-75 peer-focus:-translate-y-6"
                      >
                        {f({ id: "form-username" })}
                      </label>
                    </div>
                    <div className="h-[20px] mb-5">
                      <ErrorMessage
                        name="username"
                        component="div"
                        className="text-red-500 text-xs font-medium"
                      />
                    </div>

                    <div className="relative z-0">
                      <Field
                        type={show ? "text" : "password"}
                        name="password"
                        id="password"
                        className="block py-2.5 px-0 w-full text-sm text-gray-900 bg-transparent border-0 border-b-2 border-gray-300 appearance-none dark:text-white dark:border-gray-600 dark:focus:border-c5 focus:outline-none focus:ring-0 focus:border-c5 peer"
                        placeholder=" "
                      />
                      <label
                        htmlFor="password"
                        className="absolute text-sm text-gray-500 dark:text-gray-400 duration-300 transform -translate-y-6 scale-75 top-3 -z-10 origin-[0] peer-focus:left-0 peer-focus:text-c5 peer-focus:dark:text-c5 peer-placeholder-shown:scale-100 peer-placeholder-shown:translate-y-0 peer-focus:scale-75 peer-focus:-translate-y-6"
                      >
                        {f({ id: "form-password" })}
                      </label>
                      <button
                        className="absolute inset-y-0 right-0 flex items-center px-4 text-gray-600"
                        onClick={() => {
                          show ? setShow(false) : setShow(true);
                        }}
                        type="button"
                      >
                        {show ? (
                          <svg
                            xmlns="http://www.w3.org/2000/svg"
                            fill="none"
                            viewBox="0 0 24 24"
                            strokeWidth={1.5}
                            stroke="currentColor"
                            className="w-5 h-5"
                          >
                            <path
                              strokeLinecap="round"
                              strokeLinejoin="round"
                              d="M2.036 12.322a1.012 1.012 0 010-.639C3.423 7.51 7.36 4.5 12 4.5c4.638 0 8.573 3.007 9.963 7.178.07.207.07.431 0 .639C20.577 16.49 16.64 19.5 12 19.5c-4.638 0-8.573-3.007-9.963-7.178z"
                            />
                            <path
                              strokeLinecap="round"
                              strokeLinejoin="round"
                              d="M15 12a3 3 0 11-6 0 3 3 0 016 0z"
                            />
                          </svg>
                        ) : (
                          <svg
                            xmlns="http://www.w3.org/2000/svg"
                            fill="none"
                            viewBox="0 0 24 24"
                            strokeWidth={1.5}
                            stroke="currentColor"
                            className="w-5 h-5"
                          >
                            <path
                              strokeLinecap="round"
                              strokeLinejoin="round"
                              d="M3.98 8.223A10.477 10.477 0 001.934 12C3.226 16.338 7.244 19.5 12 19.5c.993 0 1.953-.138 2.863-.395M6.228 6.228A10.45 10.45 0 0112 4.5c4.756 0 8.773 3.162 10.065 7.498a10.523 10.523 0 01-4.293 5.774M6.228 6.228L3 3m3.228 3.228l3.65 3.65m7.894 7.894L21 21m-3.228-3.228l-3.65-3.65m0 0a3 3 0 10-4.243-4.243m4.242 4.242L9.88 9.88"
                            />
                          </svg>
                        )}
                      </button>
                    </div>

                    <div className="h-[20px] mb-5">
                      <ErrorMessage
                        name="password"
                        component="div"
                        className="text-red-500 text-xs font-medium"
                      />
                    </div>

                    <div className="relative z-0">
                      <Field
                        type={show ? "text" : "password"}
                        name="passwordConfirm"
                        id="passwordConfirm"
                        className="block py-2.5 px-0 w-full text-sm text-gray-900 bg-transparent border-0 border-b-2 border-gray-300 appearance-none dark:text-white dark:border-gray-600 dark:focus:border-c5 focus:outline-none focus:ring-0 focus:border-c5 peer"
                        placeholder=" "
                      />
                      <label
                        htmlFor="password"
                        className="absolute text-sm text-gray-500 dark:text-gray-400 duration-300 transform -translate-y-6 scale-75 top-3 -z-10 origin-[0] peer-focus:left-0 peer-focus:text-c5 peer-focus:dark:text-c5 peer-placeholder-shown:scale-100 peer-placeholder-shown:translate-y-0 peer-focus:scale-75 peer-focus:-translate-y-6"
                      >
                        {f({ id: "form-password-confirm" })}
                      </label>
                      <button
                        className="absolute inset-y-0 right-0 flex items-center px-4 text-gray-600"
                        onClick={() => {
                          show ? setShow(false) : setShow(true);
                        }}
                        type="button"
                      >
                        {show ? (
                          <svg
                            xmlns="http://www.w3.org/2000/svg"
                            fill="none"
                            viewBox="0 0 24 24"
                            strokeWidth={1.5}
                            stroke="currentColor"
                            className="w-5 h-5"
                          >
                            <path
                              strokeLinecap="round"
                              strokeLinejoin="round"
                              d="M2.036 12.322a1.012 1.012 0 010-.639C3.423 7.51 7.36 4.5 12 4.5c4.638 0 8.573 3.007 9.963 7.178.07.207.07.431 0 .639C20.577 16.49 16.64 19.5 12 19.5c-4.638 0-8.573-3.007-9.963-7.178z"
                            />
                            <path
                              strokeLinecap="round"
                              strokeLinejoin="round"
                              d="M15 12a3 3 0 11-6 0 3 3 0 016 0z"
                            />
                          </svg>
                        ) : (
                          <svg
                            xmlns="http://www.w3.org/2000/svg"
                            fill="none"
                            viewBox="0 0 24 24"
                            strokeWidth={1.5}
                            stroke="currentColor"
                            className="w-5 h-5"
                          >
                            <path
                              strokeLinecap="round"
                              strokeLinejoin="round"
                              d="M3.98 8.223A10.477 10.477 0 001.934 12C3.226 16.338 7.244 19.5 12 19.5c.993 0 1.953-.138 2.863-.395M6.228 6.228A10.45 10.45 0 0112 4.5c4.756 0 8.773 3.162 10.065 7.498a10.523 10.523 0 01-4.293 5.774M6.228 6.228L3 3m3.228 3.228l3.65 3.65m7.894 7.894L21 21m-3.228-3.228l-3.65-3.65m0 0a3 3 0 10-4.243-4.243m4.242 4.242L9.88 9.88"
                            />
                          </svg>
                        )}
                      </button>
                    </div>

                    <div className="h-[20px] mb-2">
                      <ErrorMessage
                        name="passwordConfirm"
                        component="div"
                        className="text-red-500 text-xs font-medium"
                      />
                    </div>

                    <div className="flex space-x-2">
                      <div className="w-1/2">
                        <div className="relative z-0">
                          <Field
                            type="lastname"
                            name="lastname"
                            id="lastname"
                            className="block py-2.5 px-0 w-full text-sm text-gray-900 bg-transparent border-0 border-b-2 border-gray-300 appearance-none dark:text-white dark:border-gray-600 dark:focus:border-c5 focus:outline-none focus:ring-0 focus:border-c5 peer"
                            placeholder=" "
                          />
                          <label
                            htmlFor="lastname"
                            className="absolute text-sm text-gray-500 dark:text-gray-400 duration-300 transform -translate-y-6 scale-75 top-3 -z-10 origin-[0] peer-focus:left-0 peer-focus:text-c5 peer-focus:dark:text-c5 peer-placeholder-shown:scale-100 peer-placeholder-shown:translate-y-0 peer-focus:scale-75 peer-focus:-translate-y-6"
                          >
                            {f({ id: "form-lastname" })}
                          </label>
                        </div>
                        <div className="h-[20px] mb-5">
                          <ErrorMessage
                            name="lastname"
                            component="div"
                            className="text-red-500 text-xs font-medium"
                          />
                        </div>
                      </div>
                      <div className="w-1/2">
                        <div className="relative z-0">
                          <Field
                            type="firstname"
                            name="firstname"
                            id="firstname"
                            className="block py-2.5 px-0 w-full text-sm text-gray-900 bg-transparent border-0 border-b-2 border-gray-300 appearance-none dark:text-white dark:border-gray-600 dark:focus:border-c5 focus:outline-none focus:ring-0 focus:border-c5 peer"
                            placeholder=" "
                          />
                          <label
                            htmlFor="firstname"
                            className="absolute text-sm text-gray-500 dark:text-gray-400 duration-300 transform -translate-y-6 scale-75 top-3 -z-10 origin-[0] peer-focus:left-0 peer-focus:text-c5 peer-focus:dark:text-c5 peer-placeholder-shown:scale-100 peer-placeholder-shown:translate-y-0 peer-focus:scale-75 peer-focus:-translate-y-6"
                          >
                            {f({ id: "form-firstname" })}
                          </label>
                        </div>
                        <div className="h-[20px] mb-5">
                          <ErrorMessage
                            name="firstname"
                            component="div"
                            className="text-red-500 text-xs font-medium"
                          />
                        </div>
                      </div>
                    </div>

                    <div className="relative z-0">
                      <Field
                        type="email"
                        name="email"
                        id="email"
                        className="block py-2.5 px-0 w-full text-sm text-gray-900 bg-transparent border-0 border-b-2 border-gray-300 appearance-none dark:text-white dark:border-gray-600 dark:focus:border-c5 focus:outline-none focus:ring-0 focus:border-c5 peer"
                        placeholder=" "
                      />
                      <label
                        htmlFor="email"
                        className="absolute text-sm text-gray-500 dark:text-gray-400 duration-300 transform -translate-y-6 scale-75 top-3 -z-10 origin-[0] peer-focus:left-0 peer-focus:text-c5 peer-focus:dark:text-c5 peer-placeholder-shown:scale-100 peer-placeholder-shown:translate-y-0 peer-focus:scale-75 peer-focus:-translate-y-6"
                      >
                        {f({ id: "form-email" })}
                      </label>
                    </div>

                    <div className="h-[20px] mb-5">
                      <ErrorMessage
                        name="email"
                        component="div"
                        className="text-red-500 text-xs font-medium"
                      />
                    </div>

                    {/* <div className="relative z-0">
                      <Field
                        type="country"
                        as="select"
                        name="country"
                        id="country"
                        className="block py-2.5 px-0 w-full text-sm text-gray-900 bg-transparent border-0 border-b-2 border-gray-300 appearance-none dark:text-white dark:border-gray-600 dark:focus:border-c5 focus:outline-none focus:ring-0 focus:border-c5 peer"
                        placeholder=" "
                      />
                      <label
                        htmlFor="country"
                        className="absolute text-sm text-gray-500 dark:text-gray-400 duration-300 transform -translate-y-6 scale-75 top-3 -z-10 origin-[0] peer-focus:left-0 peer-focus:text-c5 peer-focus:dark:text-c5 peer-placeholder-shown:scale-100 peer-placeholder-shown:translate-y-0 peer-focus:scale-75 peer-focus:-translate-y-6"
                      >
                        Country
                      </label>
                    </div>

                    <div className="h-[20px] mb-5">
                      <ErrorMessage
                        name="country"
                        component="div"
                        className="text-red-500 text-xs font-medium"
                      />
                    </div> */}

                    <label className="flex items-center text-[12px]">
                      <Field
                        type="checkbox"
                        name="agreement"
                        className="mr-2"
                      />
                      <Link
                        href="/terms-and-agreements"
                        className="font-semibold text-c6"
                        target="_blank"
                      >
                        {f({ id: "signup-agreement" })}
                      </Link>
                      <span className="inline-block ml-[3px]">
                        {f({ id: "signup-accept" })}
                      </span>
                    </label>

                    <div className="h-[20px] mb-2">
                      <ErrorMessage
                        name="agreement"
                        component="div"
                        className="text-red-500 text-xs font-medium"
                      />
                    </div>

                    <button
                      type="submit"
                      disabled={isSubmitting}
                      className="flex items-center justify-center w-full bg-[#011C5A] hover:bg-c5/80 active:bg-c5 text-white p-3 rounded-full font-semibold"
                    >
                      <img src="/images/logo-sign.svg" className="mr-2.5" />
                      {f({ id: "signup-btn" })}
                    </button>
                    <p className="text-center font-medium text-[12px] my-6">
                      {f({ id: "form-or" })}
                    </p>

                    <p className="text-center">
                      {f({ id: "signup-signin" })}
                      <button
                        className="text-c3 font-medium text-center ml-2"
                      >
                        {f({ id: "signin-title" })}
                      </button>
                    </p>
                  </Form>
                )}
              </Formik>
            )}
          </div>
        </div>
      </div>
    </div>
  );
};

export default SignUpContainer;
