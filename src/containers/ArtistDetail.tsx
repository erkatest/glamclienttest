import React from "react";
import dayjs from "dayjs";
import { useRouter } from "next/router";
import { useQuery } from "react-query";
import apiItem from "~/api/item";
import DetailSidebar from "~/components/Detail/DetailSidebar";
import CustomCategory from "~/components/libs/CustomCategory";
import LoadingOverly from "~/components/libs/LoadingOverly";

const ArtistDetailContainers = () => {
  const router = useRouter();
  const [show, setShow] = React.useState(false);
  const [showField, setShowField] = React.useState(false);
  const { data, isLoading } = useQuery(
    ["artist-by-id", router.query.id],
    () => apiItem.findById(router.query.id, "artwork"),
    {
      refetchOnWindowFocus: false,
      refetchInterval: false,
    }
  );

  return (
    <div className="space-y-4 mb-12">
      <LoadingOverly visible={isLoading} />
      <div className="flex items-center justify-center py-8 w-full bg-[#F9FBFC]">
        <img src={data?.data.mainImg || "/images/empty.svg"} className="h-96" />
      </div>
      <div className="flex items-center justify-center my-4">
        {data?.data?.images?.map((item: any, i: number) => {
          return (
            <div key={`image-${i}`} className="mr-4">
              <img src={item} height={40} width={40} />
            </div>
          );
        })}
      </div>
      <div className="flex-col flex items-center justify-center">
        <div className="flex justify-center">
          <CustomCategory type={"artist"} bold={true} />
        </div>

        <p className="font-bold w-1/2 text-center my-4  text-black text-2xl">
          {data?.data?.name}
        </p>
        <p className="text-center">{data?.data.itemInf?.artist?.name}</p>
      </div>
      <div className="flex justify-center items-center space-x-4">
        {data?.data?.tags?.map((item: any, i: number) => {
          return (
            <div
              key={`tags-${i}`}
              onClick={() => {
                router.push({
                  pathname: "/search",
                  search: `?q=${item?.name}`,
                });
              }}
              className="flex items-center rounded-full py-1 px-2 bg-[#F2F2F7] mb-2 space-x-2 cursor-pointer"
            >
              <img src="/images/tag.svg" />
              <p>{item?.name}</p>
            </div>
          );
        })}
      </div>
      <div className="container mx-auto grid grid-cols-1 xl:grid-cols-4 gap-[97px]  ">
        <div className="w-full">
          <DetailSidebar data={data} />
          <div className="border-b pb-3 mt-10">
            <div className="flex items-center 2xl:space-x-6 xl:space-x-3 md:space-x-20 justify-center bg-[#F4F7FA] p-3 rounded-xl">
              <div className="text-center ">
                {data?.data?.viewCount || 0}
                <div className="text-[#767676] text-sm">Үзсэн</div>
              </div>
              <div className="text-center ">
                {dayjs(data?.data?.updatedAt).format("DD-MM-YYYY") || ""}
                <div className="text-[#767676] text-sm">Шинэчилсэн</div>
              </div>
              <div className="text-center ">
                {0}
                <div className="text-[#767676] text-sm">Хадгалсан</div>
              </div>
            </div>
          </div>
        </div>
        <div className="space-y-8 w-full col-span-2">
          {data?.data.description && (
            <div>
              <p className="text-[#012E93] font-bold text-lg mb-4">Тайлбар</p>
              <p
                className={` text-justify ${
                  show ? "" : "h-24 overflow-hidden"
                }`}
              >
                {data?.data.description}
              </p>
              <p
                className="text-center text-[#6C86C0] underline cursor-pointer"
                onClick={() => setShow((o) => !o)}
              >
                {show ? "Хураангуйлах" : "Цааш үзэх"}
              </p>
            </div>
          )}
          {data?.data.fields && (
            <div className="space-y-4">
              <p className={`text-[#012E93] font-bold text-lg`}>
                Нэмэлт мэдээлэл
              </p>
              <div className={`${showField ? "" : "h-24 overflow-hidden"}`}>
                {data?.data?.fields?.map((item: any, i: number) => {
                  return (
                    <div
                      key={`fields-${i}`}
                      className={`w-full h-8 grid grid-cols-2 ${
                        i % 2 === 1 ? "bg-white" : "bg-[#F4F7FA]"
                      }`}
                    >
                      <p className="flex pl-2 items-center text-sm text-[#767676]">
                        {item?.name}:
                      </p>
                      <p className="flex items-center font-medium text-sm">
                        {item?.type === "text"
                          ? item?.t_value
                          : item?.type === "date"
                          ? dayjs(item?.d_value).format("YYYY/MM/DD")
                          : item?.type === "number"
                          ? item?.n_value
                          : ""}
                      </p>
                    </div>
                  );
                })}
              </div>
              <p
                className="text-center text-[#6C86C0] underline cursor-pointer"
                onClick={() => setShowField((o) => !o)}
              >
                {showField ? "Хураангуйлах" : "Цааш үзэх"}
              </p>
            </div>
          )}
          {data?.data?.links?.length > 0 && (
            <div className="space-y-4">
              <p className="text-[#012E93] font-bold text-lg">
                Бичлэг, холбоос
              </p>
              {data?.data?.links.map((item: any, i: number) => {
                return (
                  <div key={`link${i}`}>
                    <iframe
                      className="rounded-2xl"
                      width={268}
                      height={180}
                      src={`https://www.youtube.com/embed/${
                        item?.value.split("=")[1]
                      }`}
                    ></iframe>
                  </div>
                );
              })}
            </div>
          )}
        </div>
        <div className="w-full font-bold mb-4 text-lg space-y-6">
          <p className="text-black">Бусад Зохиолч</p>
          {/* {(item?.data || []).map((item: any, index: number) => (
            <div
              key={`result-${index}`}
              onClick={() => {}}
              className="cursor-pointer"
            >
              <ResultGrid data={item} key={`item-${index}`} />
            </div>
          ))} */}
        </div>
      </div>
    </div>
  );
};

export default ArtistDetailContainers;
