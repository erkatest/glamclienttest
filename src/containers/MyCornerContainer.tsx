import React from "react";
import { FaEllipsisV } from "react-icons/fa";
import { useMutation, useQuery } from "react-query";
import apiFolder from "~/api/folder";
import Slider from "react-slick";
import useMessage from "~/hooks/useMessage";
import ResultGrid from "~/components/ResultGrid";
import { NotFoundData } from "~/components/libs";
import CustomSelect from "~/components/libs/CustomSelect";
import CreateFolderModal from "~/components/Modals/CreateFolderModal";
import { toast } from "react-toastify";
import Pagination from "~/components/pagination";
import { useUser } from "~/context/auth";
import CustomModal from "~/components/Modals/CustomModal";
import apiItem from "~/api/item";
import { useRouter } from "next/router";
import LoadingOverly from "~/components/libs/LoadingOverly";

const MyCornerContainer = () => {
  const sliderRef = React.useRef<any>();
  const f = useMessage();
  const { token } = useUser();
  const router = useRouter();
  const [q, setQ] = React.useState("");
  const [folder, setFolder] = React.useState<any>();
  const [create, setCreate] = React.useState(false);
  const [remove, setRemove] = React.useState(false);
  const [currentData, setCurrentData] = React.useState<any>();
  const [page, setPage] = React.useState(1);

  const settings = {
    className: "center",
    centerMode: false,
    infinite: true,
    centerPadding: "200px",
    slidesToShow: 3,
    dots: false,
    swipeToSlide: false,
    autoplay: false,
    responsive: [
      {
        breakpoint: 1920,
        settings: {
          slidesToShow: 3,
          centerPadding: "200px",
        },
      },
      {
        breakpoint: 1080,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
          initialSlide: 1,
          centerPadding: "100px",
        },
      },
      {
        breakpoint: 900,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
          centerPadding: "50px",
        },
      },
      {
        breakpoint: 600,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
          centerPadding: "50px",
        },
      },
    ],
  };

  return (
    <div className="contain mx-auto px-8 max-w-7xl">
      <div className="flex justify-between items-center my-10 flex-col lg:flex-row">
        <div className="relative w-1/2">
          <div
            onClick={() => {
              if (sliderRef.current) {
                sliderRef.current?.slickNext();
              }
            }}
            className="absolute right-[-50px] top-0 cursor-pointer w-[30px] h-[30px] ml-12 rounded-full border-2 border-c1 hover:opacity-60 flex items-center justify-center"
          >
            <img src="/images/arrow-dark.svg" className="w-1/2" />
          </div>
        </div>
        <button
          type="button"
          className="flex cursor-pointer items-center border-2 border-[#6C86C0] border-dashed  ml-2 py-2 px-6 w-60 rounded-full space-x-2"
          onClick={() => setCreate(true)}
        >
          <img src="/images/add-folder.svg" className="w-6" />
          <p className="text-[#6C86C0] text-sm">Шинэ хавтас</p>
        </button>
      </div>

      <div className="relative ">
        <div className="w-full border-b border-c1 "></div>
        <div className="absolute -top-[17px] left-1/2 transform translate-x-[17px] lg:translate-x-0 lg:-right-[17px]">
          <img src={"/images/logo-small.svg"} className="w-[34px]" />
        </div>
      </div>

      <div className="flex w-full items-center justify-between mt-4 flex-col lg:flex-row">
        <div className={`relative rounded-lg my-4 lg:w-[550px] w-[400px]`}>
          <input
            placeholder="Миний булангаас хайна уу"
            className="py-3 px-[8px] rounded-lg pl-10 focus:outline-none bg-[#F4F7FA] w-full"
            value={q}
            onChange={(e) => {
              setQ(e.target.value);
            }}
          />
          <div className="absolute top-1/2 left-2 transform -translate-y-1/2">
            <img src="/images/search-light.svg" />
          </div>
          {/* <LoadingOverly visible={loadingItem} /> */}
        </div>
        <div className="w-72">
          <CustomSelect
            defaultValue=""
            items={[
              {
                label: "Эрэмбэ сонгоно уу",
                value: "",
              },
              {
                label: "Шинэ бүтээл эхэндээ",
                value: "1",
              },
              {
                label: "Хуучин бүтээл эхэндээ",
                value: "2",
              },
              {
                label: "Бүтээлийн нэрээр (A-Я, A-Z)",
                value: "3",
              },
              {
                label: "Бүтээлийн нэрээр (Я-А, Z-A)",
                value: "4",
              },
            ]}
          />
        </div>
      </div>
     
      {/* {create && (
        <CreateFolderModal
          isOpen={create}
          onClose={() => setCreate(false)}
          refetch={refetch}
        />
      )} */}
    </div>
  );
};

export default MyCornerContainer;
