import * as React from "react";
import { Formik, Form, Field, ErrorMessage } from "formik";
import * as Yup from "yup";
import Link from "next/link";
import useMessage from "~/hooks/useMessage";
import LoadingOverly from "~/components/libs/LoadingOverly";
import { useUser } from "~/context/auth";
import { IoChevronBackOutline } from "react-icons/io5";
import { ToastContainer, toast } from "react-toastify";

const ForgetContainer = (props: { setOpen: any }) => {
  const f = useMessage();
  const { forgotPassword } = useUser();
  const [loading, setLoading] = React.useState(false);
  const [data, setData] = React.useState<any>();
  const [done, setDone] = React.useState(false);

  const { setOpen } = props;

  return (
    <div>
      <div
        className="flex items-center gap-1 text-2xl mb-6 cursor-pointer w-fit"
        onClick={() => setOpen("signin")}
      >
        <IoChevronBackOutline className="text-c7" />

        <p className="text-base text-c7 font-normal text-[16px] 2xl:text-[18px]">
          {" "}
          {f({ id: "form-back" })}
        </p>
      </div>
      <LoadingOverly visible={loading} />
      <h3 className="text-[32px] 2xl:text-[38px] mb-[20px] lg:mb-[24px] font-bold text-c4">
        {f({ id: "forget-title" })}
      </h3>
      <p className="text-c7 text-left mb-5 2xl:mb-7 text-[16px] 2xl:text-[18px] dark:text-white">
        {f({ id: "forgot-desc" })}
      </p>

      <Formik
        initialValues={{
          email: "",
        }}
        validationSchema={Yup.object({
          email: Yup.string()
            .email(f({ id: "form-email-error" }))
            .required(
              `${f({ id: "form-email" })} ${f({ id: "form-required" })}`
            ),
        })}
        onSubmit={(values, { setSubmitting }) => {
          setLoading(true);
          setTimeout(async () => {
            await forgotPassword(values?.email);
            setData(values);
            setSubmitting(false);
            setDone(true);
            setLoading(false);
            setOpen("signin");
          }, 1000);
        }}
      >
        {({ isSubmitting }) => (
          <Form autoComplete="off">
            <div className="relative z-0">
              <Field
                type="email"
                name="email"
                id="email"
                className="block py-[10px] px-[8px] 2xl:py-[14px] 2xl:px-[10px] w-full text-[16px] 2xl:text-[18px] text-gray-900 bg-transparent border-0 border-b-[0.5px] border-c12 appearance-none dark:text-white dark:border-gray-600 dark:focus:border-c5 focus:outline-none focus:ring-0 focus:border-c5 peer"
                placeholder=" "
              />
              <label
                htmlFor="email"
                className="absolute text:[16px] 2xl:text-[18px] text-c7 dark:text-gray-400 duration-300 transform -translate-y-6 scale-75 top-3 -z-10 origin-[0] peer-focus:left-0 peer-focus:text-c5 peer-focus:dark:text-c5 peer-placeholder-shown:scale-100 peer-placeholder-shown:translate-y-0 peer-focus:scale-75 peer-focus:px-[12px] peer-focus:-translate-y-6 px-[12px]"
              >
                {f({ id: "form-email" })}
              </label>
            </div>

            <div className="h-[20px] mb-5">
              <ErrorMessage
                name="email"
                component="div"
                className="text-red-500 text-xs font-medium"
              />
            </div>

            <button
              type="submit"
              disabled={isSubmitting}
              className="flex items-center justify-center w-full bg-c13 active:bg-c5 text-[16px] 2xl:text-[18px] text-white p-2.5 rounded-full hover:bg-c15"
            >
              {f({ id: "forget-btn" })}
            </button>
          </Form>
        )}
      </Formik>

      <p className="text-c4 mt-5 text-justify mb-5 text-[14px]">
        {done &&
          f(
            { id: "forget-success" },
            {
              email: data?.email || "-----",
              strong: (value: any) => {
                return <span className="font-bold text-c6">{value[0]}</span>;
              },
            }
          )}
      </p>
    </div>
  );
};

export default ForgetContainer;
