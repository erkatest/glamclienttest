import React, { useState } from "react";
import useMessage from "~/hooks/useMessage";
import apiItem from "~/api/item";
import { useMutation, useQuery } from "react-query";
import LoadingOverly from "~/components/libs/LoadingOverly";
import Pagination from "~/components/pagination";
import { toast } from "react-toastify";
import SkeletonText from "~/components/Skeletons/SkeletonText";
import { useUser } from "~/context/auth";
import ConfirmModal from "~/components/Modals/ConfirmModal";

const SearchHistoryContainer = () => {
  const f = useMessage();
  const [page, setPage] = useState(1);
  const [modal, setModal] = useState(false);

  const { token } = useUser();
  const { data, refetch, isLoading } = useQuery(
    ["history-search-page", page, token],
    () => token && apiItem.searchHistory(page, 14),
    {
      refetchOnWindowFocus: false,
      refetchInterval: false,
    }
  );

  const deleteMutation = useMutation(
    (id: string) => apiItem.deleteHistory(id),
    {
      onSuccess() {
        refetch();
      },
      onError(error: any) {
        toast.error(error);
      },
    }
  );

  const deleteAllMutation = useMutation(() => apiItem.deleteAllHistory(), {
    onSuccess() {
      refetch();
      setModal(false);
      toast.success(f({ id: "success" }));
    },
    onError(error: any) {
      toast.error(error);
    },
  });

  return (
    <section className="container mx-auto px-8 max-w-7xl">
      <h1 className="font-bold text-c1 text-2xl my-10 dark:text-[#BE8100]">
        {f({ id: "search-history-title" })}
      </h1>
      <div className="border-b border-c1"></div>
      <div className="flex flex-col lg:flex-row relative">
        <section className="relative p-10">
          <div>
            <div className="absolute -top-[17px] right-1/2 transform translate-x-[17px] lg:translate-x-0 lg:-right-[17px]">
              <img src={"/images/logo-small.svg"} className="w-[34px]" />
            </div>
            <button className="flex items-center rounded-full bg-[#E6EAF4] py-2 w-full pl-4">
              <img src="/images/search-light.svg" className="text-2xl mr-2" />
              Хайлтын түүх
            </button>
          </div>
          <div className="mt-4">
            <button className="flex items-center rounded-full bg-white py-2 pl-4 pr-12">
              <img src="/images/eye.svg" className="text-2xl mr-2" />
              Үзэлтийн түүх
            </button>
          </div>
        </section>

        <div className="relative flex-1 lg:border-l p-10 border-c1">
          <LoadingOverly visible={isLoading || deleteMutation?.isLoading} />
          {data?.data.map((item: any, i: number) => {
            return (
              <div
                key={`text-${i}-page`}
                className="flex cursor-pointer mr-4 mb-4 lg:m-2 whitespace-nowrap items-center space-x-3 py-3  text-[#55606B] rounded-[5px] w-fit"
              >
                <img src="/images/clock.svg" className="w-5" />
                <p className="mx-4 font-medium">{item?.value}</p>
                <img
                  src="/images/x-icon-circle.svg"
                  className="w-5"
                  onClick={async (e) => {
                    e.preventDefault();
                    e.stopPropagation();
                    await deleteMutation.mutateAsync(item?._id);
                  }}
                />
              </div>
            );
          })}
          {isLoading && <SkeletonText />}
        </div>
        <button
          className="flex items-center rounded-full px-4 h-9 bg-[#F4F7FA] m-6 space-x-2"
          onClick={() => setModal(true)}
        >
          <img src="/images/trash.svg" />
          <p className="text-black text-sm">Бүх түүх устгах</p>
        </button>
      </div>
      <div className="flex mb-8">
        <div className="w-1/3"></div>
        <div className="w-2/3">
          {data?.data.length > 0 && (
            <div className="flex justify-center">
              <Pagination
                setPage={(page) => setPage(page)}
                page={page}
                total={data?.pagination?.total}
                limit={data?.pagination?.limit}
              />
            </div>
          )}
        </div>
      </div>
      {modal && (
        <ConfirmModal
          isOpen={modal}
          onClose={() => setModal(false)}
          onChange={deleteAllMutation}
        />
      )}
    </section>
  );
};

export default SearchHistoryContainer;
