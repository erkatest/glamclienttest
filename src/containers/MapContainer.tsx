import React, { useEffect, useRef, useState } from "react";
import { FaArrowCircleDown, FaArrowCircleUp } from "react-icons/fa";
import {
  MapContainer,
  TileLayer,
  Marker,
  Popup,
  useMap,
  ZoomControl,
} from "react-leaflet";
import L from "leaflet";
import "leaflet/dist/leaflet.css";
import LocationModal from "~/components/Modals/AskLocationModal";
import { useQuery } from "react-query";
import apiMap from "~/api/map";
import { useRouter } from "next/router";
import { useLocationPrompt } from "~/context/location";

import { TabContext } from "~/context/tab";
import { IconCheck, IconCopy, IconX } from "@tabler/icons-react";
import { useClipboard, useDebouncedValue } from "@mantine/hooks";
import { Button, Skeleton } from "@mantine/core";

const MapContainers = () => {
  const router = useRouter();
  const clipboard = useClipboard({ timeout: 1000 });
  const [selectedPlace, setSelectedPlace] = useState<any>();
  const [myLocation, setMyLocation] = useState<any>();
  const [tab, setTab] = useState<any>("");

  const [tempSearchValue, setTempSearchValue] = useState("");
  const [debounced] = useDebouncedValue(tempSearchValue, 500);
  const { data, isLoading, refetch } = useQuery(
    ["find_map", debounced, tab],
    () => apiMap.find(tab, debounced)
  );

  useEffect(() => {
    if (router?.query?.id) {
      setSelectedPlace(
        data?.data?.find(
          (el: any) =>
            el?._id === router?.query?.id ||
            el?.heritage_id === router?.query?.id
        )
      );
      setLocation({
        lat: data?.data?.find(
          (el: any) =>
            el?._id === router?.query?.id ||
            el?.heritage_id === router?.query?.id
        )?.latitude,
        lng: data?.data?.find(
          (el: any) =>
            el?._id === router?.query?.id ||
            el?.heritage_id === router?.query?.id
        )?.longitude,
        zoom: data?.data?.find(
          (el: any) =>
            el?._id === router?.query?.id ||
            el?.heritage_id === router?.query?.id
        )?.zoom,
      });
    }
  }, [data]);

  const getMarkerIcon = (item: any) => {
    const iconUrl =
      selectedPlace &&
      item.latitude === selectedPlace.latitude &&
      item.longitude === selectedPlace.longitude
        ? "/images/selectedPlace.svg"
        : item?.type === "museum"
        ? "/images/museum.svg"
        : item?.type === "library"
        ? "/images/library.svg"
        : item?.type === "theatre"
        ? "/images/theatre.svg"
        : item?.type === "artwork"
        ? "/images/atwork.svg"
        : item?.type === "immovable heritage"
        ? "/images/ulHodlog.svg"
        : "/images/place.svg";

    return L.icon({
      iconUrl: iconUrl,
      iconSize: [
        selectedPlace &&
        item.latitude === selectedPlace.latitude &&
        item.longitude === selectedPlace.longitude
          ? 35
          : 25,
        41,
      ], // Size of the icon
      iconAnchor: [12, 41], // Point of the icon which will correspond to marker's location
      popupAnchor: [0, -41], // Point from which the popup should open relative to the iconAnchor
    });
  };
  const customIcon = new L.Icon({
    iconUrl: "/images/point.svg",
    iconSize: [30, 41], // Size of the icon
    iconAnchor: [12, 41], // Point of the icon which will correspond to marker's location
    popupAnchor: [1, -34], // Point from which the popup should open relative to the iconAnchor
  });

  const [currentZoom, setCurrentZoom] = useState<any>();

  const [location, setLocation] = useState<any>({
    lat: 47.9189,
    lng: 106.9176,
    zoom: 14,
  });

  const [hide, setHide] = useState<any>(false);
  const [isDragging, setIsDragging] = useState(false);
  const [startX, setStartX] = useState(0);
  const [scrollLeft, setScrollLeft] = useState(0);
  const scrollContainerRef = useRef<any>(null);
  const onMouseDown = (e: any) => {
    setIsDragging(true);
    setStartX(e.pageX - scrollContainerRef.current.offsetLeft);
    setScrollLeft(scrollContainerRef.current.scrollLeft);
  };

  const onMouseUp = () => {
    setIsDragging(false);
  };

  const onMouseMove = (e: any) => {
    if (!isDragging) return;
    e.preventDefault();

    const x = e.pageX - scrollContainerRef.current.offsetLeft;
    const walk = (x - startX) * 2; // Scroll-fastness
    scrollContainerRef.current.scrollLeft = scrollLeft - walk;
  };

  useEffect(() => {
    const storedData = localStorage.getItem("locationData");
    if (storedData) {
      const locationData = JSON.parse(storedData);
      setMyLocation({
        lat: locationData.lat,
        lng: locationData.lng,
      });
      setLocation({
        lat: locationData.lat,
        lng: locationData.lng,
        zoom: locationData.zoom || 14, // Use a default zoom if not provided
      });
    } else {
      // Fallback location if not stored in localStorage
      // setMyLocation({ lat: 47.9189, lng: 106.9176 });
      setLocation({ lat: 47.9189, lng: 106.9176, zoom: 14 });
    }
  }, []);

  const ChangeMapView = ({ center, zoom }: any) => {
    const map = useMap();

    useEffect(() => {
      if (center[0] && center[1]) {
        map?.setView(center, zoom);
      }
    }, [center, map, zoom]);

    return null;
  };

  // useEffect(() => {
  //   if (selectedPlace) {
  //     setTab(selectedPlace?.type);
  //   }
  // }, [selectedPlace]);
  const ShowZoomLevel = () => {
    const map = useMap();

    useEffect(() => {
      const currentZoomLevel = map.getZoom();
      setCurrentZoom(currentZoomLevel);
    }, [map]);

    return null;
  };
  // useEffect(() => {
  //   router?.query?.type ? setTab(router?.query?.type) : setTab("");
  // }, [tempSearchValue]);

  const typeList = [
    {
      value: "museum",
      label: "Музей",
      hex: "#13452F",
    },
    {
      value: "library",
      label: "Номын сан",
      hex: "#1A3D7A",
    },
    {
      value: "theatre",
      label: "Театр",
      hex: "#1A3D7A",
    },
    {
      value: "artwork",
      label: "Уран зураг",
      hex: "#BE3455",
    },
    {
      value: "immovable heritage",
      label: "Үл хөдлөх өв",
      hex: "#BE3455",
    },
  ];

  return (
    <div className={`relative`}>
      <div className="absolute top-[86px] z-10 right-2.5 ">
        {myLocation && (
          <div
            className=" bg-white p-[6px] rounded-[2.5px] cursor-pointer"
            onClick={() => {
              setLocation({ ...myLocation, zoom: 14 });
              setSelectedPlace(null);
            }}
          >
            <img src="/images/findMe.svg" className="w-full h-full" />
          </div>
        )}
      </div>

      <div
        className=" w-full"
        onClick={() => {
          // setTempSearchValue("");
          // setSelectedPlace(null);
          // selectedPlace &&
          //   setLocation({
          //     lat: location.lat,
          //     lng: location.lng,
          //     zoom: location?.zoom,
          //   });
        }}
      >
        <MapContainer
          attributionControl={false}
          center={[location?.lat, location?.lng]}
          zoom={location?.zoom || 14}
          style={{ height: "90vh", width: "100%", zIndex: 0 }}
          zoomControl={false} // Disable default zoom control
        >
          <ChangeMapView
            center={[location.lat, location.lng]}
            zoom={location.zoom || 14}
          />

          <ShowZoomLevel />
          <TileLayer url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png" />
          {myLocation?.lat && (
            <Marker
              position={[myLocation.lat, myLocation.lng]}
              icon={customIcon}
            >
              <Popup>Таны байршил</Popup>
            </Marker>
          )}
          {data?.data?.map((item: any, index: any) => (
            <Marker
              key={index}
              position={[item.latitude, item.longitude]}
              icon={getMarkerIcon(item)}
              eventHandlers={{
                click: (e: any) => {
                  e.originalEvent.stopPropagation();
                  setSelectedPlace(item);
                  setTab(item?.type);

                  setLocation({
                    lat: item.latitude,
                    lng: item.longitude,
                    zoom: item?.zoom,
                  });
                },
                popupclose: () => {
                  setSelectedPlace(null);
                  selectedPlace &&
                    setLocation({
                      lat: location.lat,
                      lng: location.lng,
                      zoom: 14,
                    });
                },
              }}
            >
              <Popup>{item?.name}</Popup>
            </Marker>
          ))}

          <ZoomControl position="topright" />
        </MapContainer>
      </div>

      <div
        className={`hidden md:block absolute z-10 top-5 left-5 p-[20px] bg-white w-[350px] xl:w-[450px] rounded-[5px] space-y-[10px] transition-opacity duration-500 ease-in-out ${
          selectedPlace ? "opacity-100 visible" : "opacity-0 invisible"
        }`}
      >
        <div className="flex items-center justiy-between space-x-5 w-full">
          <div className="text-[20px] font-[600] w-full">
            {" "}
            {selectedPlace?.name}
          </div>
          <div
            className="cursor-pointer"
            onClick={() => {
              setSelectedPlace(null);
              selectedPlace &&
                setLocation({
                  lat: location.lat,
                  lng: location.lng,
                  zoom: 14,
                });
            }}
          >
            <IconX />
          </div>
        </div>
        <div className="text-[#969696] text-[14px] tracking-[0.035px] line-clamp-[10]">
          {selectedPlace?.description}
        </div>
        <div className="flex items-center space-x-[10px]">
          <img src="/images/placeAddressIcon.svg" />
          <div
            className="text-[12px] tracking-[0.03px] cursor-pointer flex gap-2 items-center"
            onClick={() => clipboard.copy(selectedPlace?.address)}
          >
            {selectedPlace?.address}
            {clipboard.copied ? (
              <p className="ml-2 text-[14px] font-[600] flex gap-1 items-center">
                <IconCheck size={16} color="green" /> Хуулагдлаа.
              </p>
            ) : (
              <IconCopy size={16} strokeWidth={1.75} />
            )}
          </div>
        </div>
        {selectedPlace?.type === "immovable heritage" && (
          <div className="flex justify-end">
            <button
              className="text-[16px] text-white bg-[#13452F] py-1 px-3 rounded-lg"
              onClick={() =>
                router.push({
                  pathname: `/detail/${selectedPlace?.heritage_id}/heritage?heritage_type=immovable`,
                })
              }
            >
              Дэлгэрэнгүй
            </button>
          </div>
        )}
      </div>

      <form className="lg:hidden absolute top-2.5  md:right-14 right-16 ">
        <div className="relative">
          <div className="absolute inset-y-0 start-0 flex items-center ps-3 pointer-events-none">
            <svg
              className="w-4 h-4 text-gray-500 dark:text-gray-400 "
              aria-hidden="true"
              xmlns="http://www.w3.org/2000/svg"
              fill="none"
              viewBox="0 0 20 20"
            >
              <path
                stroke="currentColor"
                strokeLinecap="round"
                strokeLinejoin="round"
                strokeWidth="2"
                d="m19 19-4-4m0-7A7 7 0 1 1 1 8a7 7 0 0 1 14 0Z"
              />
            </svg>
          </div>
          <input
            type="text"
            id="default-search"
            className={`block w-[300px] lg:w-[400px] py-[12px] px-[10px] ps-10 text-sm text-gray-900 border border-gray-300 rounded-full
                   bg-gray-50  `}
            placeholder="Хайна уу"
            required
            value={tempSearchValue}
            onChange={(e) => setTempSearchValue(e.target.value)}
          />
          {tempSearchValue && (
            <button
              onClick={(e) => {
                e.preventDefault();
                setTempSearchValue("");
                setTab(tab);
              }}
              className="text-white absolute end-2.5 bottom-2.5"
            >
              <img src="/images/x.svg" />
            </button>
          )}
        </div>
      </form>
      <div
        className={`flex justify-between items-center space-y-[10px] lg:space-y-0 absolute ${
          hide
            ? "bottom-[20px] lg:bottom-[30px]"
            : "bottom-[220px] lg:bottom-[230px] z-50"
        }  left-2 right-2 lg:left-5 lg:right-5 `}
      >
        <div className="flex flex-wrap gap-y-3 gap-x-2">
          <div
            className={` ${
              tab === ""
                ? "bg-[#011C5A] text-white"
                : "bg-white  text-[##011C5A]"
            } p-[10px] rounded-[0.5rem] cursor-pointer text-[16px]`}
            onClick={() => setTab("")}
          >
            Бүгд
          </div>
          {typeList?.map((item: any, index: number) => (
            <div
              key={`map-places-tab-${index}`}
              className={` ${
                tab === item?.value
                  ? `bg-[${item?.hex}] text-white`
                  : "bg-white  text-[#112437]"
              } px-6 py-2 rounded-[0.5rem] cursor-pointer text-[16px]`}
              onClick={() => setTab(item?.value)}
            >
              {item?.label}
            </div>
          ))}
        </div>
        <div className="cursor-pointer lg:hidden">
          {hide ? (
            <FaArrowCircleUp
              size={28}
              color="#14452F"
              onClick={() => setHide(false)}
            />
          ) : (
            <FaArrowCircleDown
              size={28}
              color="#14452F"
              onClick={() => setHide(true)}
            />
          )}
        </div>
        <div className=" items-center space-x-2.5 hidden lg:flex">
          <div>
            <div className="relative">
              <div className="absolute inset-y-0 start-0 flex items-center ps-3 pointer-events-none">
                <svg
                  className="w-4 h-4 text-gray-500 dark:text-gray-400 "
                  aria-hidden="true"
                  xmlns="http://www.w3.org/2000/svg"
                  fill="none"
                  viewBox="0 0 20 20"
                >
                  <path
                    stroke="currentColor"
                    strokeLinecap="round"
                    strokeLinejoin="round"
                    strokeWidth="2"
                    d="m19 19-4-4m0-7A7 7 0 1 1 1 8a7 7 0 0 1 14 0Z"
                  />
                </svg>
              </div>
              <input
                type="text"
                id="default-search"
                className={`block w-[300px] lg:w-[400px] py-[12px] px-[10px] ps-10 text-sm text-gray-900 border border-gray-300 rounded-full
                   bg-gray-50  `}
                placeholder="Хайна уу"
                required
                spellCheck={false}
                onFocus={() => setHide(false)}
                value={tempSearchValue}
                onChange={(e) => setTempSearchValue(e.target.value)}
              />
              {tempSearchValue && (
                <button
                  type="submit"
                  onClick={(e) => {
                    e.preventDefault();
                    setTempSearchValue("");
                    setTab(tab);
                  }}
                  className="text-white absolute end-2.5 bottom-2.5"
                >
                  <img src="/images/x.svg" />
                </button>
              )}
            </div>
          </div>
          <div className="cursor-pointer">
            {hide ? (
              <FaArrowCircleUp
                size={28}
                color="#14452F"
                onClick={() => setHide(false)}
              />
            ) : (
              <FaArrowCircleDown
                size={28}
                color="#14452F"
                onClick={() => setHide(true)}
              />
            )}
          </div>
        </div>
      </div>

      <div
        ref={scrollContainerRef}
        className={`absolute  bottom-2 lg:bottom-5 left-2  lg:left-5 right-0 overflow-x-auto scroll-container space-y-2 transition duration-300 ease-in-out ${
          hide ? "opacity-0 -z-10" : "opacity-100 z-10"
        }`}
        onMouseDown={onMouseDown}
        onMouseLeave={onMouseUp}
        onMouseUp={onMouseUp}
        onMouseMove={onMouseMove}
      >
        <div className="flex space-x-5 min-w-max">
          {isLoading ? (
            [0, 1, 2, 3, 4, 5]?.map((index) => (
              <div className="w-[350px]" key={`map-loading-${index}`}>
                <Skeleton height={200} />
              </div>
            ))
          ) : data?.data?.length > 0 ? (
            data?.data?.map((item: any, index: number) => (
              <div
                key={`map-place-${index}`}
                className={`relative z-50 rounded-lg h-[200px] w-[350px] shrink-0 cursor-pointer overflow-hidden zoom-hover ${
                  selectedPlace?._id === item?._id &&
                  "border-2  border-[#14452F] zoom "
                }`}
                onClick={() => {
                  setSelectedPlace(item);
                  setLocation({
                    lat: item.latitude,
                    lng: item.longitude,
                    zoom: item?.zoom,
                  });
                }}
              >
                {selectedPlace?._id === item?._id && (
                  <div className="bg-black absolute opacity-50 inset-0 z-10" />
                )}
                <div className="absolute inset-0 ">
                  <img
                    src={item?.image ? item?.image : "/images/bg.jpg"}
                    className="w-full h-full object-cover rounded-lg brightness-[75%]"
                  />
                </div>
                <div className="absolute inset-0 z-10 flex items-center justify-center px-1">
                  <div className="text-white text-[24px] font-semibold text-center ">
                    {item?.name}
                  </div>
                </div>
              </div>
            ))
          ) : (
            <div className="w-[350px]  bg-gray-200 rounded-lg mr-5 text-center text-[14px] py-2">
              <img src="/empty.png" className="w-40 mx-auto" />
              Илэрц байхгүй байна.
            </div>
          )}
        </div>
      </div>
    </div>
  );
};

export default MapContainers;
