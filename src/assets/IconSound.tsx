import React from "react";

const IconSound = () => {
  return (
    <svg
      width="18"
      height="18"
      viewBox="0 0 18 18"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
    >
      <path
        d="M0.75 6.875H3.75L7.5 3.875V14.375L3.75 11.375H0.75V6.875Z"
        stroke="white"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
      <path
        d="M14.777 3C15.5431 3.76608 16.1508 4.67556 16.5654 5.67649C16.98 6.67743 17.1934 7.75023 17.1934 8.83363C17.1934 9.91704 16.98 10.9898 16.5654 11.9908C16.1508 12.9917 15.5431 13.9012 14.777 14.6673"
        stroke="white"
        strokeLinecap="round"
      />
      <path
        d="M11.5765 5.85156C11.9944 6.26943 12.3258 6.7655 12.552 7.31147C12.7781 7.85743 12.8945 8.44259 12.8945 9.03354C12.8945 9.62449 12.7781 10.2097 12.552 10.7556C12.3258 11.3016 11.9944 11.7977 11.5765 12.2155"
        stroke="white"
        strokeLinecap="round"
      />
    </svg>
  );
};

export default IconSound;
