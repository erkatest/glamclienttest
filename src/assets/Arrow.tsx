const Arrow = () => {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      width="10"
      height="20"
      viewBox="0 0 10 20"
      fill="none"
    >
      <path
        d="M1 1.00032L8.34907 8.34939C9.21698 9.2173 9.21698 10.6375 8.34907 11.5054L1 18.8545"
        stroke="#112437"
        strokeWidth="1.80345"
        strokeMiterlimit="10"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
    </svg>
  );
};
export default Arrow;
