const RightIcon = ({ open }: any) => {
  return (
    <div>
      {open ? (
        <svg
          xmlns="http://www.w3.org/2000/svg"
          width="14"
          height="8"
          viewBox="0 0 14 8"
          fill="none"
        >
          <path
            d="M12.1589 1.5L7.56569 6.09317C7.02325 6.63561 6.13561 6.63561 5.59317 6.09317L1 1.5"
            stroke="#112437"
            strokeWidth="2"
            strokeMiterlimit="10"
            strokeLinecap="round"
            strokeLinejoin="round"
          />
        </svg>
      ) : (
        <svg
          xmlns="http://www.w3.org/2000/svg"
          width="7"
          height="14"
          viewBox="0 0 7 14"
          fill="none"
        >
          <path
            d="M1 1.42024L5.59317 6.01341C6.13561 6.55586 6.13561 7.44349 5.59317 7.98594L1 12.5791"
            stroke="#A6B2C3"
            strokeWidth="2"
            strokeMiterlimit="10"
            strokeLinecap="round"
            strokeLinejoin="round"
          />
        </svg>
      )}
    </div>
  );
};
export default RightIcon;
