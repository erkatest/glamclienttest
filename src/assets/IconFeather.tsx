const IconFeather = () => {
  return (
    <svg
      width="16"
      height="16"
      viewBox="0 0 16 16"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
    >
      <path
        d="M3.09903 7.56894L8.58922 2.07875C10.1053 0.562672 12.5633 0.562674 14.0794 2.07875V2.07875C15.5955 3.59482 15.5955 6.05286 14.0794 7.56894L8.58922 13.0591L3.49119 12.667L3.09903 7.56894Z"
        stroke="white"
      />
      <path d="M6.62695 9.53125H12.1171" stroke="white" />
      <path d="M0.746094 15.4102L10.1578 5.9984" stroke="white" />
    </svg>
  );
};
export default IconFeather;
