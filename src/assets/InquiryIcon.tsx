const Inquiry = ({ color, size }: any) => {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      width={size ? "24" : "32"}
      height={size ? "24" : "32"}
      viewBox="0 0 22 22"
      stroke-width="1.5"
      stroke="currentColor"
      fill="none"
      stroke-linecap="round"
      stroke-linejoin="round"
    >
      <path stroke="none" d="M0 0h24v24H0z" fill="none" />
      <path d="M15 3v4a1 1 0 0 0 1 1h4" />
      <path
        d="M18 17h-7a2 2 0 0 1 -2 -2v-10a2 2 0 0 1 2 -2h4l5 5v7a2 2 0 0 1 -2 2z"
        fill={color ? "#112437" : "#A6B2C3"}
      />
      <path d="M16 17v2a2 2 0 0 1 -2 2h-7a2 2 0 0 1 -2 -2v-10a2 2 0 0 1 2 -2h2" />
    </svg>
  );
};
export default Inquiry;
