import axios from "axios";
import store from "~/utils/store";

const instance = axios.create({
  baseURL: process.env.NEXT_PUBLIC_API,
  headers: {
    "Cache-Control": "no-cache",
    Pragma: "no-cache",
    Expires: "0",
  },
});

instance.interceptors.request.use(
  async (config: any) => {
    const user = store.get("user");

    if (user) {
      config.headers.Authorization = user && `Bearer ${user?.accessToken}`;
    }
    config.headers["accept-language"] = "mn";
    return config;
  },
  (error) => {
    return Promise.reject(error);
  }
);

export default instance;
