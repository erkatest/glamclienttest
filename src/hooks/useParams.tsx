import { useRouter } from "next/router";
import { useEffect, useState } from "react";

const useParams = () => {
  const [params, setParams] = useState<any>({});
  const { asPath } = useRouter();

  useEffect(() => {
    const params = new URLSearchParams(window.location.search);

    setParams(
      Array.from(params.entries())?.reduce((s: any, n: any) => {
        const [key, value] = n;
        return { ...s, [key]: value };
      }, {})
    );
  }, [asPath]);

  return params;
};

export default useParams;
