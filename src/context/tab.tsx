import { useRouter } from "next/router";
import React, { createContext, useEffect, useState } from "react";

interface IContext {
  active: any;
  setActive: any;
  tabs: any;
  activeTabProps: any;
  activeCheck: any;
  setActiveCheck: any;
  setOpen: any;
  open: any;
  setEng: any;
  eng: any;
  setShowModal: any;
  showModal: any;
  activeArtwork: any;
  setActiveArtwork: any;
  heritageTypeCheck: any;
  setHeritageTypeCheck: any;
  typeEng: any;
  setTypeEng: any;
  heritageImageCheck: any;
  setHeritageImageCheck: any;
}

interface IProps {
  children: React.ReactNode;
}

export const TabContext = createContext<IContext>({} as IContext);

const tabs = [
  {
    type: "default",
    param: "artwork",
    title: "",
    color: "#112437",
    textColor: "text-[#112437]",
    backgroundColor: "bg-[#112437]",
    borderColor: "border-[#112437]",
  },
  {
    type: "gallery",
    param: "artwork",
    title: "Урын сан",
    color: "#BE3455",
    textColor: "text-[#BE3455]",
    backgroundColor: "bg-[#BE3455]",
    borderColor: "border-[#BE3455]",
  },
  {
    type: "library",
    param: "koha",
    title: "Номын сан",
    color: "#01417A",
    textColor: "text-[#01417A]",
    backgroundColor: "bg-[#01417A]",
    borderColor: "border-[#01417A]",
  },
  {
    type: "archive",
    param: "education",
    title: "Сан хөмрөг",
    color: "#2E294E",
    textColor: "text-[#2E294E]",
    backgroundColor: "bg-[#2E294E]",
    borderColor: "border-[#2E294E]",
  },
  {
    type: "museum",
    param: "heritage",
    title: "Соёлын өв",
    color: "#13452F",
    textColor: "text-[#13452F]",
    backgroundColor: "bg-[#13452F]",
    borderColor: "border-[#13452F]",
  },
  {
    type: "myLibrary",
    color: "#13452F",
    textColor: "text-[#112437]",
    backgroundColor: "bg-[#112437]",
    borderColor: "border-[#112437]",
  },
];

const TabProvider = ({ children }: IProps) => {
  const [active, setActive] = useState("gallery");
  const [activeArtwork, setActiveArtwork] = useState("");
  const [heritageTypeCheck, setHeritageTypeCheck] = useState("");
  const [activeCheck, setActiveCheck] = useState([]);
  const [heritageImageCheck, setHeritageImageCheck] = React.useState<string>();
  const [open, setOpen] = useState("signin");
  const [eng, setEng] = useState();
  const [typeEng, setTypeEng] = React.useState<string>();

  const router = useRouter();
  const [showModal, setShowModal] = useState(false);

  // const values: any = {
  //   gallery: ["gallery", "artwork"],
  //   library: ["library", "koha"],
  //   archive: ["archive", "education"],
  //   museum: ["museum", "heritage"],
  // };

  // Set active by page type value
  useEffect(() => {
    const activeKey = router.asPath?.includes("artwork")
      ? "gallery"
      : router.asPath?.includes("gallery")
      ? "gallery"
      : router.asPath?.includes("library")
      ? "library"
      : router.asPath?.includes("koha")
      ? "library"
      : router.asPath?.includes("archive")
      ? "archive"
      : router.asPath?.includes("education")
      ? "archive"
      : router.asPath?.includes("heritage")
      ? "museum"
      : router.asPath?.includes("museum")
      ? "museum"
      : router?.asPath?.split("/")[2]?.split("").length === 4
      ? "library"
      : router.asPath?.includes("myLibrary")
      ? "myLibrary"
      : router.asPath?.includes("inquiry")
      ? "myLibrary"
      : router.asPath?.includes("privacy")
      ? "myLibrary"
      : router.asPath?.includes("profile")
      ? "myLibrary"
      : router.asPath?.includes("search-log")
      ? "myLibrary"
      : router.asPath?.includes("views-log")
      ? "myLibrary"
      : "gallery";
    setActive(activeKey || "gallery");
  }, [router.asPath]);

  const activeTabProps = tabs?.find((tab: any) => {
    return tab.type === active;
  });

  return (
    <div>
      <TabContext.Provider
        value={{
          active,
          setActive,
          tabs,
          activeTabProps,
          activeCheck,
          setActiveCheck,
          setOpen,
          open,
          setEng,
          eng,
          setShowModal,
          showModal,
          activeArtwork,
          setActiveArtwork,
          heritageTypeCheck,
          setHeritageTypeCheck,
          typeEng,
          setTypeEng,
          heritageImageCheck,
          setHeritageImageCheck,
        }}
      >
        {children}
      </TabContext.Provider>
    </div>
  );
};

export default TabProvider;