import React, { createContext, useContext, useEffect, useState } from "react";
import { useRouter } from "next/router";
import apiUsers from "~/api/users";
import store from "~/utils/store";
import { toast } from "react-toastify";
import useMessage from "~/hooks/useMessage";

interface IContext {
  signin: (email: string, password: string) => any;
  signup: (
    email: string,
    password: string,
    firstname: string,
    lastname: string
    // foreign?: boolean
  ) => any;
  verify: (email: string, verification: string) => any;
  resend: (email: string) => any;
  forgotPassword: (email: string) => any;
  signout: () => void;
  checkUserMe: () => void;
  user: any;
  token: any;
}

interface IProps {
  children: React.ReactNode;
}

const AuthContext = createContext<IContext>({} as IContext);

export const AuthProvider = ({ children }: IProps) => {
  const f = useMessage();
  const [user, setUser] = useState(null);
  const [token, setToken] = useState<any>(null);
  const router = useRouter();

  const signin = async (email: any, password: any) => {
    const res = await apiUsers.login(email, password);

    if (res.data && res.status === "success") {
      checkUserMe(), setToken(res.data.accessToken);
      store.set("token", res.data.accessToken);
      store.set("user", res.data);
      setUser(res.data);
      toast.success(f({ id: "success-signin-message" }));

      return { success: true, data: res };
    } else {
      toast.error(res.e);
      return { success: false };
    }
  };

  const signup = async (
    email: string,
    password: string,
    firstname: string,
    lastname: string
    // foreign?: boolean
  ) => {
    try {
      const res = await apiUsers.signup(
        email,
        password,
        firstname,
        lastname
        // foreign
      );
      return { success: true, data: res };
    } catch (e) {
      return { success: false, e };
    }
  };

  const verify = async (email: string, verification: string) => {
    try {
      const res = await apiUsers.verify(email, verification);
      if (res.data && res.status === "success") {
        toast.success(f({ id: "success-signup-message" }));
        // router.replace("/signin");
      }
      return { success: true, data: res };
    } catch (e) {
      return { success: false, e };
    }
  };

  const resend = async (email: string) => {
    try {
      const res = await apiUsers.resend(email);
      if (res.data && res.status === "success") {
        toast.success(f({ id: "success-resend-message" }));
      } else {
        toast.error(res.e);
      }
      return { success: true, data: res };
    } catch (e) {
      return { success: false, e };
    }
  };

  const forgotPassword = async (email: string) => {
    try {
      const res = await apiUsers.forgotPassword(email);

      if (res.data && res.status === "success") {
        toast.success(res.data);
        // toast.success(f({ id: "success-email-message" }));
        // router.replace("/signin");
      } else {
        toast.error(res.e);
      }
      return { success: true, data: res };
    } catch (e) {
      return { success: false, e };
    }
  };

  const signout = async () => {
    store.remove("token");
    store.remove("user");
    await setUser(null);
    await setToken(null);
    router.replace("/");
    toast.success(f({ id: "success-signout-message" }));
  };

  const checkUserMe = async () => {
    const res = await apiUsers.me();
    if (res?.data) {
      setUser(res.data);
    } else {
      store.remove("token");
      store.remove("user");
      // router.replace("/signin");
    }
  };

  useEffect(() => {
    const user = store.get("user");
    if (user?.accessToken) {
      setToken(user?.accessToken);

      checkUserMe();
    } else {
      // router.replace("/signin");
    }
  }, []);

  return (
    <AuthContext.Provider
      value={{
        signout,
        signin,
        verify,
        resend,
        forgotPassword,
        signup,
        checkUserMe,
        token,
        user,
      }}
    >
      {children}
    </AuthContext.Provider>
  );
};

export const useUser = () => useContext(AuthContext);
