import React, {
  createContext,
  useContext,
  useState,
  useEffect,
  ReactNode,
} from "react";

type LocationPromptContextType = {
  locationPrompted: boolean;
  setLocationPrompted: (prompted: boolean) => void;
};

const LocationPromptContext = createContext<
  LocationPromptContextType | undefined
>(undefined);

export const useLocationPrompt = () => {
  const context = useContext(LocationPromptContext);
  if (!context) {
    throw new Error(
      "useLocationPrompt must be used within a LocationPromptProvider"
    );
  }
  return context;
};

type LocationPromptProviderProps = {
  children: ReactNode;
};

export const LocationPromptProvider: React.FC<LocationPromptProviderProps> = ({
  children,
}) => {
  const [locationPrompted, setLocationPrompted] = useState<boolean>(false);

  useEffect(() => {
    // Check local storage only on the client side
    const prompted = localStorage.getItem("locationPrompted") === "true";
    setLocationPrompted(prompted);
  }, []);

  const value = { locationPrompted, setLocationPrompted };

  return (
    <LocationPromptContext.Provider value={value}>
      {children}
    </LocationPromptContext.Provider>
  );
};
