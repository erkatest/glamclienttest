import { useSearchParams } from "next/navigation";
import React, { useEffect, useRef } from "react";
import { ReactNode, useState } from "react";
import { useQuery } from "react-query";
import apiContent from "~/api/content";
import apiResearch from "~/api/research";
import { useDebouncedValue } from "~/hooks/useDebouncedValues";
import { useFullscreen } from "~/hooks/useFullscreen";

interface IContext {
  zoom: string;
  page: number;
  nextPage: () => void;
  prevPage: () => void;
  totalPage: number;
  setTotalPage: (page: number) => void;
  previewPages: number[];
  setPreviewPages: (pages: number[]) => void;
  zoomIn: () => void;
  zoomOut: () => void;
  redirectPage: (page: number) => void;
  full: boolean;
  setFull: (full: boolean) => void;
  setZoom: any;
  showSidebar: boolean;
  setShowSidebar: any;
  queryNotes: any;
  showNotes: any;
  setShowNotes: any;
  handleEpub: (epub: any) => void;
  handleEpubNext: () => void;
  handleEpubPrev: () => void;
  epubFirst: boolean;
  epubLast: boolean;
  setEpubFirst: any;
  setEpubLast: any;
  loading: boolean;
  setLoading: (loading: boolean) => void;
  epub: any;
  setEpub: any;
  handleEpubRedirect: any;
  setShowToc: any;
  showToc: boolean;
  showSmartTool: boolean;
  setShowSmartTool: any;
  toolPos: any;
  setToolPos: any;
  selection: any;
  setSelection: any;
  eventBus: any;
  setEventBus: any;
  domLoaded: any;
  setDomLoaded: any;
  handleChangeSettings: any;
  currentPosition: any;
  setCurrentPosition: any;
}

const ReaderContext = React.createContext({} as IContext);

const ZOOM_VALUES = ["2", "1.5", "1.25", "1.0", "0.9", "0.5", "0.25", "0.1"];

export const ReaderProvider: React.FC<{ children: ReactNode }> = (props) => {
  const { toggle, fullscreen } = useFullscreen();

  const epubRef = useRef<any>();

  const [domLoaded, setDomLoaded] = useState(false);

  const [showToc, setShowToc] = useState(false);
  const [eventBus, setEventBus] = useState<any>();
  const [toolPos, setToolPos] = useState({
    top: 0,
    left: 0,
    bottom: 0,
    right: 0,
  });

  const [showSmartTool, setShowSmartTool] = useState(false);

  const [loading, setLoading] = useState(false);
  const [epub, setEpub] = useState<any>();
  const [epubFirst, setEpubFirst] = useState(false);
  const [epubLast, setEpubLast] = useState(false);
  const [showNotes, setShowNotes] = useState(false);
  const [showSidebar, setShowSidebar] = useState(false);
  const [zoom, setZoom] = useState("0.9");
  const [page, setPage] = useState(1);
  const [selection, setSelection] = useState("");

  const [currentPosition, setCurrentPosition] = useState<any>();

  const [debouncePage] = useDebouncedValue(page, 200);

  const [totalPage, setTotalPage] = useState<number>(0);

  const [previewPages, setPreviewPages] = useState<number[]>([]);

  const params = useSearchParams();
  const contentId = params.get("id");
  const preview = params.get("preview");
  const queryNotes = useQuery(
    ["contents", contentId, "notes"],
    () => contentId && apiResearch.find(contentId)
  );
  const nextPage = () => {
    setPage((page) => (page + 1 <= totalPage ? page + 1 : totalPage));
  };

  const prevPage = () => {
    setPage((page) => (page - 1 >= 0 ? page - 1 : 0));
  };

  const redirectPage = (page: number) => {
    setPage(totalPage >= page ? page : totalPage);
  };

  const zoomIn = () => {
    const foundIndex = ZOOM_VALUES?.indexOf(zoom);

    setZoom(
      foundIndex - 1 >= 0
        ? ZOOM_VALUES[foundIndex - 1]
        : ZOOM_VALUES[foundIndex]
    );
  };
  const zoomOut = () => {
    const foundIndex = ZOOM_VALUES?.indexOf(zoom);
    setZoom(
      foundIndex + 1 < ZOOM_VALUES.length
        ? ZOOM_VALUES[foundIndex + 1]
        : ZOOM_VALUES[foundIndex]
    );
  };

  useEffect(() => {
    const page = params.get("page");
    const search = params.get("search");
    if (page) {
      try {
        setPage(Number.parseInt(page, 10) || 1);
      } catch (e) {
        setPage(1);
      }
    }
    if (search) {
      setShowSidebar(true);
    }
  }, []);
  //   useEffect(() => {
  //     const request = async () => {
  //       await apiContent.save_reading({
  //         content_item_id: contentId,
  //         reading_state: {
  //           type: "pdf",
  //           page_number: page,
  //           total_pages: totalPage,
  //         },
  //       });
  //     };
  //     if (!preview) {
  //       request();
  //     }
  //   }, [page]);

  const handleEpub = (epub: any) => {
    epubRef.current = epub;
  };

  const handleEpubNext = () => {
    setCurrentPosition(null);
    epubRef.current?.rendition?.next();
  };

  const handleEpubPrev = () => {
    setCurrentPosition(null);
    epubRef.current?.rendition?.prev();
  };

  const handleEpubRedirect = (page: number) => {
    if (epubRef.current && epubRef.current.book) {
      const cfi = epubRef.current.book.locations.cfiFromPercentage(
        page / epubRef.current.book.locations.total
      );
      epubRef.current.rendition.display(cfi);
    } else {
      console.error("EPUB instance is not loaded or epubRef is not set.");
    }
  };

  const handleChangeSettings = (settings: any) => {
    if (epubRef.current) {
      const colorIndex = ["#fff", "#f7f3e7", "#48484a", "#2c2c2f"].indexOf(
        settings?.currentColor
      );
      if (
        settings?.currentFont &&
        settings?.currentSize &&
        settings?.currentColor
      ) {
        document.body.style.backgroundColor = settings?.currentColor;
        epubRef.current?.rendition.themes.default({
          p: {
            "font-family": `${settings?.currentFont} !important; font-size: ${settings?.currentSize}px !important`,
            color: colorIndex > 1 ? "#fff" : "#000",
          },
          ul: {
            "font-family": `${settings?.currentFont} !important; font-size: ${settings?.currentSize}px !important`,
            color: colorIndex > 1 ? "#fff" : "#000",
          },
          li: {
            "font-family": `${settings?.currentFont} !important; font-size: ${settings?.currentSize}px !important`,
            color: colorIndex > 1 ? "#fff" : "#000",
          },
          span: {
            "font-family": `${settings?.currentFont} !important; font-size: ${settings?.currentSize}px !important`,
            color: colorIndex > 1 ? "#fff" : "#000",
          },
          body: {
            background: `${settings?.currentColor}`,
          },
        });
      }
    }
  };

  return (
    <ReaderContext.Provider
      value={{
        page: debouncePage,
        nextPage,
        prevPage,
        totalPage,
        zoom,
        zoomIn,
        zoomOut,
        setTotalPage,
        previewPages,
        setPreviewPages,
        redirectPage,
        full: fullscreen,
        setFull: (value) => {
          toggle();
          setShowSidebar(false);
        },
        setZoom,
        showSidebar,
        setShowSidebar,
        queryNotes,
        showNotes,
        setShowNotes,
        handleEpub,
        handleEpubNext,
        handleEpubPrev,
        epubFirst,
        setEpubFirst,
        epubLast,
        setEpubLast,
        loading,
        setLoading,
        setEpub,
        epub,
        handleEpubRedirect,
        showToc,
        setShowToc,
        showSmartTool,
        setShowSmartTool,
        toolPos,
        setToolPos,
        selection,
        setSelection,
        eventBus,
        setEventBus,
        domLoaded,
        setDomLoaded,
        handleChangeSettings,
        currentPosition,
        setCurrentPosition,
      }}
    >
      {props.children}
    </ReaderContext.Provider>
  );
};

export default ReaderContext;
