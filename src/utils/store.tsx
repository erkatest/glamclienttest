/** store helper */
const store = {
  set: (name: string, value: any) => {
    if (value) {
      localStorage.setItem(name, JSON.stringify(value));
    }
  },
  remove: (name: string) => {
    localStorage.removeItem(name);
  },
  get: (name: string) => {
    try {
      if (typeof window !== "undefined") {
        const tmp = localStorage.getItem(name);
        const parsed = tmp ? JSON.parse(tmp) : undefined;
        return parsed;
      }
      return undefined;
    } catch (error) {
      console.error(`Error parsing JSON from ${name}:`, error);
      return undefined;
    }
  },
};

export default store;