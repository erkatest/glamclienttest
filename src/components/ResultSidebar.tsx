import * as React from "react";
import CustomSelect from "./libs/CustomSelect";
import CustomFilter from "./libs/CutsomFilter";
import apiItem from "~/api/item";
import { useQuery } from "react-query";
import LoadingOverly from "./libs/LoadingOverly";
import useParams from "~/hooks/useParams";
import { useRouter } from "next/router";
interface IProps {
  type: string;
}

const ResultSidebar = ({ type }: IProps) => {
  const { data, isLoading } = useQuery(
    ["filter-category"],
    () => apiItem.category(),
    {
      refetchInterval: false,
      refetchOnWindowFocus: false,
    }
  );
  const router = useRouter();
  const params = useParams();

  return (
    <section className="relative py-4 px-[26px]">
      <div className="absolute -top-[17px] right-1/2 transform translate-x-[17px] lg:translate-x-0 lg:-right-[17px]">
        <img src={"/images/logo-small.svg"} className="w-[34px]" />
      </div>
      <h3 className="text-base font-bold mb-2">Төрөл</h3>
      <div className="space-y-4">
        <CustomSelect
          onChange={(value) => {
            if (params?.q !== undefined) {
              router.push({
                pathname: "/search/item",
                search: `?q=${params?.q}&type=${value}`,
              });
            }
          }}
          defaultValue={params?.type || type}
          items={[
            {
              label: "Соёлын өв",
              value: "culture",
            },
            {
              label: "Уран бүтээл",
              value: "artwork",
            },
            {
              label: "Ном товхимол",
              value: "koha",
            },
            {
              label: "Боловсрол",
              value: "education",
            },
          ]}
        />
        {type && (
          <div className="">
            <LoadingOverly visible={isLoading} />
            <CustomFilter
              defaultValue={type}
              items={(data?.data || [])?.map((item: any) => ({
                label: `${item?.name}`,
                value: item?._id,
              }))}
            />
          </div>
        )}
      </div>
    </section>
  );
};

export default ResultSidebar;
