"use client";

import React, { ReactNode, useEffect, useState } from "react";

import { usePathname } from "next/navigation";
import Link from "next/link";
import { TabContext } from "~/context/tab";
import { MdArrowForwardIos } from "react-icons/md";
import { useRouter } from "next/router";

type TBreadCrumbProps = {
  containerClasses?: string;
  type?: any;
  data?: any;
};

const BreadCrumb = ({ data, type }: TBreadCrumbProps) => {
  const router = useRouter();
  const { activeTabProps, activeCheck, setActiveCheck } =
    React.useContext(TabContext);
  const [typeValue, setTypeValue] = useState([]);

  const activeListClassname = `${activeTabProps?.textColor} mr-2 md:text-[14px] font-[600] text-black`;
  const separator = (
    <MdArrowForwardIos
      className={`${activeTabProps?.textColor} w-[10px] mr-2`}
    />
  );

  const normalType = type || data?.data?.type;

  const correctType =
    normalType === "artwork"
      ? "Урын сан"
      : normalType === "koha"
      ? "Номын сан"
      : normalType === "heritage"
      ? "Соёлын өв"
      : normalType === "education"
      ? "Сан хөмрөг"
      : normalType === "library"
      ? "Номын сан"
      : "";

  const correctLink =
    normalType === "artwork"
      ? "/gallery"
      : normalType === "koha"
      ? "/library"
      : normalType === "education"
      ? "archive"
      : normalType === "heritage"
      ? "/museum"
      : normalType === "library"
      ? "/library"
      : "";

  useEffect(() => {
    const heritageType = data?.data?.fields?.filter((item: any) => {
      if (item?.name == "Өвийн төрөл") {
        const getItemValue = item.value;
        return getItemValue;
      }
    });
    setTypeValue(heritageType);
  }, [data]);

  const listClassname = `${
    normalType === "library" ? "text-[#01417A]" : activeTabProps?.textColor
  } mr-2 md:text-[14px]`;

  return (
    <div>
      <ul className={"flex items-center"}>
        {correctType && (
          <>
            <li className={`${listClassname} hover:cursor-pointer`}>
              <Link
                href={correctLink}
                onClick={() => {
                  setActiveCheck();
                }}
              >
                {correctType}
              </Link>
            </li>
            {separator}
          </>
        )}
        {data?.data?.heritage_type && (
          <>
            <li className={`${listClassname} hover:cursor-pointer`}>
              <Link
                href={`/search/?q=&type=museum&heritage_century=&heritage_type=${data?.data?.heritage_type}&province_type=&page=1&sort=heritage_name&sort_order=asc`}
                onClick={() => {
                  setActiveCheck();
                }}
              >
                {data?.data?.heritage_type}
              </Link>
            </li>
            {separator}
          </>
        )}
        {data?.data?.fields &&
          data?.data?.fields?.map((item: any) => (
            <>
              {item?.name == "Төрөл" ? (
                <>
                  <li className={listClassname}>{item?.t_value}</li> {separator}
                </>
              ) : (
                ""
              )}
            </>
          ))}
        {data?.data?.parentCategory?.name && (
          <>
            <li
              onClick={() => {
                router.push({
                  pathname: "/search",
                  query: {
                    parentCategory: data?.data?.parentCategory?._id,
                  },
                });
              }}
              className={listClassname}
            >
              <>
                {data?.data?.parentCategory?.name ||
                  data?.data?.heritage_attachments[0].attachment_type.name}
              </>
            </li>
            {separator}
          </>
        )}
        {/* {data?.data?.heritage_attachments && (
          <>
            <li className={listClassname}>
              <>{data?.data?.heritage_attachments[0]?.attachment_type.name}</>
            </li>
            {separator}
          </>
        )} */}
        {typeValue?.length > 0 && (
          <li className="flex gap-1 items-center">
            {typeValue?.map((item: any, index: number) => (
              <Link
                href={correctLink || "/archive"}
                className={listClassname}
                key={index}
              >
                {item?.value}
              </Link>
            ))}
            {separator}
          </li>
        )}
        {data?.data?.category?.name && (
          <>
            <li
              onClick={() => {
                router.push({
                  pathname: "/search",
                  query: {
                    category: data?.data?.category?._id,
                  },
                });
              }}
              className={listClassname}
            >
              <>{data?.data?.category?.name}</>
            </li>
            {separator}
          </>
        )}
        {/* {data?.species_name && (
          <>
            <li
              onClick={() => {
                router.push({
                  pathname: "/search",
                  query: {
                    // category: data?.data?.category?._id,
                  },
                });
              }}
              className={listClassname}
            >
              <>{data?.species_name}</>
            </li>
            {separator}
          </>
        )} */}
        <li className={`${activeListClassname}`}>
          <>{data?.data?.name}</>
        </li>
        <li className={`${activeListClassname}`}>
          <>{data?.data?.heritage_name}</>
        </li>
      </ul>
    </div>
  );
};

export default BreadCrumb;
