import { Box, Table, Tabs } from "@mantine/core";
import { IconMap, IconTable } from "@tabler/icons-react";
import {
  Circle,
  MapContainer,
  Marker,
  TileLayer,
  ZoomControl,
  useMap,
} from "react-leaflet";
import "leaflet/dist/leaflet.css";

import { useEffect, useState } from "react";

const IntangibleTab = ({ data }: any) => {
  const [currentZoom, setCurrentZoom] = useState<any>();
  const ShowZoomLevel = () => {
    const map = useMap();

    useEffect(() => {
      const currentZoomLevel = map.getZoom();
      setCurrentZoom(currentZoomLevel);
    }, [map]);

    return null;
  };
  const calculateOccurrences = (data: any) => {
    const occurrences: any = {};
    data?.data?.area_coordinate?.forEach((item: any) => {
      const lat =
        item?.latitude_d + item?.latitude_m / 60 + item?.latitude_s / 3600;
      const lng =
        item?.longitude_d + item?.longitude_m / 60 + item?.longitude_s / 3600;
      const key = `${lat}-${lng}`;
      if (!occurrences[key]) {
        occurrences[key] = { count: 1, lat, lng };
      } else {
        occurrences[key].count++;
      }
    });
    return occurrences;
  };

  const occurrences = calculateOccurrences(data);
  return (
    <Box className="space-y-2">
      <Box className="text-[24px] font-[500]">Тархац</Box>
      <Tabs defaultValue="map" color="teal">
        <Tabs.List>
          <Tabs.Tab value="map" icon={<IconMap size="0.8rem" />}>
            Газрын зургаар
          </Tabs.Tab>
          <Tabs.Tab value="table" icon={<IconTable size="0.8rem" />}>
            Хүснэгтээр
          </Tabs.Tab>
        </Tabs.List>

        <Tabs.Panel value="map" pt="xs">
          <div className="h-[50vh] w-full">
            <MapContainer
              attributionControl={false}
              center={[46.8625, 103.8467]}
              zoom={5.5}
              style={{ height: "100%", width: "100%", zIndex: 0 }}
              zoomControl={false}
            >
              <ShowZoomLevel />
              <TileLayer url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png" />
              {Object.values(occurrences).map((item: any, index: number) => (
                <Circle
                  key={index}
                  center={[item.lat, item.lng]}
                  radius={item.count > 1 ? 5000 * item.count : 5000}
                  fillColor="#13452F"
                  color="#13452F"
                />
              ))}

              <ZoomControl position="topright" />
            </MapContainer>
          </div>
        </Tabs.Panel>

        <Tabs.Panel value="table" pt="xs">
          <Table striped highlightOnHover withBorder withColumnBorders>
            <thead>
              <tr>
                <th>№</th>
                <th>Аймаг/Хот</th>
                <th>Сум/Дүүрэг</th>
                <th>Баг/Хороо</th>
              </tr>
            </thead>
            <tbody>
              {data?.data?.location?.map((item: any, index: number) => (
                <tr key={index}>
                  <td>{index + 1}</td>
                  <td>{item?.province}</td>
                  <td>{item?.region}</td>
                  <td>{item?.committee ? item?.committee : "-"}</td>
                </tr>
              ))}
            </tbody>
          </Table>
        </Tabs.Panel>
      </Tabs>
    </Box>
  );
};

export default IntangibleTab;
