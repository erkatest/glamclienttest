import React from "react";

import { useRouter } from "next/router";
import { useMutation, useQuery } from "react-query";

import useMessage from "~/hooks/useMessage";
import LoadingOverly from "../libs/LoadingOverly";

import { FiSearch } from "react-icons/fi";
import { TabContext } from "~/context/tab";

const CultureHeritageSearch = () => {
  const f = useMessage();
  const router = useRouter();
  const [q, setQ] = React.useState("");
  const [focus, setFocus] = React.useState(false);
  const { tabs, setActive, active } = React.useContext(TabContext);
  const [loading, setLoading] = React.useState(false);

  return (
    <div
      className="relative bg-[#13452F] dark:text-[#BE8100] z-[999] h-[300px] bg-gradient-to-b from-red"
      style={{
        backgroundImage:
          "linear-gradient(to bottom, rgba(0, 0, 0, 0.3), rgba(0, 0, 0, 0.1))",
      }}
    >
      <div className="container flex items-center justify-center flex-col h-full">
        <div className="uppercase text-[40px] font-semibold text-white mb-5">
          {f({ id: "about-4-title" })}
        </div>
        <div
          className={"bg-white w-full rounded-full relative overflow-hidden"}
        >
          <input
            placeholder={f({ id: "about-4-desc" })}
            className="h-[50px] lg:h-[60px] w-full p-5 pl-16 focus:outline-none"
            onFocus={() => setFocus(true)}
            onBlur={() => setFocus(false)}
            value={q}
            onChange={(e) => {
              setQ(e.target.value);
            }}
            onKeyDown={(e) => {
              if (e.key === "Enter") {
                setLoading(true);
                router.push({
                  pathname: "/search",
                  search: `?q=${q || ""}`,
                });
                setActive("museum");
              }
            }}
          />
          <div className="absolute top-1/2 left-5 transform -translate-y-1/2">
            <FiSearch size={28} />
          </div>
        </div>
      </div>
    </div>
  );
};

export default CultureHeritageSearch;
