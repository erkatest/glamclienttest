import React, { useEffect, useState } from "react";
import Link from "next/link";

import { useRouter } from "next/router";
import { useQuery } from "react-query";

import useMessage from "~/hooks/useMessage";
import LoadingOverly from "../libs/LoadingOverly";

import CultureContainer from "./HeritageContainer";
import { MdArrowForwardIos } from "react-icons/md";
import { FiSearch } from "react-icons/fi";
import useParams from "~/hooks/useParams";
import Spinner from "../libs/Spinner";

import apiItem from "~/api/item";
import { TabContext } from "~/context/tab";
import ContentList from "../ContentLIst";

const CultureIntangible = () => {
  const f = useMessage();
  const [page, setPage] = React.useState(1);
  const params = useParams();

  const {
    tabs,
    setActive,
    active,
    activeTabProps,
    activeCheck,
    setActiveCheck,
  } = React.useContext(TabContext);

  const { data, isLoading } = useQuery(
    ["intangible", active, page, params.q || ""],
    () =>
      apiItem.find(
        activeTabProps.param,
        page,
        8,
        params.q,
        "heritage_name",
        "asc",
        {
          heritage: {
            heritage_type: ["Биет бус өв"],
          },
        }
      )
  );

  return (
    <CultureContainer>
      <>
        <div className="flex justify-between items-center mb-6">
          <h1 className="uppercase font-[600] text-2xl">
            {f({ id: "culture-i" })}
          </h1>
          <Link
            href={"/search/?q=&heritage_type=Биет%20бус%20өв&province_type="}
            onClick={() => setActiveCheck(["Биет бус өв"])}
          >
            <div className="flex items-center gap-[1rem] hover:opacity-80">
              <p className={`font-[700] text-[16px]`}>{f({ id: "see-all" })}</p>
              <MdArrowForwardIos className="text-default" />
            </div>
          </Link>
        </div>
        <ContentList isLoading={isLoading} contents={data?.data} />
      </>
    </CultureContainer>
  );
};

export default CultureIntangible;
