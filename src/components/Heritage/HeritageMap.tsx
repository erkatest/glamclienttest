import { Box, Button, Table, Tabs } from "@mantine/core";
import { IconMap, IconTable } from "@tabler/icons-react";
import {
  Circle,
  MapContainer,
  Marker,
  Popup,
  TileLayer,
  ZoomControl,
  useMap,
} from "react-leaflet";
import "leaflet/dist/leaflet.css";

import { useEffect, useState } from "react";
import L from "leaflet";
import Link from "next/link";

const HeritageMap = ({ data }: any) => {
  const [currentZoom, setCurrentZoom] = useState<any>();
  const ShowZoomLevel = () => {
    const map = useMap();

    useEffect(() => {
      const currentZoomLevel = map.getZoom();
      setCurrentZoom(currentZoomLevel);
    }, [map]);

    return null;
  };
  const customIcon = new L.Icon({
    iconUrl: "/images/place.svg",
    iconSize: [30, 41], // Size of the icon
    iconAnchor: [12, 41], // Point of the icon which will correspond to marker's location
    popupAnchor: [1, -34], // Point from which the popup should open relative to the iconAnchor
  });
  return (
    <div className="h-[50vh] w-full relative">
      <Link href={`/map/?id=${data?.data?.id}`}>
        <Button bg={"#13452F"}>Map дээр харах</Button>
      </Link>
      <MapContainer
        attributionControl={false}
        center={[data?.data?.location_latitude, data?.data?.location_longitude]}
        zoom={13}
        style={{ height: "100%", width: "100%", zIndex: 0 }}
        zoomControl={false}
      >
        <ShowZoomLevel />
        <TileLayer url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png" />
        <Marker
          position={[
            data?.data?.location_latitude,
            data?.data?.location_longitude,
          ]}
          icon={customIcon}
        >
          <Popup>{data?.data?.heritage_name}</Popup>
        </Marker>
        <ZoomControl position="topright" />
      </MapContainer>
    </div>
  );
};

export default HeritageMap;
