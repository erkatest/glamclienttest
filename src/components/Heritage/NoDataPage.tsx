import * as React from "react";
import DefaultLayout from "../Layout";
import useMessage from "../../hooks/useMessage";
import { BsArrowLeft } from "react-icons/bs";
import { TabContext } from "~/context/tab";
import Link from "next/link";

const NoDataPage: React.FC = () => {
  const f = useMessage();
  const { tabs, active } = React.useContext(TabContext);

  return (
    <div className="container relative mx-auto max-w-7xl px-8 h-[80vh] flex items-center justify-center">
      {/* <img src={"/images/bg-404.png"} /> */}
      <div className="absolute top-0 left-0 right-0 bottom-0 flex items-center justify-center space-y-8 flex-col">
        <img src={"/empty.png"} className="max-w-[35%]" />
        {/* <h3 className=" font-bold text-4xl lg:text-7xl text-c1">404</h3> */}
        <p className="text-xl lg:text-3xl font-bold text-[#4d4d4d]">
          {f({ id: "no-data-found" })}
        </p>
        <Link
          href="/"
          className={`w-[400px] flex items-center justify-center space-x-2 height-[60px] text-sm p-4 rounded-[5px] text-white ${tabs.find((tab: any) => tab.type === active)?.backgroundColor
            || "bg-c1"}`}
        >
          <BsArrowLeft /> <span>{f({ id: "not-found-btn" })}</span>
        </Link>
      </div>
    </div>
  );
};

export default NoDataPage;
