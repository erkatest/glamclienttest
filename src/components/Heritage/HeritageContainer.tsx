import React from "react";
import Link from "next/link";

import { useRouter } from "next/router";
import { useMutation, useQuery } from "react-query";

import useMessage from "~/hooks/useMessage";
import LoadingOverly from "../libs/LoadingOverly";
import { MdArrowForwardIos } from "react-icons/md";

import { FiSearch } from "react-icons/fi";

interface IProps {
  children: React.ReactNode;
}

const CultureContainer = ({ children }: IProps) => {
  const f = useMessage();

  return (
    <section className="relative">
      <div className="container mt-[3rem] mb-[2rem]">{children}</div>
    </section>
  );
};

export default CultureContainer;
