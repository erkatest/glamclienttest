import React from "react";
import Link from "next/link";
import { useQuery } from "react-query";
import { MdArrowForwardIos } from "react-icons/md";

import useParams from "~/hooks/useParams";
import useMessage from "~/hooks/useMessage";
import apiItem from "~/api/item";
import { TabContext } from "~/context/tab";
import CultureContainer from "~/components//Heritage/HeritageContainer";
import ContentList from "~/components/ContentLIst";

const CultureMovable = () => {
  const f = useMessage();
  const [page, setPage] = React.useState(1);
  const params = useParams();
  const { active, activeTabProps, setActiveCheck } =
    React.useContext(TabContext);

  const { data, isLoading } = useQuery(
    ["movable", active, page, params.q || ""],
    () =>
      apiItem.find(
        activeTabProps.param,
        page,
        8,
        params.q,
        "heritage_name",
        "asc",
        {
          heritage: {
            heritage_type: ["Хөдлөх өв"],
          },
        }
      )
  );

  return (
    <>
      <CultureContainer>
        <>
          <div className="flex justify-between items-center mb-6">
            <h1 className="uppercase font-[600] text-2xl">
              {f({ id: "culture-movable-title" })}
            </h1>
            <Link
              href={"/search/?q=&heritage_type=Хөдлөх өв&province_type="}
              onClick={() => setActiveCheck(["Хөдлөх өв"])}
            >
              <div className="flex items-center gap-[1rem] hover:opacity-80">
                <p className={`font-[700] text-[16px]`}>
                  {f({ id: "see-all" })}
                </p>
                <MdArrowForwardIos className="text-default" />
              </div>
            </Link>
          </div>
          <ContentList isLoading={isLoading} contents={data?.data} />
        </>
      </CultureContainer>
    </>
  );
};

export default CultureMovable;
