import * as React from "react";
import { useRouter } from "next/router";
import { TabContext } from "~/context/tab";
import { Menu } from "@mantine/core";

export default function LanguageButton() {
  const router = useRouter();
  const { pathname, asPath, query } = router;
  const { tabs, active, setEng, eng } = React.useContext(TabContext);

  const pathsWithDynamicBackground = [
    "/",
    "/myLibrary",
    "/profile",
    "/search-log",
    "/views-log",
  ];

  const toggleLanguage = (e: any) => {
    e.preventDefault();
    if (eng) {
      router.push({ pathname, query }, asPath, { locale: "mn", scroll: false });
    } else {
      router.push({ pathname, query }, asPath, { locale: "en", scroll: false });
    }
    setEng(!eng);
  };

  return (
    <Menu trigger="hover">
      <Menu.Target>
        <div className={"flex gap-2 items-center font-[700] cursor-pointer"}>
          <div
            className={`${
              pathsWithDynamicBackground.includes(pathname)
                ? "text-default"
                : tabs.find((tab: any) => tab.type === active)?.textColor
            }`}
          >
            {eng ? "ENG" : "МОН"}
          </div>
          <svg
            width="13"
            height="9"
            viewBox="0 0 13 9"
            fill="none"
            xmlns="http://www.w3.org/2000/svg"
          >
            <path
              d="M6.85523 8.08231C7.13578 8.08231 7.36843 7.97967 7.5874 7.7607L12.74 2.4918C12.9179 2.31389 13 2.10176 13 1.84858C13 1.33538 12.5963 0.917969 12.0831 0.917969C11.8299 0.917969 11.5972 1.02745 11.4125 1.21221L6.86207 5.89263L2.29797 1.21221C2.11322 1.02745 1.88741 0.917969 1.62738 0.917969C1.11418 0.917969 0.703613 1.33538 0.703613 1.84858C0.703613 2.10176 0.792569 2.31389 0.97048 2.4918L6.12306 7.7607C6.34202 7.98651 6.57468 8.08231 6.85523 8.08231Z"
              fill={
                pathsWithDynamicBackground.includes(pathname)
                  ? "#112437"
                  : tabs.find((tab: any) => tab.type === active)?.color
              }
            />
          </svg>
        </div>
      </Menu.Target>
      <Menu.Dropdown>
        <Menu.Item onClick={toggleLanguage}>
          <div
            className={`font-[700] ${
              pathsWithDynamicBackground.includes(pathname)
                ? "text-default"
                : tabs.find((tab: any) => tab.type === active)?.textColor
            }`}
          >
            {!eng ? "ENG" : "МОН"}
          </div>
        </Menu.Item>
      </Menu.Dropdown>
    </Menu>
  );
}
