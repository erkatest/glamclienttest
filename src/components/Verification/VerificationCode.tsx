import React, { useRef, useState, useEffect } from "react";
import { PinInput } from "@mantine/core";

interface IProps {
  setVerify: Function;
}

const VerificationCode = ({ setVerify }: IProps) => {
  return (
    <div className="m-5 flex justify-center items-center space-x-2">
      <PinInput
        length={6}
        placeholder=""
        onChange={(item) => {
          setVerify(item);
        }}
      />
    </div>
  );
};

export default VerificationCode;
