import React, { useState } from "react";

const CustomButton = (props: any) => {
  const { text, rightIcon, leftIcon, onClick } = props;

  const [researchOpen, setResearchOpen] = useState(false);

  return (
    <div
      className="bg-[#F7F7FC] rounded-md transition-colors duration-200 hover:bg-[#EDEDF2] active:translate-y-[1px] px-[var(--space-sm)] py-6"
      onClick={onClick}
    >
      <div className="flex items-center gap-[8px]">
        {leftIcon && leftIcon}
        <span className="hidden lg:block">
          <div className="text-[14px]">{text}</div>
        </span>
        {rightIcon && rightIcon}
      </div>
    </div>
  );
};

export default CustomButton;
