import {
  Stack,
  UnstyledButton,
  Flex,
  Box,
  Text,
  CloseButton,
  rem,
  Textarea,
  Input,
  LoadingOverlay,
  Button,
} from "@mantine/core";
import React, { useContext, useEffect, useState } from "react";
import { IconFolder, IconPlus } from "@tabler/icons-react";
import { useRouter } from "next/router";
import ReaderContext from "~/context/Reader";

const ButtonStyle = {
  backgroundColor: "#4E4B66",
  padding: 10,
  transition: "background-color 0.2s ease",
  "&:hover": {
    backgroundColor: "#3C3952",
  },
  "&:active": {
    backgroundColor: "#2A273C",
  },
  color: "white",
};

const Note = (props: {
  page: any;
  close: () => void;
  loading: boolean;
  save: (node: string, settings: any) => void;
  title: any;
}) => {
  const { selection } = useContext(ReaderContext);
  const { loading } = props;
  const [note, setNote] = useState("");
  const router = useRouter();

  return (
    <>
      <div className="fixed inset-0 bg-white/50 z-[999999]" />
      <div
        style={{
          position: "absolute",
          top: "50%",
          left: "50%",
          transform: "translate(-50%, -50%)",
          zIndex: 999999,
        }}
      >
        <Box
          bg={"#4E4B66"}
          maw={"100%"}
          w={550}
          p={"1rem"}
          sx={{ borderRadius: 5, overflow: "hidden", color: "white" }}
          pos={"relative"}
        >
          <LoadingOverlay visible={loading} />
          <Stack>
            <Flex align={"center"} justify={"space-between"}>
              <Text weight={600} size={16}>
                Тэмдэглэл бичих
              </Text>

              <CloseButton
                aria-label="Close modal"
                sx={{}}
                onClick={() => {
                  props?.close();
                }}
              />
            </Flex>

            <Stack spacing={8}>
              <Text size={13} weight={700}>
                {props.title}
              </Text>

              <Text size={13} style={{ wordBreak: "break-word" }}>
                {selection || "-"}
              </Text>
            </Stack>

            <Textarea
              sx={{ backgroundColor: "black", color: "black" }}
              //   bg={"#262338"} // Set background color to #262338
              //   color={"#6E7191"} // Set text color to #6E7191
              placeholder="Энд бичнэ үү..."
              minRows={4}
              maxRows={6}
              value={note}
              onChange={(e) => {
                setNote(e.target.value);
              }}
            />
            <div className="flex justify-end">
              <Button
                variant="default"
                size="xs"
                onClick={() => {
                  props?.save(note, {
                    redirectUrl: router.asPath + `&page=${props.page}`,
                    note: note,
                    research_type: "note",
                  });
                }}
              >
                <Text size={12} weight={400}>
                  Хадгалах
                </Text>
              </Button>
            </div>
          </Stack>
        </Box>
      </div>
    </>
  );
};

export default Note;
