import {
  Stack,
  UnstyledButton,
  Flex,
  Box,
  Text,
  Space,
  Divider,
} from "@mantine/core";
import React, { useEffect, useState } from "react";

import { IoTextOutline } from "react-icons/io5";
import { IconArrowDown, IconFeather, IconX } from "@tabler/icons-react";
import IconScroll from "~/assets/IconScroll";
import IconSound from "~/assets/IconSound";

const ButtonStyle = {
  backgroundColor: "#4E4B66",
  padding: 10,
  transition: "background-color 0.2s ease",
  "&:hover": {
    backgroundColor: "#3C3952",
  },
  "&:active": {
    backgroundColor: "#2A273C",
  },
  color: "white",
};

const RightClickActions = (props: {
  close: any;
  play: () => void;
  quote: () => void;
  note: () => void;
  highlight: () => void;
  content?: any;
}) => {
  const { close, quote, highlight, note } = props;

  const isTouchDevice = () => {
    return (
      "ontouchstart" in window ||
      navigator.maxTouchPoints > 0 ||
      (navigator as any).msMaxTouchPoints > 0
    );
  };

  return (
    <>
      <Box
        bg={"#4E4B66"}
        sx={{
          borderRadius: 5,
          overflow: "hidden",
          boxShadow: "0 0 5px 1px #0000005a",
          position: "relative",
          whiteSpace: "nowrap",
          display: "block",
        }}
        h={50}
      >
        <Flex>
          <UnstyledButton
            bg={"#4E4B66"}
            sx={ButtonStyle}
            onClick={() => {
              if (!isTouchDevice()) {
                quote();
                close();
              }
            }}
            onTouchStart={() => {
              if (isTouchDevice()) {
                quote();
                close();
              }
            }}
            h={50}
          >
            <Flex align={"center"} gap={10}>
              <IconScroll />

              <Text className="hidden md:block" size={14}>
                Эш татах
              </Text>
            </Flex>
          </UnstyledButton>
          <Divider color="#ffffff1a" orientation="vertical" />
          <UnstyledButton
            bg={"#4E4B66"}
            sx={ButtonStyle}
            onClick={() => {
              if (!isTouchDevice()) {
                highlight();
              }
            }}
            onTouchStart={() => {
              if (isTouchDevice()) {
                highlight();
              }
            }}
            h={50}
          >
            <Flex align={"center"} gap={10}>
              <IoTextOutline size={18} />

              <Text className="hidden md:block" size={14}>
                Тодруулах
              </Text>
            </Flex>
          </UnstyledButton>
          <Divider color="#ffffff1a" orientation="vertical" />
          <UnstyledButton
            bg={"#4E4B66"}
            sx={ButtonStyle}
            onClick={() => {
              if (!isTouchDevice()) {
                note();
                close();
              }
            }}
            onTouchStart={() => {
              if (isTouchDevice()) {
                note();
                close();
              }
            }}
            h={50}
          >
            <Flex align={"center"} gap={10}>
              <IconFeather />

              <Text className="hidden md:block" size={14}>
                Тэмдэглэл бичих
              </Text>
            </Flex>
          </UnstyledButton>

          <Divider color="#ffffff1a" orientation="vertical" />
          <UnstyledButton
            bg={"#4E4B66"}
            sx={ButtonStyle}
            onClick={(e) => {
              e.preventDefault();
              e.stopPropagation();
              props?.close();
            }}
            onTouchStart={(e) => {
              e.preventDefault();
              e.stopPropagation();
              if (isTouchDevice()) {
                props?.close();
              }
            }}
            h={50}
          >
            <Flex align={"center"} gap={10}>
              <IconX />
            </Flex>
          </UnstyledButton>
        </Flex>
      </Box>
    </>
  );
};

export default RightClickActions;
