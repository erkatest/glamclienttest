import {
  Box,
  Text,
  Stack,
  Flex,
  CloseButton,
  Tabs,
  rem,
  UnstyledButton,
  LoadingOverlay,
  ScrollArea,
} from "@mantine/core";
import React, { useContext, useState } from "react";
import APA from "./APA";

import { useSearchParams } from "next/navigation";

import dayjs from "dayjs";
import ReaderContext from "~/context/Reader";

const ButtonStyle = {
  backgroundColor: "#4E4B66",
  padding: 10,
  transition: "background-color 0.2s ease",
  "&:hover": {
    backgroundColor: "#2A273C",
  },
  "&:active": {
    backgroundColor: "#2A273C",
  },
  color: "white",
};

const CitationAction = (props: {
  dataQuery: any;
  contentSlug: any;
  page: any;
  pointer?: { x: number; y: number };
  loading: boolean;
  close: () => void;
  save: (settings: any) => void;
}) => {
  const { pointer } = props;
  const TabValues = [
    { value: "APA", label: "APA" },
    { value: "MLA", label: "MLA" },
    { value: "Chicago", label: "Chicago" },
    { value: "IEEE", label: "IEEE" },
  ];
  const searchParams: any = useSearchParams();

  const id = searchParams.get("id");

  const [activeTab, setActiveTab] = useState(TabValues[0].value);

  const { selection } = useContext(ReaderContext);

  // Create a mapping of tabs to their respective components
  const tabComponents: any = {
    APA: (
      <APA
        page={props.page}
        text={`, & esan. <i>${selection}</i>(1-р хув.), Glam. х. 0, glam.mn. https://glam.mn/detail/${id}`}
        close={props.close}
        save={props?.save}
        title={"APA"}
        data={props.dataQuery.data}
        clearText={selection}
      />
    ),
    MLA: (
      <APA
        page={props.page}
        text={`esan. <i>${selection}</i>. 1-рхувилбар, Glam, ${new Date().getFullYear()}. х. 0, glam.mn. https://glam.mn/detail/${id}`}
        close={props.close}
        save={props?.save}
        title={"MLA"}
        data={props.dataQuery.data}
        clearText={selection}
      />
    ),

    Chicago: (
      <APA
        page={props.page}
        text={`, & esan. <i>${selection}</i>. 1-р хувилбар, Ulaanbaatar, Mongolia: Glam, ${new Date().getFullYear()}. х. 0, glam.mn. https://glam.mn/detail/${id}`}
        close={props.close}
        save={props?.save}
        title={"Chicago"}
        data={props.dataQuery.data}
        clearText={selection}
      />
    ),

    IEEE: (
      <APA
        page={props.page}
        text={`[${props.page}] ${
          props.dataQuery.data?.fields?.find(
            (el: any) => el?.name === "Хэвлэлийн газар"
          )?.value
        }, “${
          props.dataQuery.data?.name
        }”, <strong><em> ${selection}</em></strong>.,  p. ${props.page}, ${
          props.dataQuery.data?.fields?.find(
            (el: any) => el?.name === "Хэвлэгдсэн он"
          )?.value
        }. Accessed: ${dayjs(
          props.dataQuery.data.content_items?.find((el: any) => el?.id === id)
            ?.created_at
        ).format(
          "MMMM DD, YYYY"
        )} . [Online]. Available: <a href="glam.mn">glam.mn</a>
        `}
        close={props.close}
        save={props?.save}
        title={"Chicago"}
        data={props.dataQuery.data.content_items?.find(
          (el: any) => el?.id === id
        )}
        clearText={selection}
      />
    ),
  };

  return (
    <>
      {/* <div className="fixed inset-0 bg-[#2b273c]/50 z-[999999]" /> */}
      <div className="fixed inset-0 bg-white/50 z-[999999]" />
      <div
        style={{
          position: "fixed",
          top: "50%",
          left: "50%",
          transform: "translate(-50%, -50%)",
          zIndex: 999999,
        }}
      >
        <Box
          bg={"#4E4B66"}
          maw={"100%"}
          w={550}
          p={"1rem"}
          sx={{ borderRadius: 5, overflow: "hidden", color: "white" }}
          pos="relative"
        >
          <LoadingOverlay visible={props?.loading} />
          <Stack>
            <Flex align={"center"} justify={"space-between"}>
              <Text weight={600} size={16}>
                Эш таталт
              </Text>

              <CloseButton
                aria-label="Close modal"
                sx={{}}
                onClick={() => {
                  props?.close();
                }}
              />
            </Flex>

            <Box>
              <Flex align={"center"}>
                {TabValues.map((e) => {
                  return (
                    <UnstyledButton
                      key={e.value} // Add a unique key
                      onClick={() => {
                        setActiveTab(e.value);
                      }}
                      bg={e.value === activeTab ? "#2A273C" : "#4E4B66"} // Set background color based on active tab
                      sx={ButtonStyle}
                    >
                      <Flex align={"center"} gap={10}>
                        <Text size={14} weight={600}>
                          {e.label}
                        </Text>
                      </Flex>
                    </UnstyledButton>
                  );
                })}
              </Flex>

              {/* Render the content of the active tab */}
              <Box bg={"#2A273C"} p={10}>
                {tabComponents[activeTab]}
              </Box>
            </Box>
          </Stack>
        </Box>
      </div>
    </>
  );
};

export default CitationAction;
