import {
  Box,
  Button,
  CopyButton,
  Flex,
  Grid,
  Group,
  ScrollArea,
  Stack,
  Text,
  TextInput,
  Textarea,
  UnstyledButton,
} from "@mantine/core";
import { IconArrowRight } from "@tabler/icons-react";
import Link from "next/link";
import { useRouter } from "next/router";
import React, { useEffect, useState } from "react";

const ButtonStyle = {
  backgroundColor: "#2A273C",
  padding: 10,
  transition: "background-color 0.2s ease",
  "&:hover": {
    backgroundColor: "#4E4B66",
  },
  "&:active": {
    backgroundColor: "#4E4B66",
  },
  color: "white",
};

const APA = (props: {
  page: any;
  text: string;
  close: () => void;
  save: any;
  title: any;
  data: any;
  clearText: any;
}) => {
  const [edition, setEdition] = React.useState("1");
  const [publisher, setPublisher] = React.useState(
    props.data?.publisher_name || ""
  );
  const router = useRouter();
  const text = props?.text;
  const clearText = props?.clearText;
  const [placeOfPublication, setPoP] = React.useState("Ulaanbaatar, Mongolia");
  const [note, setNote] = React.useState("");

  // useEffect(() => {
  //   if (props.data?.publisher_name) {
  //     setPublisher(props.data?.publisher_name);
  //   }
  // }, [props.data]);
  return (
    <Box>
      <Stack>
        <Grid>
          {/* Title */}
          <Grid.Col span={6} sx={{ borderRight: "1px solid #4E4B66" }}>
            <Flex direction={"column"}>
              <TextInput
                label="[Хувилбар]"
                value={edition}
                styles={{
                  label: {
                    color: "#fff",
                  },
                }}
                onChange={(e) => {
                  setEdition(e.target.value);
                }}
              />
              <TextInput
                label="[Бүтээл нийлүүлэгч]"
                value={publisher}
                onChange={(e) => {
                  setPublisher(e.target.value);
                }}
                styles={{
                  label: {
                    color: "#fff",
                  },
                }}
              />
              <TextInput
                label="[Нийтлэгдсэн газар]"
                value={placeOfPublication}
                onChange={(e) => {
                  setPoP(e.target.value);
                }}
                styles={{
                  label: {
                    color: "#fff",
                  },
                }}
              />
            </Flex>
          </Grid.Col>
          <Grid.Col span={6}>
            <Stack>
              <Text>Эшлэл</Text>
              <ScrollArea>
                <Box
                  style={{ wordBreak: "break-word" }}
                  dangerouslySetInnerHTML={{ __html: text }}
                  h={180}
                />
              </ScrollArea>

              {/* <Text size={13} italic color="#4E4B66">
                {props?.title} 7th Citation
              </Text> */}
            </Stack>
          </Grid.Col>
        </Grid>
        <Group position="right">
          <CopyButton value={props.text}>
            {({ copied, copy }) => (
              <Button
                size="xs"
                bg={copied ? "teal" : "white"}
                onClick={copy}
                sx={{
                  color: "black",
                  ":hover": {
                    backgroundColor: "white",
                  },
                }}
              >
                <Text size={12} weight={400}>
                  {copied ? "Хуулагдлаа" : "Хуулах"}
                </Text>
              </Button>
            )}
          </CopyButton>
        </Group>
        <Textarea
          label="[Тэмдэглэл]"
          value={note}
          onChange={(e) => {
            setNote(e.target.value);
          }}
          styles={{
            label: {
              color: "#fff",
            },
          }}
        />
        <Flex align={"center"} justify={"end"}>
          {/* <Link href="/standarts">
            <Flex align={"center"} gap={5}>
              <Text size={14}>Эш татах заавар, стандартууд</Text>

              <IconArrowRight size={16} />
            </Flex>
          </Link> */}

          <Button
            variant="default"
            size="xs"
            disabled={!note}
            onClick={() => {
              props?.save({
                clearText,
                edition,
                publisher,
                placeOfPublication,
                note,
                redirectUrl: router.asPath + `&page=${props.page}`,
                research_type: "quote",
              });
            }}
          >
            <Text size={12} weight={400}>
              Эш татах
            </Text>
          </Button>
        </Flex>
      </Stack>
    </Box>
  );
};

export default APA;
