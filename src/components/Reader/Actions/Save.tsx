import {
  Stack,
  UnstyledButton,
  Flex,
  Box,
  Text,
  CloseButton,
  rem,
} from "@mantine/core";
import React from "react";
import { IconFolder, IconPlus } from "@tabler/icons-react";

const ButtonStyle = {
  backgroundColor: "#4E4B66",
  padding: 10,
  transition: "background-color 0.2s ease",
  "&:hover": {
    backgroundColor: "#3C3952",
  },
  "&:active": {
    backgroundColor: "#2A273C",
  },
  color: "white",
};

const Save = () => {
  return (
    <Box
      bg={"#4E4B66"}
      maw={300}
      py={rem(16)}
      sx={{ borderRadius: 5, overflow: "hidden", color: "#fff" }}
    >
      <Stack px={rem(16)}>
        <Flex align={"center"} justify={"space-between"}>
          <Text weight={600} size={13}>
            Эш таталт
          </Text>

          <CloseButton aria-label="Close modal" sx={{}} />
        </Flex>

        <Text size={13}>Хадгалах хавтсаа сонгоно уу.</Text>
      </Stack>

      <Stack spacing={0}>
        <UnstyledButton bg={"#4E4B66"} sx={ButtonStyle}>
          <Flex align={"center"} gap={10}>
            <IconFolder />

            <Text size={13} weight={700}>
              Миний сан
            </Text>
          </Flex>
        </UnstyledButton>
        <UnstyledButton bg={"#4E4B66"} sx={ButtonStyle}>
          <Flex align={"center"} gap={10}>
            <IconFolder />

            <Text size={13} weight={700}>
              Нийгмийн даатгалын сан
            </Text>
          </Flex>
        </UnstyledButton>
        <UnstyledButton bg={"#4E4B66"} sx={ButtonStyle}>
          <Flex align={"center"} gap={10}>
            <IconFolder />

            <Text size={13} weight={700}>
              Хувь хүний хөгжлийн
            </Text>
          </Flex>
        </UnstyledButton>
        <UnstyledButton bg={"#4E4B66"} sx={ButtonStyle}>
          <Flex align={"center"} gap={10}>
            <IconPlus color="#A0A3BD" />

            <Text size={13} weight={700} color="#A0A3BD">
              Шинэ хавтас үүсгэх
            </Text>
          </Flex>
        </UnstyledButton>
      </Stack>
    </Box>
  );
};

export default Save;
