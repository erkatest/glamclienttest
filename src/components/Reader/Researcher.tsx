import {
  Box,
  rem,
  Stack,
  Flex,
  Text,
  CloseButton,
  ActionIcon,
  Grid,
  TextInput,
  Select,
  Menu,
  ScrollArea,
  Loader,
  Navbar,
  Space,
  Tabs,
  Divider,
  Input,
  Button,
  Textarea,
} from "@mantine/core";
import {
  IconArrowsExchange2,
  IconArrowsMove,
  IconCheck,
  IconDots,
  IconEdit,
  IconFolderX,
  IconLayoutGrid,
  IconPencil,
  IconSearch,
  IconTrash,
} from "@tabler/icons-react";
import React, { useContext, useEffect, useState } from "react";
import { useQuery } from "react-query";
import YourPage from "./checkLink2";
import { useRouter } from "next/router";
import { VscNotebookTemplate } from "react-icons/vsc";
import { useDisclosure } from "@mantine/hooks";

import { useSearchParams } from "next/navigation";
import {
  PiCopyLight,
  PiNoteBlankLight,
  PiNoteLight,
  PiNotePencilBold,
  PiQuotes,
  PiQuotesLight,
  PiTextAUnderlineBold,
} from "react-icons/pi";
import dayjs from "dayjs";
// import ResearchGreateFolder from "../Modal/GreateFolder";
// import ResearchNote from "../Modal/Note";
// import ResearchDelete from "../Modal/ResearchDelete";
// import ResearchDeleteFolder from "../Modal/ResearchFolderDelete";
// import ResearchMove from "../Modal/ResearchMove";
// import ResearchQuote from "../Modal/ResearchQuote";
// import ResearchRemove from "../Modal/ResearchRemove";
// import ResearchFolderShare from "../Modal/ResearchShare";
// import useAuth from "@/hooks/useAuth";

import { useIntl } from "react-intl";
import utc from "dayjs/plugin/utc";
import apiResearch from "~/api/research";
import ReaderContext from "~/context/Reader";
import { toast } from "react-toastify";
import useMessage from "~/hooks/useMessage";
import { useUser } from "~/context/auth";
import NoteDeleteModal from "../Modals/NoteDelete";
dayjs.extend(utc);

const ResearchTypes = {
  note: "Тэмдэглэл",
};

const Researcher = (props: any) => {
  const { type } = props;
  const {
    redirectPage,
    setShowSidebar,
    showSidebar,
    queryNotes,
    handleEpubRedirect,
  } = useContext(ReaderContext);
  const { open, close } = props;
  const intl = useIntl();
  const router = useRouter();
  const searchParams: any = useSearchParams();

  const content_id = searchParams.get("id");
  const folder_id = searchParams.get("folder_id");

  const [noteModalOpen, { open: noteOpen, close: noteClose }] =
    useDisclosure(false);
  const [folderModalOpen, { open: folderOpen, close: folderClose }] =
    useDisclosure(false);

  const { id } = router.query;
  const isEpub = type === "epub";
  const f = useMessage();
  const [moveModal, setMoveModal] = useState(false);
  const [deleteModal, setDeleteModal] = useState(false);
  const [removeModal, setRemoveModal] = useState(false);
  const [deleteFolderModal, setDeleteFolderModal] = useState(false);
  const [quoteModal, setQuoteModal] = useState(false);
  const [shareModal, setShareModal] = useState(false);
  const [noteModal, setNoteModal] = useState(false);
  const [greateFolder, setGreateFolder] = useState(false);

  const { user } = useUser();
  const [quoteId, setQuoteId] = useState("");
  const [quote, setQuote] = useState("");
  const [note, setNote] = useState<any>({});

  const [edit, setEdit] = useState(false);
  const folder = useQuery(["folder_list"], () => apiResearch.findFolder());

  const { data, isLoading, refetch } = queryNotes;

  const [researchType, setResearchType] = useState("all");

  const [queryNote, setQueryNote] = useState("");

  const params = useSearchParams();
  const search = params.get("search");

  const handleSubmit = async (e: any) => {
    const res = await apiResearch.update(
      e?.id,
      e?.research_type === "quote"
        ? {
            type: e?.research_type,
            settings: {
              note: note?.note,
              clearText: e?.reference || e?.settings?.clearText,
              edition: e?.settings?.edition,
              highlightSerialize: e?.settings?.highlightSerialize,
              page: e?.settings?.page,
              placeOfPublication: e?.settings?.placeOfPublication,
              publisher: e?.settings?.publisher,
              redirectUrl: e?.settings?.redirectUrl,
            },
          }
        : {
            type: e?.research_type,
            note: note?.note,
          }
    );

    if (res?.status !== "success") {
      toast.error(res.error);
      refetch();
      setEdit(false);
    } else {
      toast.success(f({ id: "success-add-item" }));

      setEdit(false);
      refetch();
    }
  };

  return (
    <Box
      // w={600}
      pos={"relative"}
      h={"100vh"}
      sx={{ borderLeft: "1px solid #D9DBE9" }}
    >
      <NoteDeleteModal
        isOpen={deleteModal}
        onClose={() => setDeleteModal(false)}
        refetch={refetch}
        id={quoteId}
      />
      <>
        {/* Modals for editing notes and quotes */}
        <div className="fixed bottom-[20px] right-[20px] z-50">
          <CloseButton
            size={"lg"}
            variant="outline"
            color="blue"
            sx={{
              borderRadius: "50%",
            }}
            onClick={() => {
              setShowSidebar(false);
            }}
          ></CloseButton>
        </div>
      </>

      <Navbar height={"100vh"}>
        <Tabs
          defaultValue={isEpub ? "research" : search ? "search" : "research"}
        >
          <Tabs.List grow>
            <Tabs.Tab value="research">Судлаачийн булан</Tabs.Tab>
          </Tabs.List>

          <Tabs.Panel value="research" p="sm">
            <Navbar.Section mb={"md"}>
              <Box>
                <Grid>
                  <Grid.Col span={6}>
                    <TextInput
                      value={queryNote}
                      onChange={(e) => {
                        setQueryNote(e.target.value);
                      }}
                      placeholder="Тэмдэглэл хайх"
                      icon={<IconLayoutGrid size={16} color="#6E7191" />}
                    />
                  </Grid.Col>
                  <Grid.Col span={6}>
                    <Select
                      defaultValue="all"
                      value={researchType}
                      onChange={(e) => setResearchType(e || "all")}
                      data={[
                        { value: "all", label: "Бүгд" },
                        { value: "quote", label: "Эш татсан" },
                        { value: "note", label: "Тэмдэглэл" },
                        { value: "master_note", label: "Ерөнхий тэмдэглэл" },
                        { value: "highlight", label: "Тодруулга" },
                      ]}
                      icon={<IconArrowsExchange2 size={16} color="#6E7191" />}
                    />
                  </Grid.Col>
                </Grid>
              </Box>
            </Navbar.Section>
            <Navbar.Section grow mx="-xs" px="xs">
              <ScrollArea type="auto" h={"calc(100vh - 140px)"}>
                {!isLoading ? (
                  <Stack spacing={16}>
                    {data.data
                      ?.filter((e: any) => {
                        return researchType === "all"
                          ? true
                          : e.research_type === researchType ||
                              e.settings?.research_type === researchType;
                      })
                      .filter((e: any) => {
                        return (
                          (queryNote || "")?.trim() === "" ||
                          (e.note || "" || e?.settings?.note)
                            ?.toLowerCase()
                            .includes(queryNote?.toLocaleLowerCase() || "") ||
                          (e.title || "")
                            ?.toLowerCase()
                            .includes(queryNote?.toLocaleLowerCase() || "") ||
                          (e.reference || "" || e?.settings?.clearText)
                            ?.toLowerCase()
                            .includes(queryNote?.toLocaleLowerCase() || "")
                        );
                      })?.length === 0 ? (
                      <div className="flex flex-col justify-center min-h-[200px] items-center p-4">
                        <p className="text-center text-sm text-slate-400">
                          Тэмдэглэл байхгүй байна
                        </p>
                        <svg
                          xmlns="http://www.w3.org/2000/svg"
                          width="100"
                          height="100"
                          viewBox="0 0 120 120"
                          fill="none"
                          className="mt-1"
                        >
                          <path
                            d="M108.75 35.625H90V28.125C90 25.0312 87.4688 22.5 84.375 22.5H65.625V11.25C65.625 8.15625 63.0938 5.625 60 5.625H11.25C8.15625 5.625 5.625 8.15625 5.625 11.25V78.75C5.625 81.8438 8.15625 84.375 11.25 84.375H30V95.625C30 98.7188 32.5312 101.25 35.625 101.25H54.375V108.75C54.375 111.844 56.9062 114.375 60 114.375H108.75C111.844 114.375 114.375 111.844 114.375 108.75V41.25C114.375 38.1562 111.844 35.625 108.75 35.625ZM108.75 39.375C109.781 39.375 110.625 40.2188 110.625 41.25V45H90V39.375H108.75ZM86.25 28.125V31.875H33.75V28.125C33.75 27.0938 34.5938 26.25 35.625 26.25H84.375C85.4062 26.25 86.25 27.0938 86.25 28.125ZM11.25 9.375H60C61.0312 9.375 61.875 10.2188 61.875 11.25V15H9.375V11.25C9.375 10.2188 10.2188 9.375 11.25 9.375ZM11.25 80.625C10.2188 80.625 9.375 79.7812 9.375 78.75V18.75H61.875V22.5H35.625C32.5312 22.5 30 25.0312 30 28.125V30.9375H18.75C17.7188 30.9375 16.875 31.7812 16.875 32.8125C16.875 33.8438 17.7188 34.6875 18.75 34.6875H30V42.1875H18.75C17.7188 42.1875 16.875 43.0312 16.875 44.0625C16.875 45.0938 17.7188 45.9375 18.75 45.9375H30V53.4375H18.75C17.7188 53.4375 16.875 54.2812 16.875 55.3125C16.875 56.3438 17.7188 57.1875 18.75 57.1875H30V64.6875H18.75C17.7188 64.6875 16.875 65.5312 16.875 66.5625C16.875 67.5938 17.7188 68.4375 18.75 68.4375H30V80.625H11.25ZM33.75 95.625V35.625H86.25V95.625C86.25 96.6562 85.4062 97.5 84.375 97.5H35.625C34.5938 97.5 33.75 96.6562 33.75 95.625ZM108.75 110.625H60C58.9688 110.625 58.125 109.781 58.125 108.75V101.25H84.375C86.4563 101.25 88.2375 100.106 89.2125 98.4375H101.25C102.281 98.4375 103.125 97.5938 103.125 96.5625C103.125 95.5312 102.281 94.6875 101.25 94.6875H90V87.1875H101.25C102.281 87.1875 103.125 86.3438 103.125 85.3125C103.125 84.2812 102.281 83.4375 101.25 83.4375H90V75.9375H101.25C102.281 75.9375 103.125 75.0938 103.125 74.0625C103.125 73.0312 102.281 72.1875 101.25 72.1875H90V64.6875H101.25C102.281 64.6875 103.125 63.8438 103.125 62.8125C103.125 61.7812 102.281 60.9375 101.25 60.9375H90V48.75H110.625V108.75C110.625 109.781 109.781 110.625 108.75 110.625ZM46.875 61.35C46.3875 61.35 45.9188 61.1625 45.5438 60.8063C44.8125 60.075 44.8125 58.8937 45.5438 58.1625L47.6437 56.0625L45.5438 53.9625C44.8125 53.2313 44.8125 52.05 45.5438 51.3188C46.275 50.5875 47.4563 50.5875 48.1875 51.3188L50.2875 53.4188L52.3875 51.3188C53.1188 50.5875 54.3 50.5875 55.0312 51.3188C55.7625 52.05 55.7625 53.2313 55.0312 53.9625L52.9312 56.0625L55.0312 58.1625C55.7625 58.8937 55.7625 60.075 55.0312 60.8063C54.6562 61.1813 54.1875 61.35 53.7 61.35C53.2125 61.35 52.7438 61.1625 52.3688 60.8063L50.2687 58.7062L48.1688 60.8063C47.7938 61.1813 47.325 61.35 46.8375 61.35H46.875ZM64.95 58.1437L67.05 56.0437L64.95 53.9438C64.2188 53.2125 64.2188 52.0312 64.95 51.3C65.6812 50.5688 66.8625 50.5688 67.5938 51.3L69.6937 53.4L71.7938 51.3C72.525 50.5688 73.7063 50.5688 74.4375 51.3C75.1688 52.0312 75.1688 53.2125 74.4375 53.9438L72.3375 56.0437L74.4375 58.1437C75.1688 58.875 75.1688 60.0562 74.4375 60.7875C74.0625 61.1625 73.5938 61.3312 73.1063 61.3312C72.6188 61.3312 72.15 61.1437 71.775 60.7875L69.675 58.6875L67.575 60.7875C67.2 61.1625 66.7313 61.3312 66.2438 61.3312C65.7563 61.3312 65.2875 61.1437 64.9125 60.7875C64.1812 60.0562 64.1812 58.875 64.9125 58.1437H64.95ZM60 72.4688C55.6125 72.4688 51.6 74.8688 49.5375 78.75C49.05 79.6687 47.9062 80.0062 47.0062 79.5188C46.0875 79.0312 45.75 77.8875 46.2375 76.9875C48.9562 71.8875 54.225 68.7188 60.0187 68.7188C65.8125 68.7188 71.0813 71.8875 73.8 76.9875C74.2875 77.9062 73.95 79.0312 73.0312 79.5188C72.75 79.6687 72.45 79.7437 72.15 79.7437C71.475 79.7437 70.8375 79.3875 70.5 78.75C68.4375 74.8688 64.425 72.4688 60.0375 72.4688H60Z"
                            fill="#A6B2C3"
                          />
                        </svg>
                      </div>
                    ) : (
                      data?.data
                        ?.filter((e: any) => {
                          return (
                            (e?.reference && e?.reference !== "") ||
                            e.research_type === "master_note" ||
                            e?.settings?.clearText
                          );
                        })
                        .filter((e: any) => {
                          return researchType === "all"
                            ? true
                            : e.research_type === researchType ||
                                e.settings?.research_type === researchType;
                        })
                        .filter((e: any) => {
                          return (
                            (queryNote || "")?.trim() === "" ||
                            (e.note || "")
                              ?.toLowerCase()
                              .includes(queryNote?.toLocaleLowerCase() || "") ||
                            (e.title || "")
                              ?.toLowerCase()
                              .includes(queryNote?.toLocaleLowerCase() || "") ||
                            (e.reference || "" || e?.settings?.clearText)
                              ?.toLowerCase()
                              .includes(queryNote?.toLocaleLowerCase() || "")
                          );
                        })
                        .sort((a: any, b: any) => {
                          const tmpA = new Date(a.createdAt).getTime();
                          const tmpB = new Date(b.createdAt).getTime();
                          if (tmpA < tmpB) return 1;
                          if (tmpA > tmpB) return -1;
                          return 0;
                        })
                        .filter((el: any) => el?.settings?.fileType === type)
                        .map((e: any, index: number) => {
                          return (
                            <Box
                              key={`list-${e.id}`}
                              bg={"white"}
                              p={rem(16)}
                              sx={{
                                border: "1px solid #D9DBE9",
                                ...(e?.settings?.page
                                  ? {
                                      cursor: "pointer",
                                      ":hover": {
                                        backgroundColor: "#D9DBE92a",
                                      },
                                    }
                                  : {
                                      cursor: "default",
                                    }),
                              }}
                              onClick={() => {
                                if (e?.settings?.page) {
                                  type === "pdf"
                                    ? redirectPage(e?.settings?.page)
                                    : handleEpubRedirect(e?.settings?.page);
                                }
                              }}
                            >
                              <Stack spacing={rem(10)}>
                                <Flex
                                  align={"center"}
                                  justify={"space-between"}
                                >
                                  {/* Type */}
                                  <Flex gap={rem(10)}>
                                    {e.research_type === "master_note" && (
                                      <PiNoteBlankLight className="text-red-400 text-2xl" />
                                    )}
                                    {e?.settings?.research_type ===
                                      "highlight" && (
                                      <PiTextAUnderlineBold className="text-red-400 text-2xl" />
                                    )}

                                    {e?.research_type === "note" &&
                                      e?.settings?.research_type !==
                                        "highlight" && (
                                        <PiNoteLight className="text-red-400 text-2xl" />
                                      )}

                                    {e?.research_type === "quote" && (
                                      <PiQuotes className="text-red-400 text-2xl" />
                                    )}

                                    <Box
                                      sx={{ flex: 1 }}
                                      className="text-[13px]"
                                    >
                                      {e?.reference ||
                                        (e?.settings?.clearText &&
                                          (
                                            e?.reference ||
                                            "" ||
                                            e?.settings?.clearText
                                          )?.trim() !== "" && (
                                            <Text
                                              size={13}
                                              weight={500}
                                              italic
                                              mb="sm"
                                              className="reference-text"
                                              sx={{
                                                wordBreak: "break-word",
                                              }}
                                              dangerouslySetInnerHTML={{
                                                __html:
                                                  e?.reference ||
                                                  e?.settings?.clearText,
                                              }}
                                            ></Text>
                                          ))}
                                      <Text
                                        size={12}
                                        color="#6E7191"
                                        lineClamp={1}
                                      >
                                        {e.createdAt &&
                                          dayjs(e.createdAt)
                                            .utc()
                                            .format("YYYY-MM-DD HH:mm:ss")}
                                      </Text>
                                    </Box>
                                  </Flex>

                                  {/* Edit actions */}
                                  <Menu
                                    position="left-end"
                                    shadow="md"
                                    width={200}
                                  >
                                    <Menu.Target>
                                      <ActionIcon
                                        onClick={() => {
                                          // setNote(e);
                                        }}
                                      >
                                        <IconDots />
                                      </ActionIcon>
                                    </Menu.Target>

                                    <Menu.Dropdown>
                                      <Menu.Item
                                        icon={
                                          <IconEdit color="#6E7191" size={16} />
                                        }
                                        // onClick={noteOpen}
                                        onClick={() => {
                                          setEdit(true);
                                          setNote({
                                            id: e?.id,
                                          });
                                        }}
                                      >
                                        <Text
                                          weight={700}
                                          color="#6E7191"
                                          size={14}
                                        >
                                          Тэмдэглэл засах
                                        </Text>
                                      </Menu.Item>
                                      {/* <Menu.Item
                                        icon={
                                          <IconPencil
                                            color="#6E7191"
                                            size={16}
                                          />
                                        }
                                        // onClick={noteOpen}
                                        onClick={() => {
                                          setNoteModal(true);
                                        }}
                                      >
                                        <Text
                                          weight={700}
                                          color="#6E7191"
                                          size={14}
                                        >
                                          Тэмдэглэл бичих
                                        </Text>
                                      </Menu.Item> */}

                                      <Menu.Item
                                        onClick={() => {
                                          setQuoteId(e?._id);
                                          setDeleteModal(true);
                                        }}
                                        icon={
                                          <IconTrash color="red" size={16} />
                                        }
                                      >
                                        <Text
                                          weight={700}
                                          color="red"
                                          size={14}
                                        >
                                          Тэмдэглэл устгах
                                        </Text>
                                      </Menu.Item>
                                    </Menu.Dropdown>
                                  </Menu>
                                </Flex>
                                <Divider />
                                {edit && note?.id === e?.id ? (
                                  <Flex
                                    align={"start"}
                                    className="space-x-3 w-full"
                                  >
                                    <Textarea
                                      placeholder="Тэмдэглэл"
                                      variant="unstyled"
                                      className=" bg-white px-2 rounded-[5px] w-full"
                                      size="sm"
                                      value={
                                        note?.id === e?.id && note?.note
                                          ? note?.note
                                          : e.settings.note
                                      }
                                      onChange={(text) =>
                                        setNote({
                                          id: e?.id,
                                          note: text.target.value,
                                        })
                                      }
                                    />
                                    <Box
                                      className="bg-[#F5A623] rounded-[5px] p-[4px] cursor-pointer"
                                      onClick={() => handleSubmit(e)}
                                    >
                                      <IconCheck size={14} color="white" />
                                    </Box>
                                  </Flex>
                                ) : (
                                  <Text size={13} color="black" lineClamp={4}>
                                    {e.note || e.settings.note}
                                  </Text>
                                )}
                              </Stack>
                            </Box>
                          );
                        })
                    )}
                  </Stack>
                ) : (
                  <Flex align={"center"} justify={"center"} h={"80vh"}>
                    <Loader />
                  </Flex>
                )}
              </ScrollArea>
            </Navbar.Section>
          </Tabs.Panel>
        </Tabs>
      </Navbar>
    </Box>
  );
};

export default Researcher;
