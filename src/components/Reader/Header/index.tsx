import {
  Box,
  Text,
  Container,
  Flex,
  TextInput,
  Tooltip,
  ActionIcon,
  Select,
  Group,
  Divider,
  Popover,
  Drawer,
  Slider,
} from "@mantine/core";
import {
  IconArrowLeft,
  IconSearch,
  IconZoomIn,
  IconZoomOut,
  IconArrowsDiagonal,
  IconRefresh,
  IconLayoutGrid,
  IconDotsVertical,
  IconSettings,
  IconCheck,
} from "@tabler/icons-react";
import { HiMiniEllipsisVertical } from "react-icons/hi2";
import Link from "next/link";

import dynamic from "next/dynamic";
import React, { useEffect, useRef, useState, useContext } from "react";
import CustomButton from "../CustomButton";
import { useQuery } from "react-query";

import { useRouter } from "next/router";
import { useDebouncedValue } from "@mantine/hooks";
import ReaderContext from "~/context/Reader";
import apiContent from "~/api/content";
import apiLibrary from "~/api/library";
// import AdvancedSearch from "./AdvancedSearch";

const AdvancedSearch = dynamic(() => import("./AdvancedSearch"), {
  ssr: false,
});

const fontSizes = [12, 14, 16, 18, 24, 30, 36, 48, 60, 72, 84];

const Header = (props: any) => {
  const { setFull, full, showSidebar, setShowSidebar, handleChangeSettings } =
    useContext(ReaderContext);
  const router = useRouter();
  const { type, token, mobile } = props;

  // const [searchOpened, { open: searchOpen, close: searchClose }] =
  //   useDisclosure(false);
  const [searchOpened, setSearchOpened] = useState(false);

  const [search, setSearch] = useState("");
  const searchRef = useRef<any>(null);

  const handleClickOutside = (event: MouseEvent) => {
    if (
      searchRef.current &&
      !searchRef.current.contains(event.target as Node) // Assert the type here
    ) {
      setSearchOpened(false);
    }
  };

  useEffect(() => {
    // if (typeof window !== "undefined") {
    // Your client-side code here

    document.addEventListener("mousedown", handleClickOutside);

    return () => {
      document.removeEventListener("mousedown", handleClickOutside);
    };
    // }
  }, []);

  const handleEnter = (e: any) => {
    if (e.keyCode === 13 && search) {
      router.push(`/c/content?search=${search}&type=title`);
    }
  };

  const dataQuery = useQuery(["contents"], () =>
    apiLibrary.findBookById(router.query?.id)
  );

  const { zoomIn, zoomOut, zoom, setZoom } = useContext(ReaderContext);

  const [currentColor, setCurrentColor] = useState("#fff");
  const [currentFont, setCurrentFont] = useState("Athelas");

  const [size, setSize] = useState();

  const [fontIndex, setFontIndex] = useState(1);

  const [dFontIndex] = useDebouncedValue(fontIndex, 200);

  const [showSettings, setShowSettings] = useState(false);

  useEffect(() => {
    handleChangeSettings({
      currentColor,
      currentFont,
      currentSize: fontSizes[dFontIndex],
    });
    return () => {
      document.body.style.backgroundColor = "#fff";
    };
  }, [currentColor, currentFont, dFontIndex]);

  return (
    <Box
      bg={"white"}
      pos={"fixed"}
      sx={{
        borderBottom: "1px solid #D9DBE9",
        zIndex: 50,
      }}
      top={0}
      left={0}
      right={0}
    >
      <Drawer
        opened={showSettings}
        onClose={() => setShowSettings(false)}
        title="Тохиргоо"
        zIndex={999999999}
        position="right"
        size="xs"
      >
        <div className="flex items-center p-5 space-x-4">
          <span
            className={`text-center flex-1 select-none active:opacity-80 font-bold ${
              fontIndex === 0 ? "opacity-50" : ""
            }`}
            // onClick={() => {
            //   setFontIndex(fontIndex - 1 < 0 ? 0 : fontIndex - 1);
            // }}
          >
            A
          </span>

          {/* <Divider orientation="vertical" className="flex-1" /> */}

          {/* <Slider
            color="blue"
            marks={new Array(fontSizes.length)
              .fill(0)
              ?.map((_, value) => ({ label: value + 1, value: value + 1 }))}
          /> */}

          <Slider
            w={"100%"}
            defaultValue={0}
            size="sm"
            step={1}
            value={fontIndex}
            min={0}
            max={11}
            marks={fontSizes?.map((size, i) => ({ label: size, value: i }))}
            styles={{
              markLabel: { display: "none" },
              label: { display: "none" },
            }}
            onChange={(e) => {
              setFontIndex(e);
            }}
          />

          <span
            className={`text-xl text-center  flex select-none font-bold  ${
              fontIndex + 1 === fontSizes?.length ? "opacity-50" : ""
            }`}
            // onClick={() => {
            //   setFontIndex(
            //     fontIndex + 1 > fontSizes?.length - 1
            //       ? fontSizes?.length - 1
            //       : fontIndex + 1
            //   );
            // }}
          >
            A
          </span>
        </div>
        <Divider />
        <div className="flex p-5 justify-between">
          {["#fff", "#f7f3e7", "#48484a", "#2c2c2f"]?.map((color, i) => (
            <div
              className="h-[35px] w-[35px] drop-shadow cursor-pointer active:opacity-80 rounded-full flex items-center justify-center"
              style={{ background: color }}
              key={`color-${color}`}
              onClick={() => {
                setCurrentColor(color);
              }}
            >
              {/* {color === currentColor && (
                )} */}
              <span style={{ color: i > 1 ? "#f7f3e7" : "#2c2c2f" }}>A</span>
            </div>
          ))}
        </div>
        <Divider />
        <div className="p-5 space-y-2">
          {[
            "Athelas",
            "Avenir Next",
            "Charter",
            "Georgia",
            "Iowan",
            "Palatino",
            "San Francisco",
            "New York",
            "Times New Roman",
          ]?.map((font) => (
            <div
              key={font}
              className="hover:opacity-80 cursor-pointer flex items-center space-x-2"
              onClick={() => {
                setCurrentFont(font);
              }}
            >
              <div className="w-[20px]">
                {currentFont === font && <IconCheck size={18} />}
              </div>
              <span style={{ fontFamily: font }}>{font}</span>
            </div>
          ))}
        </div>
      </Drawer>
      <Container size="xl">
        <Flex
          justify={"space-between"}
          direction={{ base: "column", sm: "row" }}
          className="md:items-center space-y-3 md:h-[70px] py-3 md:py-0 md:space-y-0"
        >
          <Flex align={"center"} gap={24} className="flex-1">
            {/* <CustomButton
              text={"Буцах"}
              leftIcon={<IconArrowLeft size="1rem" />}
              onClick={() => {
                // navigator.back
                router.back();
              }}
            /> */}

            {!mobile && (
              <ActionIcon
                variant="default"
                onClick={() => {
                  router.back();
                }}
              >
                <IconArrowLeft size="1rem" />
              </ActionIcon>
            )}

            <Text weight={"bold"}>
              <span className="text-sm lg:text-base truncate">
                {dataQuery?.data?.data?.name || "-"}
              </span>
            </Text>
            {/* <div className="lg:hidden block">
              <ActionIcon variant="default">
                <IconDotsVertical size="1rem" />
              </ActionIcon>
            </div> */}
          </Flex>
          <div className="block">
            {!props?.isPreview && !props?.hideTools && (
              <Flex align={"center"} gap={24}>
                {type === "epub" && (
                  <Tooltip label="Тохиргоо">
                    <ActionIcon
                      onClick={() => {
                        setShowSettings(true);
                      }}
                    >
                      <IconSettings />
                    </ActionIcon>
                  </Tooltip>
                )}

                {/* <TextInput
                radius={"md"}
                placeholder="Агуулгаар хайх"
                rightSection={<IconSearch size={20} color="#6E7191" />}
                value={search}
                onChange={(e) => setSearch(e.target.value)}
                onKeyDown={handleEnter}
                onClick={() => {
                  setSearchOpened(true);
                }}
                // ref={searchRef}
              /> */}
                {type !== "epub" && (
                  <Flex
                    align={"center"}
                    gap="xs"
                    sx={{ border: "solid 1px #0000002a", borderRadius: 4 }}
                    p="5px"
                  >
                    <Select
                      size="xs"
                      placeholder="Сонгох"
                      w={90}
                      value={zoom + ""}
                      styles={{
                        root: {
                          border: "none",
                        },
                        input: {
                          border: "none",
                        },
                      }}
                      data={[
                        // { value: "16", label: "1600%" },
                        // { value: "8", label: "800%" },
                        // { value: "4", label: "400%" },
                        { value: "2", label: "200%" },
                        { value: "1.5", label: "150%" },
                        { value: "1.25", label: "125%" },
                        { value: "1.0", label: "100%" },
                        { value: "0.5", label: "50%" },
                        { value: "0.25", label: "25%" },
                        { value: "0.1", label: "10%" },
                        { value: "0.9", label: "Дэлгэц" },
                      ].reverse()}
                      onChange={(e) => {
                        setZoom(e);
                      }}
                    />
                    <Box h={20} w={1} bg={"#0000002a"} />
                    <Tooltip label="Томруулах">
                      <ActionIcon
                        onClick={zoomIn}
                        size={"sm"}
                        sx={
                          zoom === "2"
                            ? {
                                color: "#868e965a",
                              }
                            : {}
                        }
                      >
                        <IconZoomIn />
                      </ActionIcon>
                    </Tooltip>
                    <Box h={20} w={1} bg={"#0000002a"} />
                    <Tooltip label="Жижигрүүлэх">
                      <ActionIcon
                        onClick={zoomOut}
                        size={"sm"}
                        sx={
                          zoom === "0.1"
                            ? {
                                color: "#868e965a",
                              }
                            : {}
                        }
                      >
                        <IconZoomOut />
                      </ActionIcon>
                    </Tooltip>
                  </Flex>
                )}

                <Tooltip label="Бүтэн дэлгэцээр харах">
                  <ActionIcon
                    onClick={() => {
                      setFull(!full);
                    }}
                  >
                    <IconArrowsDiagonal />
                  </ActionIcon>
                </Tooltip>

                {/* <Tooltip label="Өөрчлөлтүүдийн устгах">
                <ActionIcon>
                  <IconRefresh />
                </ActionIcon>
              </Tooltip> */}

                <Tooltip label="Хайлт/Судлаачийн булан">
                  <ActionIcon
                    onClick={() => {
                      setShowSidebar(!showSidebar);
                    }}
                  >
                    <IconLayoutGrid />
                  </ActionIcon>
                </Tooltip>
              </Flex>
            )}
          </div>
        </Flex>
      </Container>
      {searchOpened && (
        <Box ref={searchRef} pos={"relative"} sx={{ zIndex: 99 }}>
          <AdvancedSearch search={search} searchOpened={searchOpened} />
        </Box>
      )}
    </Box>
  );
};

export default Header;
