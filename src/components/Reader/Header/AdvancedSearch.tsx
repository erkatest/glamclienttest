import {
  Loader,
  Grid,
  TextInput,
  Select,
  Flex,
  Container,
  Box,
} from "@mantine/core";
import { useDebouncedValue } from "@mantine/hooks";

import Link from "next/link";
import React, { useRef, useState } from "react";
import { TbArrowsExchange2 } from "react-icons/tb";
import { CiGrid41 } from "react-icons/ci";
import { useIntl } from "react-intl";
import { useQuery } from "react-query";
import apiContent from "~/api/content";

const AdvancedSearch = (props: any) => {
  const { search } = props;

  const intl = useIntl();

  const [searchDebounced] = useDebouncedValue(search, 200);

  // const handleLinkMouseDown = () => {
  //   setSearchResultsClick(true);
  // };

  const { data, isLoading } = useQuery(
    ["search", searchDebounced],
    () => searchDebounced && apiContent.HomeSearchFind(searchDebounced)
  );

  return (
    <Box>
      <Container pos={"relative"}>
        {/* {searchOpened && ( */}
        <Box
          className="absolute top-[0px] rounded-[10px] lg:w-full bg-white min-h-[400px] z-10 shadow-xl left-0 space-y-4 max-h-80 overflow-auto "
          p={"md"}
        >
          <Box>
            <Grid>
              <Grid.Col span={6}>
                <TextInput
                  radius={"md"}
                  placeholder="Нийгмийн даатгал"
                  icon={<CiGrid41 size={16} color="#6E7191" />}
                />
              </Grid.Col>
              <Grid.Col span={6}>
                <Select
                  defaultValue="Тэмдэглэл"
                  data={[
                    { value: "Тэмдэглэл", label: "Тэмдэглэл" },
                    // { value: "ng", label: "Angular" },
                    // { value: "svelte", label: "Svelte" },
                    // { value: "vue", label: "Vue" },
                  ]}
                  icon={<TbArrowsExchange2 size={16} color="#6E7191" />}
                />
              </Grid.Col>
            </Grid>
          </Box>
          {isLoading ? (
            <div className="h-full flex flex-col justify-center items-center">
              <Loader />
            </div>
          ) : data?.data?.length !== 0 && search ? (
            <Box>
              {data?.data?.map((result: any) => (
                // <Link href={`/p/${result.slug}`} key={result.id}>
                <Flex
                  // onMouseDown={handleLinkMouseDown}\
                  key={result.id}
                  justify={"center"}
                  align={"center"}
                  className="p-3 rounded transition duration-200 hover:bg-gray-100"
                >
                  <Box className="mr-4">
                    <img
                      src={
                        result.cover_image_url || "/images/contentDefault.png"
                      }
                      alt={result.title}
                      width={70}
                    />
                  </Box>
                  <Box className="flex-1">
                    <Box className="line-clamp-1 font-lora text-black text-[16px] lg:text-[20px] font-[600]">
                      {result.title}
                    </Box>
                    <Box>
                      {result?.authors?.length !== 0 && (
                        <Flex className="text-[13px]  font-lora leading-[23px]">
                          <Box>
                            {" "}
                            {result?.authors?.length > 1
                              ? "Зохиогчид:"
                              : "Зохиогч:"}
                          </Box>
                          {result?.authors?.map((item: any, index: number) => (
                            <Link
                              href={`/a/${item?.author_id}`}
                              key={`author-${index}`}
                              style={{ textDecoration: "none" }}
                            >
                              <Box
                                mx={5}
                                className=" hover:underline hover:text-[#3EAEFF]"
                              >
                                {item?.author_display_name}
                              </Box>
                            </Link>
                          ))}
                        </Flex>
                      )}
                    </Box>
                    <Box
                      className="line-clamp-2 font-lora text-[9px] lg:text-[13px] leading-[22px] tracking-[0.25px] text-[#8B8B8B]"
                      dangerouslySetInnerHTML={{ __html: result?.summary }}
                    />
                  </Box>
                </Flex>
                // </Link>
              ))}
            </Box>
          ) : (
            <div className="flex items-center justify-center py-8 w-full bg-[#F9FBFC]">
              <img
                src={data?.data.mainImg || "/images/empty.svg"}
                className="h-96"
              />
            </div>
          )}
        </Box>
        {/* )} */}
      </Container>
    </Box>
  );
};

export default AdvancedSearch;
