// pages/api/checkLink.js

export default async function handler(req: any, res: any) {
  const baseUrl =
    "https://docs.google.com/forms/d/e/1FAIpQLSfNRY-YbNES-6bsO7bNiTELwECRrU6zFAQkRgS86";
  const endUrl = "dA1rZYbw/viewform";
  const validLinks = [];

  // Iterate through letters A to Z
  for (
    let letter = "A";
    letter <= "Z";
    letter = String.fromCharCode(letter.charCodeAt(0) + 1)
  ) {
    const candidateLink = baseUrl + letter + endUrl;

    try {
      // Send an HTTP GET request to the candidate link
      const response = await fetch(candidateLink);

      // Check if the response status is 200 OK
      if (response.ok) {
        validLinks.push(candidateLink);
      }
    } catch (error) {
      // Ignore errors for invalid links
    }
  }

  for (
    let letter = "a";
    letter <= "z";
    letter = String.fromCharCode(letter.charCodeAt(0) + 1)
  ) {
    const candidateLink = baseUrl + letter + endUrl;

    try {
      // Send an HTTP GET request to the candidate link
      const response = await fetch(candidateLink);

      // Check if the response status is 200 OK
      if (response.ok) {
        validLinks.push(candidateLink);
      }
    } catch (error) {
      // Ignore errors for invalid links
    }
  }

  res.status(200).json({ validLinks });
}
