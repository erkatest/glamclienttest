import { useEffect, useState } from "react";

export default function YourPage() {
  const [validLinks, setValidLinks] = useState([]);

  useEffect(() => {
    async function fetchData() {
      const response = await fetch("/api/checkLink");
      const data = await response.json();
      setValidLinks(data.validLinks);
    }

    fetchData();
  }, []);

  return (
    <div>
      <h1>Valid Links</h1>
      <ul>
        {validLinks.map((link) => (
          <li key={link}>{link}</li>
        ))}
      </ul>
    </div>
  );
}
