import {
  Box,
  Container,
  ActionIcon,
  Flex,
  Text,
  Button,
  NumberInput,
  Tooltip,
} from "@mantine/core";

import React, { use, useContext, useEffect, useState } from "react";
import CustomButton from "./CustomButton";
import { useQuery } from "react-query";

import { MdOutlineFormatListBulleted } from "react-icons/md";
import { FaAngleLeft, FaAngleRight } from "react-icons/fa";
import ReaderContext from "~/context/Reader";
import apiContent from "~/api/content";

const Footer = (props: { id: any; type: any; token: any }) => {
  const {
    nextPage,
    prevPage,
    page,
    setTotalPage,
    totalPage,
    setPreviewPages,
    previewPages,
    redirectPage,
    handleEpubNext,
    handleEpubPrev,
    epubFirst,
    epubLast,
    loading,
    epub,
    handleEpubRedirect,
    setShowToc,
  } = useContext(ReaderContext);

  const { id, type } = props;
  const dataQuery = useQuery(
    ["contents", id],
    () => id && apiContent.findOne(id, "koha")
  );

  const isEpub = type === "epub";

  return (
    <>
      <Box
        bg={"white"}
        sx={{
          borderTop: "1px solid #D9DBE9",
          position: "fixed",
          bottom: 0,
          right: 0,
          left: 0,
          zIndex: 50,
          display: "flex",
          alignItems: "center",
        }}
        p="lg"
        h={"100px"}
      >
        <Container size={"xl"} className="relative" w={"100%"}>
          <div className="absolute hidden lg:block left-0 bottom-0 top-0 ">
            <Tooltip label="Гарчиг">
              <ActionIcon size="lg" onClick={() => setShowToc(true)}>
                <MdOutlineFormatListBulleted />
              </ActionIcon>
            </Tooltip>
          </div>

          <div className="flex justify-between lg:justify-center">
            <div className="hidden lg:inline-flex items-center space-x-4">
              <Button
                leftIcon={<FaAngleLeft size="1.2rem" />}
                variant="outline"
                disabled={isEpub ? (loading ? true : epubFirst) : page <= 1}
                onClick={() => {
                  if (isEpub) {
                    handleEpubPrev();
                  } else prevPage();
                }}
              >
                <span>Өмнөх</span>
              </Button>

              {/* {item?.type === "pdf" && (
         
                  <Flex direction="column" justify={"center"} align={"center"}>
                    {props?.isPreview ? (
                      <Text weight={700} color="#6E7191" size={13}>
                        {(props?.isPreview ? previewPages[page - 1] : page) > 0
                          ? props?.isPreview
                            ? previewPages[page - 1]
                            : page
                          : 1}
                      </Text>
                    ) : ( */}
              {type === "pdf" && (
                <Box>
                  <Flex direction="column" justify={"center"} align={"center"}>
                    <NumberInput
                      placeholder="Хуудас"
                      value={page}
                      onChange={(value) => {
                        redirectPage(value || 1);
                      }}
                      min={1}
                      max={totalPage || 1}
                      w={"100px"}
                      styles={{
                        input: {
                          textAlign: "center",
                        },
                      }}
                    />
                    {/* )} */}
                    <Text
                      color="#6E7191"
                      size={13}
                      sx={{ whiteSpace: "nowrap" }}
                    >
                      Нийт хуудас:{" "}
                      <Text span weight={700} id="total-page">
                        {totalPage || "-"}
                      </Text>
                    </Text>
                  </Flex>
                </Box>
              )}
              {/* )} */}

              {isEpub && (
                <Box>
                  <Flex direction="column" justify={"center"} align={"center"}>
                    <NumberInput
                      placeholder="Хуудас"
                      value={isEpub ? epub?.start?.displayed.page : 1}
                      onChange={(value) => {
                        // redirectPage(value || 1);
                        handleEpubRedirect(value);
                      }}
                      min={1}
                      max={totalPage || 1}
                      w={"100px"}
                      styles={{
                        input: {
                          textAlign: "center",
                        },
                      }}
                    />
                    <Text
                      color="#6E7191"
                      size={13}
                      sx={{ whiteSpace: "nowrap" }}
                    >
                      Нийт хуудас:{" "}
                      <Text span weight={700} id="total-page">
                        {totalPage || "-"}
                      </Text>
                    </Text>
                    {/* <Text
                      color="#6E7191"
                      size={13}
                      sx={{ whiteSpace: "nowrap" }}
                    >
                      Нийт хуудас:{" "}
                      <Text span weight={700} id="total-page">
                        {totalPage || 0 || "-"}
                      </Text>
                    </Text> */}
                  </Flex>
                </Box>
              )}

              <Button
                rightIcon={<FaAngleRight size="1.2rem" />}
                variant="outline"
                disabled={
                  isEpub ? (loading ? true : epubLast) : page === totalPage
                }
                onClick={() => {
                  if (isEpub) {
                    handleEpubNext();
                  } else {
                    nextPage();
                  }
                }}
              >
                <span>Дараах</span>
              </Button>

              {/* <CustomButton
              text={"Дараах"}
              rightIcon={<IconChevronRight size="1.2rem" />}
            /> */}
            </div>
            <div
              className={`flex w-full lg:hidden items-center ${
                epub ? "justify-between" : "justify-center"
              }  space-x-4`}
            >
              {epub && (
                <div>
                  <Tooltip label="Гарчиг">
                    <ActionIcon size="lg" onClick={() => setShowToc(true)}>
                      <MdOutlineFormatListBulleted />
                    </ActionIcon>
                  </Tooltip>
                </div>
              )}

              <div className="flex space-x-2">
                <Button
                  variant="outline"
                  disabled={isEpub ? (loading ? true : epubFirst) : page <= 1}
                  onClick={() => {
                    if (isEpub) {
                      handleEpubPrev();
                    } else prevPage();
                  }}
                >
                  <FaAngleLeft size="1.2rem" />
                </Button>
                {/* {true && ( */}
                <Box>
                  <Flex direction="column" justify={"center"} align={"center"}>
                    {/* {props?.isPreview ? (
                        <Text weight={700} color="#6E7191" size={13}>
                          {(props?.isPreview ? previewPages[page - 1] : page) >
                          0
                            ? props?.isPreview
                              ? previewPages[page - 1]
                              : page
                            : 1}
                        </Text>
                      ) : ( */}
                    <NumberInput
                      placeholder="Хуудас"
                      value={isEpub ? epub?.start?.displayed.page : page}
                      onChange={(value) => {
                        isEpub
                          ? handleEpubRedirect(value)
                          : redirectPage(value || 1);
                      }}
                      min={1}
                      max={totalPage || 1}
                      w={"100px"}
                      styles={{
                        input: {
                          textAlign: "center",
                        },
                      }}
                    />
                    {/* )} */}
                    <Text
                      color="#6E7191"
                      size={13}
                      sx={{ whiteSpace: "nowrap" }}
                    >
                      Нийт хуудас:{" "}
                      <Text span weight={700} id="total-page">
                        {totalPage || "-"}
                      </Text>
                    </Text>
                  </Flex>
                </Box>
                {/* )} */}
                <Button
                  variant="outline"
                  disabled={
                    isEpub ? (loading ? true : epubLast) : page === totalPage
                  }
                  onClick={() => {
                    if (isEpub) {
                      handleEpubNext();
                    } else {
                      nextPage();
                    }
                  }}
                >
                  <FaAngleRight size="1.2rem" />
                </Button>
              </div>

              {/* {isEpub && (
                <Box>
                  <Flex direction="column" justify={"center"} align={"center"}>
                    {true ? (
                      <Text weight={700} color="#6E7191" size={13}>
                        {epub?.start?.displayed.page || "-"}
                        /{" "} {totalPage || 0 || "-"}
                      </Text>
                    ) : (
                      <NumberInput
                        placeholder="Хуудас"
                        value={epub?.start?.displayed.page || 1}
                        onChange={(value) => {
                          redirectPage(value || 1);
                          handleEpubRedirect(value);
                        }}
                        min={1}
                        max={totalPage || 1}
                        w={"100px"}
                        styles={{
                          input: {
                            textAlign: "center",
                          },
                        }}
                      />
                    )}
                  </Flex>
                </Box>
              )} */}

              {/* <CustomButton
              text={"Дараах"}
              rightIcon={<IconChevronRight size="1.2rem" />}
            /> */}
            </div>
          </div>
          {/*
        <Grid align={"center"} justify={"space-between"} h={70}>
          {props?.isPreview && (
            <Grid.Col span={6}>
              <div className="flex items-center space-x-2 h-[55px] text-gray-500">
                <IconInfoCircle />
                <span className="italic">Жишээ хуудас</span>
              </div>
            </Grid.Col>
          )}

          {!props?.isPreview && (
            <Grid.Col span={6}>
              <Flex align={"center"} justify={"center"} gap={"4rem"}>
                <CustomButton
                  text={"Өмнөх"}
                  leftIcon={<IconChevronLeft size="1.2rem" />}
                />

                <Box>
                  <Flex direction="column" justify={"center"} align={"center"}>
                    <Text weight={700} color="#6E7191" size={13}>
                      17 - 18
                    </Text>
                    <Text color="#6E7191" size={13}>
                      Нийт хуудас{" "}
                      <Text span weight={700}>
                        204
                      </Text>
                    </Text>
                  </Flex>
                </Box>

                <CustomButton
                  text={"Дараах"}
                  rightIcon={<IconChevronRight size="1.2rem" />}
                />
              </Flex>
            </Grid.Col>
          )}
          <Grid.Col span={props?.isPreview ? 6 : 12}>
            {!props?.isPreview && (
              <Flex align={"center"} justify={"flex-end"}>
                <Menu position="top-end" shadow="md" width={350}>
                  <Menu.Target>
                    <ActionIcon>
                      <IconList />
                    </ActionIcon>
                  </Menu.Target>

                  <Menu.Dropdown>
                    <Menu.Label>
                      <Text weight={700} size={16} color="black">
                        Гарчиг
                      </Text>
                    </Menu.Label>
                    <Menu.Item>
                      Хүмүүстэй харилцахдаа өөртөө итгэлтэй байх арга
                    </Menu.Item>
                    <Menu.Item>
                      Хүмүүстэй харилцахдаа өөртөө итгэлтэй байх арга
                    </Menu.Item>
                    <Menu.Item>
                      Хүмүүстэй харилцахдаа өөртөө итгэлтэй байх арга
                    </Menu.Item>
                  </Menu.Dropdown>
                </Menu>
              </Flex>
            )}
          </Grid.Col>
        </Grid>
        */}
        </Container>
      </Box>
    </>
  );
};

export default Footer;
