import * as React from "react";
import { Formik, Form, Field, ErrorMessage } from "formik";
import * as Yup from "yup";
import Link from "next/link";
import useMessage from "~/hooks/useMessage";
import { useUser } from "~/context/auth";
import LoadingOverly from "~/components/libs/LoadingOverly";

const SignInContainer = (props: { setOpen: any; onClose: any }) => {
  const f = useMessage();
  const { signin } = useUser();
  const [loading, setLoading] = React.useState(false);
  const [show, setShow] = React.useState(false);

  const { setOpen, onClose } = props;
  const handleLogin = async (values: any) => {
    setLoading(true);
    await signin(values?.email.trim() || "", values?.password || "");
    setLoading(false);
    onClose();
  };
  return (
    <div className="z-50 h-fit font-lora">
      <LoadingOverly visible={loading} />
      <h3 className="font-sans text-[32px] 2xl:text-[38px] mb-[20px] lg:mb-[24px] font-bold text-c4">
        {f({ id: "signin-title" })}
      </h3>
      <p className="text-c7 text-left mb-5 2xl:mb-7 text-[16px] 2xl:text-[18px] dark:text-white">
        {f({ id: "signin-signup-desc" })}
      </p>

      <Formik
        initialValues={{ email: "", password: "" }}
        validationSchema={Yup.object({
          email: Yup.string()
            .email(f({ id: "form-email-error" }))
            .required(
              `${f({ id: "form-email" })} ${f({ id: "form-required" })}`
            ),
          password: Yup.string().required(
            `${f({ id: "form-password" })} ${f({
              id: "form-required",
            })}`
          ),
        })}
        onSubmit={(values, { setSubmitting }) => {
          handleLogin(values);
          setSubmitting(false);
          // router.push({
          //   pathname: `/profile/`,
          // });
        }}
      >
        {({ isSubmitting }) => (
          <Form autoComplete="off">
            <div className="relative z-0">
              <Field
                type="email"
                name="email"
                id="email"
                className="block py-[10px] px-[8px] 2xl:py-[14px] 2xl:px-[10px] w-full text-[16px] 2xl:text-[18px] text-gray-900 bg-transparent border-0 border-b-[0.5px] border-c12 appearance-none dark:text-white dark:border-gray-600 dark:focus:border-c5 focus:outline-none focus:ring-0 focus:border-c5 peer"
                placeholder=" "
              />
              <label
                htmlFor="email"
                className="absolute text:[16px] 2xl:text-[18px] text-c7 dark:text-gray-400 duration-300 transform -translate-y-6 scale-75 top-3 -z-10 origin-[0] peer-focus:left-0 peer-focus:text-c5 peer-focus:dark:text-c5 peer-placeholder-shown:scale-100 peer-placeholder-shown:translate-y-0 peer-focus:scale-75 peer-focus:px-[12px] peer-focus:-translate-y-6 px-[12px]"
              >
                {f({ id: "form-email" })}
              </label>
            </div>

            <div className="h-[20px] mb-5">
              <ErrorMessage
                name="email"
                component="div"
                className="text-red-500 text-xs font-medium"
              />
            </div>

            <div className="relative z-0">
              <Field
                type={show ? "text" : "password"}
                name="password"
                id="password"
                className="block py-[10px] px-[8px] 2xl:py-[14px] 2xl:px-[10px] w-full text-[16px] 2xl:text-[18px] text-gray-900 bg-transparent border-0 border-b-[0.5px] border-c12 appearance-none dark:text-white dark:border-gray-600 dark:focus:border-c5 focus:outline-none focus:ring-0 focus:border-c5 peer"
                placeholder=" "
              />

              <label
                htmlFor="password"
                className="absolute text:[16px] 2xl:text-[18px] text-c7 dark:text-gray-400 duration-300 transform -translate-y-6 scale-75 top-3 -z-10 origin-[0] peer-focus:left-0 peer-focus:text-c5 peer-focus:dark:text-c5 peer-placeholder-shown:scale-100 peer-placeholder-shown:translate-y-0 peer-focus:scale-75 peer-focus:px-[12px] peer-focus:-translate-y-6 px-[12px]"
              >
                {f({ id: "form-password" })}
              </label>
              <button
                className="absolute inset-y-0 right-0 flex items-center px-4 text-gray-600"
                onClick={() => {
                  show ? setShow(false) : setShow(true);
                }}
                type="button"
              >
                {show ? (
                  <svg
                    xmlns="http://www.w3.org/2000/svg"
                    fill="none"
                    viewBox="0 0 24 24"
                    strokeWidth={1.5}
                    stroke="currentColor"
                    className="w-5 h-5"
                  >
                    <path
                      strokeLinecap="round"
                      strokeLinejoin="round"
                      d="M2.036 12.322a1.012 1.012 0 010-.639C3.423 7.51 7.36 4.5 12 4.5c4.638 0 8.573 3.007 9.963 7.178.07.207.07.431 0 .639C20.577 16.49 16.64 19.5 12 19.5c-4.638 0-8.573-3.007-9.963-7.178z"
                    />
                    <path
                      strokeLinecap="round"
                      strokeLinejoin="round"
                      d="M15 12a3 3 0 11-6 0 3 3 0 016 0z"
                    />
                  </svg>
                ) : (
                  <svg
                    xmlns="http://www.w3.org/2000/svg"
                    fill="none"
                    viewBox="0 0 24 24"
                    strokeWidth={1.5}
                    stroke="currentColor"
                    className="w-5 h-5"
                  >
                    <path
                      strokeLinecap="round"
                      strokeLinejoin="round"
                      d="M3.98 8.223A10.477 10.477 0 001.934 12C3.226 16.338 7.244 19.5 12 19.5c.993 0 1.953-.138 2.863-.395M6.228 6.228A10.45 10.45 0 0112 4.5c4.756 0 8.773 3.162 10.065 7.498a10.523 10.523 0 01-4.293 5.774M6.228 6.228L3 3m3.228 3.228l3.65 3.65m7.894 7.894L21 21m-3.228-3.228l-3.65-3.65m0 0a3 3 0 10-4.243-4.243m4.242 4.242L9.88 9.88"
                    />
                  </svg>
                )}
              </button>
            </div>

            <div className="h-[20px] mb-2">
              <ErrorMessage
                name="password"
                component="div"
                className="text-red-500 text-xs font-medium"
              />
            </div>
            <div className="flex justify-end">
              <button
                type="button"
                onClick={() => setOpen("forget")}
                className="text-c13 text-[14px] mb-6 block font-medium underline dark:text-white font-sans"
              >
                {f({ id: "signin-forget" })}
              </button>
            </div>
            <button
              type="submit"
              disabled={isSubmitting}
              className="flex items-center justify-center w-full bg-c13 active:bg-c5 text-[16px] 2xl:text-[18px] text-white p-2.5 rounded-full hover:bg-c15 font-sans"
            >
              {f({ id: "signin-btn" })}
            </button>
            <div className="inline-flex items-center justify-center w-full">
              <hr className="w-24 h-px my-8 bg-c7 border-0" />
              <span className="absolute px-3 font-medium -translate-x-1/2 bg-white left-1/2 dark:text-white dark:bg-gray-900 text-center text-[16px] my-6 text-c7">
                {f({ id: "form-or" })}
              </span>
            </div>
            <button
              type="button"
              onClick={() => setOpen("signup")}
              className="text-c13 underline my-5 block font-medium text-center dark:text-white w-full mt-0 text-[16px] 2xl:text-[18px] font-sans"
            >
              {f({ id: "signin-signup" })}
            </button>
          </Form>
        )}
      </Formik>
    </div>
  );
};

export default SignInContainer;
