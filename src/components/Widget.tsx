import { useTheme } from "next-themes";
import Link from "next/link";
import * as React from "react";
import { HiOutlineGlobe } from "react-icons/hi";
interface IProps {}

const Widget = () => {
  const [zoom, setZoom] = React.useState(1);
  const { theme, setTheme } = useTheme();

  const handleZoom = () => {
    setZoom((z) => (z + 0.1 > 1.5 ? 1.5 : z + 0.1));
  };
  const handleZoomOut = () => {
    setZoom((z) => (z - 0.1 <= 1 ? 1 : z - 0.1));
  };

  React.useEffect(() => {
    if (typeof document !== "undefined") {
      (document.body.style as any).zoom = zoom;
    }
  }, [zoom]);
  return (
    <section className="fixed hidden shadow-lg z-50 top-1/2 p-[10px] space-y-6 rounded-tl-lg rounded-bl-lg transform -translate-y-1/2 right-0 bg-c1 lg:flex flex-col items-center justify-center text-white dark:text-[#BE8100]">
      <div className="flex flex-col justify-center items-center space-y-2">
        <span
          onClick={handleZoom}
          className="select-none cursor-pointer hover:opacity-80 uppercase font-bold text-xl"
        >
          A +
        </span>
        <span className="w-full h-[2px] bg-white"></span>
        <span
          onClick={handleZoomOut}
          className="select-none cursor-pointer hover:opacity-80 uppercase font-bold text-xl"
        >
          A -
        </span>
      </div>
      <div className="flex flex-col justify-center items-center space-y-2">
        <Link href={""} locale={"mn"}>
          <span className="select-none cursor-pointer hover:opacity-80 uppercase font-bold">
            МN
          </span>
        </Link>
        <div className="border-b-2 w-full"></div>
        <Link href={""} locale={""}>
          <span className="select-none cursor-pointer hover:opacity-80 uppercase font-bold">
            Мон
          </span>
        </Link>
        <div className="border-b-2 w-full"></div>
        <Link href={""} locale={"en"}>
          <span className="select-none cursor-pointer hover:opacity-80 uppercase font-bold">
            En
          </span>
        </Link>
      </div>
      <img
        src="/images/blind-color.svg"
        className="w-5/6 "
        onClick={() => {
          theme === "light" ? setTheme("dark") : setTheme("light");
        }}
      />
    </section>
  );
};

export default Widget;
