import { Dialog, Transition } from "@headlessui/react";
import React, { Fragment } from "react";
import useMessage from "~/hooks/useMessage";
import LoadingOverly from "../libs/LoadingOverly";

const CustomModal = (props: {
  isOpen: boolean;
  onClose: any;
  onChange: any;
  title: string;
  text: string;
}) => {
  const { isOpen, onClose, onChange, title, text } = props;
  const f = useMessage();

  return (
    <Transition appear show={isOpen} as={Fragment}>
      <Dialog as="div" className="relative z-[9999999]" onClose={onClose}>
        <Transition.Child
          as={Fragment}
          enter="ease-out duration-300"
          enterFrom="opacity-200"
          enterTo="opacity-200"
          leave="ease-in duration-200"
          leaveFrom="opacity-100"
          leaveTo="opacity-0"
        >
          <div className="fixed inset-0 bg-black/50 backdrop-blur-sm" />
        </Transition.Child>

        <div className="fixed inset-0 overflow-y-auto font-lora">
          <div className="flex  min-h-full items-center justify-center p-4 text-center">
            <Transition.Child
              as={Fragment}
              enter="ease-out duration-300"
              enterFrom="opacity-0 scale-95"
              enterTo="opacity-100 scale-100"
              leave="ease-in duration-200"
              leaveFrom="opacity-100 scale-100"
              leaveTo="opacity-0 scale-95"
            >
              <Dialog.Panel className="w-full max-w-md transform overflow-hidden rounded-md bg-white p-6 text-left align-middle shadow-xl transition-all">
                <Dialog.Title
                  as="h3"
                  className="text-xl font-semibold leading-6 text-[#051332] flex items-center justify-between"
                >
                  {title}
                  <img
                    src="/images/x-icon.svg"
                    className="w-5 cursor-pointer"
                    onClick={onClose}
                  />
                </Dialog.Title>
                <p
                  className="text-black my-8 text-justify text-md"
                  dangerouslySetInnerHTML={{ __html: text }}
                >
                  {/* {text} */}
                </p>

                <div className="flex mt-4 w-full">
                  <button
                    type="button"
                    className="w-1/2 text-c1 py-2.5 px-5 font-lora"
                    onClick={onClose}
                  >
                    {f({ id: "modal-close" })}
                  </button>
                  <button
                    type="button"
                    className="relative w-1/2 rounded-full text-white bg-c1 py-2.5 px-5 font-lora"
                    onClick={async () => {
                      onChange.mutateAsync();
                    }}
                  >
                    <LoadingOverly visible={onChange?.isLoading} />
                    {f({ id: "modal-delete" })}
                  </button>
                </div>
              </Dialog.Panel>
            </Transition.Child>
          </div>
        </div>
      </Dialog>
    </Transition>
  );
};

export default CustomModal;
