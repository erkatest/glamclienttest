import { Dialog, Transition } from "@headlessui/react";
import React, { Fragment } from "react";
import { useMutation, useQuery } from "react-query";
import apiFolder from "~/api/folder";
import LoadingOverly from "../libs/LoadingOverly";
import { toast } from "react-toastify";
import useMessage from "~/hooks/useMessage";
import CreateFolderModal from "./CreateFolderModal";
import { useUser } from "~/context/auth";
import { CustomLogin } from "../libs";
import FolderIcon from "../../../public/images/folders";
import DotSvg from "../../../public/images/dot";

const AddItemModal = (props: { isOpen: boolean; onClose: any; item: any }) => {
  const { isOpen, onClose, item } = props;
  const f = useMessage();
  const { user, token } = useUser();
  const [color, setColor] = React.useState<any>([]);
  const [create, setCreate] = React.useState(false);
  const [selectedFolders, setSelectedFolders] = React.useState<string[]>([]);

  const itemType = {
    artwork: "Урын сан",
    koha: "Номын сан",
    education: "Сан хөмрөг",
    heritage: "Соёлын өв",
  };
  const itemColor = {
    artwork: "#BE3455",
    koha: "#01417A",
    education: "#2E294E",
    heritage: "#13452F",
  };

  const handleFolder = (folderId: any) => {
    setSelectedFolders((prevSelectedFolders: any) => {
      const isString = String(folderId);
      if (prevSelectedFolders.includes(isString)) {
        return prevSelectedFolders.filter((id: any) => id !== isString);
      } else {
        return [...prevSelectedFolders, isString];
      }
    });
  };
  return (
    <Transition appear show={isOpen} as={Fragment}>
      <Dialog
        as="div"
        className="relative z-[9999999]"
        onClose={() => {
          onClose();
          setColor([]);
        }}
      >
        <Transition.Child
          as={Fragment}
          enter="ease-out duration-300"
          enterFrom="opacity-200"
          enterTo="opacity-200"
          leave="ease-in duration-200"
          leaveFrom="opacity-100"
          leaveTo="opacity-0"
        >
          <div className="fixed inset-0 bg-black/50 backdrop-blur-sm" />
        </Transition.Child>

        <div className="fixed inset-0 overflow-y-auto">
          <div className="flex min-h-full items-center justify-center p-6 text-center">
            <Transition.Child
              as={Fragment}
              enter="ease-out duration-300"
              enterFrom="opacity-0 scale-95"
              enterTo="opacity-100 scale-100"
              leave="ease-in duration-200"
              leaveFrom="opacity-100 scale-100"
              leaveTo="opacity-0 scale-95"
            >
              <Dialog.Panel className="w-[1000px] transform overflow-hidden rounded-md bg-white p-6 text-left align-middle shadow-xl transition-all">
                <Dialog.Title
                  as="h3"
                  className="text-xl mb-[36px] font-lora font-bold leading-6 text-[#051332] flex items-center justify-between"
                >
                  Хадгалах
                  <img
                    src="/images/x-icon.svg"
                    className="w-5 cursor-pointer"
                    onClick={() => {
                      onClose();
                      setColor([]);
                    }}
                  />
                </Dialog.Title>
                {user ? (
                  <>
                    <div className="flex">
                      <img
                        className="object-contain"
                        src={item?.mainImg}
                        onError={(e: any) => {
                          e.target.src = "/images/empty.svg";
                        }}
                        height={120}
                        width={120}
                      />
                      <div className="ml-[16px]">
                        <div className="mb-[10px] font font-[20px]">
                          <div className="flex items-center my-auto text-center font-bold gap-1 text-[14px]">
                            <DotSvg
                              className={``}
                              color={`${
                                itemColor[item?.type as keyof typeof itemColor]
                              }`}
                            />
                            {itemType[item?.type as keyof typeof itemType]}
                          </div>
                        </div>
                        <div className="font-bold text-[16px]">
                          {item?.name}
                        </div>
                        <div className="line-clamp-2 text[12px]  text-[#A6B2C3]">
                          {item?.description}
                        </div>
                      </div>
                    </div>
                    <div
                      color="#A6B2C3"
                      className="w-full border-2 my-[18px]"
                    />
                    <div className="flex mx-auto justify-between items-center">
                      <div className="font-sm font-semibold">Хавтаснууд</div>
                      <button
                        className="flex py-[8px] px-[14px] border-2 rounded-full border-[#A6B2C3] items-center gap-[16px] font-lora"
                        onClick={() => setCreate(true)}
                      >
                        <img src="/images/add-folder.svg" className="w-6" />
                        <p className="text-[#A6B2C3] text-sm">Хавтас нэмэх</p>
                      </button>
                    </div>
                    <div className="flex justify-end mt-4 w-full">
                      <button
                        type="button"
                        className=" text-c1 px-6 py-2.5 rounded-full border-2 mr-[20px] text-sm font-lora"
                        onClick={() => {
                          onClose();
                          setColor([]);
                        }}
                      >
                        {f({ id: "modal-close" })}
                      </button>
                    </div>
                  </>
                ) : (
                  <div className="flex items-center justify-center my-10">
                    <CustomLogin />
                  </div>
                )}
              </Dialog.Panel>
            </Transition.Child>
          </div>
        </div>
      </Dialog>
    </Transition>
  );
};

export default AddItemModal;
