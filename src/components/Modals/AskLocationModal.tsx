import { Dialog, Transition } from "@headlessui/react";
import React, { Fragment } from "react";
import useMessage from "~/hooks/useMessage";
import LoadingOverly from "../libs/LoadingOverly";
import { useRouter } from "next/router";

const LocationModal = (props: {
  isOpen: boolean;
  onClose: any;
  error: any;
  onDecline: any;
  onAccept: any;
  loading: any;
}) => {
  const { isOpen, onClose, onAccept, onDecline, error, loading } = props;
  const f = useMessage();
  const router = useRouter();
  return (
    <Transition appear show={isOpen} as={Fragment}>
      <Dialog as="div" className="relative z-[9999999]" onClose={onClose}>
        <Transition.Child
          as={Fragment}
          enter="ease-out duration-300"
          enterFrom="opacity-200"
          enterTo="opacity-200"
          leave="ease-in duration-200"
          leaveFrom="opacity-100"
          leaveTo="opacity-0"
        >
          <div className="fixed inset-0 bg-black/50 backdrop-blur-sm" />
        </Transition.Child>

        <div className="fixed inset-0 overflow-y-auto font-lora">
          <div className="flex  min-h-full items-center justify-center p-4 text-center">
            <Transition.Child
              as={Fragment}
              enter="ease-out duration-300"
              enterFrom="opacity-0 scale-95"
              enterTo="opacity-100 scale-100"
              leave="ease-in duration-200"
              leaveFrom="opacity-100 scale-100"
              leaveTo="opacity-0 scale-95"
            >
              <Dialog.Panel className="w-full max-w-md transform overflow-hidden rounded-md bg-white p-6 text-left align-middle shadow-xl transition-all">
                <Dialog.Title
                  as="h3"
                  className="text-xl font-semibold leading-6 text-[#051332] flex items-center justify-between"
                >
                  <div>
                    {router?.locale === "mn"
                      ? "Байршилд хандахыг зөвшөөрөх"
                      : "Allow Location Access"}{" "}
                  </div>
                  <img
                    src="/images/x-icon.svg"
                    className="w-5 cursor-pointer"
                    onClick={onClose}
                  />
                </Dialog.Title>
                <div className="mt-5">
                  {error ? (
                    error
                  ) : (
                    <div className="space-y-[20px]">
                      <div className="text-[16px]">
                        {router?.locale === "mn"
                          ? "Бид хувийн тохиргоог хийхийн тулд таны байршилд хандах шаардлагатай."
                          : "We need to access your location for personalized features."}{" "}
                      </div>
                      <div className="flex space-x-[10px] justify-end">
                        <button
                          onClick={onAccept}
                          className="text-[14px] bg-green-500 text-white px-4 py-2 rounded-md font-lora"
                        >
                          {loading ? (
                            <svg
                              aria-hidden="true"
                              className="w-4 h-4 text-gray-200 animate-spin dark:text-gray-600 mx-2 fill-blue-600"
                              viewBox="0 0 100 101"
                              fill="none"
                              xmlns="http://www.w3.org/2000/svg"
                            >
                              <path
                                d="M100 50.5908C100 78.2051 77.6142 100.591 50 100.591C22.3858 100.591 0 78.2051 0 50.5908C0 22.9766 22.3858 0.59082 50 0.59082C77.6142 0.59082 100 22.9766 100 50.5908ZM9.08144 50.5908C9.08144 73.1895 27.4013 91.5094 50 91.5094C72.5987 91.5094 90.9186 73.1895 90.9186 50.5908C90.9186 27.9921 72.5987 9.67226 50 9.67226C27.4013 9.67226 9.08144 27.9921 9.08144 50.5908Z"
                                fill="currentColor"
                              />
                              <path
                                d="M93.9676 39.0409C96.393 38.4038 97.8624 35.9116 97.0079 33.5539C95.2932 28.8227 92.871 24.3692 89.8167 20.348C85.8452 15.1192 80.8826 10.7238 75.2124 7.41289C69.5422 4.10194 63.2754 1.94025 56.7698 1.05124C51.7666 0.367541 46.6976 0.446843 41.7345 1.27873C39.2613 1.69328 37.813 4.19778 38.4501 6.62326C39.0873 9.04874 41.5694 10.4717 44.0505 10.1071C47.8511 9.54855 51.7191 9.52689 55.5402 10.0491C60.8642 10.7766 65.9928 12.5457 70.6331 15.2552C75.2735 17.9648 79.3347 21.5619 82.5849 25.841C84.9175 28.9121 86.7997 32.2913 88.1811 35.8758C89.083 38.2158 91.5421 39.6781 93.9676 39.0409Z"
                                fill="currentFill"
                              />
                            </svg>
                          ) : router?.locale === "mn" ? (
                            "Зөвшөөрөх"
                          ) : (
                            "Allow"
                          )}
                        </button>
                        <button
                          onClick={onDecline}
                          className="text-[14px] bg-red-600 text-white px-4 py-2 rounded-md font-lora"
                        >
                          {router?.locale === "mn" ? "Татгалзах" : "Decline"}{" "}
                        </button>
                      </div>
                    </div>
                  )}
                </div>
              </Dialog.Panel>
            </Transition.Child>
          </div>
        </div>
      </Dialog>
    </Transition>
  );
};

export default LocationModal;
