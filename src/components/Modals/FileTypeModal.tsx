import { Dialog, Transition } from "@headlessui/react";
import React, { Fragment } from "react";
import useMessage from "~/hooks/useMessage";
import LoadingOverly from "../libs/LoadingOverly";
import { useRouter } from "next/router";
import Link from "next/link";

const FileTypeModal = (props: {
  isOpen: boolean;
  onClose: any;
  data: any;
  token: any;
}) => {
  const { isOpen, onClose, data, token } = props;
  const f = useMessage();
  const router = useRouter();

  return (
    <Transition appear show={isOpen} as={Fragment}>
      <Dialog as="div" className="relative z-[9999999]" onClose={onClose}>
        <Transition.Child
          as={Fragment}
          enter="ease-out duration-300"
          enterFrom="opacity-200"
          enterTo="opacity-200"
          leave="ease-in duration-200"
          leaveFrom="opacity-100"
          leaveTo="opacity-0"
        >
          <div className="fixed inset-0 bg-black/50 backdrop-blur-sm" />
        </Transition.Child>

        <div className="fixed inset-0 overflow-y-auto font-lora">
          <div className="flex  min-h-full items-center justify-center p-4 text-center">
            <Transition.Child
              as={Fragment}
              enter="ease-out duration-300"
              enterFrom="opacity-0 scale-95"
              enterTo="opacity-100 scale-100"
              leave="ease-in duration-200"
              leaveFrom="opacity-100 scale-100"
              leaveTo="opacity-0 scale-95"
            >
              <Dialog.Panel className="w-full max-w-md transform overflow-hidden rounded-md bg-white p-6 text-left align-middle shadow-xl transition-all">
                <Dialog.Title
                  as="h3"
                  className="text-xl font-semibold leading-6 text-[#051332] flex items-center justify-between"
                >
                  <div>Файлын төрлөө сонгоно уу.</div>
                  <img
                    src="/images/x-icon.svg"
                    className="w-5 cursor-pointer"
                    onClick={onClose}
                  />
                </Dialog.Title>
                <div className="flex space-x-10 justify-center mt-5">
                  {data?.file_type?.length === 0 && (
                    <div>Уучлаарай тус бүтээлд файл байхгүй байна.</div>
                  )}
                  {data?.file_type?.find(
                    (el: any) => el?.book_file_type_id === 2
                  ) && (
                    <Link
                      href={`/reader?id=${router?.query?.id}&type=epub&t=${token}`}
                      className="px-[20px] py-[5px] w-28 border text-center hover:bg-[#1A3D7A] hover:text-white rounded-[5px]"
                    >
                      EPUB
                    </Link>
                  )}
                  {data?.file_type?.find(
                    (el: any) => el?.book_file_type_id === 1
                  ) && (
                    <Link
                      href={`/reader?id=${router?.query?.id}&type=pdf&t=${token}`}
                      className="px-[20px] py-[5px] border w-28 text-center hover:bg-[#1A3D7A] hover:text-white rounded-[5px]"
                    >
                      PDF
                    </Link>
                  )}
                </div>
              </Dialog.Panel>
            </Transition.Child>
          </div>
        </div>
      </Dialog>
    </Transition>
  );
};

export default FileTypeModal;
