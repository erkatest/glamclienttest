import { Dialog, Transition } from "@headlessui/react";
import React, { Fragment, useState } from "react";
import useMessage from "~/hooks/useMessage";
import apiUsers from "~/api/users";
import LoadingOverly from "../libs/LoadingOverly";
import * as Yup from "yup";
import { ErrorMessage, Field, Form, Formik } from "formik";
import { toast } from "react-toastify";

const ChangePasswordModal = (props: { isOpen: boolean; onClose: any }) => {
  const { isOpen, onClose } = props;
  const f = useMessage();
  const [loading, setLoading] = useState(false);
  const [showOldPassword, setShowOldPassword] = useState(false);
  const [showNewPassword, setShowNewPassword] = useState(false);
  const [showNewPasswordRepeat, setShowNewPasswordRepeat] = useState(false);

  const PasswordChangeSchema = Yup.object().shape({
    oldPassword: Yup.string().required("Бөглөх шаардлагатай."),
    newPassword: Yup.string()
      .min(8, "Нууц үг хамгийн багадаа 8 тэмдэгт агуулсан байх.")
      .matches(
        /^(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{8,}$/,
        "Нууц үг ядаж нэг том үсэг, ядаж нэг тоо, ядаж нэг тусгай тэмдэгт орсон байх ёстой."
      )
      .required("Бөглөх шаардлагатай."),
    newPasswordRepeat: Yup.string()
      .oneOf([Yup.ref("newPassword")], "Нууц үг таарахгүй байна.")
      .required("Бөглөх шаардлагатай."),
  });

  const handleSubmit = async (values: any) => {
    setLoading(true);
    delete values?.newPasswordRepeat;
    const res = await apiUsers.changePassword(values);
    if (res.success === true) {
      toast.success(f({ id: "success-add-item" }));
      setLoading(false);
      onClose();
    } else {
      toast.error(res.e);
      setLoading(false);
      onClose();
    }
  };

  return (
    <Transition appear show={isOpen} as={Fragment}>
      <Dialog as="div" className="relative z-[9999999]" onClose={onClose}>
        <Transition.Child
          as={Fragment}
          enter="ease-out duration-300"
          enterFrom="opacity-200"
          enterTo="opacity-200"
          leave="ease-in duration-200"
          leaveFrom="opacity-100"
          leaveTo="opacity-0"
        >
          <div className="fixed inset-0 bg-black/50 backdrop-blur-sm" />
        </Transition.Child>

        <div className="fixed inset-0 overflow-y-auto font-lora">
          <div className="flex  min-h-full items-center justify-center p-4 text-center">
            <Transition.Child
              as={Fragment}
              enter="ease-out duration-300"
              enterFrom="opacity-0 scale-95"
              enterTo="opacity-100 scale-100"
              leave="ease-in duration-200"
              leaveFrom="opacity-100 scale-100"
              leaveTo="opacity-0 scale-95"
            >
              <Dialog.Panel className="w-full max-w-md transform overflow-hidden rounded-md bg-white p-8 text-left align-middle shadow-xl transition-all">
                <Dialog.Title
                  as="h3"
                  className="text-xl font-semibold leading-6 text-[#051332] flex items-center justify-between"
                >
                  Нууц үг солих
                  <img
                    src="/images/x-icon.svg"
                    className="w-5 cursor-pointer"
                    onClick={onClose}
                  />
                </Dialog.Title>
                <p className=" text-[#A6B2C3] my-8 text-[14px]">
                  Соёлын Яамын GLAM систем нь Монгол Улсын өв соёлын дижитал
                  шилжилтийг идэвхжүүлэх зорилготой.
                </p>
                <Formik
                  initialValues={{
                    oldPassword: "",
                    newPassword: "",
                    newPasswordRepeat: "",
                  }}
                  validationSchema={PasswordChangeSchema}
                  onSubmit={handleSubmit}
                >
                  {({ errors, touched, isSubmitting }) => (
                    <Form className="space-y-[24px]" autoComplete="off">
                      <div className="space-y-[4px]">
                        <div className=" relative">
                          <Field
                            type={showOldPassword ? "text" : "password"}
                            name="oldPassword"
                            placeholder="Хуучин нууц үг"
                            className="border-b w-full px-[10px] py-[14px] focus:outline-none"
                          />

                          <button
                            className="absolute inset-y-0 right-0 flex items-center px-4 text-gray-600"
                            onClick={() => {
                              showOldPassword
                                ? setShowOldPassword(false)
                                : setShowOldPassword(true);
                            }}
                            type="button"
                          >
                            {showOldPassword ? (
                              <svg
                                xmlns="http://www.w3.org/2000/svg"
                                fill="none"
                                viewBox="0 0 24 24"
                                strokeWidth={1.5}
                                stroke="currentColor"
                                className="w-5 h-5"
                              >
                                <path
                                  strokeLinecap="round"
                                  strokeLinejoin="round"
                                  d="M2.036 12.322a1.012 1.012 0 010-.639C3.423 7.51 7.36 4.5 12 4.5c4.638 0 8.573 3.007 9.963 7.178.07.207.07.431 0 .639C20.577 16.49 16.64 19.5 12 19.5c-4.638 0-8.573-3.007-9.963-7.178z"
                                />
                                <path
                                  strokeLinecap="round"
                                  strokeLinejoin="round"
                                  d="M15 12a3 3 0 11-6 0 3 3 0 016 0z"
                                />
                              </svg>
                            ) : (
                              <svg
                                xmlns="http://www.w3.org/2000/svg"
                                fill="none"
                                viewBox="0 0 24 24"
                                strokeWidth={1.5}
                                stroke="currentColor"
                                className="w-5 h-5"
                              >
                                <path
                                  strokeLinecap="round"
                                  strokeLinejoin="round"
                                  d="M3.98 8.223A10.477 10.477 0 001.934 12C3.226 16.338 7.244 19.5 12 19.5c.993 0 1.953-.138 2.863-.395M6.228 6.228A10.45 10.45 0 0112 4.5c4.756 0 8.773 3.162 10.065 7.498a10.523 10.523 0 01-4.293 5.774M6.228 6.228L3 3m3.228 3.228l3.65 3.65m7.894 7.894L21 21m-3.228-3.228l-3.65-3.65m0 0a3 3 0 10-4.243-4.243m4.242 4.242L9.88 9.88"
                                />
                              </svg>
                            )}
                          </button>
                        </div>
                        <div
                          className={`text-red-500 text-[12px] ${
                            touched.oldPassword && errors.oldPassword
                              ? "opacity-100"
                              : "opacity-0"
                          }`}
                        >
                          {errors.oldPassword}
                        </div>
                      </div>
                      <div className="space-y-[4px]">
                        <div className=" relative">
                          <Field
                            type={showNewPassword ? "text" : "password"}
                            name="newPassword"
                            placeholder="Шинэ нууц үг"
                            className="border-b w-full px-[10px] py-[14px] focus:outline-none"
                          />
                          <button
                            className="absolute inset-y-0 right-0 flex items-center px-4 text-gray-600 font-lora"
                            onClick={() => {
                              showNewPassword
                                ? setShowNewPassword(false)
                                : setShowNewPassword(true);
                            }}
                            type="button"
                          >
                            {showNewPassword ? (
                              <svg
                                xmlns="http://www.w3.org/2000/svg"
                                fill="none"
                                viewBox="0 0 24 24"
                                strokeWidth={1.5}
                                stroke="currentColor"
                                className="w-5 h-5"
                              >
                                <path
                                  strokeLinecap="round"
                                  strokeLinejoin="round"
                                  d="M2.036 12.322a1.012 1.012 0 010-.639C3.423 7.51 7.36 4.5 12 4.5c4.638 0 8.573 3.007 9.963 7.178.07.207.07.431 0 .639C20.577 16.49 16.64 19.5 12 19.5c-4.638 0-8.573-3.007-9.963-7.178z"
                                />
                                <path
                                  strokeLinecap="round"
                                  strokeLinejoin="round"
                                  d="M15 12a3 3 0 11-6 0 3 3 0 016 0z"
                                />
                              </svg>
                            ) : (
                              <svg
                                xmlns="http://www.w3.org/2000/svg"
                                fill="none"
                                viewBox="0 0 24 24"
                                strokeWidth={1.5}
                                stroke="currentColor"
                                className="w-5 h-5"
                              >
                                <path
                                  strokeLinecap="round"
                                  strokeLinejoin="round"
                                  d="M3.98 8.223A10.477 10.477 0 001.934 12C3.226 16.338 7.244 19.5 12 19.5c.993 0 1.953-.138 2.863-.395M6.228 6.228A10.45 10.45 0 0112 4.5c4.756 0 8.773 3.162 10.065 7.498a10.523 10.523 0 01-4.293 5.774M6.228 6.228L3 3m3.228 3.228l3.65 3.65m7.894 7.894L21 21m-3.228-3.228l-3.65-3.65m0 0a3 3 0 10-4.243-4.243m4.242 4.242L9.88 9.88"
                                />
                              </svg>
                            )}
                          </button>
                        </div>
                        <div
                          className={`text-red-500 text-[12px] ${
                            touched.newPassword && errors.newPassword
                              ? "opacity-100"
                              : "opacity-0"
                          }`}
                        >
                          {errors.newPassword}
                        </div>
                      </div>
                      <div className="space-y-[4px]">
                        <div className="relative">
                          <Field
                            type={showNewPasswordRepeat ? "text" : "password"}
                            name="newPasswordRepeat"
                            placeholder="Шинэ нууц үг давтах"
                            className="border-b w-full px-[10px] py-[14px] focus:outline-none"
                          />
                          <button
                            className="absolute inset-y-0 right-0 flex items-center px-4 text-gray-600"
                            onClick={() => {
                              showNewPasswordRepeat
                                ? setShowNewPasswordRepeat(false)
                                : setShowNewPasswordRepeat(true);
                            }}
                            type="button"
                          >
                            {showNewPasswordRepeat ? (
                              <svg
                                xmlns="http://www.w3.org/2000/svg"
                                fill="none"
                                viewBox="0 0 24 24"
                                strokeWidth={1.5}
                                stroke="currentColor"
                                className="w-5 h-5"
                              >
                                <path
                                  strokeLinecap="round"
                                  strokeLinejoin="round"
                                  d="M2.036 12.322a1.012 1.012 0 010-.639C3.423 7.51 7.36 4.5 12 4.5c4.638 0 8.573 3.007 9.963 7.178.07.207.07.431 0 .639C20.577 16.49 16.64 19.5 12 19.5c-4.638 0-8.573-3.007-9.963-7.178z"
                                />
                                <path
                                  strokeLinecap="round"
                                  strokeLinejoin="round"
                                  d="M15 12a3 3 0 11-6 0 3 3 0 016 0z"
                                />
                              </svg>
                            ) : (
                              <svg
                                xmlns="http://www.w3.org/2000/svg"
                                fill="none"
                                viewBox="0 0 24 24"
                                strokeWidth={1.5}
                                stroke="currentColor"
                                className="w-5 h-5"
                              >
                                <path
                                  strokeLinecap="round"
                                  strokeLinejoin="round"
                                  d="M3.98 8.223A10.477 10.477 0 001.934 12C3.226 16.338 7.244 19.5 12 19.5c.993 0 1.953-.138 2.863-.395M6.228 6.228A10.45 10.45 0 0112 4.5c4.756 0 8.773 3.162 10.065 7.498a10.523 10.523 0 01-4.293 5.774M6.228 6.228L3 3m3.228 3.228l3.65 3.65m7.894 7.894L21 21m-3.228-3.228l-3.65-3.65m0 0a3 3 0 10-4.243-4.243m4.242 4.242L9.88 9.88"
                                />
                              </svg>
                            )}
                          </button>
                        </div>

                        <div
                          className={`text-red-500 text-[12px] ${
                            touched.newPasswordRepeat &&
                            errors.newPasswordRepeat
                              ? "opacity-100"
                              : "opacity-0"
                          }`}
                        >
                          {errors.newPasswordRepeat}
                        </div>
                      </div>
                      <div className="flex justify-end mt-4 w-full">
                        <button
                          type="button"
                          className="w-[168px] text-c1 py-2.5 px-5 font-lora"
                          onClick={onClose}
                        >
                          {f({ id: "modal-close" })}
                        </button>
                        <button
                          type="submit"
                          disabled={isSubmitting}
                          className="relative w-[168px] rounded-full text-white bg-[#112437] py-2.5 px-5 font-lora"
                        >
                          {loading ? (
                            <LoadingOverly visible={loading} />
                          ) : (
                            f({ id: "modal-save" })
                          )}
                        </button>
                      </div>
                    </Form>
                  )}
                </Formik>
              </Dialog.Panel>
            </Transition.Child>
          </div>
        </div>
      </Dialog>
    </Transition>
  );
};

export default ChangePasswordModal;
