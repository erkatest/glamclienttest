import { Dialog, Transition } from "@headlessui/react";
import React, { Fragment } from "react";
import { useMutation } from "react-query";
import apiFolder from "~/api/folder";
import LoadingOverly from "../libs/LoadingOverly";
import { toast } from "react-toastify";
import useMessage from "~/hooks/useMessage";
import { useUser } from "~/context/auth";
import { CustomLogin } from "../libs";
import { ColorPicker } from "@mantine/core";

const CreateFolderModal = (props: {
  isOpen: boolean;
  onClose: any;
  refetch: () => void;
}) => {
  const { isOpen, onClose, refetch } = props;
  const f = useMessage();
  const { user, token } = useUser();

  const [currentIndex, setCurrentIndex] = React.useState<any>();
  const [color, setColor] = React.useState("");
  const [name, setName] = React.useState("");

  const createMutation = useMutation(
    () => token && apiFolder.createFolder(name, color?.split("#")?.[1]),
    {
      onSuccess() {
        toast.success(f({ id: "success-create-folder" }));
        refetch();
        onClose();
      },
      onError(error: any) {
        toast.error(error);
      },
    }
  );

  return (
    <Transition appear show={isOpen} as={Fragment}>
      <Dialog as="div" className="relative z-[9999999]" onClose={onClose}>
        <Transition.Child
          as={Fragment}
          enter="ease-out duration-300"
          enterFrom="opacity-200"
          enterTo="opacity-200"
          leave="ease-in duration-200"
          leaveFrom="opacity-100"
          leaveTo="opacity-0"
        >
          <div className="fixed inset-0 bg-black/50 backdrop-blur-sm" />
        </Transition.Child>

        <div className="fixed inset-0 overflow-y-auto font-lora">
          <div className="flex min-h-full items-center justify-center p-4 text-center">
            <Transition.Child
              as={Fragment}
              enter="ease-out duration-300"
              enterFrom="opacity-0 scale-95"
              enterTo="opacity-100 scale-100"
              leave="ease-in duration-200"
              leaveFrom="opacity-100 scale-100"
              leaveTo="opacity-0 scale-95"
            >
              <Dialog.Panel className="w-full max-w-md transform overflow-hidden rounded-md bg-white p-6 text-left align-middle shadow-xl transition-all">
                <Dialog.Title
                  as="h3"
                  className="text-xl font-semibold leading-6 text-[#051332] flex items-center justify-between"
                >
                  Хавтас үүсгэх
                  <img
                    src="/images/x-icon.svg"
                    className="w-6 cursor-pointer"
                    onClick={onClose}
                  />
                </Dialog.Title>
                {user ? (
                  <>
                    <div className="my-6">
                      <input
                        type="text"
                        className="w-full p-2.5 border-b mb-4"
                        placeholder={`Хавтасны нэр`}
                        onChange={(e) => {
                          setName(e.target.value);
                        }}
                      />
                      <label className="w-full p-2.5">
                        Хавтасны өнгө сонгоно уу.
                      </label>
                      <div className="flex items-center justify-between p-4">
                        <svg
                          xmlns="http://www.w3.org/2000/svg"
                          width="90"
                          height="90"
                          viewBox="0 0 72 72"
                          fill="none"
                        >
                          <path
                            d="M44.2229 13.5H62.1419C63.3925 13.4999 64.6295 13.7604 65.7738 14.265C66.9181 14.7695 67.9448 15.507 68.7882 16.4305C69.6316 17.3539 70.2733 18.443 70.6724 19.6283C71.0714 20.8136 71.2191 22.069 71.1059 23.3145L68.2394 54.8145C68.0362 57.0506 67.0045 59.13 65.3468 60.6445C63.6891 62.159 61.5252 62.9991 59.2799 63H12.7094C10.464 62.9991 8.30013 62.159 6.64246 60.6445C4.98478 59.13 3.95307 57.0506 3.74987 54.8145L0.883372 23.3145C0.69161 21.23 1.23614 19.1442 2.42237 17.4195L2.24687 13.5C2.24687 11.1131 3.19508 8.82387 4.88291 7.13604C6.57074 5.44821 8.85992 4.5 11.2469 4.5H27.7709C30.1576 4.50051 32.4464 5.44906 34.1339 7.137L37.8599 10.863C39.5473 12.5509 41.8361 13.4995 44.2229 13.5ZM6.77387 14.04C7.73687 13.689 8.77187 13.5 9.85187 13.5H34.1339L30.9524 10.3185C30.1087 9.47453 28.9642 9.00025 27.7709 9H11.2469C10.0681 8.99979 8.93624 9.46214 8.09474 10.2876C7.25324 11.1132 6.76927 12.2359 6.74687 13.4145L6.77387 14.04Z"
                            fill={color ? color : "#6466F1"}
                          />
                        </svg>
                        <div>
                          <ColorPicker
                            format="hex"
                            swatchesPerRow={7}
                            defaultValue="#6466F1"
                            swatches={[
                              "#2e2e2e",
                              "#868e96",
                              "#fa5252",
                              "#e64980",
                              "#be4bdb",
                              "#7950f2",
                              "#4c6ef5",
                              "#228be6",
                              "#15aabf",
                              "#12b886",
                              "#40c057",
                              "#82c91e",
                              "#fab005",
                              "#fd7e14",
                            ]}
                            onChange={(e) => setColor(e)}
                          />
                        </div>
                      </div>
                    </div>
                    <div className="mt-4 w-full">
                      <button
                        type="button"
                        className="w-1/2 text-c1 py-2.5 px-5 font-lora"
                        onClick={onClose}
                      >
                        {f({ id: "modal-close" })}
                      </button>
                      <button
                        type="button"
                        className="relative w-1/2 rounded-full text-white bg-[#012E93] py-2.5 px-5 font-lora"
                        onClick={async () => {
                          await createMutation.mutateAsync();
                        }}
                      >
                        <LoadingOverly visible={createMutation?.isLoading} />
                        {f({ id: "create" })}
                      </button>
                    </div>
                  </>
                ) : (
                  <div className="flex items-center justify-center my-10">
                    <CustomLogin />
                  </div>
                )}
              </Dialog.Panel>
            </Transition.Child>
          </div>
        </div>
      </Dialog>
    </Transition>
  );
};

export default CreateFolderModal;
