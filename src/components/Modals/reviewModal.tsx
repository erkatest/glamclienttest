import { Dialog, Transition } from "@headlessui/react";
import React, { Fragment } from "react";
import { toast } from "react-toastify";
import { FacebookShareButton, TwitterShareButton } from "next-share";
import { useRouter } from "next/router";

const RequestModal = (props: { isOpen: boolean; onClose: any; item: any }) => {
  const { isOpen, onClose, item } = props;
  const inputRef = React.useRef<HTMLInputElement>(null);
  const { asPath } = useRouter();
  const origin =
    typeof window !== "undefined" && window?.location.origin
      ? window?.location.origin
      : "";
  const ownURL = `${origin}${asPath}`;
  const handleCopyClick = () => {
    if (inputRef.current) {
      inputRef.current.select();
    }
    toast.success("Хуулсан", {
      position: "bottom-center",
      autoClose: 1000,
    });
  };
  return (
    <Transition appear show={isOpen} as={Fragment}>
      <Dialog as="div" className="relative z-[9999999]" onClose={onClose}>
        <Transition.Child
          as={Fragment}
          enter="ease-out duration-300"
          enterFrom="opacity-200"
          enterTo="opacity-200"
          leave="ease-in duration-200"
          leaveFrom="opacity-100"
          leaveTo="opacity-0"
        >
          <div className="fixed inset-0 bg-black/25" />
        </Transition.Child>

        <div className="fixed inset-0 overflow-y-auto font-lora ">
          <div className="flex  min-h-full items-center justify-center p-4 text-center">
            <Transition.Child
              as={Fragment}
              enter="ease-out duration-300"
              enterFrom="opacity-0 scale-95"
              enterTo="opacity-100 scale-100"
              leave="ease-in duration-200"
              leaveFrom="opacity-100 scale-100"
              leaveTo="opacity-0 scale-95"
            >
              <Dialog.Panel className="w-full max-w-[890px] h-[740px] transform overflow-hidden rounded-md bg-white text-left p-6 align-middle shadow-xl transition-all">
                <Dialog.Title
                  as="h3"
                  className="text-xl mb-[10px] font-bold leading-6 text-[#051332] flex items-center justify-between"
                >
                  Санал хүсэлт
                  <img
                    src="/images/x-icon.svg"
                    className="w-5 cursor-pointer"
                    onClick={() => {
                      onClose();
                    }}
                  />
                </Dialog.Title>
                <div className="">
                  <div className="flex py-8">
                    <img
                      className="object-contain"
                      src={item?.mainImg}
                      height={80}
                      width={80}
                    />
                    <div className="ml-[16px]">
                      <div className="mb-[10px] font font-[20px]">
                        {item?.name}
                      </div>
                      <div className="line-clamp-2 text-[#A6B2C3]">
                        {item?.description}
                      </div>
                    </div>
                  </div>
                  <div className="w-full h-full ">
                    <h3>Хүсэлтийн дэлгэрэнгүй </h3>
                    <textarea
                      className="h-[370px] pt-6 pl-4 w-full my-[10px] bg-[#A6B2C31A]"
                      placeholder="Энд бичнэ үү"
                    ></textarea>
                  </div>
                  <div className="flex justify-end mt-[18px] gap-4">
                    <button
                      type="button"
                      onClick={onClose}
                      className="rounded-full py-2 px-4 text-black border font-lora"
                    >
                      Болих
                    </button>
                    <button
                      type="button"
                      onClick={onClose}
                      className="rounded-full py-3 px-5 text-white bg-[#112437] border font-lora"
                    >
                      Илгээх
                    </button>
                  </div>
                </div>
              </Dialog.Panel>
            </Transition.Child>
          </div>
        </div>
      </Dialog>
    </Transition>
  );
};

export default RequestModal;
