import { Dialog, Transition } from "@headlessui/react";
import React, { Fragment, useState } from "react";
import useMessage from "~/hooks/useMessage";
import SignIn from "../../components/SignIn";
import SignUp from "../../components/SignUp";
import Forget from "../../containers/Forget";
import { TabContext } from "~/context/tab";

import { IoMdClose } from "react-icons/io";

const SignInSignUpModal = (props: { isOpen: boolean; onClose: any }) => {
  const { isOpen, onClose } = props;
  const f = useMessage();

  const { setOpen, open } = React.useContext(TabContext);

  return (
    <>
      <Transition appear show={isOpen} as={Fragment}>
        <Dialog as="div" className="relative  z-[9999999]" onClose={onClose}>
          <Transition.Child
            as={Fragment}
            enter="ease-out duration-300"
            enterFrom="opacity-0"
            enterTo="opacity-100"
            leave="ease-in duration-200"
            leaveFrom="opacity-100"
            leaveTo="opacity-0"
          >
            <div className="fixed inset-0 bg-black/25" />
          </Transition.Child>

          <div className="fixed inset-0 overflow-y-auto font-lora">
            <div className="z-50 relative flex min-h-screen items-center justify-center p-4 text-center">
              <Transition.Child
                as={Fragment}
                enter="ease-out duration-300"
                enterFrom="opacity-0 scale-95"
                enterTo="opacity-100 scale-100"
                leave="ease-in duration-200"
                leaveFrom="opacity-100 scale-100"
                leaveTo="opacity-0 scale-95"
              >
                <Dialog.Panel className="relative w-full max-w-[600px] transform overflow-hidden bg-white text-left align-middle shadow-xl transition-all py-10 px-11 md:py-12 md:px-14 2xl:py-16 2xl:px-16 max-h-fit">
                  <IoMdClose
                    className="absolute top-7 right-7 text-[25px] cursor-pointer"
                    onClick={onClose}
                  />

                  {open === "signin" ? (
                    <SignIn setOpen={setOpen} onClose={onClose} />
                  ) : open === "signup" ? (
                    <SignUp setOpen={setOpen} />
                  ) : open === "forget" ? (
                    <Forget setOpen={setOpen} />
                  ) : (
                    ""
                  )}
                </Dialog.Panel>
              </Transition.Child>
            </div>
          </div>
        </Dialog>
      </Transition>
    </>
  );
};

export default SignInSignUpModal;
