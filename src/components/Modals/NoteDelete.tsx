import { Dialog, Transition } from "@headlessui/react";
import React, { Fragment } from "react";
import useMessage from "~/hooks/useMessage";
import LoadingOverly from "../libs/LoadingOverly";
import { useMutation } from "react-query";
import apiFolder from "~/api/folder";
import { toast } from "react-toastify";
import apiResearch from "~/api/research";

const NoteDeleteModal = (props: {
  isOpen: boolean;
  onClose: any;
  id: any;
  refetch: any;
}) => {
  const { isOpen, onClose, refetch, id } = props;
  const f = useMessage();

  const deleteMutation = useMutation((id: string) => apiResearch.delete(id), {
    onSuccess() {
      refetch();
      onClose();
      toast.success("Амжилттай тэмдэглэл устлаа");
    },
  });
  return (
    <Transition appear show={isOpen} as={Fragment}>
      <Dialog as="div" className="relative z-[9999999]" onClose={onClose}>
        <Transition.Child
          as={Fragment}
          enter="ease-out duration-300"
          enterFrom="opacity-200"
          enterTo="opacity-200"
          leave="ease-in duration-200"
          leaveFrom="opacity-100"
          leaveTo="opacity-0"
        >
          <div className="fixed inset-0 bg-black/50 backdrop-blur-sm" />
        </Transition.Child>

        <div className="fixed inset-0 overflow-y-auto">
          <div className="flex  min-h-full items-center justify-center p-4 text-center">
            <Transition.Child
              as={Fragment}
              enter="ease-out duration-300"
              enterFrom="opacity-0 scale-95"
              enterTo="opacity-100 scale-100"
              leave="ease-in duration-200"
              leaveFrom="opacity-100 scale-100"
              leaveTo="opacity-0 scale-95"
            >
              <Dialog.Panel className="w-full max-w-sm transform overflow-hidden rounded-md bg-white p-6 text-left align-middle shadow-xl transition-all font-lora">
                <Dialog.Title
                  as="h3"
                  className="text-xl font-semibold leading-6 text-[#051332] flex items-center justify-between"
                >
                  Анхааруулга
                  <img
                    src="/images/x-icon.svg"
                    className="w-5 cursor-pointer"
                    onClick={onClose}
                  />
                </Dialog.Title>
                <p className="text-center text-black my-8 text-md">
                  Та энэхүү тэмдэглэлийг устгахдаа итгэлтэй байна уу?
                </p>

                <div className="flex justify-end mt-4 w-full">
                  <button
                    type="button"
                    className="w-[168px] text-c1 py-2.5 px-5 font-lora"
                    onClick={onClose}
                  >
                    {f({ id: "modal-close" })}
                  </button>
                  <button
                    type="button"
                    className="relative w-[168px] rounded-full text-white bg-red-600 py-2.5 px-5 font-lora"
                    onClick={async () => {
                      deleteMutation.mutateAsync(id);
                    }}
                  >
                    <LoadingOverly visible={deleteMutation?.isLoading} />
                    {f({ id: "modal-delete" })}
                  </button>
                </div>
              </Dialog.Panel>
            </Transition.Child>
          </div>
        </div>
      </Dialog>
    </Transition>
  );
};

export default NoteDeleteModal;
