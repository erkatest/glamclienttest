import React from "react";
import Link from "next/link";
import { Tooltip } from "@mantine/core";
import { IoMdHeart } from "react-icons/io";

import { useUser } from "~/context/auth";
import { TabContext } from "~/context/tab";
import AddItemModal from "~/components/Modals/AddItemModal";

const BookCard = ({ book }: any) => {
  const { user } = useUser();
  const { setOpen: setModalOpen, setShowModal } = React.useContext(TabContext);
  const [showFavouriteModal, setShowFavouriteModal] =
    React.useState<boolean>(false);
  return (
    <div className="pr-4 md:pr-8 lg:pr-12">
      <AddItemModal
        isOpen={showFavouriteModal}
        onClose={() => setShowFavouriteModal(false)}
        item={{
          ...book,
          type: "library",
        }}
      />
      <Link className="cursor-pointer group" href={`detail/${book?._id}`}>
        <div className="relative">
          <div className="aspect-[5/8] overflow-hidden">
            <img
              src={book?.mainImg || "/images/empty.svg"}
              className="w-full h-full object-cover"
              onError={(e: any) => {
                e.target.src = "/images/empty.svg";
              }}
            />
          </div>
          <div className="absolute top-2 right-2 group-hover:block hidden p-1 bg-slate-500 rounded-full justify-center items-center">
            <IoMdHeart
              size={24}
              className="text-white"
              onClick={(e: any) => {
                e.preventDefault();
                if (!user) {
                  setShowModal(true);
                  setModalOpen("signin");
                } else {
                  setShowFavouriteModal(true);
                }
              }}
            />
          </div>
        </div>

        <Tooltip label={book?.name}>
          <h2 className="text-black mt-1 font-bold text-[14px] line-clamp-1 uppercase w-0 min-w-[100%]">
            {book?.name}
          </h2>
        </Tooltip>
        <p className="font-[500] text-sm line-clamp-2 text-gray-500">
          <span className="first-letter:uppercase block">
            {book.description}
          </span>
        </p>
      </Link>
    </div>
  );
};

export default BookCard;
