import { Skeleton } from "@mantine/core";

const BookSkeleton = () => {
  return (
    <div className="group bg-[#f2f2f2] rounded-[1rem] overflow-hidden">
      <div className="w-full aspect-[3.5/4] overflow-hidden">
        <Skeleton h={400} />
      </div>
      <div className="flex flex-col p-4 pb-6 gap-2">
        <Skeleton h={16} />
        <Skeleton h={14} />
      </div>
    </div>
  );
};

export default BookSkeleton;
