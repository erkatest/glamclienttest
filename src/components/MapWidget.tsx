// import * as React from "react";
// import { BsClock, BsMap } from "react-icons/bs";
// import { FaCircle } from "react-icons/fa";

// interface IProps {
//   imageUrl: string;
//   title: string;
//   location: string;
//   date: string;
//   status: string;
//   index: number;
// }

// const MapWidget = (props: IProps) => {
//   return (
//     <section className={`relative bg-white border rounded shadow-lg h-[150px]`}>
//       <div className="flex">
//         <div className="w-1/3">
//           <img src={props?.imageUrl} alt="" className="w-full h-[150px]" />
//         </div>
//         <div>
//           <h1 className="font-bold my-4">{props?.title}</h1>
//           <div className="flex space-x-4 items-center mb-2">
//             <BsMap />
//             <p>{props?.location}</p>
//           </div>
//           <div className="flex space-x-4 items-center mb-2">
//             <BsClock />
//             <p>{props?.date}</p>
//           </div>
//           <div className="flex space-x-4 items-center mb-2">
//             <FaCircle color="green" />
//             <p>{props?.status}</p>
//           </div>
//         </div>
//       </div>
//     </section>
//   );
// };

// export default MapWidget;
import * as React from "react";
import { BsPinMap } from "react-icons/bs";

interface IProps {}

const MapWidget = (props: IProps) => {
  return (
    // <section className="bg-white border rounded overflow-hidden shadow-lg w-full h-full flex items-center justify-center">
    //   <BsPinMap />
    // </section>
    <img
      src="/images/location-image.png"
      className="mt-[-50px] ml-16 h-[450px] flex"
    />
  );
};

export default MapWidget;
