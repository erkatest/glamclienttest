import * as React from "react";
import CustomCategory from "./libs/CustomCategory";
import AddItemModal from "./Modals/AddItemModal";
import { useRouter } from "next/router";

interface IProps {
  data: any;
}

const ResultList = ({ data }: IProps) => {
  const [modal, setModal] = React.useState(false);
  const router = useRouter();
  return (
    <section className="relative flex w-full h-28 rounded-2xl mb-8 space-x-6 shadow-[rgba(0,_0,_0,_0.24)_0px_3px_8px] dark:text-[#BE8100]">
      <div
        className="rounded-xl overflow-hidden "
        style={{
          aspectRatio: "1 / 1",
        }}
      >
        <img
          src={data?.mainImg || "/images/empty.svg"}
          className="pointer-events-none rounded-2xl mx-auto bg-contain p-4 h-full"
        />
      </div>

      <div className="space-y-2 mt-4 w-2/3">
        <CustomCategory type={data?.type} />
        <p className="font-bold text-sm leading-5">{data?.name}</p>
        <p className="text-xs overflow-auto h-4">{data?.description}</p>
      </div>
      <button
        onClick={(e) => {
          e.preventDefault();
          e.stopPropagation();
          setModal(true);
        }}
      >
        <img
          src="/images/detail-love.svg"
          className="absolute top-0 right-4 w-7 border rounded-full bg-[#F6F6F6] h-7 p-1 mt-4"
        />
      </button>
      <button
        onClick={(e) => {
          e.preventDefault();
          e.stopPropagation();
          router.push({
            pathname: `/detail/${data?._id || ""}/${data?.type || ""}`,
          });
        }}
      >
        <img
          src="/images/eye.svg"
          className="absolute top-0 right-14 w-7 border rounded-full bg-[#F6F6F6] h-7 p-1 mt-4"
        />
      </button>
      {modal && (
        <AddItemModal
          isOpen={modal}
          onClose={() => setModal(false)}
          item={data}
        />
      )}
    </section>
  );
};

export default ResultList;
