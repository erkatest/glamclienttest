import { LottiePlayer } from "lottie-web";
import * as React from "react";

const Loader = () => {
  const ref = React.useRef<HTMLDivElement>(null);
  const [lottie, setLottie] = React.useState<LottiePlayer | null>(null);

  React.useEffect(() => {
    import("lottie-web").then((Lottie) => setLottie(Lottie.default));
  }, []);

  React.useEffect(() => {
    if (lottie && ref.current) {
      const animation = lottie.loadAnimation({
        container: ref.current,
        renderer: "svg",
        loop: true,
        autoplay: true,
        // path to your animation file, place it inside public folder
        path: "/loader.json",
      });

      return () => animation.destroy();
    }
  }, [lottie, ref]);

  return <div ref={ref}></div>;
};

export default Loader;
