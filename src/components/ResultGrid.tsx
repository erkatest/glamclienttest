import * as React from "react";
import CustomCategory from "./libs/CustomCategory";

interface IProps {
  data: any;
}

const ResultGrid = ({ data }: IProps) => {
  return (
    <section className="rounded-2xl shadow-[rgba(0,_0,_0,_0.2)_0px_3px_8px]  p-2 group relative dark:text-[#BE8100] hover:bg-black/10 transition-all hover:duration-500 ">
      <div
        className="rounded-lg mb-4 overflow-hidden"
        style={{
          aspectRatio: "1 / 1",
        }}
      >
        <img
          src={data?.mainImg || "/images/empty.svg"}
          className="h-full pointer-events-none rounded-md mx-auto bg-contain "
        />
      </div>

      <div className="flex flex-col space-y-3 p-1 m-2">
        <CustomCategory type={data?.type} />
        <h4 className="font-bold text-sm h-6 overflow-hidden">{data?.name}</h4>
      </div>
    </section>
  );
};

export default ResultGrid;
