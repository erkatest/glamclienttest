import Link from "next/link";
import * as React from "react";
import useMessage from "../../hooks/useMessage";
import MapWidget from "../MapWidget";

interface IProps {}

const Section3 = (props: IProps) => {
  const f = useMessage();

  const maps = [
    {
      imageUrl: "/images/book0.png",
      title: "Чингис хаан музей",
      location: "Улаанбаатар, Чингэлттэй Дүүрэг, 4-р хороо",
      date: "Өнөөдөр: 10:00 - 21: 00",
      status: "Нээлттэй",
    },
    {
      imageUrl: "/images/book2.png",
      title: "Чингис хаан музей",
      location: "Улаанбаатар, Чингэлттэй Дүүрэг, 4-р хороо",
      date: "Өнөөдөр: 10:00 - 21: 00",
      status: "Нээлттэй",
    },
    {
      imageUrl: "/images/book1.png",
      title: "Чингис хаан музей",
      location: "Улаанбаатар, Чингэлттэй Дүүрэг, 4-р хороо",
      date: "Өнөөдөр: 10:00 - 21: 00",
      status: "Нээлттэй",
    },
    {
      imageUrl: "/images/book3.png",
      title: "Чингис хаан музей",
      location: "Улаанбаатар, Чингэлттэй Дүүрэг, 4-р хороо",
      date: "Өнөөдөр: 10:00 - 21: 00",
      status: "Нээлттэй",
    },
    {
      imageUrl: "/images/book4.png",
      title: "Чингис хаан музей",
      location: "Улаанбаатар, Чингэлттэй Дүүрэг, 4-р хороо",
      date: "Өнөөдөр: 10:00 - 21: 00",
      status: "Нээлттэй",
    },
  ];
  return (
    <section className="relative min-h-auto flex items-center ">
      <img
        src={"/images/map-bg.png"}
        className="absolute top-1/2 transform -translate-y-1/2 left-0 right-0 w-screen h-screen min-h-[500px] lg:h-[60vh] object-cover"
      />
      <div className="relative container mx-auto px-8 max-w-7xl">
        <div className="flex flex-col lg:flex-row items-center lg:space-x-20">
          <div className="w-full mb-10 lg:w-3/5 h-[30vh] lg:h-[40vh]">
            {/* {[...maps]?.map((map, i) => (
              <div key={`map=${i}`}>
                <MapWidget {...map} index={i} />
              </div>
            ))} */}
            <MapWidget />
          </div>
          <div className="w-full lg:w-2/5">
            <h1 className="mb-5 text-4xl text-white font-bold">
              {f({ id: "map-title" })}
            </h1>
            <p
              className="text-white mb-8 text-justify text-md w-3/4"
              dangerouslySetInnerHTML={{ __html: f({ id: "map-desc" }) }}
            ></p>
            {/* <div className="flex space-x-4 mb-10">
              <div className="border-l-2 px-5 border-white">
                <strong className="block text-lg text-white font-bold">
                  50{`'`}000+
                </strong>
                <span className="text-[#97A9D3]">
                  {f({ id: "map-stat-1" })}
                </span>
              </div>
              <div className="border-l-2 px-5 border-white">
                <strong className="block text-lg text-white font-bold">
                  100+
                </strong>
                <span className="text-[#97A9D3]">
                  {f({ id: "map-stat-2" })}
                </span>
              </div>
            </div> */}
            <Link
              href="/map"
              className="bg-c3 font-bold space-x-4 inline-flex px-6 py-4 rounded-full text-white items-center"
            >
              <span>{f({ id: "map-btn" })}</span>
              <img src={"/images/arrow.svg"} width={35} height={19} />
            </Link>
          </div>
        </div>
      </div>
    </section>
  );
};

export default Section3;
