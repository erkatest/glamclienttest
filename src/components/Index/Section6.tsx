import * as React from "react";
import useMessage from "../../hooks/useMessage";
import Slider from "react-slick";
import { useQuery } from "react-query";
import apiArtist from "~/api/artist";
import { useRouter } from "next/router";

interface IProps {}

const Section6 = (props: IProps) => {
  const f = useMessage();
  const router = useRouter();
  const sliderRef = React.useRef<any>();

  const settings = {
    className: "center",
    centerMode: true,
    centerPadding: "30px",
    infinite: true,
    slidesToShow: 7,
    swipeToSlide: true,
    autoplay: true,
    responsive: [
      {
        breakpoint: 1920,
        settings: {
          slidesToShow: 5,
          slidesToScroll: 5,
          infinite: true,
          centerPadding: "30px",
        },
      },
      {
        breakpoint: 1080,
        settings: {
          slidesToShow: 4,
          slidesToScroll: 4,
          initialSlide: 4,
          infinite: true,
          centerPadding: "30px",
        },
      },
      {
        breakpoint: 800,
        settings: {
          slidesToShow: 3,
          slidesToScroll: 3,
          initialSlide: 3,
          centerPadding: "30px",
        },
      },
      {
        breakpoint: 600,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
          centerPadding: "30px",
        },
      },
    ],
  };

  const { data } = useQuery(["artist"], () => apiArtist.find(0, 10, ""), {
    refetchOnWindowFocus: false,
    refetchInterval: false,
  });

  return (
    <section className="contain h-100vh bg-[url('/images/map-bg.png')] bg-cover bg-center">
      <div className="container mx-auto px-8 max-w-7xl pt-16">
        <div className="flex justify-between">
          <h1 className="font-bold text-white text-2xl lg:text-4xl mb-4">
            <span>{f({ id: "artist-title" })}</span>
          </h1>
          <div className="flex space-x-2 justify-end mb-6">
            <button
              className="flex items-center rounded-full space-x-4 text-white bg-c1 py-2 px-4"
              onClick={() => router.push("/search/artist")}
            >
              <p className="font-bold">Нийт уран бүтээлчид</p>
              <img src="/images/arrow.svg" className="w-10" />
            </button>
          </div>
        </div>
      </div>
      <Slider ref={(c) => (sliderRef.current = c)} {...settings}>
        {(data?.data || []).map((artist: any, index: number) => {
          return (
            <div
              key={`artist-${index}`}
              onClick={() =>
                router.replace(`/search/artist/detail/${artist?._id}`)
              }
            >
              <div className="rounded-2xl overflow-hidden bg-white mx-5 mt-10 mb-32 h-96 px-4 cursor-pointer space-y-3">
                <img
                  src={artist?.mainImg}
                  className="w-full h-72 pt-2.5 rounded-2xl"
                />
                <p className="text-gray-300 text-sm ">
                  {artist?.category?.name}
                </p>
                <p>{artist?.name}</p>
              </div>
            </div>
          );
        })}
      </Slider>
    </section>
  );
};

export default Section6;
