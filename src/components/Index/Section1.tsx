import * as React from "react";
import Slider from "react-slick";
import { FiSearch } from "react-icons/fi";
import { VscChromeClose } from "react-icons/vsc";
import { useMutation, useQuery } from "react-query";
import { useRouter } from "next/router";
import { useUser } from "~/context/auth";
import useMessage from "~/hooks/useMessage";
import Spinner from "~/components/libs/Spinner";
import { TabContext } from "~/context/tab";
import LoadingOverly from "~/components/libs/LoadingOverly";
import apiBanner from "~/api/banner";
import apiItem from "~/api/item";
import { Skeleton } from "@mantine/core";
import Link from "next/link";

const settings = {
  centerMode: false,
  infinite: true,
  slidesToShow: 1,
  speed: 800,
  swipeToSlide: false,
  autoplay: true,
  autoPlaySpeed: 3000,
  pauseOnHover: true,
};
const Section1 = () => {
  const router: any = useRouter();
  const { token } = useUser();
  const [q, setQ] = React.useState("");

  const f = useMessage();
  const [focus, setFocus] = React.useState(false);
  const [loading, setLoading] = React.useState(false);
  const { setActive, setActiveArtwork } = React.useContext(TabContext);
  const sliderRef = React.useRef<any>();
  const [seearchType, setSearchType] = React.useState(false);
  const [seearchTypeValue, setSearchTypeValue] = React.useState<any>();
  const { data: banners, isLoading: bannersIsLoading } = useQuery(
    ["banner"],
    () => apiBanner.find(),
    {
      refetchOnWindowFocus: false,
      refetchInterval: false,
    }
  );

  const { user } = useUser();

  const { data, isLoading } = useQuery(
    ["common-search"],
    () => apiItem.commonSearch(8),
    {
      refetchOnWindowFocus: false,
      refetchInterval: false,
    }
  );

  const {
    data: searchHistory,
    isLoading: LoadingHistory,
    refetch: refetchHistory,
  } = useQuery(
    ["history-search", token],
    () => token && apiItem.searchHistory(0, 8),
    {
      refetchOnWindowFocus: false,
      refetchInterval: false,
    }
  );

  const deleteMutation = useMutation(
    (id: string) => apiItem.deleteHistory(id),
    {
      onSuccess() {
        refetchHistory();
      },
    }
  );
  const types = [
    {
      label: "gallery",
      value: "Урын сан",
      hex: "#BE3455",
    },
    {
      label: "library",
      value: "Номын сан",
      hex: "#1A3D7A",
    },
    {
      label: "archive",
      value: "Сан хөмрөг",
      hex: "#312C59",
    },
    {
      label: "museum",
      value: "Соёлын өв",
      hex: "#14452F",
    },
  ];

  return (
    <div
      className="relative dark:text-[#BE8100] z-10"
      onClick={() => seearchType && setSearchType(false)}
    >
      {bannersIsLoading ? (
        <Skeleton visible={bannersIsLoading} height={500} />
      ) : (
        // </div>
        <Slider
          ref={(c) => (sliderRef.current = c)}
          {...settings}
          className="max-h-[500px] min-h-[300px] h-full"
        >
          {banners?.data?.map((item: any, i: number) => {
            return (
              <div key={`banner-${i}`} className="relative">
                <img
                  // onClick={(e) => {
                  //   e.stopPropagation();
                  //   if (item?.link) {
                  //     window.open(item?.link, "_blank");
                  //   }
                  // }}
                  src={item?.img}
                  alt={item?.name}
                  className="min-h-[300px] max-h-[430px] object-cover w-full"
                />
              </div>
            );
          })}
        </Slider>
      )}
      {!bannersIsLoading && (
        <div className="absolute top-[50%] -translate-y-[50%] left-0 right-0 flex items-center justify-center flex-col px-2">
          <div className=" w-full  relative mx-auto">
            <div
              className={`bg-white container mx-auto rounded-full relative overflow-hidden`}
            >
              <input
                placeholder={f({ id: "search-placeholder" })}
                className="lg:h-[60px] w-full p-5 pl-16 focus:outline-none"
                onFocus={() => setFocus(true)}
                onBlur={() => setFocus(false)}
                value={q}
                onChange={(e) => {
                  setQ(e.target.value);
                }}
                onKeyDown={async (e) => {
                  if (e.key === "Enter") {
                    setLoading(true);
                    await apiItem
                      .saveSearch(q)
                      .then(() => {
                        router.push({
                          pathname: "/search",
                          search: `?q=${q || ""}&type=gallery&category=&page=1`,
                        });
                        setActive(
                          seearchTypeValue?.label
                            ? seearchTypeValue?.label
                            : "gallery"
                        );
                      })
                      .catch((e: any) => {
                        console.log(e);
                      });
                  }
                }}
              />
              <div className=" absolute top-1/2 right-2 transform -translate-y-1/2">
                {seearchTypeValue ? (
                  <div
                    style={{ backgroundColor: seearchTypeValue?.hex }}
                    className={`p-[10px] rounded-full cursor-pointer flex space-x-[10px]`}
                    onClick={() => setSearchType(!seearchType)}
                  >
                    <svg
                      xmlns="http://www.w3.org/2000/svg"
                      width="28"
                      height="28"
                      viewBox="0 0 28 28"
                      fill="none"
                    >
                      <path
                        d="M16.3333 14V23.1933C16.38 23.5433 16.2633 23.9167 15.995 24.1617C15.887 24.2698 15.7588 24.3556 15.6177 24.4142C15.4766 24.4727 15.3253 24.5028 15.1725 24.5028C15.0197 24.5028 14.8684 24.4727 14.7272 24.4142C14.5861 24.3556 14.4579 24.2698 14.35 24.1617L12.005 21.8167C11.8777 21.6923 11.781 21.5401 11.7223 21.3722C11.6636 21.2042 11.6445 21.0249 11.6666 20.8483V14H11.6316L4.91164 5.39C4.72218 5.14679 4.63669 4.83847 4.67386 4.53242C4.71102 4.22637 4.86781 3.94747 5.10997 3.75667C5.33164 3.59333 5.57664 3.5 5.8333 3.5H22.1666C22.4233 3.5 22.6683 3.59333 22.89 3.75667C23.1321 3.94747 23.2889 4.22637 23.3261 4.53242C23.3632 4.83847 23.2778 5.14679 23.0883 5.39L16.3683 14H16.3333Z"
                        fill="white"
                      />
                    </svg>
                    {/* <div className="text-[16px] text-white">
                      {seearchTypeValue?.value}
                    </div> */}
                  </div>
                ) : (
                  <div
                    className="p-[10px] rounded-full bg-[#A6B2C3] cursor-pointer"
                    onClick={() => setSearchType(!seearchType)}
                  >
                    <svg
                      xmlns="http://www.w3.org/2000/svg"
                      width="28"
                      height="28"
                      viewBox="0 0 28 28"
                      fill="none"
                    >
                      <path
                        d="M16.3334 14V23.1933C16.38 23.5433 16.2634 23.9167 15.995 24.1617C15.8871 24.2698 15.7589 24.3556 15.6178 24.4142C15.4766 24.4727 15.3253 24.5028 15.1725 24.5028C15.0197 24.5028 14.8684 24.4727 14.7273 24.4142C14.5862 24.3556 14.458 24.2698 14.35 24.1617L12.005 21.8167C11.8778 21.6923 11.781 21.5401 11.7223 21.3722C11.6637 21.2042 11.6446 21.0249 11.6667 20.8483V14H11.6317L4.9117 5.39C4.72224 5.14679 4.63675 4.83847 4.67392 4.53242C4.71108 4.22637 4.86787 3.94747 5.11003 3.75667C5.3317 3.59333 5.5767 3.5 5.83336 3.5H22.1667C22.4234 3.5 22.6684 3.59333 22.89 3.75667C23.1322 3.94747 23.289 4.22637 23.3261 4.53242C23.3633 4.83847 23.2778 5.14679 23.0884 5.39L16.3684 14H16.3334Z"
                        fill="white"
                      />
                    </svg>
                  </div>
                )}
              </div>
              <div className="absolute top-1/2 left-5 transform -translate-y-1/2">
                <FiSearch size={28} />
              </div>
              {/* <LoadingOverly visible={loading || isLoading || LoadingHistory} /> */}
            </div>

            {/* Common search items */}
            <div
              style={{ zIndex: 99999 }}
              className={`container mx-auto absolute top-[100%] py-[10px]    right-0 left-0 bg-[#F9F9F9]  transition-all border-t border-[#E6EAF4] rounded-[1.25rem] w-full min-h-[72px] flex-wrap items-center px-2 ${
                seearchType
                  ? "visible opacity-100 mt-[1rem]"
                  : "invisible opacity-0"
              }`}
            >
              <div className=" lg:flex lg:justify-between items-center space-y-3 lg:space-y-0">
                {types?.map((item: any, index: number) => (
                  <div
                    className="flex justify-between items-center w-full"
                    key={`types-${index}`}
                  >
                    <Link
                      className="flex items-center rounded-full space-x-[16px] bg-[#f2f4f7] px-[20px] py-[10px] cursor-pointer"
                      href={`/search?q=${q || ""}&type=${
                        item?.label
                      }&category=&sub_category=${""}&page=1&sort=${
                        item?.label == "museum" ? "heritage_name" : "name"
                      }`}
                      onClick={async () => {
                        seearchTypeValue?.value === item?.value
                          ? setSearchTypeValue(null)
                          : setSearchTypeValue(item);
                        q &&
                          (await apiItem
                            .saveSearch(q)
                            .then(() => {
                              router.push({
                                pathname: "/search",
                                search: `?q=${q || ""}`,
                              });
                              setActive(item?.label ? item?.label : "gallery");
                            })
                            .catch((e: any) => {
                              console.log(e);
                            }));
                      }}
                    >
                      <div
                        style={{ backgroundColor: item?.hex }}
                        className={`rounded-full p-5 `}
                      />
                      <div
                        className={`text-[16px] ${
                          seearchTypeValue?.value === item?.value && "font-bold"
                        }`}
                      >
                        {item?.value}
                      </div>
                    </Link>
                    {types?.length !== index + 1 && (
                      <div className="border-l h-10 border-[#A6B2C3] mx-auto" />
                    )}
                  </div>
                ))}
              </div>
            </div>
            <div
              style={{ zIndex: 9999 }}
              className={`container mx-auto absolute top-[100%] right-0 left-0 bg-[#F9F9F9] transition-all border-t border-[#E6EAF4] rounded-[1.25rem]   w-full min-h-[72px] p-[20px] flex-wrap items-center ${
                focus ? "visible opacity-100 mt-[1rem]" : "invisible opacity-0"
              }`}
            >
              {loading || isLoading || LoadingHistory ? (
                <div className="flex justify-center">
                  <Spinner />
                </div>
              ) : (
                <>
                  {user && searchHistory?.data?.length > 0 && (
                    <div className="space-y-3 mb-6">
                      <h3 className="text-[14px] mb-1 text-[#767676]">
                        {f({ id: "search-history-title" })}
                      </h3>
                      <div className="flex flex-wrap gap-[1rem]">
                        {searchHistory?.data?.map((txt: any, i: number) => (
                          <div
                            key={`text-${i}`}
                            className="flex  cursor-pointer whitespace-nowrap items-center space-x-1 bg-[#EEEEEE] text-[#55606B] hover:bg-gray-400 hover:text-white rounded-[5px]"
                          >
                            <button
                              onClick={async () => {
                                setLoading(true);
                                await apiItem
                                  .saveSearch(txt.value)
                                  .then(() => {
                                    router.push({
                                      pathname: "/search",
                                      search: `?q=${txt.value}`,
                                    });
                                    setActiveArtwork("");
                                    setActive(
                                      seearchTypeValue?.label
                                        ? seearchTypeValue?.label
                                        : "gallery"
                                    );
                                  })
                                  .catch((e: any) => {
                                    console.log(e);
                                  });
                              }}
                              className="flex items-center"
                            >
                              <div className="flex p-2 py-1 items-center">
                                <span className="mx-2 text-[14px] font-lora">
                                  {txt?.value}
                                </span>
                              </div>
                            </button>

                            <button
                              onClick={async (e) => {
                                e.preventDefault();
                                e.stopPropagation();
                                await deleteMutation.mutateAsync(txt?._id);
                              }}
                            >
                              <VscChromeClose
                                size={16}
                                className="mr-2 hover:text-black"
                              />
                            </button>
                          </div>
                        ))}
                      </div>
                    </div>
                  )}

                  <div className="space-y-3">
                    <h3 className="text-[14px] text-[#767676]">
                      {f({ id: "common-search" })}
                    </h3>
                    <div className="flex flex-wrap gap-[1rem]">
                      {data?.data.map((txt: any, i: number) => (
                        <div
                          key={`text-${i}`}
                          className="flex cursor-pointer text-[14px] whitespace-nowrap items-center space-x-1 p-2 py-1 bg-[#EEEEEE] hover:bg-gray-400 hover:text-white text-[#55606B] rounded-[5px]"
                          onClick={async () => {
                            setLoading(true);
                            await apiItem
                              .saveSearch(txt.value)
                              .then(() => {
                                router.push({
                                  pathname: "/search",
                                  search: `?q=${txt.value}&type=gallery&page=1&sort=name`,
                                });
                                setActiveArtwork("");
                                setActive(
                                  seearchTypeValue?.label
                                    ? seearchTypeValue?.label
                                    : "gallery"
                                );
                              })
                              .catch((e: any) => {
                                console.log(e);
                              });
                          }}
                        >
                          <span className="mx-4">{txt?.value}</span>
                        </div>
                      ))}
                    </div>
                  </div>
                </>
              )}
            </div>
          </div>
        </div>
      )}
    </div>
    // </Container>
  );
};

export default Section1;
