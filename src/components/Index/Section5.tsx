import * as React from "react";
import useMessage from "../../hooks/useMessage";
import { useQuery } from "react-query";
import Link from "next/link";
import { MdArrowForwardIos } from "react-icons/md";
import Divider from "../Divider";
import apiMap from "~/api/map";
import { useState } from "react";
import { useRouter } from "next/router";
import LocationSwiper from "../LocationSwiper";

interface IProps {}

const Section5 = (props: IProps) => {
  const f = useMessage();

  const [myLocation, setMyLocation] = useState<any>(null);
  React.useEffect(() => {
    // Retrieve location data from local storage
    const storedData = localStorage.getItem("locationData");
    if (storedData) {
      const locationData = JSON.parse(storedData);
      setMyLocation({
        lat: locationData.lat,
        lng: locationData.lng,
      });
    }
  }, []);

  const { data, isLoading }: any = useQuery(
    ["near-my-locations"],
    () => apiMap.find("", ""),
    {
      refetchOnWindowFocus: false,
      refetchInterval: false,
    }
  );

  const calculateDistance = (lat1: any, lon1: any, lat2: any, lon2: any) => {
    const R = 3185; // Radius of the earth in km
    const dLat = (lat2 - lat1) * (Math.PI / 180);
    const dLon = (lon2 - lon1) * (Math.PI / 180);
    const a =
      Math.sin(dLat / 2) * Math.sin(dLat / 2) +
      Math.cos(lat1 * (Math.PI / 180)) *
        Math.cos(lat2 * (Math.PI / 180)) *
        Math.sin(dLon / 2) *
        Math.sin(dLon / 2);
    const c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
    const distance = R * c; // Distance in km
    return distance;
  };

  const filterAndSortLocations = (locations: any) => {
    if (!myLocation || !locations) return [];

    const threshold = 2; // Threshold distance in km
    const filteredLocations = locations.filter((location: any) => {
      const distance = calculateDistance(
        myLocation.lat,
        myLocation.lng,
        location.latitude,
        location.longitude
      );
      return distance <= threshold;
    });

    // Sort locations by distance
    filteredLocations.sort((a: any, b: any) => {
      const distA = calculateDistance(
        myLocation.lat,
        myLocation.lng,
        a.latitude,
        a.longitude
      );
      const distB = calculateDistance(
        myLocation.lat,
        myLocation.lng,
        b.latitude,
        b.longitude
      );
      return distA - distB;
    });

    return filteredLocations;
  };

  return (
    <>
      {myLocation && (
        <section className="relative container z-10">
          <Divider />
          <div className="relative">
            <div className="items-center mb-[2rem]">
              <div className="flex justify-between items-center">
                <h2 className="uppercase text-[#112437]">
                  {f({ id: "near-my-location" })}
                </h2>

                <Link href={"/map"}>
                  <div className="flex items-center gap-[1rem] hover:opacity-70">
                    <p className={`font-[700] text-[16px]`}>
                      {f({ id: "see-all" })}
                    </p>
                    <MdArrowForwardIos />
                  </div>
                </Link>
              </div>
            </div>
            <LocationSwiper locations={filterAndSortLocations(data?.data)} />
          </div>
        </section>
      )}
    </>
  );
};

export default Section5;
