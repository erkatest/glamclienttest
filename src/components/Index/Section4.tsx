import Link from "next/link";
import * as React from "react";
import useMessage from "../../hooks/useMessage";
import Slider from "react-slick";
import { useQuery } from "react-query";
import apiItem from "~/api/item";
interface IProps {}

const Section4 = (props: IProps) => {
  const [currentIndex, setCurrentIndex] = React.useState(2);
  const f = useMessage();

  // const { data } = useQuery(["special-items"], () => apiItem.specialItems(), {
  //   refetchInterval: false,
  //   refetchOnWindowFocus: false,
  // });

  const books = [
    {
      imageUrl: "/images/book0.png",
    },
    {
      imageUrl: "/images/book2.png",
    },
    {
      imageUrl: "/images/book1.png",
    },
    {
      imageUrl: "/images/book3.png",
    },
    {
      imageUrl: "/images/book4.png",
    },
  ];

  // React.useEffect(() => {
  //   setTimeout(() => {
  //     setCurrentIndex((index) => (index + 1 > 6 ? 0 : index + 1));
  //   }, 200);
  // }, [currentIndex]);

  return (
    <section className="relative min-h-screen py-20 z-20 -bottom-10">
      <img
        src={"/images/feature-bg-2.png"}
        className="absolute top-0 left-0 right-0 w-screen h-screen object-cover"
      />

      <div className="relative">
        <div className="container mx-auto px-8 max-w-7xl ">
          <div className="flex justify-between">
            <div className="flex items-center">
              <h1
                className="font-bold mb-5 text-4xl text-[#4d4d4d]"
                dangerouslySetInnerHTML={{ __html: f({ id: "feature-title" }) }}
              ></h1>
              <Link
                href={`/search/item?type=koha`}
                className="bg-c3 font-bold space-x-4 inline-flex px-6 py-2 rounded-full text-white items-center ml-20"
              >
                <span>{f({ id: "feature-btn" })}</span>
                <img src={"/images/arrow.svg"} />
              </Link>
            </div>
            <div className="flex items-center justify-end pt-[100px] space-x-5">
              <div className="cursor-pointer w-[43px] h-[43px] rounded-full border-2 border-c1 hover:opacity-60 flex items-center justify-center">
                <img
                  src="/images/arrow-dark.svg"
                  className="transform w-1/2 -rotate-180"
                />
              </div>
              <div className="cursor-pointer w-[43px] h-[43px] rounded-full border-2 border-c1 hover:opacity-60 flex items-center justify-center">
                <img src="/images/arrow-dark.svg" className="w-1/2" />
              </div>
            </div>
          </div>
          <p
            className="text-lg mb-10 font-medium text-[#767676]"
            dangerouslySetInnerHTML={{ __html: f({ id: "feature-desc" }) }}
          ></p>

          <div className="flex items-center">
            {[...books]?.map((book, i) => (
              <div key={`book-${i}`}>
                <div
                  onClick={() => {
                    setCurrentIndex(i);
                  }}
                  className={`w-[250px] flex items-center justify-center h-[300px] cursor-pointer ${
                    i === currentIndex ? "" : ""
                  }`}
                >
                  <img
                    src={book.imageUrl}
                    className={`transition-all duration-500 hover:w-[250px] ${
                      i === currentIndex ? "w-[200px]" : "w-[200px]"
                    }`}
                  />
                </div>
              </div>
            ))}
          </div>
          <img src="/images/feature-footer.png" className="mt-10" />
        </div>
      </div>
    </section>
  );
};

export default Section4;
