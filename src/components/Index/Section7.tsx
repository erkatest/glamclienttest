import Link from "next/link";
import * as React from "react";
import useMessage from "../../hooks/useMessage";
import Slider from "react-slick";
import { TabContext } from "../../context/tab";

interface IProps {}

const Section7 = (props: IProps) => {
  const f = useMessage();
  const {
    setOpen: setModalOpen,
    setShowModal,
  } = React.useContext(TabContext);

  const settings = {
    className: "center",
    centerMode: true,
    // centerPadding: "200px",
    infinite: true,
    slidesToShow: 4,
    swipeToSlide: true,
    autoplay: false,
    responsive: [
      {
        breakpoint: 1024,
        settings: {
          slidesToShow: 3,
          slidesToScroll: 3,
          infinite: true,
          dots: true,
          centerPadding: "100px",
        },
      },
      {
        breakpoint: 600,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
          initialSlide: 1,
          centerPadding: "50px",
        },
      },
      {
        breakpoint: 480,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
          centerPadding: "50px",
        },
      },
    ],
  };

  const item = [
    {
      name: "Монгол толь",
      href: "https://mongoltoli.mn/",
      image: "/images/link-1.png",
    },
    {
      name: "Монголын үндэсний түүхийн музей",
      href: "http://nationalmuseum.mn/",
      image: "/images/link-2.png",
    },
    {
      name: "Монгол улсын үндэсний номын сан",
      href: "https://nationallibrary.mn/",
      image: "/images/link-3.png",
    },
    {
      name: "Чингис хаан музей",
      href: "https://chinggismuseum.com/",
      image: "/images/link-4.png",
    },
    {
      name: "Чингис хаан музей",
      href: "https://chinggismuseum.com/",
      image: "/images/link-4.png",
    },
  ];

  return (
    <section>
      <div className="relative py-[3rem] justify-center">
        <div className="container">
          <div className="flex justify-between items-center">
            <div className="flex w-4/5 items-center lg:space-x-10">
              <img src="/images/logo-black.svg" className="hidden lg:block" />
              <div>
                <h3 className="font-bold uppercase text-2xl lg:text-3xl mb-2 text-default">
                  <img
                    src="/images/logo-black.svg"
                    className="w-[70px] mb-4 lg:hidden"
                  />{" "}
                  {f({ id: "subscribe-title" })}
                </h3>
                <p className="lg:text-xl">{f({ id: "subscribe-desc" })}</p>
              </div>
            </div>
            <button>
              <div
                className="cursor-pointer w-[43px] h-[43px] rounded-full border-2 border-default hover:opacity-60 flex items-center justify-center"
                onClick={() => {
                  setShowModal(true);
                  setModalOpen("signup");
                }}
              >
                <img src="/images/arrow-dark.svg" className="w-1/2" />
              </div>
            </button>
          </div>
        </div>
        {/* <div className="flex-none md:flex bg-gradient-to-b from-white to-blue-100 justify-center items-center">
        <img src="/images/mobile.png" />
        <div className="bg-[#E6EAF4] rounded-2xl w-auto h-auto p-8 flex flex-col justify-center items-center col-span-1">
          <img src="/images/qr-code.png" className="mb-16" />
          <div className="flex flex-row justify-center items-center space-x-2">
            <Link href="/">
              <img
                // className="w-[40%] md:w-[20%] lg:w-[40%]"
                className="w-32 h-10"
                src="/images/apple.png"
              />
            </Link>
            <Link href="/">
              <img
                className="w-32 h-10"
                // className="w-[40%] md:w-[20%] lg:w-[40%]"
                src="/images/google.png"
              />
            </Link>
          </div>
        </div>
      </div> */}
        {/* <div className="flex justify-center mt-36">
          <div className="flex bg-[#011C5A] h-100vh rounded-xl container mx-auto relative max-w-7xl">
            <img
              src="/images/footer.png"
              alt="footer"
              className="absolute light-0 top-0 bottom-0"
            />
            <div className="flex items-center ml-[140px] space-x-10">
              <img
                src="/images/qr-code.png"
                alt="qr-code"
                className="w-44 h-44 mb-10"
              />
              <div className="mb-10">
                <h1 className="font-bold text-white text-3xl mb-4 w-96">
                  {f({ id: "suggest-install-app" })}
                </h1>
                <div className="flex space-x-5">
                  <img
                    src="/images/apple.png"
                    alt="apple"
                    className="hover:opacity-75 cursor-pointer"
                  />
                  <img
                    src="/images/google.png"
                    alt="google"
                    className="hover:opacity-75 cursor-pointer"
                  />
                </div>
              </div>
              <img
                src="/images/mobile.png"
                alt="qr-code"
                className="mt-[-60px]"
              />
            </div>
          </div>
        </div> */}
      </div>
      {/* <div className="my-20">
        <Slider ref={(c) => (sliderRef.current = c)} {...settings}>
          {(item || []).map((item: any, i: number) => {
            return (
              <div key={`link-${i}`}>
                <Link href={item?.href}>
                  <div className="flex items-center mx-8 my-4">
                    <div className="shadow-[rgba(0,_0,_0,_0.1)_0px_3px_8px] h-[127px] w-[127px] flex items-center justify-center">
                      <img src={item?.image} className="mx-16" />
                    </div>
                    <p className="ml-6">{item?.name}</p>
                  </div>
                </Link>
              </div>
            );
          })}
        </Slider>
      </div> */}
    </section>
  );
};

export default Section7;
