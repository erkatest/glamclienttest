import * as React from "react";
import useMessage from "../../hooks/useMessage";
import { useRouter } from "next/router";
import Link from "next/link";
import { MdArrowForwardIos } from "react-icons/md";
import Divider from "../Divider";
import { useQuery } from "react-query";
import apiItem from "~/api/item";
import { TabContext } from "~/context/tab";
interface IProps {}

const Section2 = (props: IProps) => {
  const { eng, setActive } = React.useContext(TabContext);
  const f = useMessage();
  const { data, isLoading } = useQuery(
    ["content-cards"],
    () => apiItem.contentCards(),
    {
      refetchOnWindowFocus: false,
      refetchInterval: false,
    }
  );
  const colors = [
    {
      color: "text-[#BE3455]",
      url: "/gallery",
    },
    {
      color: "text-[#01417A]",
      url: "/library",
    },
    {
      color: "text-[#2E294E]",
      url: "/archive",
    },
    {
      color: "text-[#13452F]",
      url: "/museum",
    },
  ];

  return (
    <section className="relative">
      <div className="relative z-1">
        <div className="container">
          <Divider />

          <div className="grid grid-cols-1 gap-12 mb-20">
            {!isLoading &&
              data?.data?.map((item: any, i: number) => (
                <div
                  key={`section2-${item?._id}`}
                  className={`bg-[#f2f2f2] overflow-hidden grid grid-cols-1 md:grid-cols-2 transition-all relative md:aspect-[3/1]`}
                >
                  {i % 2 !== 0 && (
                    <>
                      <div
                        className="image-container hidden md:block overflow-hidden"
                        style={{
                          backgroundImage: `url(${item.img})`,
                          backgroundSize: "cover",
                          backgroundPosition: "center",
                        }}
                      >
                        <img
                          className="scale-100 hover:scale-150 transition ease-out duration-1000"
                          src={item.img}
                          alt=""
                        />
                      </div>
                    </>
                  )}

                  <div
                    className="image-container md:hidden"
                    style={{
                      backgroundImage: `url(${item.img})`,
                      backgroundSize: "cover",
                      backgroundPosition: "center",
                    }}
                  >
                    <img className="" src={item.img} alt="" />
                  </div>

                  <div className="p-[2rem] md:p-[3rem] flex flex-col justify-center items-left">
                    <h1 className={`${colors[i]?.color} uppercase text-[18px]`}>
                      {eng
                        ? item?.languages?.en?.name
                        : item?.languages?.mn?.name}
                    </h1>
                    <p className="mt-[0.75rem] text-[14px]">
                      {eng
                        ? item?.languages?.en?.description
                        : item?.languages?.mn?.description}
                    </p>
                    <Link
                      href={item?.link}
                      onClick={() => setActive(item?.link?.replace("/", ""))}
                    >
                      <div className="flex items-center gap-[1.25rem] mt-[1rem] hover:opacity-75">
                        <p
                          className={`font-[700] ${colors[i]?.color} text-[16px]`}
                        >
                          {f({ id: "continue" })}
                        </p>
                        <MdArrowForwardIos
                          className={`${colors[i]?.color} mt-[2px]`}
                        />
                      </div>
                    </Link>
                  </div>
                  {i % 2 === 0 && (
                    <>
                      <div
                        className="image-container hidden md:block overflow-hidden
                      "
                        style={{
                          backgroundImage: `url(${item.img})`,
                          backgroundSize: "cover",
                          backgroundPosition: "center",
                        }}
                      >
                        <img
                          src={item.img}
                          className="scale-100 hover:scale-150 transition ease-out duration-1000"
                          alt=""
                        />
                      </div>
                    </>
                  )}
                </div>
              ))}
          </div>
        </div>
      </div>
    </section>
  );
};

export default Section2;
