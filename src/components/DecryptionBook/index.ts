import crypto, { AES, enc, lib } from "crypto-js";
import NodeRSA from "node-rsa";

export default function bookDecryption(data: any, privateKey: any) {
  const { encryptedSymmetricKey, encryptedIv, content } = data;
  try {
    const pk = new NodeRSA(privateKey, "private");
    const decryptedKey = pk.decrypt(Buffer.from(encryptedSymmetricKey.data));
    const decryptedIv = pk.decrypt(Buffer.from(encryptedIv.data));
    let str = "";
    for (let i = 0; i < content.data.length; i++) {
      str += String.fromCharCode(content.data[i]);
    }
    const wordArray = enc.Latin1.parse(str);
    const decryptedData = AES.decrypt(
      {
        ciphertext: wordArray,
      } as any,
      lib.WordArray.create(decryptedKey as any),
      {
        iv: lib.WordArray.create(decryptedIv as any),
      }
    ).toString(enc.Base64);
    return decryptedData.toString();
  } catch (error) {
    console.log(error);
    // throw new Error(error);
  }
}
