import { Swiper, SwiperSlide } from "swiper/react";
import "swiper/css";

import ContentCard from "~/components/ContentCard";

const LocationSwiper = ({ locations = [] }: any) => {

  if (!locations || locations.length <= 0) return null;

  return (
    <>
      <Swiper
        height={300}
        breakpoints={{
          0: {
            slidesPerView: 1.5,
          },
          768: {
            slidesPerView: 2,
          },
          1024: {
            slidesPerView: 3,
          },
        }}
      >
        {locations.map((cat: any, i: number): any => {
          return (
            <SwiperSlide key={i} className="pr-2 md:pr-4">
              <ContentCard
                hideHeart
                url={`/map?id=${cat?._id}`}
                imageUrl={cat?.image}
                title={cat?.name}
                description={
                  <div className="flex mt-1 items-start gap-[0.5rem] h-[42px]">
                    <img
                      src="/images/location.svg"
                      alt=""
                      width={20}
                      height={20}
                    />
                    <p className="text-[14px]">{cat.address}</p>
                  </div>
                }
              />
            </SwiperSlide>
          );
        })}
      </Swiper>
    </>
  );
};

export default LocationSwiper;
