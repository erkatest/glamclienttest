import Link from "next/link";
import { useRouter } from "next/router";
import { IoIosCloseCircleOutline } from "react-icons/io";
import LogOutIcon from "~/assets/LogOutIcon";
import MyLibraryIcon from "~/assets/MyLibraryIcon";
import RightIcon from "~/assets/RightIcon";
import TimeIcon from "~/assets/TimeIcon";
import UserIcon from "~/assets/UserIcon";
import { useUser } from "~/context/auth";
import InquiryIcon from "~/assets/InquiryIcon";
import { useEffect, useState } from "react";

const ProfileBurger = ({
  setIsNavbarOpen,
  isNavbarOpen,
  setLogOpen,
  logOpen,
}: any) => {
  const router = useRouter();
  const { user, signout } = useUser();
  const [inquiryOpen, setInquiryOpen] = useState(false);

  useEffect(() => {
    if (
      router?.pathname === "/inquiry" ||
      router?.pathname === "/inquiryList"
    ) {
      setInquiryOpen(true);
    }
  }, [router]);

  return (
    <div
      className={`flex  flex-col lg:hidden py-14 bg-white ${
        isNavbarOpen ? "sidebar-open" : "sidebar-closed"
      } fixed inset-y-0 left-0 z-[999999]
   `}
    >
      <div
        className="absolute top-3 right-3 cursor-pointer"
        onClick={() => setIsNavbarOpen(false)}
      >
        <IoIosCloseCircleOutline size={24} />
      </div>
      <div className="space-y-[24px]">
        <div>
          <Link href="/profile">
            <div
              className={`${
                router?.pathname === "/profile"
                  ? "bg-[#A6B2C333] text-[#112437]"
                  : "text-[#A6B2C3]"
              } px-[16px] py-[14px]  flex space-x-[20px] items-center text-[20px] leading-[22px] -tracking-[0.408px] `}
            >
              <UserIcon color={router?.pathname === "/profile"} />
              <div>Хувийн мэдээлэл</div>
            </div>
          </Link>
        </div>
        {/* <div>
        <Link href="/notification">
          <div
            className={`${
              router?.pathname === "/notification"
                ? "bg-[#A6B2C333] text-[#112437]"
                : "text-[#A6B2C3]"
            } px-[16px] py-[14px]  flex space-x-[20px] items-center text-[20px] leading-[22px] -tracking-[0.408px] `}
          >
            <NotifIcon color={router?.pathname === "/notification"} />
            <div>Мэдэгдэл</div>
          </div>
        </Link>
      </div> */}
        <div>
          <Link href="/myLibrary">
            <div
              className={`${
                router?.pathname === "/myLibrary"
                  ? "bg-[#A6B2C333] text-[#112437]"
                  : "text-[#A6B2C3]"
              } px-[16px] py-[14px]  flex space-x-[20px] items-center text-[20px] leading-[22px] -tracking-[0.408px] `}
            >
              <MyLibraryIcon color={router?.pathname === "/myLibrary"} />
              <div>Миний булан</div>
            </div>
          </Link>
        </div>
        <div className="space-y-[16px]">
          <div
              onClick={() => setInquiryOpen(!inquiryOpen)}
            className={`${
              inquiryOpen ? " text-[#112437]" : "text-[#A6B2C3]"
            } px-[16px] py-[14px] cursor-pointer  flex justify-between items-center text-[20px] leading-[22px] -tracking-[0.408px] `}
          >
            <div className="space-x-[14px] flex items-center">
            <InquiryIcon color={inquiryOpen} />
              <div className="ml-[6px]">Лавлагаа</div>
            </div>
            <div className="cursor-pointer">
              <RightIcon open={inquiryOpen} />
            </div>
          </div>
          {inquiryOpen && (
            <div className="space-y-[16px]">
              <div>
                <Link href={"/inquiry"}>
                  <div
                    className={`${
                      router?.pathname === "/inquiry"
                        ? "bg-[#A6B2C333] text-[#112437]"
                        : "text-[#A6B2C3]"
                    } px-[48px] py-[14px]  flex space-x-[14px] items-center text-[20px] leading-[22px] -tracking-[0.408px] `}
                  >
                    <svg
                      xmlns="http://www.w3.org/2000/svg"
                      width="10"
                      height="10"
                      viewBox="0 0 10 10"
                      fill="none"
                    >
                      <circle
                        cx="5"
                        cy="5"
                        r="5"
                        fill={
                          router?.pathname === "/inquiry"
                            ? "#112437"
                            : "#A6B2C3"
                        }
                      />
                    </svg>
                    <div>Лавлагаа бөглөх</div>
                  </div>
                </Link>
              </div>
              <div>
                <Link href={"/inquiryList"}>
                  <div
                    className={`${
                      router?.pathname === "/inquiryList"
                        ? "bg-[#A6B2C333] text-[#112437]"
                        : "text-[#A6B2C3]"
                    } px-[48px] py-[14px]  flex space-x-[14px] items-center text-[20px] leading-[22px] -tracking-[0.408px] `}
                  >
                    <svg
                      xmlns="http://www.w3.org/2000/svg"
                      width="10"
                      height="10"
                      viewBox="0 0 10 10"
                      fill="none"
                    >
                      <circle
                        cx="5"
                        cy="5"
                        r="5"
                        fill={
                          router?.pathname === "/inquiryList"
                            ? "#112437"
                            : "#A6B2C3"
                        }
                      />
                    </svg>
                    <div>Лавлагааны жагсаалт</div>
                  </div>
                </Link>
              </div>
            </div>
          )}
        </div>
        {/* <div>
        <Link href="/requests">
          <div
            className={`${
              router?.pathname === "/requests"
                ? "bg-[#A6B2C333] text-[#112437]"
                : "text-[#A6B2C3]"
            } px-[16px] py-[14px]  flex space-x-[20px] items-center text-[20px] leading-[22px] -tracking-[0.408px] `}
          >
            <RequestIcon color={router?.pathname === "/requests"} />
            <div>Хүсэлт илгээх</div>
          </div>
        </Link>
      </div> */}

        <div className="space-y-[16px]">
          <div
            onClick={() => setLogOpen(!logOpen)}
            className={`${
              logOpen ? " text-[#112437]" : "text-[#A6B2C3]"
            } px-[16px] py-[14px] cursor-pointer  flex justify-between items-center text-[20px] leading-[22px] -tracking-[0.408px] `}
          >
            <div className="space-x-[14px] flex items-center">
              <TimeIcon color={logOpen} />
              <div className="ml-[6px]">Ашиглалтын түүх</div>
            </div>
            <div className="cursor-pointer">
              <RightIcon open={logOpen} />
            </div>
          </div>
          {logOpen && (
            <div className="space-y-[16px]">
              <div>
                <Link href={"/search-log"}>
                  <div
                    className={`${
                      router?.pathname === "/search-log"
                        ? "bg-[#A6B2C333] text-[#112437]"
                        : "text-[#A6B2C3]"
                    } px-[48px] py-[14px]  flex space-x-[14px] items-center text-[20px] leading-[22px] -tracking-[0.408px] `}
                  >
                    <svg
                      xmlns="http://www.w3.org/2000/svg"
                      width="10"
                      height="10"
                      viewBox="0 0 10 10"
                      fill="none"
                    >
                      <circle
                        cx="5"
                        cy="5"
                        r="5"
                        fill={
                          router?.pathname === "/search-log"
                            ? "#112437"
                            : "#A6B2C3"
                        }
                      />
                    </svg>
                    <div>Хайлтын түүх</div>
                  </div>
                </Link>
              </div>
              <div>
                <Link href={"/views-log"}>
                  <div
                    className={`${
                      router?.pathname === "/views-log"
                        ? "bg-[#A6B2C333] text-[#112437]"
                        : "text-[#A6B2C3]"
                    } px-[48px] py-[14px]  flex space-x-[14px] items-center text-[20px] leading-[22px] -tracking-[0.408px] `}
                  >
                    <svg
                      xmlns="http://www.w3.org/2000/svg"
                      width="10"
                      height="10"
                      viewBox="0 0 10 10"
                      fill="none"
                    >
                      <circle
                        cx="5"
                        cy="5"
                        r="5"
                        fill={
                          router?.pathname === "/views-log"
                            ? "#112437"
                            : "#A6B2C3"
                        }
                      />
                    </svg>
                    <div>Үзэлтийн түүх</div>
                  </div>
                </Link>
              </div>
            </div>
          )}
        </div>
        <div onClick={signout} className="cursor-pointer">
          <div
            className={`text-[#FF0F00] px-[16px] py-[14px]  flex space-x-[20px] items-center text-[20px] leading-[22px] -tracking-[0.408px] `}
          >
            <LogOutIcon />
            <div>Холболт салгах</div>
          </div>
        </div>
      </div>
    </div>
  );
};
export default ProfileBurger;
