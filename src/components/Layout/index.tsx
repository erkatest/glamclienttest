import * as React from "react";
import Footer from "../Footer";
import Header from "../Header";
import Widget from "../Widget";
import { useState } from "react";

interface IProps {
  header?: any;
  footer?: any;
  children: any;
}

const DefaultLayout = (props: IProps) => {
  const [profileDropdown, serProfileDropdown] = useState(false);

  return (
    <main
      onClick={() => profileDropdown && serProfileDropdown(false)}
      className="font-lora min-h-screen flex flex-col"
    >
      {/* <Widget /> */}
      <Header
        {...(props.header || {})}
        serProfileDropdown={serProfileDropdown}
        profileDropdown={profileDropdown}
      />
      <main className={`flex-1 pb-[60px] py-[48px]`}>
        {props.children}
      </main>

      <Footer {...(props.footer || {})} />
    </main>
  );
};

export default DefaultLayout;
