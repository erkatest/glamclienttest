import Hamburger from "hamburger-react";
import Link from "next/link";
import { useRouter } from "next/router";
import { useEffect, useState } from "react";
import { IoIosCloseCircleOutline } from "react-icons/io";
import { useIntl } from "react-intl";
import LogOutIcon from "~/assets/LogOutIcon";
import MyLibraryIcon from "~/assets/MyLibraryIcon";
import InquiryIcon from "~/assets/InquiryIcon";
import NotifIcon from "~/assets/NotifIcon";
import ProfilePageIcon from "~/assets/ProfilePageIcon";
import RequestIcon from "~/assets/RequestIcon";
import RightIcon from "~/assets/RightIcon";
import TimeIcon from "~/assets/TimeIcon";
import UserIcon from "~/assets/UserIcon";
import { useUser } from "~/context/auth";
import ProfileBurger from "./ProfileBurger";
export interface LayoutProps {
  children: React.ReactNode;
}

const ProfileLayout = ({ children }: LayoutProps) => {
  const [isNavbarOpen, setIsNavbarOpen] = useState(false);
  const router = useRouter();
  const intl = useIntl();
  const [logOpen, setLogOpen] = useState(false);
  const [inquiryOpen, setInquiryOpen] = useState(false);
  const { user, signout } = useUser();

  useEffect(() => {
    if (
      router?.pathname === "/search-log" ||
      router?.pathname === "/views-log"
    ) {
      setLogOpen(true);
    }
  }, [router]);

  useEffect(() => {
    if (
      router?.pathname === "/inquiry" ||
      router?.pathname === "/inquiryList"
    ) {
      setInquiryOpen(true);
    }
  }, [router]);

  return (
    <div className="container mx-auto  px-[16px]">
      {/* <ProfileBurgerNavbar opened={opened} onClose={close} /> */}

      <div className="lg:hidden ">
        <button onClick={() => setIsNavbarOpen(!isNavbarOpen)}>
          <Hamburger
            // color={isOpen ? "#021c5a" : isIndex ? "#fff" : "#021c5a"}
            size={24}
            toggled={isNavbarOpen}
            toggle={setIsNavbarOpen}
          />
        </button>
      </div>
      <ProfileBurger
        setIsNavbarOpen={setIsNavbarOpen}
        isNavbarOpen={isNavbarOpen}
        setLogOpen={setLogOpen}
        logOpen={logOpen}
      />
      <div className={`flex lg:space-x-[32px] `}>
        <div className="w-1/4 hidden lg:block space-y-[24px]">
          <div>
            <Link href="/profile">
              <div
                className={`${
                  router?.pathname === "/profile"
                    ? "bg-[#A6B2C333] text-[#112437] font-[600]"
                    : "text-[#A6B2C3]"
                } p-[1rem] text-[17px] rounded-full  flex space-x-[20px] items-center leading-[22px] -tracking-[0.408px] `}
              >
                <UserIcon color={router?.pathname === "/profile"} />
                <div>Хувийн мэдээлэл</div>
              </div>
            </Link>
          </div>
          {/* <div>
            <Link href="/notification">
              <div
                className={`${
                  router?.pathname === "/notification"
                    ? "bg-[#A6B2C333] text-[#112437] font-[600]"
                    : "text-[#A6B2C3]"
                } p-[1rem] text-[17px] rounded-full  flex space-x-[20px] items-center leading-[22px] -tracking-[0.408px] `}
              >
                <NotifIcon color={router?.pathname === "/notification"} />
                <div>Мэдэгдэл</div>
              </div>
            </Link>
          </div> */}
          <div>
            <Link href="/myLibrary">
              <div
                className={`${
                  router?.pathname === "/myLibrary"
                    ? "bg-[#A6B2C333] text-[#112437] font-[600]"
                    : "text-[#A6B2C3]"
                } p-[1rem] text-[17px] rounded-full  flex space-x-[20px] items-center leading-[22px] -tracking-[0.408px] `}
              >
                <MyLibraryIcon color={router?.pathname === "/myLibrary"} />
                <div>Миний булан</div>
              </div>
            </Link>
          </div>

          <div className="space-y-[16px]">
            <div
              onClick={() => setInquiryOpen(!inquiryOpen)}
              className={`${
                inquiryOpen ? " text-[#112437]" : "text-[#A6B2C3]"
              } p-[1rem] text-[17px] rounded-full cursor-pointer  flex justify-between items-center leading-[22px] -tracking-[0.408px] `}
            >
              <div className="space-x-[14px] flex items-center">
                <InquiryIcon color={inquiryOpen} />
                <div className="ml-[6px]">Лавлагаа</div>
              </div>
              <div className="cursor-pointer">
                <RightIcon open={inquiryOpen} />
              </div>
            </div>
            {inquiryOpen && (
              <div className="space-y-[16px]">
                <div>
                  <Link href={"/inquiry"}>
                    <div
                    className={`${
                      router?.pathname === "/inquiry"
                        ? "bg-[#A6B2C333] text-[#112437] font-[600]"
                        : "text-[#A6B2C3]"
                    } px-[48px] py-[14px] rounded-full  flex space-x-[14px] items-center leading-[22px] -tracking-[0.408px] `}
                    >
                      <svg
                        xmlns="http://www.w3.org/2000/svg"
                        width="10"
                        height="10"
                        viewBox="0 0 10 10"
                        fill="none"
                      >
                        <circle
                          cx="5"
                          cy="5"
                          r="5"
                          fill={
                            router?.pathname === "/inquiry"
                              ? "#112437"
                              : "#A6B2C3"
                          }
                        />
                      </svg>
                      <div>Лавлагаа бөглөх</div>
                    </div>
                  </Link>
                </div>
                <div>
                  <Link href={"inquiryList"}>
                    <div
                      className={`${
                        router?.pathname === "/inquiryList"
                          ? "bg-[#A6B2C333] text-[#112437] font-[600]"
                          : "text-[#A6B2C3]"
                      } px-[48px] py-[14px] rounded-full  flex space-x-[14px] items-center leading-[22px] -tracking-[0.408px] `}
                    >
                      <svg
                        xmlns="http://www.w3.org/2000/svg"
                        width="10"
                        height="10"
                        viewBox="0 0 10 10"
                        fill="none"
                      >
                        <circle
                          cx="5"
                          cy="5"
                          r="5"
                          fill={
                            router?.pathname === "/inquiryList"
                              ? "#112437"
                              : "#A6B2C3"
                          }
                        />
                      </svg>
                      <div>Лавлагааны жагсаалт</div>
                    </div>
                  </Link>
                </div>
              </div>
            )}
          </div>

          <div className="space-y-[16px]">
            <div
              onClick={() => setLogOpen(!logOpen)}
              className={`${
                logOpen ? " text-[#112437]" : "text-[#A6B2C3]"
              } p-[1rem] text-[17px] rounded-full cursor-pointer  flex justify-between items-center leading-[22px] -tracking-[0.408px] `}
            >
              <div className="space-x-[14px] flex items-center">
                <TimeIcon color={logOpen} />
                <div className="ml-[6px]">Ашиглалтын түүх</div>
              </div>
              <div className="cursor-pointer">
                <RightIcon open={logOpen} />
              </div>
            </div>
            {logOpen && (
              <div className="space-y-[16px]">
                <div>
                  <Link href={"/search-log"}>
                    <div
                      className={`${
                        router?.pathname === "/search-log"
                          ? "bg-[#A6B2C333] text-[#112437] font-[600]"
                          : "text-[#A6B2C3]"
                      } px-[48px] py-[14px] rounded-full  flex space-x-[14px] items-center leading-[22px] -tracking-[0.408px] `}
                    >
                      <svg
                        xmlns="http://www.w3.org/2000/svg"
                        width="10"
                        height="10"
                        viewBox="0 0 10 10"
                        fill="none"
                      >
                        <circle
                          cx="5"
                          cy="5"
                          r="5"
                          fill={
                            router?.pathname === "/search-log"
                              ? "#112437"
                              : "#A6B2C3"
                          }
                        />
                      </svg>
                      <div>Хайлтын түүх</div>
                    </div>
                  </Link>
                </div>
                <div>
                  <Link href={"/views-log"}>
                    <div
                      className={`${
                        router?.pathname === "/views-log"
                          ? "bg-[#A6B2C333] text-[#112437] font-[600]"
                          : "text-[#A6B2C3]"
                      } px-[48px] py-[14px] rounded-full  flex space-x-[14px] items-center leading-[22px] -tracking-[0.408px] `}
                    >
                      <svg
                        xmlns="http://www.w3.org/2000/svg"
                        width="10"
                        height="10"
                        viewBox="0 0 10 10"
                        fill="none"
                      >
                        <circle
                          cx="5"
                          cy="5"
                          r="5"
                          fill={
                            router?.pathname === "/views-log"
                              ? "#112437"
                              : "#A6B2C3"
                          }
                        />
                      </svg>
                      <div>Үзэлтийн түүх</div>
                    </div>
                  </Link>
                </div>
              </div>
            )}
          </div>
          {/* <div>
            <Link href="/requests">
              <div
                className={`${
                  router?.pathname === "/requests"
                    ? "bg-[#A6B2C333] text-[#112437] font-[600]"
                    : "text-[#A6B2C3]"
                } p-[1rem] text-[17px] rounded-full  flex space-x-[20px] items-center leading-[22px] -tracking-[0.408px] `}
              >
                <RequestIcon color={router?.pathname === "/requests"} />
                <div>Хүсэлт илгээх</div>
              </div>
            </Link>
          </div> */}
          <div onClick={signout} className="cursor-pointer">
            <div
              className={`text-[#FF0F00] p-[1rem] text-[17px] rounded-full  flex space-x-[20px] items-center leading-[22px] -tracking-[0.408px] `}
            >
              <LogOutIcon />
              <div>Холболт салгах</div>
            </div>
          </div>
        </div>
        {isNavbarOpen && (
          <div
            className="fixed inset-0 bg-black opacity-50 z-[99999]"
            onClick={() => setIsNavbarOpen(false)}
          ></div>
        )}
        <div className="relative lg:w-3/4 w-full lg:border-l border-[#011C5A] mb-[100px]">
          <div className="absolute -left-5 top-1/2 transform -translate-y-1/2 hidden lg:block">
            <ProfilePageIcon />
          </div>
          {children}
        </div>
      </div>
    </div>
  );
};

export default ProfileLayout;
