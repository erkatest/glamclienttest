import React from "react";
import { useQuery } from "react-query";
import Link from "next/link";
import { Swiper, SwiperSlide } from "swiper/react";
import { EffectCoverflow } from "swiper/modules";

import apiItem from "~/api/item";
import Spinner from "../libs/Spinner";
import useMessage from "~/hooks/useMessage";

import "swiper/css";
import "swiper/css/effect-coverflow";
import "swiper/css/pagination";
import { TabContext } from "~/context/tab";

const CategorySwiper = () => {
  const f = useMessage();
  const { setActiveArtwork, setActive } = React.useContext(TabContext);
  const { data: artworkCategory, isLoading: artworkCategoryIsLoading } =
    useQuery(["artworkCategory"], () => apiItem.category(), {
      refetchOnWindowFocus: false,
      refetchInterval: false,
    });

  return (
    <div className="container mx-auto">
      {!artworkCategoryIsLoading ? (
        <Swiper
          effect={"coverflow"}
          watchSlidesProgress
          grabCursor={true}
          setWrapperSize={true}
          loopAdditionalSlides={5}
          loopPreventsSliding
          centeredSlides={true}
          slidesPerView={4}
          spaceBetween={0}
          initialSlide={10}
          breakpoints={{
            0: {
              slidesPerView: 1.5,
            },
            640: {
              slidesPerView: 2,
            },
            768: {
              slidesPerView: 3,
            },
            1024: {
              slidesPerView: 4,
            },
          }}
          coverflowEffect={{
            rotate: 0,
            stretch: 0,
            depth: 80,
            modifier: 1,
          }}
          loop={true}
          modules={[EffectCoverflow]}
          className="mySwiper"
        >
          {artworkCategory?.data?.map((e: any) => {
            return (
              <SwiperSlide key={e._id} className="space-y-4">
                <div
                  className="aspect-square p-[3rem] flex flex-col items-center justify-center space-y-4"
                  style={{
                    backgroundImage: `linear-gradient(rgba(0, 0, 0, 0.4), rgba(0, 0, 0, 0.4)), url(${
                      e.img !== "" ? e.img : "/images/empty.svg"
                    })`,
                    backgroundSize: "cover",
                    backgroundPosition: "center",
                    backgroundColor: e.img === "" ? "white" : "",
                  }}
                >
                  <h1 className="text-center text-white text-[24px] font=[600] tracking-wider">
                    {e.name}
                  </h1>

                  <div className="h-[1px] bg-white w-[60%]"></div>

                  <Link
                    href={"/search/?q="}
                    onClick={() => {
                      setActiveArtwork(e._id);
                      setActive("gallery");
                    }}
                  >
                    <button className="text-[15px] font-lora tracking-wide relative z-10 rounded-[1.25rem] border-[1px] p-3 px-4 mt-1  text-white border-white font-[700] hover:bg-white hover:text-black">
                      {f({ id: "see-next" })}
                    </button>
                  </Link>
                </div>
              </SwiperSlide>
            );
          })}
        </Swiper>
      ) : (
        <div className="flex items-center justify-center h-[300px]">
          <Spinner />
        </div>
      )}
    </div>
  );
};

export default CategorySwiper;
