import React from "react";
import { useQuery } from "react-query";

import useParams from "~/hooks/useParams";
import apiItem from "~/api/item";
import ContentCard from "~/components/ContentCard";
import { TabContext } from "~/context/tab";
import { Skeleton } from "@mantine/core";
import ContentSkeleton from "../ContentCard/Skeleton";

const Creations = () => {
  const [page, setPage] = React.useState(1);
  const params = useParams();

  const { activeTabProps } = React.useContext(TabContext);

  const { data, isLoading } = useQuery(
    ["artwork", activeTabProps.param, page, params.q || ""],
    () => apiItem.find(activeTabProps.param, page, 8, params.q)
  );
  return (
    <div className="grid grid-cols-2 md:grid-cols-3 lg:grid-cols-4 gap-4 gap-y-6 container mx-auto ">
      {isLoading &&
        [0, 1, 2, 3, 4, 5, 6, 7, 8].map((i: any) => {
          return <ContentSkeleton key={i} />;
        })}
      {!isLoading &&
        data?.data?.map((item: any, index: number) => {
          return (
            <ContentCard
              key={`content-${item._id}${index}`}
              imageUrl={item?.mainImg}
              title={item?.name}
              description={item?.category?.name}
              url={`/detail/${item?._id}/artwork`}
              id={item?._id}
              type={item?.type}
            />
          );
        })}
    </div>
  );
};

export default Creations;
