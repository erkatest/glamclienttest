import React from "react";
import { useQuery } from "react-query";

import apiCollections from "~/api/collections";
import ContentSkeleton from "../ContentCard/Skeleton";
import CollectionsCard from "../ContentCard/CollectionsCard";

const Collections = () => {
  const { data, isLoading } = useQuery(
    ["collections"],
    () => apiCollections.getCollections(),
    {
      refetchOnWindowFocus: false,
      refetchInterval: false,
    }
  );

  return (
    <div className="grid grid-cols-2 md:grid-cols-3 lg:grid-cols-4 gap-4 gap-y-6 container mx-auto ">
      {isLoading &&
        [0, 1, 2, 3].map((i: any) => {
          return <ContentSkeleton key={i} />;
        })}
      {!isLoading &&
        data?.data?.map((item: any, index: number) => {
          return (
            <CollectionsCard
              key={`content-${item._id}${index}`}
              name={item?.name}
              description={item?.description}
              url={`/collections/${item?._id}/artwork`}
            />
          );
        })}
    </div>
  );
};

export default Collections;
