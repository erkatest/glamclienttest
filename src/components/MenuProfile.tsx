import Link from "next/link";
import { useRouter } from "next/router";
import React from "react";
import { FaAngleDown, FaUserCircle } from "react-icons/fa";
import { useUser } from "~/context/auth";
import useMessage from "~/hooks/useMessage";

interface IProps {
  drop: boolean;
  setDrop: any;
}

const MenuProfile = ({ drop, setDrop }: IProps) => {
  const f = useMessage();
  const { user, signout } = useUser();

  React.useEffect(() => {
    document.body.addEventListener("click", () => {
      setDrop(false);
    });
  }, []);

  return (
    <div className="relative dark:text-[#BE8100]">
      <div
        className={`fixed inset-0 bg-black/10 backdrop-blur-sm ${
          drop ? "" : "hidden"
        }`}
      />
      <button
        onClick={e => {
          e.preventDefault();
          e.stopPropagation();
          setDrop((o: any) => !o);
        }}
        className="flex items-center transition-all group relative px-4 pb-[10px] font-semibold"
        type="button"
      >
        <FaUserCircle size={24} className="mr-2.5" />
        <div>{user?.firstname || ""}</div>
        <FaAngleDown size={24} className="ml-2.5" />
      </button>
      <div
        className={`z-10 absolute bg-white divide-y 
        w-64 divide-gray-100 rounded-lg shadow dark:bg-gray-700 dark:divide-gray-600 ${
          drop ? "" : "hidden"
        }`}
      >
        <div className="py-2 text-sm w-full text-gray-700 dark:text-gray-200">
          <Link
            href="#"
            className="flex items-center px-4 py-2 hover:bg-gray-100 dark:hover:bg-gray-600 dark:hover:text-white"
          >
            <img src="/images/menu-1.svg" className="mr-3 w-5" />
            {f({ id: "menu-1" })}
          </Link>
        </div>
        <ul className="py-2 text-sm text-gray-700 dark:text-gray-200">
          <li>
            <Link
              href="#"
              className="flex items-center px-4 py-2 hover:bg-gray-100 dark:hover:bg-gray-600 dark:hover:text-white"
            >
              <img src="/images/menu-2.svg" className="mr-3 w-5" />
              {f({ id: "menu-2" })}
            </Link>
          </li>
          <li>
            <Link
              href="/myCorner"
              className="flex items-center px-4 py-2 hover:bg-gray-100 dark:hover:bg-gray-600 dark:hover:text-white"
            >
              <img src="/images/menu-3.svg" className="mr-3 w-5" />
              {f({ id: "menu-3" })}
            </Link>
          </li>
          <li>
            <Link
              href="#"
              className="flex items-center px-4 py-2 hover:bg-gray-100 dark:hover:bg-gray-600 dark:hover:text-white"
            >
              <img src="/images/menu-4.svg" className="mr-3 w-5" />
              {f({ id: "menu-4" })}
            </Link>
          </li>
          <li>
            <Link
              href="/search-history"
              className="flex items-center px-4 py-2 hover:bg-gray-100 dark:hover:bg-gray-600 dark:hover:text-white"
            >
              <img src="/images/menu-5.svg" className="mr-3 w-5" />
              {f({ id: "menu-5" })}
            </Link>
          </li>
        </ul>
        <div className="py-2">
          <Link
            onClick={signout}
            href="/"
            className="flex items-center px-4 py-2 text-sm text-gray-700 hover:bg-gray-100 dark:hover:bg-gray-600 dark:text-gray-200 dark:hover:text-white"
          >
            <img src="/images/menu-6.svg" className="mr-3 w-5" />
            <p className="text-[#FF0F00]">{f({ id: "menu-6" })}</p>
          </Link>
        </div>
      </div>
    </div>
  );
};

export default MenuProfile;
