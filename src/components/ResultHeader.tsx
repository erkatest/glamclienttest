import * as React from "react";
import CustomSelect from "./libs/CustomSelect";
import { FaList } from "react-icons/fa";
import { BsGrid3X3GapFill } from "react-icons/bs";
import useMessage from "~/hooks/useMessage";
import useParams from "~/hooks/useParams";
import { useRouter } from "next/router";
interface IProps {
  setView?: any;
  view?: any;
  total: number;
}

const ResultHeader = ({ setView, view, total }: IProps) => {
  const f = useMessage();
  const router = useRouter();
  const params = useParams();
  return (
    <section className="flex flex-col lg:flex-row justify-between items-center border-b border-c1 pb-10">
      <div className="w-full mb-5 lg:mb-0">
        <strong className="font-bold">{total} </strong>
        {f({ id: "item-total" })}
        {params.q && (
          <span className="bg-[#E6EAF4] rounded-[20px] px-[12px] inline-block mx-4">
            <div className="flex items-center space-x-2 py-1">
              <p className="text-[#011C5A]">{params.q}</p>
              <img
                src="/images/x-icon-circle.svg"
                className="cursor-pointer"
                onClick={() => {
                  router.push({ search: `?q=&type=${params.type || ""}` });
                }}
              />
            </div>
          </span>
        )}
      </div>

      <div className="flex flex-1 w-full lg:w-auto items-center justify-between space-x-10">
        <div className="w-72">
          <CustomSelect
            defaultValue=""
            items={[
              {
                label: "Эрэмбэ сонгоно уу",
                value: "",
              },
              {
                label: "Шинэ бүтээл эхэндээ",
                value: "1",
              },
              {
                label: "Хуучин бүтээл эхэндээ",
                value: "2",
              },
              {
                label: "Бүтээлийн нэрээр (A-Я, A-Z)",
                value: "3",
              },
              {
                label: "Бүтээлийн нэрээр (Я-А, Z-A)",
                value: "4",
              },
            ]}
          />
        </div>
        <div className="flex space-x-4">
          <FaList
            onClick={() => setView("list")}
            size={24}
            className={`cursor-pointer hover:opacity-80 active:opacity-100 ${
              view !== "grid" ? "text-c1" : "text-gray-300"
            }`}
          />
          <BsGrid3X3GapFill
            onClick={() => setView("grid")}
            size={24}
            className={`cursor-pointer hover:opacity-80 active:opacity-100 ${
              view === "grid" ? "text-c1" : "text-gray-300"
            }`}
          />
        </div>
      </div>
    </section>
  );
};

export default ResultHeader;
