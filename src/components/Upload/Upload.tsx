import { TextInput, useMantineTheme, Text, Group, Box } from "@mantine/core";
import message from "./Message";
import * as React from "react";

import { IconUpload, IconPhoto, IconX } from "@tabler/icons-react";

import { Dropzone, DropzoneProps, IMAGE_MIME_TYPE } from "@mantine/dropzone";
interface IProps {
  value: any;
  onChange: (value: any) => void;
  handleFile?: (file: any) => void;
  field?: any;
  getImageUrl?: any;
  previewStyle?: any;
  clearFile: any;
  setClearFile: any;
}

const Upload = (props: IProps | any) => {
  const { previewStyle = {} } = props;
  const theme = useMantineTheme();
  const { value, onChange, handleFile, field, clearFile, setClearFile } = props;

  const [preview, setPreview] = React.useState<any>(props.value);
  const [file, setFile] = React.useState<any>(null);
  const handleChange = (files: any) => {
    const [file] = files;
    if (file) {
      const size = file.size / 1024 / 1024;
      if (size <= 3) {
        const reader = new FileReader();
        reader.readAsDataURL(file);
        reader.onload = function () {
          setPreview(reader.result);
          onChange && onChange(reader.result);
          setFile(file);
        };
        reader.onerror = function (error: any) {
          console.log("Error: ", error);
        };
      } else {
        message.warning(
          `Уучлаарай таны файлын хэмжээ ${size?.toFixed(
            2
          )}mb байна. 3mb аас бага байх ёстой!`
        );
      }
    }
  };
  React.useEffect(() => {
    if (clearFile) {
      // setFile(null);
      setPreview(null);
      // onChange(null);
    }
    setClearFile(false);
  }, [clearFile]);

  React.useEffect(() => {
    // if (value && value?.length < 20) {
    // 20 -> not base64
    // setPreview(`${props.getImageUrl(value)}`);
    // }
    if (value) {
      setPreview(value);
    }
  }, [value]);

  React.useEffect(() => {
    handleFile(file);
  }, [file]);

  return (
    <Box sx={{ maxWidth: "100%" }} mb={"lg"}>
      {field?.label && (
        <Text size="sm" mb="xs" className="mantine-InputWrapper-label">
          {field?.label}{" "}
          {field?.required && <span style={{ color: "#fa5252" }}>*</span>}
        </Text>
      )}

      <Dropzone
        onDrop={handleChange}
        onReject={(files) => {}}
        maxSize={6 * 1024 ** 2}
        multiple={false}
        accept={IMAGE_MIME_TYPE}
        maw={500}
        // miw={550}
        // maw={550}
        sx={(theme) => ({
          padding: 0,
          backgroundColor:
            theme.colorScheme === "dark"
              ? theme.colors.dark[6]
              : theme.colors.gray[0],
          borderRadius: 17,
          border: "1px solid #e5e7eb",
        })}
        {...props}
      >
        <Group
          position="center"
          spacing="xl"
          sx={(theme) => ({
            backgroundColor: "#eef0f3",
            borderRadius: 17,
          })}
          style={{
            pointerEvents: "none",
            padding: 16,
          }}
        >
          <Dropzone.Accept>
            <IconUpload size={50} stroke={1.5} />
          </Dropzone.Accept>
          <Dropzone.Reject>
            <IconX size={50} stroke={1.5} />
          </Dropzone.Reject>
          <Dropzone.Idle>
            {preview ? (
              <img
                src={preview}
                style={{
                  // width: "70%",
                  height: "100px",
                  margin: "0 auto",
                  marginBottom: 10,
                  ...previewStyle,
                }}
              />
            ) : (
              <IconPhoto size={40} stroke={1.5} />
            )}
          </Dropzone.Idle>

          <div>
            <Text
              align="center"
              size="md"
              inline
              color="#384455"
              className="font-lora"
            >
              {/* <Text align="center" weight="bold" size="sm" inline color="#384455"> */}
              Зураг оруулах эсвэл чирч оруулж болно.
            </Text>
            {/* <Text size="xs" inline mt={7} color="#868E96">
              Зургийн хэмжээ хамгийн ихдээ 4мб байх хэрэгтэй!
            </Text> */}
          </div>
        </Group>
      </Dropzone>
      {props?.error && (
        <span style={{ color: "#fa5252", fontSize: 12 }}>{props?.error}</span>
      )}
    </Box>
  );
};

export default Upload;
