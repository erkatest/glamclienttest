import { showNotification } from "@mantine/notifications";
import { IconCheck, IconExclamationMark, IconBug } from "@tabler/icons-react";

const message = {
  warning: (msg: string) => {
    showNotification({
      title: "Анхааруулга",
      message: msg,
    //   icon: <IconExclamationMark size={16} />,
      color: "yellow",

      styles: (theme) => ({
        root: {
          backgroundColor: theme.colors.white,

          "&::before": { backgroundColor: theme.white },
        },

        closeButton: {
          color: theme.white,
          "&:hover": { backgroundColor: theme.colors.green[6] },
        },
      }),
    });
  },
};
export default message;
