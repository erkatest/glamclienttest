import { useRouter } from "next/router";
import React, { useRef } from "react";
import { useEffect, useState } from "react";
import { VscChromeClose } from "react-icons/vsc";
import { useMutation, useQuery } from "react-query";
import apiItem from "~/api/item";
import LoadingOverly from "./libs/LoadingOverly";
import useMessage from "~/hooks/useMessage";
import { useUser } from "~/context/auth";

const SearchWidget = (props: { visible: boolean; onCancel: () => void }) => {
  const inputRef = useRef<any>();
  const f = useMessage();
  const router = useRouter();
  const { token } = useUser();
  const [show, setShow] = React.useState(false);
  const [type, setType] = React.useState(false);
  const [filter, setFilter] = React.useState(false);
  const [q, setQ] = useState("");
  const visible = props.visible;

  const items = [
    {
      title: f({ id: "about-1-title" }),
      image: "/images/v1.svg",
      type: "culture",
    },
    {
      title: f({ id: "about-2-title" }),
      image: "/images/v2.svg",
      type: "artwork",
    },
    {
      title: f({ id: "about-3-title" }),
      image: "/images/v3.svg",
      type: "koha",
    },
    {
      title: f({ id: "about-4-title" }),
      image: "/images/v4.svg",
      type: "education",
    },
    {
      title: f({ id: "about-5-title" }),
      image: "/images/v5.svg",
      type: "artist",
    },
  ];

  const { data } = useQuery(["common-search"], () => apiItem.commonSearch(8), {
    refetchOnWindowFocus: false,
    refetchInterval: false,
  });

  const { data: searchHistory, refetch: refetchHistory } = useQuery(
    ["history-search", token],
    () => token && apiItem.searchHistory(0, 8),
    {
      refetchOnWindowFocus: false,
      refetchInterval: false,
    }
  );

  const deleteMutation = useMutation(
    (id: string) => apiItem.deleteHistory(id),
    {
      onSuccess() {
        refetchHistory();
      },
    }
  );

  useEffect(() => {
    if (visible) {
      setTimeout(() => {
        setShow(true);
        inputRef.current?.focus();
      }, 500);
    } else {
      setShow(false);
    }
  }, [visible]);

  return (
    <section
      className={`fixed top-0 transition-all left-0 right-0 bottom-0 z-[999999] ${
        visible ? "visible opacity-100" : "invisible opacity-0"
      }`}
    >
      <div
        onClick={() => {
          props.onCancel();
        }}
        className="bg-black/30 absolute top-0 left-0 bottom-0 right-0"
      ></div>
      <div className="relative w-full bg-white">
        <div className="container relative bg-white z-20 mx-auto px-4">
          <div className="h-[80px] space-x-4 flex items-center w-full">
            <img
              src={"/images/arrow-dark.svg"}
              className="cursor-pointer rotate-180 w-5"
              onClick={() => props.onCancel()}
            />
            <input
              ref={inputRef}
              autoFocus
              placeholder={f({ id: "search-placeholder" })}
              className="w-full bg-white text-lg py-4 focus:outline-none"
              onChange={(e) => {
                setQ(e.target.value);
              }}
              onKeyDown={(e) => {
                if (e.key === "Enter") {
                  props.onCancel();
                  router.push({
                    pathname: "/search",
                    search: `?q=${q || ""}`,
                  });
                }
              }}
              value={q}
            />
            <img
              src="/images/detail-filter.svg"
              className="w-5 cursor-pointer"
              onClick={() => setFilter((o) => !o)}
            />
          </div>
        </div>
        <div
          className={`border-t absolute left-0 right-0 bg-white z-10 transition-all transform ${
            show ? "translate-y-0" : "-translate-y-[100%]"
          }`}
        >
          <div className="container mx-auto px-4">
            {filter ? (
              <div className="flex items-center my-4 mx-12 justify-center">
                {items?.map((item: any, i: number) => {
                  return (
                    <div
                      key={`types-${i}`}
                      onClick={() => {
                        setType(item?.type);
                      }}
                      className={`flex items-center justify-center w-full space-x-4 cursor-pointer ${
                        type === item?.type
                          ? "rounded-full py-2 bg-[#E6EAF4]"
                          : ""
                      }`}
                    >
                      <img src={item?.image} className="w-[40px] h-10" />
                      <p>{item?.title}</p>
                    </div>
                  );
                })}
              </div>
            ) : (
              <>
                <div className="flex flex-wrap py-5 lg:space-x-4">
                  {searchHistory?.data.map((txt: any, i: number) => (
                    <div
                      key={`text-${i}`}
                      className="flex cursor-pointer mr-4 mb-4 lg:m-2 whitespace-nowrap items-center space-x-2 p-2 py-1 bg-[#EEEEEE] text-[#55606B] rounded-[5px]"
                      onClick={() => {
                        setQ(txt?.value);
                        props.onCancel();
                        router.push({
                          pathname: "/search",
                          search: `?q=${txt?.value || ""}`,
                        });
                      }}
                    >
                      <span className="mx-4">{txt?.value}</span>
                      <VscChromeClose
                        size={19}
                        onClick={async (e) => {
                          e.preventDefault();
                          e.stopPropagation();
                          await deleteMutation.mutateAsync(txt?._id);
                        }}
                      />
                    </div>
                  ))}
                </div>

                <LoadingOverly visible={deleteMutation?.isLoading} />
                <h3 className="text-[14px] mb-1 text-[#767676]">
                  {f({ id: "common-search" })}
                </h3>
                <div className="flex flex-wrap py-5 lg:space-x-4">
                  {data?.data.map((txt: any, i: number) => (
                    <div
                      key={`text-${i}`}
                      className="flex cursor-pointer text-[14px] mr-4 mb-4 lg:m-0 whitespace-nowrap items-center space-x-2 p-2 py-1 bg-[#EEEEEE] text-[#55606B] rounded-[5px]"
                      onClick={() => {
                        setQ(txt?.value);
                        props.onCancel();
                        router.push({
                          pathname: "/search",
                          search: `?q=${txt?.value || ""}`,
                        });
                      }}
                    >
                      <span className="mx-4">{txt?.value}</span>
                    </div>
                  ))}
                </div>
              </>
            )}
          </div>
        </div>
      </div>
    </section>
  );
};

export default SearchWidget;
