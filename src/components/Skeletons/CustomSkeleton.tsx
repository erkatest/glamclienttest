import React from "react";

const CustomSkeleton = () => {
  return (
    <div className="flex flex-col animate-pulse space-y-4">
      <div className="bg-slate-300 p-[1rem] h-[300px] w-[220px]"></div>

      <div className="h-[20px] bg-slate-300"></div>
      <div className="h-[20px] w-[70%] bg-slate-300"></div>
    </div>
  );
};

export default CustomSkeleton;
