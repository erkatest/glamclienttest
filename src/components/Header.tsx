import Link from "next/link";
import * as React from "react";
import useMessage from "../hooks/useMessage";
import { Sling as Hamburger } from "hamburger-react";
import { useRouter } from "next/router";
import SearchWidget from "./SearchWidget";
import { useUser } from "~/context/auth";
import { TabContext } from "../context/tab";
import SignInSignUpModal from "~/components/Modals/SignInSignUpModal";
import UserIcon from "~/assets/UserIcon";
import RightIcon from "~/assets/RightIcon";
import TimeIcon from "~/assets/TimeIcon";
import LanguageButton from "./LanguageButton";
import store from "~/utils/store";
import LogOutIcon from "~/assets/LogOutIcon";
import MyLibraryIcon from "~/assets/MyLibraryIcon";
import InquiryIcon from "~/assets/InquiryIcon";
interface IProps {
  isIndex?: boolean;
  profileDropdown?: boolean;
  serProfileDropdown?: any;
}

const Header = (props: IProps) => {
  const router = useRouter();
  const f = useMessage();
  const { user, checkUserMe, signout } = useUser();
  const { pathname } = router;

  const [showSearch, setShowSearch] = React.useState(false);
  const [isOpen, setOpen] = React.useState(false);

  const {
    tabs,
    setActive,
    setOpen: setModalOpen,
    setShowModal,
    showModal,
    active,
  } = React.useContext(TabContext);

  const { isIndex, profileDropdown, serProfileDropdown } = props;

  React.useEffect(() => {
    if (store?.get("user") !== null) {
      checkUserMe();
    }
  }, []);

  const menuItems = [
    {
      title: f({ id: "header-menu-1" }),
      href: "/gallery",
      hoverColor: "hover:bg-[#BE3455]",
      backgroundColor: "bg-[#BE3455]",
      type: "gallery",
      type2: "artwork",
    },
    {
      title: f({ id: "header-menu-2" }),
      href: "/library",
      hoverColor: "hover:bg-[#01417A]",
      backgroundColor: "bg-[#01417A]",
      type: "library",
      type2: "koha",
    },
    {
      title: f({ id: "header-menu-3" }),
      href: "/archive",
      hoverColor: "hover:bg-[#2E294E]",
      backgroundColor: "bg-[#2E294E]",
      type: "archive",
      type2: "education",
    },
    {
      title: f({ id: "header-menu-4" }),
      href: "/museum",
      hoverColor: "hover:bg-[#13452F]",
      backgroundColor: "bg-[#13452F]",
      type: "museum",
      type2: "heritage",
    },
  ];

  React.useEffect(() => {
    if (typeof document !== "undefined") {
      document.body.style.overflow = isOpen ? "hidden" : "visible";
      document.body.style.height = isOpen ? "100vh" : "auto";
    }
  }, [isOpen]);

  const pathsWithDynamicBackground = ["/"];

  const buttonColor = `${
    pathsWithDynamicBackground.includes(pathname)
      ? "bg-default"
      : tabs.find((tab: any) => tab.type === active)?.backgroundColor
  }`;

  return (
    <>
      <SignInSignUpModal
        isOpen={showModal}
        onClose={() => setShowModal(false)}
      />

      <SearchWidget
        visible={showSearch}
        onCancel={() => {
          setShowSearch(false);
        }}
      />
      <div
        className={`top-0 left-0 right-0  z-[99999] shadow-sm ${
          isOpen ? "fixed" : "sticky"
        }`}
      >
        <section className=" bg-white opacity-95 flex px-4 w-full items-center justify-between">
          <div className="container p-0">
            <div className="flex items-center justify-between">
              {router?.pathname === "/" ? (
                <div
                  className="cursor-pointer"
                  onClick={() =>
                    window.scrollTo({
                      top: 0,
                      behavior: "smooth",
                    })
                  }
                >
                  {isIndex ? (
                    <>
                      {isOpen ? (
                        <img
                          src={"/images/logo-blue.svg"}
                          className="h-[35px] relative z-50 lg:h-[50px] lg:hidden"
                        />
                      ) : (
                        <img
                          src={"/images/logo-blue.svg"}
                          className="h-[35px] lg:h-[50px]"
                        />
                      )}
                    </>
                  ) : (
                    <img
                      src={"/images/logo-blue.svg"}
                      className="h-[35px] lg:h-[50px]"
                    />
                  )}
                </div>
              ) : (
                <Link href={"/"}>
                  {isIndex ? (
                    <>
                      {isOpen ? (
                        <img
                          src={"/images/logo-blue.svg"}
                          className="h-[35px] relative z-50 lg:h-[50px] lg:hidden"
                        />
                      ) : (
                        <img
                          src={"/images/logo-blue.svg"}
                          className="h-[35px] lg:h-[50px]"
                        />
                      )}
                    </>
                  ) : (
                    <img
                      src={"/images/logo-blue.svg"}
                      className="h-[35px] lg:h-[50px]"
                    />
                  )}
                </Link>
              )}

              <div className={`lg:hidden relative -right-[8px] z-50`}>
                <Hamburger size={24} toggled={isOpen} toggle={setOpen} />
              </div>
              <div
                className={`fixed z-[99999999] flex flex-col space-y-4 items-center justify-center transition-[opacity] top-[40px] h-max py-[2rem] left-0 right-0 transform bottom-0 bg-white shadow-lg ${
                  isOpen
                    ? "opacity-100 visible translate-y-0 lg:hidden"
                    : "opacity-0 invisible -translate-y-[200%]"
                }`}
              >
                {isOpen && user ? (
                  <div>
                    <div
                      className="relative cursor-pointer"
                      onClick={() => serProfileDropdown(!profileDropdown)}
                    >
                      <img
                        src={user?.img}
                        className="w-[48px] h-[48px] rounded-full object-cover border-[3px] border-[#112437]"
                      />
                      <div className="bg-[#112437] px-[4px] py-[6px] rounded-r-[4px] absolute top-1/2 transform -translate-y-1/2 -right-4">
                        <svg
                          xmlns="http://www.w3.org/2000/svg"
                          width="11"
                          height="6"
                          viewBox="0 0 11 6"
                          fill="none"
                        >
                          <path
                            d="M10.0003 1L6.3258 4.67453C5.89184 5.10849 5.18173 5.10849 4.74778 4.67453L1.07324 1"
                            stroke="white"
                            strokeWidth="0.901726"
                            strokeMiterlimit="10"
                            strokeLinecap="round"
                            strokeLinejoin="round"
                          />
                        </svg>
                      </div>
                    </div>
                    {profileDropdown && (
                      <div className="bg-white z-[99999] px-[20px] py-[14px] space-y-[10px] absolute right-20  top-[100px] ">
                        <div>
                          <Link href="/profile">
                            <div
                              className={`w-full justify-between flex space-x-[12px] py-[10px]  items-center text-[16px] leading-[22px] -tracking-[0.408px] `}
                            >
                              <div className="flex items-center space-x-[12px]">
                                <UserIcon color={true} size={true} />
                                <div>Хувийн мэдээлэл</div>
                              </div>
                              <RightIcon />
                            </div>
                          </Link>
                        </div>
                        <div className="border-t w-full border-[#A6B2C3]" />
                        <div>
                          <Link href="/myLibrary">
                            <div
                              className={`w-full justify-between flex py-[10px]  space-x-[12px] items-center text-[16px] leading-[22px] -tracking-[0.408px] `}
                            >
                              <div className="flex items-center space-x-[12px]">
                                <MyLibraryIcon color={true} size={true} />
                                <div>Миний булан</div>
                              </div>
                              <RightIcon />
                            </div>
                          </Link>
                        </div>
                        <div>
                          <Link href="/inquiry">
                            <div
                              className={`w-full justify-between flex py-[10px]  space-x-[12px] items-center text-[16px] leading-[22px] -tracking-[0.408px] `}
                            >
                              <div className="flex items-center space-x-[12px]">
                              <InquiryIcon color={true} size={true} />
                                <div>Лавлагаа</div>
                              </div>
                              <RightIcon />
                            </div>
                          </Link>
                        </div>


                        <div>
                          <Link
                            href={"/search-log"}
                            className={`w-full justify-between flex py-[10px] space-x-[12px] items-center text-[16px] leading-[22px] -tracking-[0.408px] `}
                          >
                            <div className="space-x-[12px] flex items-center">
                              <TimeIcon color={true} size={true} />
                              <div className="ml-[6px]">Ашиглалтын түүх</div>
                            </div>
                            <RightIcon />
                          </Link>
                        </div>
                        <div className="border-t w-full border-[#A6B2C3]" />
                        <div onClick={signout} className="cursor-pointer">
                          <div
                            className={`w-full space-x-[12px] py-[10px] flex px-[4px]   items-center text-[16px] leading-[22px] -tracking-[0.408px] `}
                          >
                            <LogOutIcon size={true} />
                            <div>Холболт салгах</div>
                          </div>
                        </div>
                      </div>
                    )}
                  </div>
                ) : (
                  <button
                    className={`block text-white	py-1 px-4 text-base	font-bold	text-center ${buttonColor}`}
                    onClick={() => {
                      setShowModal(true);
                      setOpen(false);
                      setModalOpen("signin");
                    }}
                  >
                    {f({ id: "signin-btn" })}
                  </button>
                )}
                {isOpen &&
                  menuItems?.map((m, i) => (
                    <Link
                      key={`m-${i}`}
                      data-aos="fade-up"
                      data-aos-delay={100 * i + 200}
                      className={`uppercase transition-all group text-[#021c5a] hover:underline underline-offset-2 relative px-4 pb-[10px] font-semibold ${
                        router.pathname === m.href ? "is-active" : ""
                      }`}
                      href={m.href}
                      onClick={() => {
                        setActive(m.type);
                      }}
                    >
                      {m.title}
                    </Link>
                  ))}
                {isOpen && (
                  <Link
                    data-aos="fade-up"
                    data-aos-delay={100 * menuItems?.length + 200}
                    className={`px-4 pb-[10px] text-[#021c5a] hover:underline underline-offset-2 font-semibold ${
                      router.pathname === "/map/" ? "is-active" : ""
                    }`}
                    href={"/map/"}
                  >
                    <img src="/images/mapLogo.svg" />
                  </Link>
                )}
                {isOpen && (
                  <div
                    data-aos="fade-up"
                    data-aos-delay={100 * menuItems?.length + 400}
                  >
                    <LanguageButton />
                  </div>
                )}
              </div>
              {/* Web header items */}
              <div
                className={`hidden ml-4 lg:gap-x-4 justify-around lg:flex items-center dark:text-[#BE8100] ${
                  isIndex ? "text-c1" : "text-c1"
                }`}
              >
                {menuItems?.map((m, i) => {
                  return (
                    <Link
                      key={`m2-${i}`}
                      href={m.href}
                      onClick={() => {
                        setActive(m.type);
                      }}
                    >
                      <div
                        key={`menu-${i}`}
                        className={`flex items-center h-[100px] ${
                          m.hoverColor
                        } ${
                          router.pathname.includes(m.href) ||
                          router.query.type?.includes(m.type2)
                            ? m.backgroundColor + " text-white"
                            : ""
                        } hover:text-white`}
                      >
                        <div
                          className={`uppercase transition-all group text-right relative px-4 font-semibold ${
                            router.pathname === m.href ? `is-active` : ""
                          }`}
                        >
                          {m.title}
                        </div>
                      </div>
                    </Link>
                  );
                })}
                <Link
                  className={` px-4  h-[100px] w-20 flex flex-col hover:bg-[#f6f7f9] justify-center ml-[-1rem] ${
                    router.pathname === "/map" ? `bg-[#f6f7f9]` : ""
                  }`}
                  href={"/map"}
                >
                  <img src="/images/mapLogo.svg" />
                </Link>
                {user ? (
                  <div className="relative w-16 mr-4">
                    <div
                      className="relative cursor-pointer"
                      onClick={() => serProfileDropdown(!profileDropdown)}
                    >
                      <img
                        src={user?.img || "/images/logo-black.svg"}
                        className="w-[48px] h-[48px] rounded-full object-cover border-[3px] border-[#112437]"
                      />
                      <div className="bg-[#112437] px-[4px] py-[6px] rounded-r-[4px] absolute top-1/2 transform -translate-y-1/2 -right-4">
                        <svg
                          xmlns="http://www.w3.org/2000/svg"
                          width="11"
                          height="6"
                          viewBox="0 0 11 6"
                          fill="none"
                        >
                          <path
                            d="M10.0003 1L6.3258 4.67453C5.89184 5.10849 5.18173 5.10849 4.74778 4.67453L1.07324 1"
                            stroke="white"
                            strokeWidth="0.901726"
                            strokeMiterlimit="10"
                            strokeLinecap="round"
                            strokeLinejoin="round"
                          />
                        </svg>
                      </div>
                    </div>
                    {profileDropdown && (
                      <div className="bg-white px-[20px] py-[14px] space-y-[10px] absolute top-[74px] min-w-[250px] transform right-0 shadow-lg ">
                        <div>
                          <Link href="/profile">
                            <div
                              className={`w-full justify-between flex space-x-[12px] py-[10px]  items-center text-[16px] leading-[22px] -tracking-[0.408px] `}
                            >
                              <div className="flex items-center space-x-[12px]">
                                <UserIcon color={true} size={true} />
                                <div>Хувийн мэдээлэл</div>
                              </div>
                              <RightIcon />
                            </div>
                          </Link>
                        </div>
                        <div className="border-t w-full border-[#A6B2C3]" />
                        <div>
                          <Link href="/myLibrary">
                            <div
                              className={`w-full justify-between flex py-[10px]  space-x-[12px] items-center text-[16px] leading-[22px] -tracking-[0.408px] `}
                            >
                              <div className="flex items-center space-x-[12px]">
                                <MyLibraryIcon color={true} size={true} />
                                <div>Миний булан</div>
                              </div>
                              <RightIcon />
                            </div>
                          </Link>
                        </div>
                        <div>
                          <Link href="/inquiry">
                            <div
                              className={`w-full justify-between flex py-[10px]  space-x-[12px] items-center text-[16px] leading-[22px] -tracking-[0.408px] `}
                            >
                              <div className="flex items-center space-x-[12px]">
                                <InquiryIcon color={true} size={true} />
                                <div>Лавлагаа</div>
                              </div>
                              <RightIcon />
                            </div>
                          </Link>
                        </div>
                        <div>
                          <Link
                            href={"/search-log"}
                            className={`w-full justify-between flex py-[10px] space-x-[12px] items-center text-[16px] leading-[22px] -tracking-[0.408px] `}
                          >
                            <div className="space-x-[12px] flex items-center">
                              <TimeIcon color={true} size={true} />
                              <div className="ml-[6px]">Ашиглалтын түүх</div>
                            </div>
                            <RightIcon />
                          </Link>
                        </div>
                        <div className="border-t w-full border-[#A6B2C3]" />
                        <div onClick={signout} className="cursor-pointer">
                          <div
                            className={`w-full space-x-[12px] py-[10px] flex px-[4px]   items-center text-[16px] leading-[22px] -tracking-[0.408px] `}
                          >
                            <LogOutIcon size={true} />
                            <div>Холболт салгах</div>
                          </div>
                        </div>
                      </div>
                    )}
                  </div>
                ) : (
                  <button
                    className={`block text-white	py-3 px-6 text-base	font-bold	rounded-[1.25rem] text-center font-sans ${buttonColor}`}
                    onClick={() => {
                      setShowModal(true);
                      setModalOpen("signin");
                    }}
                  >
                    {f({ id: "signin-btn" })}
                  </button>
                )}
              </div>
            </div>
          </div>
          <div
            className={`${
              user
                ? "hidden lg:block ml-6 mr-2 1xl:ml-0 2xl:mr-0 2xl:absolute 2xl:right-[15px] 3xl:right-[25px] 4xl:right-[50px]"
                : "hidden lg:block mx-2 1xl:ml-0 2xl:mr-0 2xl:absolute 2xl:right-[30px] 4xl:right-[60px]"
            }`}
          >
            <LanguageButton />
          </div>
        </section>
      </div>
    </>
  );
};

export default Header;
