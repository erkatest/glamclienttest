import useMessage from "~/hooks/useMessage";
import ContentCard from "./ContentCard";
import ContentSkeleton from "./ContentCard/Skeleton";
import { TabContext } from "~/context/tab";
import * as React from "react";

const ContentList = ({ contents = [], isLoading, enabled }: any) => {
  const f = useMessage();
  return (
    <>
      {contents.length <= 0 && !isLoading ? (
        <div className="col-start-1 flex my-10 md:my-40 flex-col items-center lg:col-start-2">
          <img alt="" src="/empty.png" className="w-20" />
          <p
            className="font-[600] text-[18px] text-center"
            dangerouslySetInnerHTML={{ __html: f({ id: "no-data-found" }) }}
          ></p>
        </div>
      ) : (
        <div className="grid grid-cols-2 sm:grid-cols-3 lg:grid-cols-4 gap-10">
          {isLoading
            ? [1, 2, 3, 4].map((i: any) => {
                return <ContentSkeleton key={i} />;
              })
            : contents.map((item: any, i: any) => {
                return (
                  <ContentCard
                    key={item?._id || i}
                    title={item?.name}
                    type={item?.type}
                    url={
                      item?.heritage_type
                        ? `/detail/${item?._id || ""}/${
                            item?.type || ""
                          }?heritage_type=${item?.heritage_type}`
                        : enabled
                        ? `detail/${item?._id}`
                        : `/detail/${item?._id || ""}/${item?.type || ""}`
                    }
                    imageUrl={item?.mainImg}
                    description={item?.description}
                    id={item?._id}
                    heritageType={item?.heritage_type}
                  />
                );
              })}
        </div>
      )}
    </>
  );
};

export default ContentList;
