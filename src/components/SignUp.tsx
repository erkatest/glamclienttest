import * as React from "react";
import { Formik, Form, Field, ErrorMessage } from "formik";
import * as Yup from "yup";
import Link from "next/link";
import useMessage from "~/hooks/useMessage";
import LoadingOverly from "~/components/libs/LoadingOverly";
import { useUser } from "~/context/auth";
import { ToastContainer, toast } from "react-toastify";
import { IoChevronBackOutline } from "react-icons/io5";
import VerificationCode from "./Verification/VerificationCode";

const SignUpContainer = (props: { setOpen: any }) => {
  const f = useMessage();
  const { signup, verify, resend } = useUser();
  const [loading, setLoading] = React.useState(false);
  const [data, setData] = React.useState<any>();
  const [verifyCod, setVerify] = React.useState("");
  const [done, setDone] = React.useState(false);
  const [show, setShow] = React.useState(false);
  const { setOpen } = props;

  const handleSignUp = async (values: any) => {
    const res = await signup(
      values.email,
      values.password,
      values.firstname,
      values.lastname
    );
    setData(values);
    if (res.success === true) {
      setDone(true);
    } else {
      toast.error(res.e);
    }
    setLoading(false);
  };

  const onFormSubmit = async (e:any) => {
    e.preventDefault();
    setLoading(true);
    setTimeout(async () => {
      const res = await verify(data?.email, verifyCod);
      if (res?.data?.success === true) {
        setLoading(false);
        setOpen("signin");
      } else if (res?.data?.success === false) {
        toast.error(res?.data?.e);
        setLoading(false);
      }
    }, 1000);
  };

  return (
    <div className="h-fit">
      <div
        className="flex items-center gap-1 text-2xl mb-6 cursor-pointer w-fit"
        onClick={() => {
          setOpen("signin");
        }}
      >
        <IoChevronBackOutline className="text-c7" />

        <p className="text-base text-c7 font-normal text-[16px] 2xl:text-[18px]">
          {f({ id: "form-back" })}
        </p>
      </div>
      <LoadingOverly visible={loading} />
      <h3 className="text-[32px] 2xl:text-[38px] mb-[20px] lg:mb-[24px] font-bold text-c4">
        {f({ id: "signup-title" })}
      </h3>
      <p className="text-c7 text-left mb-5 2xl:mb-7 text-[16px] 2xl:text-[18px] dark:text-white">
        {done
          ? f(
              { id: "signup-success" },
              {
                email: data?.email || "-----",
                strong: (value: any) => {
                  return <span className="font-bold text-c6">{value[0]}</span>;
                },
                a: (value: any) => {
                  return (
                    <button
                      className="underline text-c13 text-[16px] 2xl:text-[18px] font-medium dark:text-white"
                      onClick={async () => {
                        setLoading(true);
                        await resend(data?.email);
                        setLoading(false);
                      }}
                    >
                      {value[0]}
                    </button>
                  );
                },
              }
            )
          : f({ id: "signin-signup-desc" })}
      </p>

      {done ? (
        <form onSubmit={e => onFormSubmit(e)}>
          {/* <input
            className="w-full mb-6 p-2.5 border rounded-md outline-none	focus:ring-1 focus:ring-c15"
            placeholder="Баталгаажуулах код оруулах"
            onChange={(e) => {
              setVerify(e.target.value);
            }}
          /> */}

          <VerificationCode
            setVerify={setVerify}
          />

          <button
            type="submit"
            className="w-full bg-c13 active:bg-c13 hover:bg-c15 text-white p-2 rounded-[5px] text-[16px] 2xl:text-[18px]"
          >
            {f({ id: "signup-btn" })}
          </button>
        </form>
      ) : (
        <Formik
          initialValues={{
            username: "",
            email: "",
            password: "",
            passwordConfirm: "",
            firstname: "",
            lastname: "",
            agreement: false,
          }}
          validationSchema={Yup.object({
            firstname: Yup.string().required(
              `${f({ id: "form-firstname-error" })}`
            ),
            lastname: Yup.string().required(
              `${f({ id: "form-lastname-error" })}`
            ),
            email: Yup.string()
              .email(f({ id: "form-email-error" }))
              .required(
                `${f({ id: "form-email" })} ${f({
                  id: "form-required",
                })}`
              ),
            password: Yup.string().required(
              `${f({ id: "form-password" })} ${f({
                id: "form-required",
              })}`
            ),
            passwordConfirm: Yup.string().required(
              `${f({ id: "form-password" })} ${f({
                id: "form-required",
              })}`
            ),
            // agreement: Yup.boolean().oneOf(
            //   [true],
            //   f({ id: "form-agreement-error" })
            // ),
          })}
          onSubmit={(values, { setSubmitting }) => {
            setLoading(true);
            if (values.password !== values.passwordConfirm) {
              toast.error("Нууц үг таарахгүй байна.");
              setLoading(false);
            } else {
              handleSignUp(values);
            }
            setSubmitting(false);
          }}
        >
          {({ isSubmitting }) => (
            <Form autoComplete="off">
              <div className="flex space-x-2">
                <div className="w-1/2">
                  <div className="relative z-0">
                    <Field
                      type="lastname"
                      name="lastname"
                      id="lastname"
                      className="block py-[10px] px-[8px] 2xl:py-[14px] 2xl:px-[10px] w-full text-[16px] 2xl:text-[18px] text-gray-900 bg-transparent border-0 border-b-[0.5px] border-c12 appearance-none dark:text-white dark:border-gray-600 dark:focus:border-c5 focus:outline-none focus:ring-0 focus:border-c5 peer"
                      placeholder=" "
                    />
                    <label
                      htmlFor="lastname"
                      className="absolute text:[16px] 2xl:text-[18px] text-c7 dark:text-gray-400 duration-300 transform -translate-y-6 scale-75 top-3 -z-10 origin-[0] peer-focus:left-0 peer-focus:text-c5 peer-focus:dark:text-c5 peer-placeholder-shown:scale-100 peer-placeholder-shown:translate-y-0 peer-focus:scale-75 peer-focus:px-[12px] peer-focus:-translate-y-6 px-[12px]"
                    >
                      {f({ id: "form-lastname" })}
                    </label>
                  </div>
                  <div className="h-[20px] mb-2 2xl:mb-5">
                    <ErrorMessage
                      name="lastname"
                      component="div"
                      className="text-red-500 text-xs font-medium"
                    />
                  </div>
                </div>
                <div className="w-1/2">
                  <div className="relative z-0">
                    <Field
                      type="firstname"
                      name="firstname"
                      id="firstname"
                      className="block py-[10px] px-[8px] 2xl:py-[14px] 2xl:px-[10px] w-full text-[16px] 2xl:text-[18px] text-gray-900 bg-transparent border-0 border-b-[0.5px] border-c12 appearance-none dark:text-white dark:border-gray-600 dark:focus:border-c5 focus:outline-none focus:ring-0 focus:border-c5 peer"
                      placeholder=" "
                    />
                    <label
                      htmlFor="firstname"
                      className="absolute text:[16px] 2xl:text-[18px] text-c7 dark:text-gray-400 duration-300 transform -translate-y-6 scale-75 top-3 -z-10 origin-[0] peer-focus:left-0 peer-focus:text-c5 peer-focus:dark:text-c5 peer-placeholder-shown:scale-100 peer-placeholder-shown:translate-y-0 peer-focus:scale-75 peer-focus:px-[12px] peer-focus:-translate-y-6 px-[12px]"
                    >
                      {f({ id: "form-firstname" })}
                    </label>
                  </div>
                  <div className="h-[20px] mb-2 2xl:mb-5">
                    <ErrorMessage
                      name="firstname"
                      component="div"
                      className="text-red-500 text-xs font-medium"
                    />
                  </div>
                </div>
              </div>
              <div className="relative z-0">
                <Field
                  type="email"
                  name="email"
                  id="email"
                  className="block py-[10px] px-[8px] 2xl:py-[14px] 2xl:px-[10px] w-full text-[16px] 2xl:text-[18px] text-gray-900 bg-transparent border-0 border-b-[0.5px] border-c12 appearance-none dark:text-white dark:border-gray-600 dark:focus:border-c5 focus:outline-none focus:ring-0 focus:border-c5 peer"
                  placeholder=" "
                />
                <label
                  htmlFor="email"
                  className="absolute text:[16px] 2xl:text-[18px] text-c7 dark:text-gray-400 duration-300 transform -translate-y-6 scale-75 top-3 -z-10 origin-[0] peer-focus:left-0 peer-focus:text-c5 peer-focus:dark:text-c5 peer-placeholder-shown:scale-100 peer-placeholder-shown:translate-y-0 peer-focus:scale-75 peer-focus:px-[12px] peer-focus:-translate-y-6 px-[12px]"
                >
                  {f({ id: "form-email" })}
                </label>
              </div>

              <div className="h-[20px] mb-2 2xl:mb-5">
                <ErrorMessage
                  name="email"
                  component="div"
                  className="text-red-500 text-xs font-medium"
                />
              </div>

              <div className="relative z-0">
                <Field
                  type={show ? "text" : "password"}
                  name="password"
                  id="password"
                  className="block py-[10px] px-[8px] 2xl:py-[14px] 2xl:px-[10px] w-full text-[16px] 2xl:text-[18px] text-gray-900 bg-transparent border-0 border-b-[0.5px] border-c12 appearance-none dark:text-white dark:border-gray-600 dark:focus:border-c5 focus:outline-none focus:ring-0 focus:border-c5 peer"
                  placeholder=" "
                />
                <label
                  htmlFor="password"
                  className="absolute text:[16px] 2xl:text-[18px] text-c7 dark:text-gray-400 duration-300 transform -translate-y-6 scale-75 top-3 -z-10 origin-[0] peer-focus:left-0 peer-focus:text-c5 peer-focus:dark:text-c5 peer-placeholder-shown:scale-100 peer-placeholder-shown:translate-y-0 peer-focus:scale-75 peer-focus:px-[12px] peer-focus:-translate-y-6 px-[12px]"
                >
                  {f({ id: "form-password" })}
                </label>
                <button
                  className="absolute inset-y-0 right-0 flex items-center px-4 text-gray-600"
                  onClick={() => {
                    show ? setShow(false) : setShow(true);
                  }}
                  type="button"
                >
                  {show ? (
                    <svg
                      xmlns="http://www.w3.org/2000/svg"
                      fill="none"
                      viewBox="0 0 24 24"
                      strokeWidth={1.5}
                      stroke="currentColor"
                      className="w-5 h-5"
                    >
                      <path
                        strokeLinecap="round"
                        strokeLinejoin="round"
                        d="M2.036 12.322a1.012 1.012 0 010-.639C3.423 7.51 7.36 4.5 12 4.5c4.638 0 8.573 3.007 9.963 7.178.07.207.07.431 0 .639C20.577 16.49 16.64 19.5 12 19.5c-4.638 0-8.573-3.007-9.963-7.178z"
                      />
                      <path
                        strokeLinecap="round"
                        strokeLinejoin="round"
                        d="M15 12a3 3 0 11-6 0 3 3 0 016 0z"
                      />
                    </svg>
                  ) : (
                    <svg
                      xmlns="http://www.w3.org/2000/svg"
                      fill="none"
                      viewBox="0 0 24 24"
                      strokeWidth={1.5}
                      stroke="currentColor"
                      className="w-5 h-5"
                    >
                      <path
                        strokeLinecap="round"
                        strokeLinejoin="round"
                        d="M3.98 8.223A10.477 10.477 0 001.934 12C3.226 16.338 7.244 19.5 12 19.5c.993 0 1.953-.138 2.863-.395M6.228 6.228A10.45 10.45 0 0112 4.5c4.756 0 8.773 3.162 10.065 7.498a10.523 10.523 0 01-4.293 5.774M6.228 6.228L3 3m3.228 3.228l3.65 3.65m7.894 7.894L21 21m-3.228-3.228l-3.65-3.65m0 0a3 3 0 10-4.243-4.243m4.242 4.242L9.88 9.88"
                      />
                    </svg>
                  )}
                </button>
              </div>

              <div className="h-[20px] mb-2 2xl:mb-5">
                <ErrorMessage
                  name="password"
                  component="div"
                  className="text-red-500 text-xs font-medium"
                />
              </div>

              <div className="relative z-0">
                <Field
                  type={show ? "text" : "password"}
                  name="passwordConfirm"
                  id="passwordConfirm"
                  className="block py-[10px] px-[8px] 2xl:py-[14px] 2xl:px-[10px] w-full text-[16px] 2xl:text-[18px] text-gray-900 bg-transparent border-0 border-b-[0.5px] border-c12 appearance-none dark:text-white dark:border-gray-600 dark:focus:border-c5 focus:outline-none focus:ring-0 focus:border-c5 peer"
                  placeholder=" "
                />
                <label
                  htmlFor="password"
                  className="absolute text:[16px] 2xl:text-[18px] text-c7 dark:text-gray-400 duration-300 transform -translate-y-6 scale-75 top-3 -z-10 origin-[0] peer-focus:left-0 peer-focus:text-c5 peer-focus:dark:text-c5 peer-placeholder-shown:scale-100 peer-placeholder-shown:translate-y-0 peer-focus:scale-75 peer-focus:px-[12px] peer-focus:-translate-y-6 px-[12px]"
                >
                  {f({ id: "form-password-confirm" })}
                </label>
                <button
                  className="absolute inset-y-0 right-0 flex items-center px-4 text-gray-600"
                  onClick={() => {
                    show ? setShow(false) : setShow(true);
                  }}
                  type="button"
                >
                  {show ? (
                    <svg
                      xmlns="http://www.w3.org/2000/svg"
                      fill="none"
                      viewBox="0 0 24 24"
                      strokeWidth={1.5}
                      stroke="currentColor"
                      className="w-5 h-5"
                    >
                      <path
                        strokeLinecap="round"
                        strokeLinejoin="round"
                        d="M2.036 12.322a1.012 1.012 0 010-.639C3.423 7.51 7.36 4.5 12 4.5c4.638 0 8.573 3.007 9.963 7.178.07.207.07.431 0 .639C20.577 16.49 16.64 19.5 12 19.5c-4.638 0-8.573-3.007-9.963-7.178z"
                      />
                      <path
                        strokeLinecap="round"
                        strokeLinejoin="round"
                        d="M15 12a3 3 0 11-6 0 3 3 0 016 0z"
                      />
                    </svg>
                  ) : (
                    <svg
                      xmlns="http://www.w3.org/2000/svg"
                      fill="none"
                      viewBox="0 0 24 24"
                      strokeWidth={1.5}
                      stroke="currentColor"
                      className="w-5 h-5"
                    >
                      <path
                        strokeLinecap="round"
                        strokeLinejoin="round"
                        d="M3.98 8.223A10.477 10.477 0 001.934 12C3.226 16.338 7.244 19.5 12 19.5c.993 0 1.953-.138 2.863-.395M6.228 6.228A10.45 10.45 0 0112 4.5c4.756 0 8.773 3.162 10.065 7.498a10.523 10.523 0 01-4.293 5.774M6.228 6.228L3 3m3.228 3.228l3.65 3.65m7.894 7.894L21 21m-3.228-3.228l-3.65-3.65m0 0a3 3 0 10-4.243-4.243m4.242 4.242L9.88 9.88"
                      />
                    </svg>
                  )}
                </button>
              </div>

              <div className="h-[20px] mb-2 2xl:mb-5">
                <ErrorMessage
                  name="passwordConfirm"
                  component="div"
                  className="text-red-500 text-xs font-medium"
                />
              </div>
              <button
                type="submit"
                disabled={isSubmitting}
                className="flex items-center justify-center w-full bg-c13 active:bg-c5 text-white p-2.5 rounded-full font-normal hover:bg-c15 text-[16px] 2xl:text-[18px] font-sans"
              >
                {f({ id: "signup-btn" })}
              </button>

              <div className="inline-flex items-center justify-center w-full">
                <hr className="w-24 h-px my-8 bg-c7 border-0" />
                <span className="absolute px-3 font-medium -translate-x-1/2 bg-white left-1/2 dark:text-white dark:bg-gray-900 text-center text-[16px] my-6 text-c7">
                  {f({ id: "form-or" })}
                </span>
              </div>
              <p className="text-center text-[16px] 2xl:text-[18px]">
                {f({ id: "signup-signin" })}
                <button
                  onClick={() => setOpen("signin")}
                  className="font-medium text-center ml-2 text-[16px] text-c13 underline 2xl:text-[18px] font-sans"
                >
                  {f({ id: "signin-title" })}
                </button>
              </p>
            </Form>
          )}
        </Formik>
      )}
    </div>
  );
};

export default SignUpContainer;
