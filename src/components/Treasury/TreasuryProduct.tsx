import React from "react";

import { TabContext } from "~/context/tab";
import useParams from "~/hooks/useParams";
import apiItem from "~/api/item";
import useMessage from "~/hooks/useMessage";
import { useQuery } from "react-query";
import ContentList from "~/components/ContentLIst";

const TreasuryProduct = (props: any) => {
  const f = useMessage();
  const [page, setPage] = React.useState(1);
  const params = useParams();
  const { active } = React.useContext(TabContext);

  const { data, isLoading } = useQuery(
    ["movable", active, page, params.q || ""],
    () =>
      apiItem.find(active, page, 12, params.q)
  );

  return (
    <div className="container">
        <h1 className="uppercase font-[600] mt-10 text-2xl mb-4">
          {f({ id: "treasury-products" })}
        </h1>
        <ContentList isLoading={isLoading} contents={data?.data} />
    </div>
  );
};

export default TreasuryProduct;
