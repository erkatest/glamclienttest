import Link from "next/link";
import { Tooltip } from "@mantine/core";

const CollectionsCard = ({
  name,
  description,
  url
}: {
  name?: string;
  description?: string;
  url?: string;
}) => {

  return (
    <Link href={url || ""}>
      <div className="group bg-[#f2f2f2] rounded-[1rem] overflow-hidden h-full relative">
        <div className="flex flex-col gap-[5px] p-4 pb-6">
          <Tooltip label={name}>
            <div className="uppercase font-[600] line-clamp-1 text-[16px]">
              {name}
            </div>
          </Tooltip>
          <div className="text-gray text-slate-500 text-[16px] line-clamp-6">
            {description}
          </div>
        </div>
      </div>
    </Link>
  );
};

export default CollectionsCard;
