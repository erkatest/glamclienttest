import { Skeleton, Grid } from "@mantine/core";

const InquirySkeleton = () => {
  return (
    <div className="h-[160px] group bg-[#f2f2f2] rounded-[1rem] overflow-hidden mb-[20px]">
      <div className="flex flex-col p-4 pb-6 gap-2">
        <Skeleton h={14} />
        <Skeleton h={16} />
        <Grid justify="space-between">
          <Grid.Col span={3}>
            <Skeleton h={14} />
          </Grid.Col>
          <Grid.Col span={3}>
            <Skeleton h={14} />
          </Grid.Col>
        </Grid>
      </div>
    </div>
  );
};

export default InquirySkeleton;
