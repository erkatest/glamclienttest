import React, { useEffect, useState } from "react";
import { Tooltip } from "@mantine/core";
import Link from "next/link";
import { IoMdHeart } from "react-icons/io";
import { BiTrash } from "react-icons/bi";
import { useMutation, useQuery } from "react-query";

import { TabContext } from "~/context/tab";
import { useUser } from "~/context/auth";
import AddItemModal from "~/components/Modals/AddItemModal";
import apiSavedItem from "~/api/savedItem";
import { toast } from "react-toastify";
import useMessage from "~/hooks/useMessage";
import { useRouter } from "next/router";

const ContentCard = ({
  title,
  description,
  type,
  url,
  imageUrl,
  id,
  hideHeart = false,
  deletable = false,
  // selectedFolders,
  refetch,
  heritageType,
}: {
  title?: string | React.ReactNode;
  description?: string | React.ReactNode;
  url?: string;
  type?: string;
  imageUrl?: string;
  id?: any;
  hideHeart?: boolean;
  deletable?: boolean;
  // selectedFolders?: string;
  heritageType?: string;
  refetch?: () => void;
}) => {
  const { user } = useUser();
  const f = useMessage();

  const {
    setOpen: setModalOpen,
    setShowModal,
    setHeritageImageCheck,
  } = React.useContext(TabContext);
  const [showFavouriteModal, setShowFavouriteModal] =
    React.useState<boolean>(false);
  const [saved, setSaved] = React.useState<boolean>(false);

  const createMutation = useMutation(
    (data: any) => user && apiSavedItem.createSavedItem(data),
    {
      onSuccess(e: any) {
        toast.success(e?.data?.message ? e?.data?.message : f({ id: "success-add-item" }));
        // refetch && refetch();
      },
      onError(error: any) {
        toast.error(error);
      },
    }
  );

  const deleteMutation = useMutation(() => user && apiSavedItem.deleteItem(id), {
    onSuccess() {
      toast.success(f({ id: "success-deleted-message" }));
      refetch && refetch();
    },
    onError(error: any) {
      toast.error(error);
    },
  });

  const { data: checkData, isLoading: checkDataIsLoading } = useQuery(
    ["check_item", id, createMutation],
    () => user && apiSavedItem.checkItem(id)
  );

  return (
    <>
      {!hideHeart && (
        <AddItemModal
          isOpen={showFavouriteModal}
          onClose={() => setShowFavouriteModal(false)}
          item={{
            title,
            description,
            type,
            mainImg: imageUrl,
            _id: id,
          }}
        />
      )}

      <div className="group bg-[#f2f2f2] rounded-[1rem] overflow-hidden h-full relative">
        <Link
          href={url || ""}
          onClick={() => {
            // setHeritageTypeCheck(heritageType);
            setHeritageImageCheck(imageUrl);
          }}
        >
          <div className="w-full aspect-[4/3.6] overflow-hidden">
            <img
              src={imageUrl}
              alt=""
              className="w-full h-full group-hover:scale-125 transition-all ease-in duration-300 object-cover"
              onError={({ currentTarget }) => {
                currentTarget.src = "/images/logo-blue-silver.png";
              }}
            />
          </div>
        </Link>
        {deletable && (
          <div className="absolute top-2 right-2 cursor-pointer group-hover:block hidden p-1 bg-slate-200 rounded-full justify-center items-center">
            <BiTrash
              size={24}
              className="text-red-500"
              onClick={(e: any) => {
                e.preventDefault();
                deleteMutation.mutate();
              }}
            />
          </div>
        )}
        {!hideHeart && !deletable && (
          <div className={`absolute top-2 cursor-pointer right-2 ${checkData?.data.length !== 1 && "hidden"} group-hover:block p-1 bg-slate-500 rounded-full justify-center items-center`}>
            <IoMdHeart
              color={checkData?.data.length == 1 ? "#112437" : "#A6B2C3"}
              size={24}
              className="text-white"
              onClick={(e: any) => {
                e.preventDefault();
                setSaved(!saved);
                if (!user) {
                  setShowModal(true);
                  setModalOpen("signin");
                }

                if (user && saved) {
                  createMutation.mutateAsync({
                    type: type,
                    item_id: id,
                    item_name: title,
                    main_img: imageUrl,
                    heritage_type: heritageType
                  });
                }
              }}
            />
          </div>
        )}
        <div className="flex flex-col p-4 pb-6">
          <Tooltip label={title}>
            <div className="uppercase font-[600] line-clamp-1 text-[16px]">
              {title}
            </div>
          </Tooltip>
          <div className="text-gray line-clamp-2 text-slate-500 text-[16px]">
            {description}
          </div>
        </div>
      </div>
    </>
  );
};

export default ContentCard;
