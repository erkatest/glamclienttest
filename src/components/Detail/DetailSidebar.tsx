import React from "react";
import AddItemModal from "../Modals/AddItemModal";
import dayjs from "dayjs";
import { useRouter } from "next/router";
import ShareModal from "../Modals/shareModal";

import SignAlertModal from "../Modals/SignAlertModal";
import store from "~/utils/store";
import FileTypeModal from "../Modals/FileTypeModal";
import { useQuery, useMutation } from "react-query";
import Users from "~/api/users";
import ConnectModal from "../Modals/ConnectModal";
import RequestModal from "../Modals/reviewModal";
import { IoMdHeart, IoIosHeartEmpty } from "react-icons/io";
import apiSavedItem from "~/api/savedItem";
import { toast } from "react-toastify";
import useMessage from "~/hooks/useMessage";
import { useUser } from "~/context/auth";

interface IProps {
  data: any;
  type?: any;
}

const DetailSidebar = ({ data, type }: IProps) => {
  const [modal, setModal] = React.useState(false);
  const [modalShare, setModalShare] = React.useState(false);
  const router = useRouter();

  const [typeModal, setTypeModal] = React.useState(false);
  const [connectModal, setConnectModal] = React.useState(false);
  const [signModal, setSignModal] = React.useState<any>(false);
  const [requestModal, setRequestModal] = React.useState(false);
  const [getIds, setIds] = React.useState<Array<string>>([]);

  const libraryToken = useQuery(["library-token"], () => Users.getToken());

  const f = useMessage();
  const { user } = useUser();
  const [typeEng, setTypeEng] = React.useState<string>();

  const createMutation = useMutation(
    (data: any) => user && apiSavedItem.createSavedItem(data),
    {
      onSuccess(e: any) {
        toast.success(
          f({ id: "success-add-item" })
          // e?.data?.message ? e?.data?.message : f({ id: "success-add-item" })
        );
        // refetch && refetch();
      },
      onError(error: any) {
        toast.error(error);
      },
    }
  );

  const deleteMutation = useMutation(
    (id) => user && apiSavedItem.deleteItem(id),
    {
      onSuccess() {
        toast.success(f({ id: "success-deleted-message" }));
        // refetch && refetch();
      },
      onError(error: any) {
        toast.error(error);
      },
    }
  );

  const { data: checkData, isLoading: checkDataIsLoading } = useQuery(
    ["check_item", createMutation, deleteMutation],
    () =>
      user &&
      apiSavedItem.checkItem(
        data?.data?.heritage_type ? data?.data?.id : data?.data?._id
      )
  );

  React.useEffect(() => {
    setTypeEng(
      data?.data?.heritage_type === "Хөдлөх өв"
        ? "movable"
        : data?.data?.heritage_type === "Үл хөдлөх өв"
        ? "immovable"
        : data?.data?.heritage_type === "Биет бус өв"
        ? "intangible"
        : ""
    );
  }, [data]);

  return (
    <div className="space-y-8">
      <div className="md:block flex flex-wrap items-center gap-[0.5rem] mt-10 md:mt-0 md:gap-[1rem] md:space-y-2 text-sm">
        {!router?.query?.type && (
          <div
            onClick={() =>
              store?.get("user")
                ? libraryToken?.data?.status === "success"
                  ? setTypeModal(true)
                  : setConnectModal(true)
                : setSignModal(true)
            }
            className="bg-[#1A3D7A] rounded-full  p-2 py-3 text-white text-center cursor-pointer"
          >
            <p>Унших</p>
          </div>
        )}

        <div
          className="flex items-center border border-c1 rounded-full px-2 py-3 cursor-pointer h-max"
          onClick={() => {
            // store?.get("user") ? setModal(true) : setSignModal(true)
            if (!user) {
              setSignModal(true);
            }
            if (user && checkData?.data.length == 0) {
              if (data?.data?.heritage_type) {
                createMutation.mutateAsync({
                  type: type,
                  item_id: data?.data?.id,
                  item_name: data?.data?.heritage_name,
                  heritage_type: typeEng,
                  main_img: data?.data?.mainImg,
                });
              } else {
                createMutation.mutateAsync({
                  type: data?.data?.type,
                  item_id: data?.data?._id,
                  item_name: data?.data?.name,
                  main_img: data?.data?.mainImg,
                });
              }
            } else if (user && checkData?.data.length == 1) {
              deleteMutation.mutateAsync(checkData?.data[0]?._id);
            }
          }}
        >
          {checkData?.data.length == 0 || checkData?.data == undefined ? (
            <>
              <IoIosHeartEmpty className="mx-2 w-4 h-4" />
              <p className="text-[16px]">Хадгалах</p>
            </>
          ) : (
            <>
              <IoMdHeart className="mx-2 w-4 h-4" />
              <p className="text-[16px]">Хадгалсан</p>
            </>
          )}
        </div>
        <div
          className="flex items-center border border-c1 rounded-full p-2 py-3 cursor-pointer h-max"
          onClick={() => setModalShare(true)}
        >
          <img src="/images/detail-share.svg" className="mx-2 w-4" />
          <p className="text-[16px]">Хуваалцах</p>
        </div>
        <div
          className="flex items-center border border-c1 rounded-full p-2 py-3 cursor-pointer h-max"
          onClick={() => setRequestModal(true)}
        >
          <img src="/images/mega.svg" className="mx-2 w-4" />
          <p className="text-[16px]">Хүсэлт илгээх</p>
        </div>

        <div className="">
          <div className="flex items-center 2xl:space-x-6 xl:space-x-10 md:space-x-10 justify-center bg-[#F4F7FA] p-3 rounded-xl gap-[1rem]">
            <div className="text-center">
              <p className="font-[500] text-[14px]">
                {data?.data?.viewCount || "-"}
              </p>
              <div className="text-[#767676] text-[14px]">Үзсэн</div>
            </div>
            <div className="text-center ">
              <p className="font-[500] text-[14px]">
                {/* {data?.data?.heritage_type == "Баримтат өв" &&
                data?.data?.heritage_attachments[0].updated_at
                  ? dayjs(
                      data?.data?.heritage_attachments[0].updated_at
                    ).format("DD-MM-YYYY")
                  : data?.data?.updatedAt
                  ? dayjs(data?.data?.updatedAt).format("DD-MM-YYYY")
                  : "-"} */}
                {data?.data?.saveCount || "-"}
              </p>
              {/* data?.data?.heritage_attachments[0].updated_at */}
              <div className="text-[#767676] text-[14px]">Хадгалсан</div>
            </div>
          </div>
        </div>
      </div>
      <SignAlertModal isOpen={signModal} onClose={() => setSignModal(false)} />
      <AddItemModal
        isOpen={modal}
        onClose={() => setModal(false)}
        item={data?.data}
      />
      <ShareModal
        isOpen={modalShare}
        onClose={() => setModalShare(false)}
        item={data?.data}
      />
      <FileTypeModal
        isOpen={typeModal}
        onClose={() => setTypeModal(false)}
        data={data?.data}
        token={libraryToken?.data?.data}
      />
      <RequestModal
        isOpen={requestModal}
        onClose={() => setRequestModal(false)}
        item={data?.data}
      />
      <ConnectModal
        isOpen={connectModal}
        onClose={() => setConnectModal(false)}
      />
    </div>
  );
};

export default DetailSidebar;
