import React from "react";
import { useSwiper } from "swiper/react";

export const SwiperNavButtons = () => {
  const swiper = useSwiper();

  return (
    <div>
      <button
        onClick={() => swiper.slidePrev()}
        className="swiper-button-prev"
      ></button>

      <button
        onClick={() => swiper.slideNext()}
        className="swiper-button-next"
      ></button>
    </div>
  );
};
