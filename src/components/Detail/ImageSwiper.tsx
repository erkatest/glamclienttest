import React, { useState } from "react";

import { Swiper, SwiperSlide } from "swiper/react";
import { EffectCoverflow } from "swiper/modules";
import { Navigation, Pagination, Scrollbar, A11y } from "swiper/modules";
import { TransformWrapper, TransformComponent } from "react-zoom-pan-pinch";
import { IconX } from "@tabler/icons-react";
import { SwiperNavButtons } from "./SwiperNavButton";
import Controls from "./Controls";

import "swiper/css";
import "swiper/css/effect-coverflow";
import "swiper/css/navigation";
import "swiper/css/pagination";
import "swiper/css/scrollbar";

const ImageSwiper = (props: any) => {
  const { data, isLoading, type } = props;
  const [activeImage, setActiveImage] = React.useState<string>("");
  const [imageErrors, setImagesError] = useState<any>({});

  if (!data || data.length < 1) return null;

  const pathsWithDynamicBackground = ["/"];

  const buttonColor = `${
    pathsWithDynamicBackground.includes(props.pathname)
      ? "bg-default"
      : props?.tabs?.find((tab: any) => tab.type === props.active)
          ?.backgroundColor
  }`;

  return (
    <div>
      {activeImage && (
        <div className="w-screen h-screen absolute top-0 bottom-0 right-0 left-0 z-[9999999] backdrop-blur-lg py-[1rem] flex items-center justify-center">
          <TransformWrapper>
            {({ zoomIn, zoomOut, resetTransform, ...rest }) => (
              <div className="flex flex-col bg-[#fff] p-[10px] gap-[10px] rounded-md">
                <TransformComponent>
                  <img
                    id="fullscreen-image"
                    src={activeImage}
                    alt=""
                    className="h-[600px] w-full mx-auto cursor-pointer object-contain rounded-md"
                    onError={({ currentTarget }) => {
                      currentTarget.onerror = null;
                      currentTarget.src = "/images/logo-blue.png";
                    }}
                  />
                </TransformComponent>
                <div className="flex justify-between gap-[15px]">
                  <Controls
                    pathname={props.pathname}
                    tabs={props.tabs}
                    active={props.active}
                  />
                  <button
                    className={`${buttonColor} w-[60px] h-[31px] rounded-md`}
                    onClick={() => {
                      setActiveImage("");
                    }}
                  >
                    <IconX
                      className="m-auto"
                      color="#fff"
                      width={24}
                      height={24}
                    />
                  </button>
                </div>
              </div>
            )}
          </TransformWrapper>
        </div>
      )}
      {!isLoading && (
        <div className="container mx-auto my-10">
          <Swiper
            // effect={"coverflow"}
            watchSlidesProgress
            grabCursor={true}
            setWrapperSize={true}
            initialSlide={0}
            coverflowEffect={{
              rotate: 0,
              stretch: 0,
              depth: 50,
              modifier: 1,
            }}
            className="mySwiper"
            pagination={{ clickable: true }}
            // scrollbar={{ draggable: true }}
            // navigation
            slidesPerView={3}
            modules={[Navigation, Pagination, Scrollbar, A11y, EffectCoverflow]}
          >
            {(Array.isArray(data) ? data : []).map((item: any, i: number) => {
              return (
                <>
                  {type === "heritage" ? (
                    <>
                      <SwiperSlide
                        className="group mb-2 cursor-pointer"
                        key={`item-${i}`}
                      >
                        <div
                          className="aspect-[3/2]"
                          onClick={() => {
                            if (imageErrors?.[i]) {
                            } else {
                              item?.path &&
                                setActiveImage(
                                  `https://ncch.cis.mn/static-files/w-2000${item?.path}`
                                );
                            }
                          }}
                        >
                          <img
                            src={
                              `https://ncch.cis.mn/static-files/w-2000${item?.path}` ||
                              "/images/empty.svg"
                            }
                            className="pointer-events-none object-cover w-full h-full"
                            onError={(
                              e: React.SyntheticEvent<HTMLImageElement, Event>
                            ) => {
                              setImagesError({ ...imageErrors, [i]: true });
                              e.currentTarget.src =
                                "/images/logo-blue-white.png";
                            }}
                            alt={`item-${i}`}
                          />
                        </div>
                      </SwiperSlide>
                    </>
                  ) : (
                    <SwiperSlide className="group mb-2" key={`item-${i}`}>
                      <div
                        className="aspect-[3/2]"
                        onClick={() => {
                          setActiveImage(item);
                        }}
                      >
                        <img
                          src={item || "/images/empty.svg"}
                          className="pointer-events-none object-cover w-full h-full"
                          onError={(
                            e: React.SyntheticEvent<HTMLImageElement, Event>
                          ) => {
                            e.currentTarget.src = "/images/empty.svg";
                          }}
                          alt={`item-${i}`}
                        />
                      </div>
                    </SwiperSlide>
                  )}
                </>
              );
            })}
            {data?.length > 3 && <SwiperNavButtons />}
          </Swiper>
        </div>
      )}
    </div>
  );
};

export default ImageSwiper;
