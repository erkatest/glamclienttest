import React from "react";
import { useControls } from "react-zoom-pan-pinch";
import { IconMinus, IconPlus, IconX, IconZoomReset } from "@tabler/icons-react";

import "swiper/css";
import "swiper/css/effect-coverflow";
import "swiper/css/navigation";
import "swiper/css/pagination";
import "swiper/css/scrollbar";

const Controls = (props: any) => {
  const { zoomIn, zoomOut, resetTransform } = useControls();
  const pathsWithDynamicBackground = ["/"];

  const buttonColor = `${
    pathsWithDynamicBackground.includes(props.pathname)
      ? "bg-default"
      : props?.tabs?.find((tab: any) => tab.type === props?.active)
          ?.backgroundColor
  }`;

  return (
    <div>
      <button
        className={`${buttonColor} rounded-md px-[5px] py-[3px] m-auto w-[60px]`}
        onClick={() => zoomIn()}
      >
        <IconPlus className="m-auto" color="#fff" width={25} height={25} />
      </button>
      <button
        className={`${buttonColor} w-[60px] rounded-md px-[5px] py-[3px] mx-[15px]`}
        onClick={() => zoomOut()}
      >
        <IconMinus className="m-auto" color="#fff" width={25} height={25} />
      </button>
      <button
        className={`${buttonColor} rounded-md px-[5px] py-[3px] m-auto w-[60px]`}
        onClick={() => resetTransform()}
      >
        <IconZoomReset className="m-auto" color="#fff" width={25} height={25} />
      </button>
    </div>
  );
};

export default Controls;
