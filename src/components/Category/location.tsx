import Link from "next/link";
import React from "react";

interface IProps {
  data: any;
  title: string;
}

const LocationBundle = ({ data, title }: IProps) => {
  return (
    <div className="mt-12">
      <div className="flex items-center mb-10">
        <div className="w-2/5 border-b border-c1"></div>
        <div className="w-1/5 flex justify-center items-center">
          <div className="h-3 w-3 rounded-full mr-3"></div>
          {title} ({data?.pagination?.total})
        </div>
        <div className="w-2/5 border-b border-c1"></div>
      </div>
      <div className="flex items-center">
        <div className="grid gap-5 lg:grid-cols-3 md:grid-cols-2">
          {data?.data?.map((item: any, i: number) => (
            <div
              key={`item-${i}`}
              className="flex border rounded-md shadow-xl h-[211px]"
            >
              <div className="w-1/2">
                {item?.mainImg ? (
                  <img
                    src={item?.mainImg}
                    className="bg-contain bg-center h-full"
                  />
                ) : (
                  <div className="rounded-md">
                    <img src="/images/empty.svg" />
                  </div>
                )}
              </div>
              <div className="w-1/2 p-4 space-y-4 text-xs">
                <h1 className="font-bold text-sm hover:text-clip truncate">
                  {item?.title}
                </h1>
                <div className="flex space-x-3">
                  <img src="/images/map.svg" className="w-5 " />
                  <p>{item?.location}</p>
                </div>
                <div className="flex space-x-3">
                  <img src="/images/clock.svg" className="w-5 " />
                  <p>{item?.date}</p>
                </div>
                <div className="flex space-x-2">
                  {item?.status === "Нээлттэй" ? (
                    <div className="bg-[#0AEF21] rounded-full w-4 ml-0.5"></div>
                  ) : (
                    <div className="bg-[#F30D37] rounded-full w-4 ml-0.5"></div>
                  )}
                  <p>{item?.status}</p>
                </div>
              </div>
            </div>
          ))}
        </div>
        <Link href="/map" className="ml-4">
          <div className="cursor-pointer w-[43px] h-[43px] rounded-full border-2 border-c1 hover:opacity-60 flex items-center justify-center">
            <img src="/images/arrow-dark.svg" className="w-1/2" />
          </div>
        </Link>
      </div>
    </div>
  );
};

export default LocationBundle;
