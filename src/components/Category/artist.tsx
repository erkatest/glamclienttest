import { useRouter } from "next/router";
import React from "react";
import useParams from "~/hooks/useParams";
import AddItemModal from "../Modals/AddItemModal";

interface IProps {
  data: any;
  title: string;
}

const ArtistBundle = ({ data, title }: IProps) => {
  const router = useRouter();
  const params = useParams();
  const [modal, setModal] = React.useState(false);
  const [currentData, setCurrentData] = React.useState<any>();
  return (
    <div className="mt-12">
      <div className="flex items-center mb-10">
        <div className="w-2/5 border-b border-c1"></div>
        <div className="w-1/5 flex justify-center items-center">
          <div className="h-3 w-3 rounded-full mr-3"></div>
          {title} ({data?.pagination?.total})
        </div>
        <div className="w-2/5 border-b border-c1"></div>
      </div>
      <div className="flex items-center">
        <div className="grid gap-10 lg:grid-cols-5 md:grid-cols-3">
          {data?.data?.map((item: any, i: number) => (
            <div className="group relative" key={`artist-${i}`}>
              <div
                className="rounded-2xl shadow-xl p-2 cursor-pointer hover:bg-black/10"
                onClick={() =>
                  router.push({
                    pathname: `/search/artist/detail/${item?._id || ""}`,
                  })
                }
              >
                <div
                  className="rounded-lg mb-4 overflow-hidden "
                  style={{
                    aspectRatio: "1 / 1",
                  }}
                >
                  <img
                    src={item?.mainImg || "/images/empty.svg"}
                    className="rounded-md w-full h-full pointer-events-none mx-auto bg-contain"
                  />
                </div>
                <p className="my-3 text-xs">{title}</p>
                <p>{item?.name}</p>
              </div>
              <img
                src="/images/detail-love.svg"
                className="absolute top-2 right-3 w-9 h-9 border rounded-full bg-[#F6F6F6] transform translate-y-[10px] transition-all opacity-0 group-hover:opacity-100 duration-500 group-hover:translate-x-0 hover:transition hover:duration-150 cursor-pointer p-0.5"
                onClick={() => {
                  setModal(true), setCurrentData(item);
                }}
              />
            </div>
          ))}
        </div>
        <div
          className="ml-10"
          onClick={() => {
            router.push({
              pathname: "/search/artist",
              search: `?q=${params.q || ""}`,
            });
          }}
        >
          <div className="cursor-pointer w-[43px] h-[43px] rounded-full border-2 border-c1 hover:opacity-60 flex items-center justify-center">
            <img src="/images/arrow-dark.svg" className="w-1/2" />
          </div>
        </div>
      </div>
      <AddItemModal
        isOpen={modal}
        onClose={() => setModal(false)}
        item={currentData}
      />
    </div>
  );
};

export default ArtistBundle;
