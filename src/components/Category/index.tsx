import React from "react";
import AddItemModal from "../Modals/AddItemModal";
import ContentList from "~/components/ContentLIst";

interface IProps {
  data: any;
  title: string;
  type: string;
  isLoading: boolean;
  enabled?: boolean;
}

const CategoryBundle = ({ data, title, type, isLoading, enabled }: IProps) => {
  const [modal, setModal] = React.useState(false);
  const [currentData, setCurrentData] = React.useState<any>();

  return (
    <div className="mt-10 container mx-auto">
      <ContentList contents={data?.data} isLoading={isLoading} enabled={enabled} />
      <AddItemModal
        isOpen={modal}
        onClose={() => setModal(false)}
        item={currentData}
      />
    </div>
  );
};

export default CategoryBundle;
