import * as React from "react";

const CustomCategory = (props: { type: any; bold?: boolean }) => {
  const { type, bold } = props;
  return (
    <div className="flex items-center">
      <div
        className={`h-3 w-3 rounded-full mr-3 ${
          type === "artwork"
            ? "bg-[#D23F97]"
            : type === "koha"
            ? "bg-[#B2C535]"
            : type === "culture"
            ? "bg-[#F15E22]"
            : type === "education"
            ? "bg-[#012E93]"
            : ""
        }`}
      ></div>
      <label className={`${bold ? "font-bold" : ""} text-xs`}>
        {type === "artwork"
          ? "Уран бүтээл"
          : type === "koha"
          ? "Ном товхимол"
          : type === "culture"
          ? "Соёлын өв"
          : type === "education"
          ? "Боловсрол"
          : type === "artist"
          ? "Уран бүтээлч"
          : ""}
      </label>
    </div>
  );
};

export default CustomCategory;
