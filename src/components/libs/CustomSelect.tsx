import * as React from "react";
import { Fragment, useEffect, useState } from "react";
import { Listbox, Transition } from "@headlessui/react";
import { TabContext } from "~/context/tab";

export default function CustomSelect(props: {
  items: { label?: any; value: any; type?: any; selected?: any; index?: any }[];
  items_all?: { label?: any; value: any; type?: any; selected?: any; index?: any }[];
  defaultValue?: string;
  onChange?: (value: any) => void;
}) {
  const { items, items_all, defaultValue, onChange } = props;
  const { tabs, active } = React.useContext(TabContext);

  const [selected, setSelected] = useState<any>(items[0] || defaultValue);

  useEffect(() => {
    if (defaultValue && defaultValue !== "" && items_all !== undefined) {
      setSelected(
         items_all && items_all?.find((i) => {
          return i?.value === defaultValue;
        }) || items[0]
      );
    } else if (defaultValue && defaultValue !== "" && items_all === undefined && items) {
      setSelected(
        items && items?.find((i) => {
         return i?.value === defaultValue;
       }) || items[0]
     );
    }
  }, [defaultValue]);

  return (
    <div className="w-full relative">
      <Listbox
        value={selected}
        onChange={(tmpSelected) => {
          onChange?.(tmpSelected);
          setSelected(tmpSelected);
        }}
      >
        {({ open }) => (
          <>
            <div className="relative">
              <Listbox.Button
                className={`font-lora relative w-full cursor-default rounded-[1rem] bg-[white] py-2 px-3 text-left focus:outline-none focus-visible:border-indigo-500 focus-visible:ring-1 focus-visible:ring-white focus-visible:ring-opacity-75 focus-visible:ring-offset-2 focus-visible:ring-offset-orange-300 sm:text-sm border-2 border ${
                  tabs.find((tab: any) => tab.type === active)?.borderColor
                } ${tabs.find((tab: any) => tab.type === active)?.textColor}`}
              >
                <p
                  className={`block truncate pr-3 text-[16px] font-[600] cursor-pointer ${
                    tabs.find((tab: any) => tab.type === active)?.textColor
                  }`}
                >
                  {selected?.label || selected?.value}
                </p>
                <span
                  className={`pointer-events-none absolute inset-y-0 right-0 flex items-center pr-3`}
                >
                  <span
                    className={`${
                      open
                        ? "transform rotate-180 duration-200"
                        : "duration-200"
                    }`}
                  >
                    <svg
                      width="13"
                      height="9"
                      viewBox="0 0 13 9"
                      fill="none"
                      xmlns="http://www.w3.org/2000/svg"
                    >
                      <path
                        d="M6.85523 8.08231C7.13578 8.08231 7.36843 7.97967 7.5874 7.7607L12.74 2.4918C12.9179 2.31389 13 2.10176 13 1.84858C13 1.33538 12.5963 0.917969 12.0831 0.917969C11.8299 0.917969 11.5972 1.02745 11.4125 1.21221L6.86207 5.89263L2.29797 1.21221C2.11322 1.02745 1.88741 0.917969 1.62738 0.917969C1.11418 0.917969 0.703613 1.33538 0.703613 1.84858C0.703613 2.10176 0.792569 2.31389 0.97048 2.4918L6.12306 7.7607C6.34202 7.98651 6.57468 8.08231 6.85523 8.08231Z"
                        fill={`${
                          tabs.find((tab: any) => tab.type === active)?.color
                        }`}
                      />
                    </svg>
                  </span>
                </span>
              </Listbox.Button>
              <Transition
                as={Fragment}
                enter="transition duration-100 ease-out"
                enterFrom="transform scale-95 opacity-0"
                enterTo="transform scale-100 opacity-100"
                leave="transition duration-75 ease-out"
                leaveFrom="transform scale-100 opacity-100"
                leaveTo="transform scale-95 opacity-0"
              >
                <Listbox.Options className="z-[999] mt-1 max-h-60 overflow-auto rounded-md bg-white py-1 pl-0 text-base shadow-lg ring-1 ring-black ring-opacity-5 focus:outline-none sm:text-sm absolute">
                  {items?.map((item, itemIdx) => (
                    <Listbox.Option
                      key={`${item.value}-${itemIdx}`}
                      className={({ active }) =>
                        `relative select-none border-l-2 hover:bg-[#eee] cursor-pointer p-2 min-w-[200px] max-w-[98%] ${
                          active ? "bg-gray-100" : "text-gray-900"
                        }`
                      }
                      value={item}
                    >
                      {({ selected }) => (
                        <>
                          <p
                            className={`block truncate text-[16px] ${
                              selected ? "font-[700]" : "font-normal"
                            }`}
                          >
                            {item?.label || item?.value}
                          </p>
                        </>
                      )}
                    </Listbox.Option>
                  ))}
                </Listbox.Options>
              </Transition>
            </div>
          </>
        )}
      </Listbox>
    </div>
  );
}
