import { useState } from "react";

export default function CustomFilter(props: {
  items: { label?: any; value: any }[];
  defaultValue: any;
}) {
  const { items, defaultValue } = props;
  const [currentIndex, setCurrentIndex] = useState<any>();
  const [open, setOpen] = useState(true);

  return (
    <div className="w-full relative">
      <div className="relative mt-2">
        <div
          className="flex justify-between items-center relative w-full cursor-default rounded-lg py-2 text-left transition-all"
          onClick={() => setOpen((o) => !o)}
        >
          <h3 className="text-base font-bold">
            {defaultValue === "artwork"
              ? "Уран бүтээлийн төрөл"
              : defaultValue === "koha"
              ? "Номын төрөл"
              : defaultValue === "culture"
              ? "Соёлын өв"
              : defaultValue === "education"
              ? "Боловсрол"
              : defaultValue}
          </h3>
          <div
            className={`pointer-events-none flex items-center pr-3 ${
              defaultValue ? "" : "hidden"
            }`}
          >
            <span
              className={`${
                open ? "transform rotate-180 duration-200" : "duration-200"
              }`}
            >
              <svg
                width="13"
                height="9"
                viewBox="0 0 13 9"
                fill="none"
                xmlns="http://www.w3.org/2000/svg"
              >
                <path
                  d="M6.85523 8.08231C7.13578 8.08231 7.36843 7.97967 7.5874 7.7607L12.74 2.4918C12.9179 2.31389 13 2.10176 13 1.84858C13 1.33538 12.5963 0.917969 12.0831 0.917969C11.8299 0.917969 11.5972 1.02745 11.4125 1.21221L6.86207 5.89263L2.29797 1.21221C2.11322 1.02745 1.88741 0.917969 1.62738 0.917969C1.11418 0.917969 0.703613 1.33538 0.703613 1.84858C0.703613 2.10176 0.792569 2.31389 0.97048 2.4918L6.12306 7.7607C6.34202 7.98651 6.57468 8.08231 6.85523 8.08231Z"
                  fill="#3C3C43"
                />
              </svg>
            </span>
          </div>
        </div>
        <div>
          {open && (
            <div className="w-full text-base focus:outline-none sm:text-sm transition-all duration-400">
              {items?.map((item, itemIdx) => (
                <div
                  key={itemIdx}
                  className={`flex items-center relative cursor-default py-2 space-x-2`}
                  onClick={() => {
                    setCurrentIndex(itemIdx);
                    if (currentIndex === itemIdx) {
                      setCurrentIndex(null);
                    }
                  }}
                >
                  {currentIndex === itemIdx ? (
                    <div>
                      <svg
                        width="24"
                        height="24"
                        viewBox="0 0 24 24"
                        fill="none"
                        xmlns="http://www.w3.org/2000/svg"
                      >
                        <rect
                          x="5.25"
                          y="5.25"
                          width="13.5"
                          height="13.5"
                          rx="2"
                          fill="#011C5A"
                          stroke="#011C5A"
                          strokeWidth="2"
                        />
                        <path
                          d="M10.3126 14.3458L7.96696 12.0002L7.16821 12.7933L10.3126 15.9377L17.0626 9.18766L16.2695 8.39453L10.3126 14.3458Z"
                          fill="white"
                          stroke="white"
                        />
                      </svg>
                    </div>
                  ) : (
                    <div>
                      <svg
                        width="24"
                        height="24"
                        viewBox="0 0 24 24"
                        fill="none"
                        xmlns="http://www.w3.org/2000/svg"
                      >
                        <rect
                          x="5.25"
                          y="5.25"
                          width="13.5"
                          height="13.5"
                          rx="2"
                          stroke="#D8D8D8"
                          strokeWidth="2"
                        />
                      </svg>
                    </div>
                  )}
                  <span className={`block truncate`}>
                    {item?.label || item?.value}
                  </span>
                </div>
              ))}
            </div>
          )}
        </div>
      </div>
    </div>
  );
}
