import * as React from "react";
import { AiOutlineLoading } from "react-icons/ai";

const LoadingOverly = (props: { visible: boolean; size?: number }) => {
  const { visible, size } = props;
  return (
    <section
      className={`absolute transition-all top-0 left-0 right-0 bottom-0 bg-white/60 flex items-center justify-center ${
        visible ? "z-50 opacity-100 visible" : "opacity-0 invisible"
      }`}
    >
      <AiOutlineLoading size={size || 24} className="animate-spin text-c5" />
    </section>
  );
};

export default LoadingOverly;
