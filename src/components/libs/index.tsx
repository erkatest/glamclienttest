import { useRouter } from "next/router";
import useMessage from "~/hooks/useMessage";

export function CustomLogin() {
  const f = useMessage();
  const router = useRouter();
  return (
    <button
      className="border-c1 rounded-full bg-c1 text-white px-4 py-2"
      onClick={() => {
        router.replace("/signin");
      }}
    >
      {f({ id: "header-menu-4" })}
    </button>
  );
}

export function NotFoundData() {
  const f = useMessage();
  return (
    <div className="flex flex-col items-center justify-center">
      <img src="/images/no-data.png" className="w-44" />
      <p
        className="font-bold text-center"
        dangerouslySetInnerHTML={{ __html: f({ id: "no-data" }) }}
      ></p>
    </div>
  );
}
