import React from "react";
import ReactPaginate, { ReactPaginateProps } from "react-paginate";
import useMessage from "~/hooks/useMessage";

interface IProps {
  setPage: (page: number) => void;
  page: number;
  total: number;
  limit: number;
}

const Pagination = ({ setPage, page, total, limit }: IProps) => {
  const f = useMessage();

  const handlePageClick = (event: any) => {
    setPage(event?.selected + 1);
    window.scrollTo({ top: 0, behavior: "smooth" });
  };

  const pageCount = Math.ceil(total / limit);

  return (
    <ReactPaginate
      breakLabel={<span className="mr-4">...</span>}
      nextLabel={
        <div className="flex items-center gap-[14px]">
          <p className="text-[14px] font-[500]">{f({ id: "next" })}</p>

          <img src="/images/arrow-dark.svg" width={20} />
        </div>
      }
      onPageChange={handlePageClick}
      // pageRangeDisplayed={3}
      pageCount={pageCount}
      previousLabel={
        <div className="flex items-center gap-[14px]">
          <img
            src="/images/arrow-dark.svg"
            className="-rotate-180"
            width={20}
          />

          <p className="text-[14px] font-[500]">{f({ id: "previous" })}</p>
        </div>
      }
      forcePage={page-1}
      containerClassName="flex flex-wrap items-center gap-4 justify-center"
      pageLinkClassName="p-[1rem] h-12 w-12 flex items-center justify-center rounded-full font-bold"
      activeLinkClassName="bg-[#F1F3F5]"
    />
  );
};

export default Pagination;
