import * as React from "react";
import useMessage from "../hooks/useMessage";
import Link from "next/link";
import { TabContext } from "~/context/tab";
import { useRouter } from "next/router";
interface IProps {}

const Footer = (props: IProps) => {
  const f = useMessage();
  const { tabs, active } = React.useContext(TabContext);
  const { pathname } = useRouter();

  const pathsWithDynamicBackground = ["/"];

  return (
    <section
      className={`relative ${
        pathsWithDynamicBackground.includes(pathname)
          ? "bg-default"
          : tabs.find((tab: any) => tab.type === active)?.backgroundColor
      }`}
    >
      <div className="container">
        <div className="">
          <div className="flex flex-col md:flex-row items-center gap-[2rem] py-10 text-white">
            <h1 className="uppercase min-w-max">
              {f({ id: "footer-title-1" })}
            </h1>
            <p className="text-white text-[14px] font-[400]">
              {f({ id: "footer-title-1-desc" })}
            </p>
          </div>
        </div>
        <div className="border-t border-white py-2">
          <div className="flex flex-col md:flex-row items-center justify-start md:space-x-2 md:gap-6 text-white">
            <Link
              href={"https://moc.gov.mn/"}
              target="_blank"
              className="text-center text-[16px] text-white py-4"
              dangerouslySetInnerHTML={{
                __html: f(
                  { id: "copyright" },
                  { date: new Date().getFullYear() }
                ),
              }}
            ></Link>
            <span className="mx-4">|</span>
            <Link
              href={"/privacy"}
              target="_blank"
              className="text-[16px] text-white font-lora m-0"
            >
              {f({ id: "privacy-policy" })}
            </Link>
          </div>
        </div>
      </div>
    </section>
  );
};

export default Footer;
