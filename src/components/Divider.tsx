import React from "react";

const Divider = () => {
  return (
    <div className="z-1 flex justify-center items-center relative my-20">
      <div className="w-full h-[1px] bg-c1 absolute"></div>
      <img src={"/images/logo-black.svg"} width={80} />
    </div>
  );
};

export default Divider;
