import React, { useEffect, useState } from "react";
import { DatePickerInput } from "@mantine/dates";
import "dayjs/locale/mn";
import { IconCalendarCheck, IconCalendarMonth } from "@tabler/icons-react";

export default function SelectDate(props: any) {
  const { startFunc, endFunc, startDateDefaultValue, endDateDefaultValue } =
    props;

  const today = new Date();
  const beforeTenYear = today.getFullYear() - 20;
  today.setFullYear(beforeTenYear);

  const [date_A_Value, setDate_A_Value] = useState<any>(today);
  const [date_B_Value, setDate_B_Value] = useState<any>(new Date());

  useEffect(() => {
    if (startDateDefaultValue !== "undefined") {
      const start_d =
        startDateDefaultValue == "undefined"
          ? today
          : new Date(startDateDefaultValue);
      setDate_A_Value(start_d);
    }

    if (endDateDefaultValue !== "undefined") {
      const end_d =
        endDateDefaultValue == "undefined"
          ? new Date()
          : new Date(endDateDefaultValue);
      setDate_B_Value(end_d);
    }
  }, [startDateDefaultValue, endDateDefaultValue]);

  return (
    <div className="flex gap-[8px] items-center">
      <div className="flex-1">
        <DatePickerInput
          value={date_A_Value}
          onChange={(d) => {
            startFunc(d);
          }}
          locale="mn"
          valueFormat="YYYY/MM/DD"
          className=""
          icon={
            <IconCalendarMonth
              size="1.4rem"
              stroke={1.5}
              className="ml-[6px]"
            />
          }
        />
      </div>
      <div>-</div>
      <div className="flex-1">
        <DatePickerInput
          value={date_B_Value}
          onChange={(d) => {
            endFunc(d);
          }}
          locale="mn"
          valueFormat="YYYY/MM/DD"
          icon={
            <IconCalendarCheck
              size="1.4rem"
              stroke={1.5}
              className="ml-[6px]"
            />
          }
        />
      </div>
    </div>
  );
}
