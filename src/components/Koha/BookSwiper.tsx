import React from "react";
import { Swiper, SwiperSlide } from "swiper/react";
import "swiper/css";

import BookCard from "~/components/BookCard";
import BookSkeleton from "~/components/BookCard/Skeleton";

const BookSwiper = (props: any) => {
  const { data, isLoading } = props;

  return (
    <div>
      {isLoading ? (
        <div className="grid lg:grid-cols-6 md:grid-cols-4 grid-cols-2 gap-14">
          {[1, 2, 3, 4, 5, 6].map((i: any) => {
            return <BookSkeleton key={i} />;
          })}
        </div>
      ) : data?.length > 6 ? (
        <Swiper
          height={310}
          breakpoints={{
            0: {
              slidesPerView: 2,
            },
            768: {
              slidesPerView: 4,
            },
            1024: {
              slidesPerView: 6,
            },
          }}
        >
          {(data || []).map((item: any, i: number) => {
            return (
              <SwiperSlide key={i}>
                <BookCard book={item} />
              </SwiperSlide>
            );
          })}
        </Swiper>
      ) : (
        <div className="grid lg:grid-cols-6 md:grid-cols-4 grid-cols-2">
          {(data || []).map((item: any, i: number) => {
            return <BookCard book={item} key={i} />;
          })}
        </div>
      )}
    </div>
  );
};

export default BookSwiper;
