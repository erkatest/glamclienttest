import React from "react";
import { useQuery } from "react-query";
import CustomSkeleton from "../Skeletons/CustomSkeleton";
import apiItem from "~/api/item";
import useParams from "~/hooks/useParams";
import Link from "next/link";

interface IProps {
  data: any;
  isLoading: boolean;
}

const RelatedContent = ({ data }: IProps) => {
  return (
    <div>
      <div className="grid grid-cols-4 gap-8">
        {(data?.data || []).map((item: any, i: number) => {
          return (
            <div key={`item-${i}`}>
              <Link href={`/detail/${item?._id}/koha`}>
                <img
                  src={item?.mainImg || "/images/empty.svg"}
                  className="w-200 h-120 object-contain"
                  onError={(e: any) => {
                    e.target.src = "/images/empty.svg";
                  }}
                />
                <h1 className="text-black font-bold text-sm overflow-hidden mb-2 line-clamp-1">
                  {item?.name}
                </h1>

                <p className="font-[500] text-gray text-sm overflow-hidden line-clamp-2 text-gray-500">
                  {item.description}
                </p>
              </Link>
            </div>
          );
        })}
      </div>
    </div>
  );
};

export default RelatedContent;
