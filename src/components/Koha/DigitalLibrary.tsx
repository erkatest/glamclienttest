import React from "react";
import { useState } from "react";
import { Switch } from "@headlessui/react";

const DigitalLibrary = (props: any) => {
  const { enabled, setEnabled, setPage } = props;

  return (
    <div className="container mx-auto">
      <div className="flex justify-center items-center gap-[15px] font-lora bg-[#01417A] w-[180px] py-[8px] rounded-[1rem] text-white">
        <div>Цахим ном</div>
        <Switch
          checked={enabled}
          onChange={() => {
            setEnabled(!enabled);
            setPage(1)
          }}
          className={`${
            enabled ? "bg-blue-600" : "bg-gray-200"
          } relative inline-flex h-6 w-11 items-center rounded-full`}
        >
          <span className="sr-only">Enable notifications</span>
          <span
            className={`${
              enabled ? "translate-x-6" : "translate-x-1"
            } inline-block h-4 w-4 transform rounded-full bg-white transition`}
          />
        </Switch>
      </div>
    </div>
  );
};

export default DigitalLibrary;
