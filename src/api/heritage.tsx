import axios from "axios";
import client from "~/lib/client";
var base64 = require("base-64");

const userNamePassword = "ict:group";
const coded = base64.encode(userNamePassword);

const heritage = {
  findImmovableSpecies: async () => {
    const res = await client.get("/heritage/species/immovable");

    if (res?.status === 200 || res?.status === 201) {
      return res?.data;
    } else {
      throw Error(res.data?.message);
    }
  },
  findMovableSpecies: async () => {
    const res = await client.get("/heritage/species/movable");

    if (res?.status === 200 || res?.status === 201) {
      return res?.data;
    } else {
      throw Error(res.data?.message);
    }
  },
  intangibleSpecies: async () => {
    const res = await client.get("/heritage/species/intangible");
    
    if (res?.status === 200 || res?.status === 201) {
      return res?.data;
    } else {
      throw Error(res.data?.message);
    }
  },
  century: async () => {
    const res = await client.get("/heritage/centuries");

    if (res?.status === 200 || res?.status === 201) {
      return res?.data;
    } else {
      throw Error(res.data?.message);
    }
  },
};
export default heritage;
