import client from "~/lib/client";

const Artist = {
  find: async (page?: number, limit?: number, search?: any) => {
    try {
      const res = await client.get(
        `/artwork/artist?page=${page}&limit=${limit}&search=${search}`
      );
      return { data: res?.data?.data, pagination: res?.data?.pagination };
    } catch (e: any) {
      throw e?.response?.data?.message;
    }
  },
  category: async () => {
    try {
      const res = await client.get(`/artwork/artist`);
      return { data: res?.data?.data };
    } catch (e: any) {
      throw e?.response?.data?.message;
    }
  },
};

export default Artist;
