import axios from "axios";
import client from "~/lib/client";

const apiContent = {
  findOne: async (content_id: any, type: any) => {
    const res = await client.get(`/items/${content_id}/${type}`);

    if (res?.status === 200) {
      return res?.data;
    } else {
      throw Error(res.data?.message);
    }
  },
  findFile: async (data: any, token: any) => {
    const res = await axios.post(
      `https://e-lib.cis.mn:3004/file/get/license`,
      data,
      {
        headers: {
          "Cache-Control": "no-cache",
          Pragma: "no-cache",
          Expires: "0",
          Authorization: `Bearer ${token}`,
        },
      }
    );

    if (res?.status === 200 || res?.status === 201) {
      return res?.data;
    } else {
      throw Error(res.data?.message);
    }
  },
  HomeSearchFind: async (search: any) => {
    let replaceSearch = search?.replace(new RegExp("%", "g"), "");
    const res = await client.get(
      `/p/api/v1/contents?page=1&page_size=10&search=${replaceSearch}`
    );

    if (res?.status === 200) {
      return res?.data;
    } else {
      throw Error(res.data?.message);
    }
  },
};

export default apiContent;
