import client from "~/lib/client";
import axios from "axios";

const apiCollections = {
  getCollections: async () => {
    const res = await axios.get(
      "https://artwork.devorchin.com/v1/collections",
      {
        headers: {
          "Content-Type": "application/json",
        },
      }
    );
    if (res?.status === 200 || res?.status === 201) {
        return res?.data;
    } else {
        throw Error(res.data?.message);
    }
  },
  getByIdCollections: async (id: any) => {
    const res = await axios.get(
      `https://artwork.devorchin.com/v1/collections/${id}`,
      {
        headers: {
          "Content-Type": "application/json",
        },
      }
    );
    if (res?.status === 200 || res?.status === 201) {
        return res?.data;
    } else {
        throw Error(res.data?.message);
    }
  },
};
export default apiCollections;
