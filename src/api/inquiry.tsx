import client from "~/lib/client";

const inquiryItem = {
  heritageTypes: async () => {
    try {
      const res = await client.get("/heritage/types");
      return { data: res?.data?.data };
    } catch (e: any) {
      throw e?.response?.data?.message;
    }
  },
  heritageSubTypes: async (id: string) => {
    try {
      const res = await client.get(`/heritage/types/${id}`);
      return { data: res?.data?.data };
    } catch (e: any) {
      throw e?.response?.data?.message;
    }
  },
  createReference: async (value: any) => {
    try {
      const res = await client.post(`/heritage/ref`, value);
      return { data: res?.data?.data, pagination: res?.data?.pagination };
    } catch (e: any) {
      throw e?.response?.data?.message;
    }
  },
  getReferenceList: async (value: any) => {
    try {
      const res = await client.post(`/heritage/refs`, value);
      return { data: res?.data?.data, pagination: res?.data?.pagination };
    } catch (e: any) {
      throw e?.response?.data?.message;
    }
  },
};

export default inquiryItem;
