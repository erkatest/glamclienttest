import client from "~/lib/client";

const apiMap = {
  find: async (type?: string, search?: any) => {
    try {
      const res = await client.get(
        `/location?page=1&limit=100&type=${type}&search=${search}`
      );
      return { data: res?.data?.data, pagination: res?.data?.pagination };
    } catch (e: any) {
      throw e?.response?.data?.message;
    }
  },
};

export default apiMap;
