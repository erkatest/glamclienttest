import client from "~/lib/client";

const apiSavedItem = {
  getSavedItem: async (limit: number, page: any) => {
    try {
      const res = await client.get(`/saved?limit=${limit}&page=${page}`);
      return { data: res?.data?.data, pagination: res?.data?.pagination };
    } catch (e: any) {
      throw e?.response?.data?.message;
    }
  },
  createSavedItem: async (data: any) => {
    try {
      const res = await client.post(`/saved`, data);
      return { data: res?.data };
    } catch (e: any) {
      throw e?.response?.data?.message;
    }
  },
  deleteItem: async (itemId: any) => {
    try {
      const res = await client.delete(`/saved/${itemId}`);
      return { data: res?.data?.data };
    } catch (e: any) {
      throw e?.response?.data?.message;
    }
  },
  checkItem: async (itemId: any) => {
    try {
      const res = await client.get(`/saved/check/${itemId}`);
      return { data: res?.data?.data };
    } catch (e: any) {
      throw e?.response?.data?.message;
    }
  },
};
export default apiSavedItem;
