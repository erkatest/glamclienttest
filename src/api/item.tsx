import client from "~/lib/client";

const apiItem = {
  find: async (
    type?: string,
    page?: number,
    limit?: number,
    search?: any,
    sort?: any,
    sortOrder?: any,
    filter?: any
  ) => {
    const num = Number(page);
    try {
      const res = await client.post(`/items`, {
        type: type,
        page: num,
        limit: limit,
        search: search,
        sort_by: sort,
        sort_order: sortOrder,
        filter: filter,
      });
      return { data: res?.data?.data, pagination: res?.data?.pagination };
    } catch (e: any) {
      throw e?.response?.data?.message;
    }
  },
  findById: async (id: any, type: any) => {
    try {
      const res = await client.get(`/items/${id}/${type}`);
      return { data: res?.data?.data };
    } catch (e: any) {
      throw e?.response?.data?.message;
    }
  },
  heritageFindById: async (id: any, type: any) => {
    try {
      const res = await client.get(
        `/items/${id}/heritage?heritage_type=${type}`
      );
      return { data: res?.data?.data };
    } catch (e: any) {
      throw e?.response?.data?.message;
    }
  },
  findViewedItemsAll: async (page: any, limit: any) => {
    try {
      const res = await client.get(
        `/items/view_history_all?page=${page}&limit=${limit}`
      );
      return { data: res?.data?.data };
    } catch (e: any) {
      throw e?.response?.data?.message;
    }
  },
  specialItems: async () => {
    try {
      const res = await client.get(`/library/books/special_books`);
      return { data: res?.data?.data };
    } catch (e: any) {
      throw e?.response?.data?.message;
    }
  },
  category: async () => {
    try {
      const res = await client.get(`/artwork/categories`);
      return { data: res?.data?.data };
    } catch (e: any) {
      throw e?.response?.data?.message;
    }
  },
  childCategory: async (id:any) => {
    try {
      const res = await client.get(`/artwork/categories?parent=${id}`);
      return { data: res?.data?.data };
    } catch (e: any) {
      throw e?.response?.data?.message;
    }
  },
  relatedItems: async (id: number) => {
    try {
      const res = await client.get(`/library/books/related_books/${id}`);
      return { data: res?.data?.data };
    } catch (e: any) {
      throw e?.response?.data?.message;
    }
  },
  commonSearch: async (limit?: number) => {
    try {
      const res = await client.get(`/items/common?limit=${limit}`);
      return { data: res?.data?.data };
    } catch (e: any) {
      throw e?.response?.data?.message;
    }
  },
  contentCards: async () => {
    try {
      const res = await client.get(`/content-item`);
      return { data: res?.data?.data };
    } catch (e: any) {
      throw e?.response?.data?.message;
    }
  },
  searchHistory: async (page?: number, limit?: number) => {
    try {
      const res = await client.get(
        `/items/history?page=${page}&limit=${limit}`
      );
      return { data: res?.data?.data, pagination: res?.data?.pagination };
    } catch (e: any) {
      throw e?.response?.data?.message;
    }
  },
  saveSearch: async (search?: string) => {
    try {
      const res = await client.get(`/items/save_search?search=${search}`);
      return { data: res?.data?.data, pagination: res?.data?.pagination };
    } catch (e: any) {
      throw e?.response?.data?.message;
    }
  },
  deleteHistory: async (id: string) => {
    try {
      const res = await client.delete(`/items/search-history/${id}`);
      return { data: res?.data?.data };
    } catch (e: any) {
      throw e?.response?.data?.message;
    }
  },
  deleteAllHistory: async () => {
    try {
      const res = await client.delete(`/items/search-history`);
      return { data: res?.data?.data };
    } catch (e: any) {
      throw e?.response?.data?.message;
    }
  },
  // removeItem: async (itemId: string, folderId: string, type: string) => {
  //   try {
  //     const res = await client.patch(
  //       `/items/${itemId}/folder/${folderId}?type=${type}`
  //     );
  //     return { data: res?.data?.data };
  //   } catch (e: any) {
  //     throw e?.response?.data?.message;
  //   }
  // },
  heritageProvince: async () => {
    try {
      const res = await client.get(`/heritage/provinces`);
      return { data: res?.data?.data };
    } catch (e: any) {
      throw e?.response?.data?.message;
    }
  },
  heritageRegion: async (id: string) => {
    try {
      const res = await client.get(`/heritage/provinces/${id}/regions`);
      return { data: res?.data?.data };
    } catch (e: any) {
      throw e?.response?.data?.message;
    }
  },
};
export default apiItem;
