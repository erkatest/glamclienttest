import axios from "axios";
import client from "~/lib/client";
import store from "~/utils/store";

const Users = {
  me: async () => {
    try {
      const res = await client.get(`/user/me`);
      return {
        success: true,
        data: res?.data?.data,
        status: res?.data?.status,
      };
    } catch (e: any) {
      return { success: false, e: e?.response?.data?.message };
    }
  },

  getToken: async () => {
    try {
      const res = await client.get(`/library/get_token`);
      return {
        success: true,
        data: res?.data?.data,
        status: res?.data?.status,
      };
    } catch (e: any) {
      return { success: false, e: e?.response?.data?.message };
    }
  },
  connect: async (password: any) => {
    try {
      const res = await client.post(`/library/connect`, {
        library_password: password,
      });
      return {
        success: true,
        data: res?.data?.data,
        status: res?.data?.status,
      };
    } catch (e: any) {
      return { success: false, e: e?.response?.data?.message };
    }
  },
  update: async (values: any) => {
    try {
      const res = await client.patch(`/auth/info`, values);
      return {
        success: true,
        data: res?.data?.data,
        status: res?.data?.status,
      };
    } catch (e: any) {
      return { success: false, e: e?.response?.data?.message };
    }
  },
  upload: async (file: any) => {
    try {
      const res = await client.post(`/upload/glam`, file);
      return {
        success: true,
        data: res?.data?.data,
        status: res?.data?.status,
      };
    } catch (e: any) {
      return { success: false, e: e?.response?.data?.message };
    }
  },
  login: async (email: string, password: string) => {
    try {
      const res = await client.post(`/auth/login`, { email, password });
      return {
        success: true,
        data: res?.data?.data,
        status: res?.data?.status,
      };
    } catch (e: any) {
      return { success: false, e: e?.response?.data?.message };
    }
  },
  signup: async (
    email: string,
    password: string,
    firstname: string,
    lastname: string,
    foreign?: boolean
  ) => {
    try {
      const res = await client.post(`/auth/register`, {
        email,
        password,
        firstname,
        lastname,
        foreign,
      });
      return {
        success: true,
        data: res?.data?.data,
        status: res?.data?.status,
      };
    } catch (e: any) {
      throw e?.response?.data?.message;
    }
  },
  verify: async (email: string, verification: string) => {
    try {
      const res = await client.patch(`/auth/verify`, { email, verification });
      return {
        success: true,
        data: res?.data?.data,
        status: res?.data?.status,
      };
    } catch (e: any) {
      return { success: false, e: e?.response?.data?.message };
    }
  },
  resend: async (email: string) => {
    try {
      const res = await client.patch(`/auth/resend`, { email });
      return {
        success: true,
        data: res?.data?.data,
        status: res?.data?.status,
      };
    } catch (e: any) {
      return { success: false, e: e?.response?.data?.message };
    }
  },
  forgotPassword: async (email: string) => {
    try {
      const res = await client.patch(`/auth/forgotPassword`, { email });
      return {
        success: true,
        data: res?.data?.data,
        status: res?.data?.status,
      };
    } catch (e: any) {
      return { success: false, e: e?.response?.data?.message };
    }
  },
  changePassword: async (values: string) => {
    try {
      const res = await client.patch(`/auth/changePassword`, values);
      return {
        success: true,
        data: res?.data?.data,
        status: res?.data?.status,
      };
    } catch (e: any) {
      return { success: false, e: e?.response?.data?.message };
    }
  },
};

export default Users;
