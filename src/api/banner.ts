import client from "~/lib/client";

const apiBanner = {
  find: async () => {
    try {
      const res = await client.get(`/banner`);
      return { data: res?.data?.data };
    } catch (e: any) {
      throw e?.response?.data?.message;
    }
  },
};

export default apiBanner;
