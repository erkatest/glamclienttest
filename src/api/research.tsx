import client from "~/lib/client";

const apiResearch = {
  insert: async (data: any) => {
    const res = await client.post(`/research`, data);
    if (res?.status === 200) {
      return res?.data;
    } else {
      throw Error(res.data?.message);
    }
  },
  find: async (id: any) => {
    const res = await client.get(`/research?content_id=${id}`);
    if (res?.status === 200) {
      return res?.data;
    } else {
      throw Error(res.data?.message);
    }
  },
  findFolder: async () => {
    try {
      const res = await client.get(`/banner`);
      return { data: res?.data?.data };
    } catch (e: any) {
      throw e?.response?.data?.message;
    }
  },
  update: async (id: string, data: any) => {
    const res = await client.put(`/research/${id}`, data);
    if (res?.status === 200) {
      return res?.data;
    } else {
      throw Error(res.data?.message);
    }
  },
  delete: async (id: string) => {
    const res = await client.delete(`/research/${id}`);
    if (res?.status === 200) {
      return res?.data;
    } else {
      throw Error(res.data?.message);
    }
  },
};

export default apiResearch;
