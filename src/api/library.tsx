import client from "~/lib/client";

const apiLibrary = {
  findBooks: async (
    page: number,
    limit: number,
    // "book_category_list": [],
    // "book_type_list": [],
    // "press_year_min": null,
    // "press_year_max": null,
    // "price_min": null,
    // "price_max": null,
    search_text?: string,
    sortBy?: string,
    sortOrder?: string
  ) => {
    try {
      const res = await client.post(`/library/books/find_books`, {
        page: page,
        take: limit,
        search_text: search_text,
        sortBy: sortBy ? "" : "",
        sortOrder: sortOrder ? "" : "",
      });

      return { data: res?.data?.data, pagination: res?.data?.pagination };
    } catch (e: any) {
      throw e?.response?.data?.message;
    }
  },
  findBookById: async (id: any) => {
    try {
      const res = await client.get(`/library/books/${id}`);
      return { data: res?.data?.data };
    } catch (e: any) {
      throw e?.response?.data?.message;
    }
  },
  findSpecialBooks: async () => {
    try {
      const res = await client.get(`/library/books/special_books`);
      return { data: res?.data?.data };
    } catch (e: any) {
      throw e?.response?.data?.message;
    }
  },
  findNewBooks: async () => {
    try {
      const res = await client.get(`/library/books/new_books`);
      return { data: res?.data?.data };
    } catch (e: any) {
      throw e?.response?.data?.message;
    }
  },
};

export default apiLibrary;
