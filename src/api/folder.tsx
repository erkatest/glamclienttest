import client from "~/lib/client";

const Folder = {
  find: async () => {
    try {
      const res = await client.get(`/folder`);
      return { data: res?.data?.data };
    } catch (e: any) {
      throw e?.response?.data?.message;
    }
  },
  createFolder: async (name: string, color: string) => {
    try {
      const res = await client.post(`/folder`, {
        name,
        color,
      });
      return { data: res?.data?.data };
    } catch (e: any) {
      throw e?.response?.data?.message;
    }
  },
  addToFolder: async (id: string, itemType: string, folders: any) => {
    try {
      const res = await client.post(`/items/${id}/save`, {
        itemType,
        folders,
      });
      return { data: res?.data?.data };
    } catch (e: any) {
      throw e?.response?.data?.message;
    }
  },
  delete: async (id: string) => {
    try {
      const res = await client.delete(`/folder/${id}`);
      return { data: res?.data?.data };
    } catch (e: any) {
      throw e?.response?.data?.message;
    }
  },
  update: async (id: string, data: any) => {
    try {
      const res = await client.patch(`/folder/${id}`, data);
      return { data: res?.data?.data };
    } catch (e: any) {
      throw e?.response?.data?.message;
    }
  },
};

export default Folder;
