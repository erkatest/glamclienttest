import * as React from "react";
import rangy from "rangy";
import "rangy/lib/rangy-classapplier";
import "rangy/lib/rangy-highlighter";
import { Button } from "@mantine/core";

interface IProps {
  target?: any;
}

const Rangy2 = (props: IProps, ref: any) => {
  const hltrRef = React.useRef<any>();
  const { target } = props;
  // const { selection } = React.useContext(ReaderContext);

  const handleHighlight = () => {
    if (hltrRef.current) {
      // const selection = window.getSelection();
      // const range = selection?.getRangeAt(0);
      // hltrRef.current.doHighlight(range);
    }
  };
  const getSerialize = () => {};

  const setDeserialize = (notes: string[]) => {};

  React.useEffect(() => {
    const el = document.getElementById("viewer");
    if (el) {
      hltrRef.current = new (window as any).TextHighlighter(el, {
        // contextClass: "textLayer",
      });
    }
    ref.current = {
      handleHighlight,
      getSerialize,
      setDeserialize,
    };
  }, []);

  return (
    <>
      {/* <Button
        onClick={() => {
          handleHighlight();
        }}
      >
        Highlight :P
      </Button>
      <Button
        onClick={() => {
          alert(getSerialize());
        }}
      >
        Save
      </Button>

      <Button
        onClick={() => {
          const serialize = "7673$7945$24$highlight$";
          setDeserialize([serialize]);
        }}
      >
        Set
      </Button>

      <Button
        onClick={() => {
          console.log("HIGHLIGHER: ", refHighlighter.current?.highlights);
        }}
      >
        LIST
      </Button> */}
    </>
  );
};

export default React.forwardRef(Rangy2);
