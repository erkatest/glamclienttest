import * as React from "react";
import ePub from "epubjs";

import { Container, Drawer } from "@mantine/core";
import { ImSpinner5 } from "react-icons/im";
import EPubToc from "./Toc";
import SmartTools from "../SmartTools";
import { useViewportSize } from "@mantine/hooks";
import { BiBookContent } from "react-icons/bi";
import { useSearchParams } from "next/navigation";
import ReaderContext from "~/context/Reader";
import NodeRSA from "node-rsa";
import { useQuery } from "react-query";
import apiContent from "~/api/content";
import bookDecryption from "~/components/DecryptionBook";
interface IProps {
  id: string;
  token: any;
}

const EpubPro = (props: IProps) => {
  const [hasError, setHasError] = React.useState(false);
  const renditionRef = React.useRef<any>();
  const {
    handleEpub,
    showSidebar,
    setEpubFirst,
    setEpubLast,
    loading,
    setLoading,
    setEpub,
    showToc,
    setShowToc,
    setTotalPage,
    handleEpubNext,
    handleEpubPrev,
    setToolPos,
    setShowSmartTool,
    setSelection,
    selection,
    full,
    totalPage,
  } = React.useContext(ReaderContext);

  const [target, setTarget] = React.useState<any>();
  const [opened, setOpened] = React.useState(false);
  const params = useSearchParams();

  const page = params.get("page");

  const tmpHighlightRef = React.useRef<any>();
  const [tocs, setTocs] = React.useState([]);

  const [id, type, t] = [params.get("id"), params.get("type"), params.get("t")];
  const getRect = (target: any, frame: any) => {
    const rect = target.getBoundingClientRect();
    const viewElementRect = frame
      ? frame.getBoundingClientRect()
      : { left: 0, top: 0 };
    const left = rect.left + viewElementRect.left;
    const right = rect.right + viewElementRect.left;
    const top = rect.top + viewElementRect.top;
    const bottom = rect.bottom + viewElementRect.top;
    return { left, right, top: top + 30, bottom };
  };

  const getSelectionText = (target: any) => {
    let text = "";
    if ((target as any)?.window.getSelection) {
      text = (target as any)?.window?.getSelection()?.toString() || "";
    } else if (
      (target as any)?.document?.selection &&
      (target as any)?.document?.selection.type != "Control"
    ) {
      text = (target as any)?.document?.selection.createRange().text;
    }
    return text;
  };
  const loadedRef = React.useRef(false);
  const [keys, setKeys] = React.useState({ publicKey: "", privateKey: "" });
  React.useEffect(() => {
    const key = new NodeRSA({ b: 2048 });
    const publicKey = key.exportKey("public");
    const privateKey = key.exportKey("private");
    setKeys({
      publicKey: publicKey,
      privateKey: privateKey,
    });
  }, []);

  const { data, isLoading } = useQuery(
    ["content-file", keys?.publicKey, loadedRef],
    () =>
      keys?.publicKey &&
      loadedRef.current === false &&
      apiContent.findFile(
        {
          book_id: id,
          book_file_type_id: 2,
          public_key: keys?.publicKey,
        },
        t
      ),
    { enabled: !!keys?.publicKey }
  );

  const [pdfBuffer, setBuffer] = React.useState<any>();

  React.useEffect(() => {
    if (data && loadedRef.current === false) {
      if (data?.success === false) {
        setHasError(true);
        return;
      }
      const decryptedData: any = bookDecryption(data, keys?.privateKey);
      var binaryString = atob(decryptedData);
      var bytes = new Uint8Array(binaryString.length);
      for (var i = 0; i < binaryString.length; i++) {
        bytes[i] = binaryString.charCodeAt(i);
      }
      setBuffer(bytes.buffer);
      loadedRef.current = true;
    } else {
      console.log(data);
    }
  }, [data, loadedRef]);

  const init = async (pdfBuffer: string) => {
    try {
      setLoading(true);

      let width: any =
        document.getElementById("get-width")?.clientWidth || window.innerWidth;
      width /= 2;
      const book: any = ePub(pdfBuffer);
      renditionRef.current = book.renderTo("area", {
        manager: "continuous",
        flow: "paginated",
        spread: "always",
        width: `${
          width - 50 < 500
            ? (document.getElementById("get-width")?.clientWidth ||
                window.innerWidth) - 50
            : width - 50
        }px`,
        height: `${window.innerHeight - 300}px`,
        allowScriptedContent: true,
        ignoreClass: "annotator-hl",

        spreads: true,
        fixedLayout: true,
      });

      const rendition = renditionRef.current;

      const displayed = await rendition.display();
      book.ready
        .then(() => {
          return book.locations.generate(1024); // Adjust the number as needed
        })
        .then(() => {
          const totalLocations = book.locations.length(); // Total number of locations
          setTotalPage(totalLocations); // Set total page count

          // Additional setup...
        });
      book.ready
        .then(() => {
          return book.locations.generate(1024);
        })
        .then(() => {
          if (page && page !== "undefined") {
            const pageNumber = parseInt(page);

            const cfi = book.locations.cfiFromPercentage(
              pageNumber / book.locations.total
            );

            renditionRef.current.display(cfi);
          }
        });

      rendition.on("rendered", (rendition: any, iframe: any) => {
        setTarget(iframe);
      });

      book.ready
        .then(() => {
          return book.locations.generate(1024);
        })
        .then(() => {
          setTocs(book.navigation.toc);
        });

      rendition.on("relocated", function (location: any) {
        setEpub(location);
        if (location.atEnd) {
          setEpubLast(true);
          setEpubFirst(false);
        } else {
          setEpubLast(false);
        }

        if (location.atStart) {
          setEpubFirst(true);
          setEpubLast(false);
        } else {
          setEpubFirst(false);
        }
      });

      // KEYBOARD PREV/NEXT
      const keyListener = function (e: any) {
        // Left Key
        if ((e.keyCode || e.which) == 37) {
          rendition.prev();
        }

        // Right Key
        if ((e.keyCode || e.which) == 39) {
          rendition.next();
        }
      };

      rendition.on("keyup", keyListener);
      document.addEventListener("keyup", keyListener, false);

      rendition.themes.default({
        "::selection": {
          background: "rgba(255, 165, 0, 0.5)",
        },
        ".highlight": {
          backgroundColor: "rgba(255, 165, 0, 0.5)",
          borderBottom: "2px solid orange",
          color: "#000",
        },
      });

      rendition.themes.registerCss(
        "default",
        `.highlight { background:  rgba(255, 165, 0, 0.5); border-bottom: solid 2px orange; color: #000; text-decoration: none; }`
      );

      rendition.on("selected", function (cfiRange: any, contents: any) {
        const selection = getSelectionText(contents);
        if (selection.replace(/[\r\n]/gm, "")?.length > 0) {
          setShowSmartTool(true);
          setSelection(selection);
        } else {
          setSelection("");
          setShowSmartTool(false);
        }
      });

      handleEpub({ rendition, book });
      setLoading(false);
    } catch (e) {
      setHasError(true);
      console.log("ERROR: ", e);
    }
  };

  React.useEffect(() => {
    if (pdfBuffer) {
      init(pdfBuffer);
    }
  }, [pdfBuffer]);

  const handleRedirect = async (url: string) => {
    const getCfiFromHref = async (href: string) => {
      const id = href.split("#")[1];
      const item = renditionRef.current.book.spine.get(href);
      await item.load(
        renditionRef.current.book.load.bind(renditionRef.current.book)
      );
      const el = id ? item.document.getElementById(id) : item.document.body;
      return item.cfiFromElement(el);
    };

    const cfi = await getCfiFromHref(url);
    await renditionRef.current.display(cfi);
  };

  const handleRemoveHighlight = (cfiRange: any) => {
    renditionRef.current?.annotations.remove(cfiRange);
  };

  const { height, width } = useViewportSize();

  React.useLayoutEffect(() => {
    if (renditionRef.current && renditionRef.current?.resize) {
      let width =
        document.getElementById("get-width")?.clientWidth || window.innerWidth;
      width /= 2;

      renditionRef.current?.resize?.(
        width - 50 < 500
          ? (document.getElementById("get-width")?.clientWidth ||
              window.innerWidth) - 50
          : width - 50,
        height - 300
      );
    }
  }, [full, width]);

  return (
    <>
      <Drawer
        opened={showToc}
        onClose={() => setShowToc(false)}
        title={
          <>
            <span className="text-[#262338] font-bold text-[16px]">Гарчиг</span>
            <div
              className={`left-4 right-4 absolute -bottom-4 h-[2px] my-4 bg-[#A0A3BD]/50`}
            ></div>
          </>
        }
        zIndex={999999999}
        styles={{
          body: {
            padding: 0,
          },
        }}
      >
        <EPubToc
          tocs={tocs}
          render={async (url) => {
            await handleRedirect(url);
            setShowToc(false);
          }}
        />
      </Drawer>
      <Container size="xl" id="epub-container">
        {((loading && !hasError) || isLoading) && (
          <div
            className={`fixed top-0 left-0 right-0 ${
              showSidebar ? "lg:pr-[400px]" : ""
            } bottom-0 z-[51] flex w-full h-full items-center bg-white justify-center flex-col`}
          >
            <div className="h-full flex flex-col justify-center items-center">
              <ImSpinner5 className="text-4xl animate-spin text-slate-400" />
              <p className="text-xl text-slate-400 font-thin mt-4">
                Файл татаж байна. Та түр хүлээнэ үү.
              </p>
            </div>
          </div>
        )}

        {hasError && (
          <div className="absolute inset-0 bg-[#EFF0F6] mt-[90px] md:mt-[70px] w-full h-[calc(100vh_-_170px)] z-50 flex items-center justify-center flex-col">
            <BiBookContent size={48} className="mb-5" />

            <code className="font-mono text-lg relative text-center">
              Уучлаарай тухай бүтээлийг унших боломжгүй байна!
            </code>
          </div>
        )}

        <div>
          <div id="area"></div>
        </div>
      </Container>
      {id && target && <SmartTools target={target} id={id} />}
    </>
  );
};

export default EpubPro;
