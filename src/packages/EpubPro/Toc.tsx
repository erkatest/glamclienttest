import { ScrollArea } from "@mantine/core";
import * as React from "react";
import { VscNotebookTemplate } from "react-icons/vsc";
import ReaderContext from "~/context/Reader";

interface IProps {
  render: (url: string) => void;
  tocs: any[];
}

const EPubToc = (props: IProps) => {
  const { setCurrentPosition, currentPosition } =
    React.useContext(ReaderContext);

  return (
    <>
      <div className="px-6 py-2">
        <div id="toc" className="flex space-y-4 flex-col">
          {props?.tocs?.length > 0 ? (
            props?.tocs?.[0]?.subitems?.map((toc: any, i: any) => (
              <div
                onClick={() => {
                  setCurrentPosition(toc.href);
                  props.render(toc.href);
                }}
                key={toc.href}
                className={`${
                  currentPosition === toc.href
                    ? "text-[#4E4B66] font-bold"
                    : "text-[#6E7191] cursor-pointer hover:text-[#4E4B66]"
                } flex justify-between`}
              >
                <span>
                  {toc.label} - {toc?.subitems?.[0]?.label}
                </span>
              </div>
            ))
          ) : (
            <div className="flex flex-col justify-center min-h-[200px] items-center p-4">
              <p className="text-center text-sm text-slate-400">
                Гарчгийн мэдээлэл <br /> байхгүй байна
              </p>
              <VscNotebookTemplate className="text-6xl mt-4 text-slate-300" />
            </div>
          )}
        </div>
      </div>
    </>
  );
};

export default EPubToc;
