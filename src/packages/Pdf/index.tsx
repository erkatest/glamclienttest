import Loader from "~/components/Loader";

import * as React from "react";
import { RiFileForbidFill } from "react-icons/ri";

import { useMutation, useQuery, useQueryClient } from "react-query";
import apiContent from "~/api/content";
import { Drawer, LoadingOverlay } from "@mantine/core";

import SmartTools from "../SmartTools";

import PDFJs4 from "../PDFJs4";

import ReaderContext from "~/context/Reader";
import PDFJs42 from "../PDFJs4";
import { useSearchParams } from "next/navigation";

// interface IProps {}
const PDF = (props: {
  id: any;
  onContextMenu: (pointer: { x: number; y: number }) => void;
  isPreview?: any;
  page: number;
  fileName?: string;
  contentSlug?: string;
  hideTools?: boolean;
  itemId: string;
  token: any;
}) => {
  const { showToc, setShowToc, setShowSmartTool, setSelection } =
    React.useContext(ReaderContext);

  const [height, setHeight] = React.useState<number>();
  const params = useSearchParams();
  const [id, type, t] = [params.get("id"), params.get("type"), params.get("t")];

  function getSelectionText() {
    let text = "";
    if (window.getSelection) {
      text = window?.getSelection()?.toString() || "";
    } else if (
      (document as any)?.selection &&
      (document as any)?.selection.type != "Control"
    ) {
      text = (document as any)?.selection.createRange().text;
    }
    return text;
  }

  return (
    <>
      <Drawer
        opened={showToc}
        onClose={() => setShowToc(false)}
        title={
          <>
            <span className="text-[#262338] font-bold text-[16px]">Гарчиг</span>
            <div
              className={`left-4 right-4 absolute -bottom-4 h-[2px] my-4 bg-[#A0A3BD]/50`}
            ></div>
          </>
        }
        zIndex={999999999}
        styles={{
          body: {
            padding: 0,
          },
        }}
      >
        {/* <div className="px-6 py-2">
          PDF TOC
          {tocs?.length > 0 ? (
            <div className="flex space-y-4 flex-col">
              {tocs?.map((t: any, ti: number) => (
                <div
                  key={`tocs-${itemId}-${ti}`}
                  className={`${
                    page == t.start
                      ? "text-[#4E4B66] font-bold"
                      : "text-[#6E7191] cursor-pointer hover:text-[#4E4B66]"
                  } flex justify-between`}
                  onClick={() => {
                    redirectPage(Number.parseInt(t.start, 10));
                    setShowToc(false);
                  }}
                >
                  <span>{t.description}</span>
                  <span>{t.start || "-"}</span>
                </div>
              ))}
            </div>
          ) : (
            <div className="flex flex-col justify-center min-h-[200px] items-center p-4">
              <p className="text-center text-sm text-slate-400">
                Гарчгийн мэдээлэл <br /> байхгүй байна
              </p>
              <VscNotebookTemplate className="text-6xl mt-4 text-slate-300" />
            </div>
          )}
        </div> */}
      </Drawer>

      {id && <SmartTools target={{ document: document }} id={id} />}
      {/* {!lo && (
        <div className="absolute top-0 left-0 right-0 bottom-0 z-10 flex items-center justify-center flex-col">
          <div className="w-[200px]">
            <Loader />
          </div>
          <code className="uppercase font-mono text-xs relative -top-[60px]">
            уншиж байна
          </code>
        </div>
      )} */}

      <div
        style={
          height
            ? {
                maxHeight: `${height}px`,
                overflow: "hidden",
              }
            : {}
        }
        className={"relative"}
      >
        {/* {props?.pdfBuffer && ( */}
        <PDFJs42
          id={props?.id}
          token={props?.token}
          events={{
            onContextMenu: (e: any) => {
              e.preventDefault();
              e.stopPropagation();
              // props?.onContextMenu({ x: e.clientX, y: e.clientY });
              setShowSmartTool(false);
              setSelection("");
            },
            onMouseDown: (e: any) => {
              setShowSmartTool(false);
              setSelection("");
            },
            onMouseUp: (e: any) => {
              e.preventDefault();
              e.stopPropagation();

              if (getSelectionText().replace(/[\r\n]/gm, "")?.length > 0) {
                setSelection(getSelectionText().replace(/[\r\n]/gm, ""));
                setShowSmartTool(true);
              } else {
                setShowSmartTool(false);
              }
            },
            onTouchStart: (e: any) => {
              setShowSmartTool(false);
              setSelection("");
            },
            onTouchEnd: (e: any) => {
              if (getSelectionText().replace(/[\r\n]/gm, "")?.length > 0) {
                setSelection(getSelectionText().replace(/[\r\n]/gm, ""));
                setShowSmartTool(true);
              } else {
                setShowSmartTool(false);
              }
            },
          }}
        />
        {/* )} */}
      </div>
    </>
  );
};

export default PDF;
