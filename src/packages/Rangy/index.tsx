import * as React from "react";
import rangy from "rangy";
import "rangy/lib/rangy-classapplier";
import "rangy/lib/rangy-highlighter";
import { Button } from "@mantine/core";

interface IProps {
  iframe: any;
}

const Rangy = (props: IProps, ref: any) => {
  const refHighlighter = React.useRef<any>();
  const { iframe } = props;
  React.useEffect(() => {
    rangy.init();
    refHighlighter.current = rangy.createHighlighter(iframe);
    refHighlighter.current?.addClassApplier(
      rangy.createClassApplier("highlight", {
        ignoreWhiteSpace: true,
        tagNames: ["a", "span"],
        elementTagName: "a",
        elementProperties: {
          href: "#",
          onclick: function () {
            const highlight =
              refHighlighter.current?.getHighlightForElement(this);
            if (highlight?.id) {
              if (window.confirm("Устгах уу ?")) {
                refHighlighter.current?.removeHighlights([highlight]);
              }
            }
            return false;
          },
        },
      })
    );
  }, [iframe]);

  const handleHighlight = React.useCallback(() => {
    refHighlighter.current.highlightSelection("highlight", {});
  }, []);

  const getSerialize = React.useCallback(() => {
    const selection = refHighlighter.current.serialize();
    const reference = selection.split("|").pop();
    return reference;
  }, []);

  const setDeserialize = React.useCallback((notes: string[]) => {
    try {
      refHighlighter.current.deserialize(
        `type:textContent|${notes?.join("|")}`
      );
    } catch (e) {
      e;
    }
  }, []);

  React.useEffect(() => {
    ref.current = {
      handleHighlight,
      getSerialize,
      setDeserialize,
    };
  }, []);

  return (
    <>
      {/* <Button
        onClick={() => {
          handleHighlight();
        }}
      >
        Highlight :P
      </Button>
      <Button
        onClick={() => {
          alert(getSerialize());
        }}
      >
        Save
      </Button>

      <Button
        onClick={() => {
          const serialize = "7673$7945$24$highlight$";
          setDeserialize([serialize]);
        }}
      >
        Set
      </Button>

      <Button
        onClick={() => {
          console.log("HIGHLIGHER: ", refHighlighter.current?.highlights);
        }}
      >
        LIST
      </Button> */}
    </>
  );
};

export default React.forwardRef(Rangy);
