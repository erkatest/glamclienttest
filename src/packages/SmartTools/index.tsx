import { useRouter } from "next/router";
import * as React from "react";

import RightClickActions from "~/components/Reader/Actions/RightClick";
import CitationAction from "~/components/Reader/Actions/Citation";
import Note from "~/components/Reader/Actions/Note";
import { useMutation, useQuery } from "react-query";
import apiContent from "~/api/content";

import apiResearch from "~/api/research";
import { useSearchParams } from "next/navigation";
import { ImSpinner5 } from "react-icons/im";
import { useClickOutside, useWindowEvent } from "@mantine/hooks";
import useTargetEvent from "./useTargetEvent";
import Rangy from "../Rangy";
import { AnimatePresence, motion } from "framer-motion";
import Rangy2 from "../Rangy2";
import ReaderContext from "~/context/Reader";
import { toast } from "react-toastify";
import useMessage from "~/hooks/useMessage";

// interface IProps {}
const SmartTools = (props: {
  id: string;

  target: any;
}) => {
  const refRangy = React.useRef<any>();

  const {
    setShowSidebar,
    queryNotes,
    page,
    showSmartTool,
    setShowSmartTool,
    toolPos,
    setSelection,
    selection,
    domLoaded,
    epub,
  } = React.useContext(ReaderContext);

  const { id } = props;

  const { data: dataNotes } = useQuery(
    ["contents", id, "notes", page],
    () => id && apiResearch.find(id),
    {
      onSettled(data, error) {
        // initHighlight(data);
      },
    }
  );

  const initHighlight = (data: any) => {
    if (data?.data?.length > 0) {
      const [content] = data?.data;
      const notes = content?.research_info || [];
      const serializes = notes
        ?.filter(
          (n: any) =>
            n.settings?.highlightSerialize &&
            (page === n?.settings?.page || n?.settings?.page === 0)
        )
        ?.map((n: any) => n.settings?.highlightSerialize);

      serializes?.length > 0 && refRangy?.current?.setDeserialize(serializes);
    }
  };

  const dataQuery = useQuery(
    ["contents", id],
    () => id && apiContent.findOne(id, "koha")
  );

  // const [selection, setSelection] = React.useState("");
  const [aiText, setAiText] = React.useState("");
  const router = useRouter();
  const [showQuote, setShowQuote] = React.useState(false);
  const [showNote, setShowNote] = React.useState(false);
  const f = useMessage();
  const params = useSearchParams();
  const type = params.get("type");
  const mutationResearch = useMutation(apiResearch.insert, {
    onSuccess: () => {
      queryNotes.refetch();
      setShowSidebar(true);
      setShowQuote(false);
      setShowNote(false);

      toast.success(f({ id: "success" }));
    },
  });

  const handleSaveQuote = (note: string, settings: any) => {
    mutationResearch.mutateAsync({
      type: "quote",
      page_number: type === "pdf" ? page : epub?.start?.displayed.page,
      reference: settings.clearText,
      note: settings.note,
      ...settings,
      settings: {
        ...(settings || {}),
        highlightSerialize: getHighlight()?.replace("$note$", "$highlight$"),
        page: type === "pdf" ? page : epub?.start?.displayed.page,
        fileType: type,
      },
      content_id: params.get("id"),
      content_type: "library",
    });
  };
  const handleSaveNote = (selection: string, note: string, settings: any) => {
    mutationResearch.mutateAsync({
      type: "note",

      page_number: type === "pdf" ? page : epub?.start?.displayed.page,
      reference: selection,
      // edition: edition,
      // publisher: publisher,
      // locale: placeOfPublication,
      settings: {
        ...(settings || {}),
        highlightSerialize: getHighlight()?.replace("$note$", "$highlight$"),
        page: type === "pdf" ? page : epub?.start?.displayed.page,
        redirectUrl: router.asPath + `&page=${page}`,
        fileType: type,
      },
      content_id: params.get("id"),
      content_type: "library",
    });
  };

  const handleSaveHighlight = (selection: string, settings?: any) => {
    mutationResearch.mutateAsync({
      type: "note",
      note: "",
      page_number: type === "pdf" ? page : epub?.start?.displayed.page,
      reference: selection,
      // edition: edition,
      // publisher: publisher,
      // locale: placeOfPublication,
      settings: {
        ...(settings || {}),
        highlightSerialize: getHighlight()?.replace("$note$", "$highlight$"),
        page: type === "pdf" ? page : epub?.start?.displayed.page,
        research_type: "highlight",
        fileType: type,
      },
      content_id: params.get("id"),
      content_type: "library",
    });
  };

  const handleHighlight = () => {
    refRangy?.current?.handleHighlight();
  };

  const getHighlight = () => {
    return refRangy?.current?.getSerialize();
  };

  const [aiLoading, setAiLoading] = React.useState(false);

  // EVENTS

  // useTargetEvent(props?.target?.document, "contextmenu", (e: Event) => {
  //   e.preventDefault();
  //   e.stopPropagation();
  // });

  useTargetEvent(props?.target?.document, "click", (e: any) => {
    e.preventDefault();
    e.stopPropagation();

    // setShowSmartTool(false);

    if (selection?.length > 0) {
      setShowSmartTool(true);
    } else {
      setShowSmartTool(false);
    }
  });

  // useTargetEvent(props?.target?.document, "mousedown", (e: MouseEvent) => {
  //   setShowSmartTool(false);
  // });

  // useTargetEvent(props?.target?.document, "mouseup", (e: MouseEvent) => {
  //   e.preventDefault();
  //   e.stopPropagation();
  // });

  // useTargetEvent(props?.target?.document, "touchstart", (e: any) => {});

  // useTargetEvent(props?.target?.document, "touchend", (e: any) => {
  //   e.preventDefault();
  //   e.stopPropagation();

  //   const selection = getSelectionText(props?.target);
  //   if (selection.replace(/[\r\n]/gm, "")?.length > 0) {
  //     setSelection(selection);
  //     setShowSmartTool(true);
  //   } else {
  //     setShowSmartTool(false);
  //   }
  // });

  const { target } = props;

  // React.useEffect(() => {
  //   if (target && dataNotes?.data?.length > 0 && refRangy.current) {
  //     initHighlight(dataNotes);
  //   }
  // }, [target, dataNotes, refRangy, page]);

  return (
    <>
      {props?.target && (
        <Rangy ref={refRangy} iframe={props?.target?.document} />
      )}

      {showQuote && (
        <CitationAction
          contentSlug={id}
          page={page}
          dataQuery={dataQuery?.data}
          pointer={{ x: 0, y: 0 }}
          close={() => {
            setShowQuote(false);
            setShowSmartTool(false);
          }}
          loading={mutationResearch?.isLoading}
          save={(settings: any) => {
            handleSaveQuote(selection, settings);
          }}
        />
      )}

      {showNote && (
        <Note
          page={page}
          close={() => {
            // props?.removeHighlight();
            setSelection("");
            setShowNote(false);
            setShowSmartTool(false);
          }}
          title={
            dataQuery?.data?.data?.content_items?.find(
              (el: any) => el?.id === id
            )?.name
          }
          loading={mutationResearch?.isLoading}
          save={(note: string, settings: any) => {
            handleSaveNote(selection, note, settings);
          }}
        />
      )}

      <AnimatePresence>
        {showSmartTool && (
          <motion.div
            initial={{ opacity: 0, y: 250, x: "-50%" }}
            animate={{ opacity: 1, y: 0 }}
            exit={{ opacity: 0, y: 250, x: "-50%" }}
            className="transition-opacity duration-300 fixed z-50 bottom-[150px] left-1/2 w-fit"
          >
            {(mutationResearch?.isLoading || aiLoading) && (
              <div
                className={`absolute inset-0 bottom-0 z-50 flex w-full h-full items-center rounded-[5px] justify-center flex-col bg-[#4E4B66]`}
              >
                <div className="h-full flex justify-center items-center space-x-2 text-slate-400">
                  <ImSpinner5 className="text-xl animate-spin text-slate-400" />{" "}
                  <span>уншиж байна...</span>
                </div>
              </div>
            )}
            <RightClickActions
              content={dataQuery?.data}
              close={() => {
                // props?.removeHighlight();
                if (typeof window !== "undefined" && window.getSelection)
                  window.getSelection()?.removeAllRanges();
                setShowSmartTool(false);
                // setSelection("");
              }}
              play={() => {
                setAiText(selection);
              }}
              quote={() => {
                handleHighlight();
                setShowQuote(true);
                setShowSmartTool(false);
              }}
              note={() => {
                handleHighlight();
                setShowNote(true);
                setShowSmartTool(false);
              }}
              highlight={() => {
                setShowSmartTool(false);
                handleHighlight();
                handleSaveHighlight(selection);
              }}
            />
          </motion.div>
        )}
      </AnimatePresence>
    </>
  );
};

export default SmartTools;
