import { useEffect } from "react";

const useTargetEvent = (target: any, eventName: string, callback: any) => {
  useEffect(() => {
    target && (target as any)?.addEventListener(eventName, callback);
    return () => {
      (target as any)?.removeEventListener(eventName, callback);
    };
  }, [target, eventName, callback]);
  return;
};

export default useTargetEvent;
