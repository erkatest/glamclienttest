import { Button } from "@mantine/core";
import * as React from "react";
import { ImSpinner5 } from "react-icons/im";

import { BiBookContent } from "react-icons/bi";
import ReaderContext from "~/context/Reader";
import NodeRSA from "node-rsa";
import { useQuery } from "react-query";
import apiContent from "~/api/content";
import bookDecryption from "~/components/DecryptionBook";
import Users from "~/api/users";
import { useSearchParams } from "next/navigation";
interface IProps {
  events?: any;
  token: any;
  id: any;
}

const DEFAULT_SCALE_VALUE = "0.0";

const PDFJs42 = (props: IProps) => {
  const [hasError, setHasError] = React.useState<any>(false);

  const {
    setEventBus,
    setDomLoaded,
    domLoaded,
    zoom,
    zoomIn,
    zoomOut,
    full,
    setTotalPage,
    setZoom,
    page,
    eventBus,
  } = React.useContext(ReaderContext);
  const loadedRef = React.useRef(false);
  const pdfEventRef = React.useRef<any>();
  const [keys, setKeys] = React.useState({ publicKey: "", privateKey: "" });
  const params = useSearchParams();
  const [id, type, t, mobile] = [
    params.get("id"),
    params.get("type"),
    params.get("t"),
    params.get("mobile"),
  ];

  React.useEffect(() => {
    if (mobile) {
      setZoom("0.6");
    }
  }, [mobile]);

  React.useEffect(() => {
    const key = new NodeRSA({ b: 2048 });
    const publicKey = key.exportKey("public");
    const privateKey = key.exportKey("private");
    setKeys({
      publicKey: publicKey,
      privateKey: privateKey,
    });
  }, []);

  const { data, isLoading } = useQuery(
    ["content-file", keys?.publicKey, loadedRef],
    () =>
      keys?.publicKey &&
      loadedRef.current === false &&
      apiContent.findFile(
        {
          book_id: id,
          book_file_type_id: 1,
          public_key: keys?.publicKey,
        },
        t
      ),
    { enabled: !!keys?.publicKey }
  );

  const [pdfBuffer, setBuffer] = React.useState<any>();

  const pdfDocumentRef = React.useRef<any>();

  React.useEffect(() => {
    if (data && loadedRef.current === false) {
      if (data?.success === false) {
        setHasError(true);
        return;
      }
      const decryptedData: any = bookDecryption(data, keys?.privateKey);
      var binaryString = atob(decryptedData);
      var bytes = new Uint8Array(binaryString.length);
      for (var i = 0; i < binaryString.length; i++) {
        bytes[i] = binaryString.charCodeAt(i);
      }
      setBuffer(bytes.buffer);
      loadedRef.current = true;
    } else {
      console.log(data);
    }
  }, [data, loadedRef]);

  const init = async (pdfBuffer: any) => {
    setDomLoaded(false);

    const pdfjsLib = await import("pdfjs-dist/build/pdf");
    const pdfjsViewer = await import("pdfjs-dist/web/pdf_viewer");

    if (!pdfjsLib.getDocument || !pdfjsViewer.PDFPageView) {
      // eslint-disable-next-line no-alert
      alert("Please build the pdfjs-dist library using\n  `gulp dist-install`");
    } else {
      pdfjsLib.GlobalWorkerOptions.workerSrc =
        window.location.origin + "/pdf.worker.min.js";

      if (!pdfjsLib.getDocument || !pdfjsViewer.PDFViewer) {
        // eslint-disable-next-line no-alert
        // alert("Please build the pdfjs-dist library using\n  `gulp dist-install`");
        setHasError(true);
      }

      // The workerSrc property shall be specified.
      //
      pdfjsLib.GlobalWorkerOptions.workerSrc = "/pdf.worker.min.js";

      // Some PDFs need external cmaps.
      //
      const CMAP_URL = "/cmaps/";
      const CMAP_PACKED = true;

      // To test the AcroForm and/or scripting functionality, try e.g. this file:
      // "../../test/pdfs/160F-2019.pdf"

      if (!pdfBuffer) {
        setHasError(true);
      }

      const ENABLE_XFA = true;

      const loadingTask = pdfjsLib.getDocument({
        cMapUrl: CMAP_URL,
        cMapPacked: CMAP_PACKED,
        enableXfa: ENABLE_XFA,
        data: pdfBuffer,
      });

      const PAGE_TO_VIEW = page;

      const pdfDocument = await loadingTask.promise;
      pdfDocumentRef.current = pdfDocument;
      setTotalPage(pdfDocument.numPages);
      setLoaded(true);
    }
  };

  const [loaded, setLoaded] = React.useState(false);

  const pdfViewerRef = React.useRef<any>();

  const renderPage = async (page: number, zoom: any) => {
    const container: any = document.getElementById("viewerContainer");
    container.innerHTML = "";

    const pdfjsViewer = await import("pdfjs-dist/web/pdf_viewer");
    const pdfDocument = pdfDocumentRef.current;
    // Document loaded, retrieving the page.
    const eventBus = new pdfjsViewer.EventBus();
    const pdfPage = await pdfDocument.getPage(page);
    if (container) {
      // Creating the page view with default parameters.
      const pdfPageView = new pdfjsViewer.PDFPageView({
        container,
        id: page,
        scale: zoom ? zoom : 1.0,
        defaultViewport: pdfPage.getViewport({ scale: zoom ? zoom : 1.0 }),
        eventBus,
      });
      // Associate the actual page with the view, and draw it.
      pdfPageView.setPdfPage(pdfPage);
      pdfPageView.draw();
      pdfViewerRef.current = pdfPageView;
    }
  };

  React.useEffect(() => {
    if (pdfBuffer) {
      init(pdfBuffer);
    }
  }, [pdfBuffer]);

  React.useEffect(() => {
    if (loaded && page && zoom) {
      renderPage(page, zoom);
    }
  }, [loaded, page, zoom]);

  return (
    <>
      {!domLoaded && !hasError && isLoading && (
        <div
          className={`absolute top-0 left-0 bottom-0 right-0 z-[49] flex w-full ${
            full ? "h-[calc(100vh_-_20px)]" : "h-[calc(100vh_-_200px)]"
          } items-center bg-white justify-center flex-col`}
        >
          <div className="h-full flex flex-col justify-center items-center">
            <ImSpinner5 className="text-4xl animate-spin text-slate-400" />
            <p className="text-xl text-slate-400 font-thin mt-4">
              Файл татаж байна. Та түр хүлээнэ үү.
            </p>
          </div>
        </div>
      )}

      {hasError && !domLoaded && !isLoading && (
        <div className="absolute inset-0 bg-[#EFF0F6] w-full h-[calc(100vh_-_170px)] z-50 flex items-center justify-center flex-col">
          <BiBookContent size={48} className="mb-5" />

          <code className="font-mono text-lg relative text-center">
            Уучлаарай тухай бүтээлийг унших боломжгүй байна!
          </code>
        </div>
      )}

      <div className="flex justify-center  relative ">
        <div
          id="viewerContainer"
          className={`absolute w-screen   ${
            full ? "h-[calc(100vh_-_20px)]" : "h-[calc(100vh_-_200px)]"
          }`}
          {...props?.events}
        >
          <div id="viewer" className="pdfViewer"></div>
        </div>
      </div>
    </>
  );
};

export default PDFJs42;
