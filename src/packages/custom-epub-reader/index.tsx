import {
  ActionIcon,
  Box,
  Container,
  Drawer,
  Grid,
  ScrollArea,
  Tooltip,
  rem,
} from "@mantine/core";
import { useDisclosure } from "@mantine/hooks";
import React, { useCallback, useContext, useMemo, useState } from "react";
// import Save from "@/components/Reader/Actions/Save";
// import Note from "@/components/Reader/Actions/Note";
import {
  IconArrowsDiagonal,
  IconArrowsDiagonalMinimize,
} from "@tabler/icons-react";
// import PDF from "../Pdf";

// import PDFJs from "../PDFJs";
import dynamic from "next/dynamic";
import ReaderContext from "~/context/Reader";
import Researcher from "~/components/Reader/Researcher";
import Header from "~/components/Reader/Header";
import Footer from "~/components/Reader/Footer";

const EpubPro = dynamic(() => import("../EpubPro"), { ssr: false });

const CustomEpubReader = (props: {
  id: any;
  type: any;
  token: any;
  mobile: any;
}) => {
  const { page, full, setFull, showSidebar, zoom, setShowSidebar } =
    useContext(ReaderContext);
  const [researcherOpened, { open, close }] = useDisclosure(false);

  return (
    <>
      <Drawer
        opened={showSidebar}
        onClose={() => setShowSidebar(false)}
        zIndex={51}
        withCloseButton={false}
        styles={{
          body: {
            padding: 0,
          },
        }}
        position="right"
      >
        <Researcher type="epub" open={open} close={close} />
      </Drawer>

      {/* <Box
        h={full ? "100vh" : "calc(100vh - 170px)"}
        mt={full ? "0" : "70px"}
        sx={{ overflow: "hidden" }}
      > */}
      {full && (
        <Box
          sx={{
            position: "absolute",
            right: rem(32),
            top: rem(16),
            zIndex: 99,
          }}
        >
          <Tooltip label="Бүтэн дэлгэцээс гарах">
            <ActionIcon onClick={() => setFull(!full)}>
              <IconArrowsDiagonalMinimize />
            </ActionIcon>
          </Tooltip>
        </Box>
      )}
      <div className="flex items-center justify-center">
        {!full && <Header {...props} type="epub" />}

        {/* Reader component */}

        {/* <ScrollArea
          type="auto"
          h={"calc(100vh - 270px)"}
          className="mt-[184px]"
        >
          <Box sx={{ zoom }}> */}
        <div className="mt-[184px] ">
          <EpubPro id={props?.id} token={props?.token} />
        </div>
      </div>
      {/* </ScrollArea> */}

      {!full && <Footer {...props} type="epub" />}
      {/* // </Box> */}
      {/* </Box> */}
    </>
  );
};

export default CustomEpubReader;
