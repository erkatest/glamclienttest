import Footer from "~/components/Reader/Footer";
import Header from "~/components/Reader/Header";
import Researcher from "~/components/Reader/Researcher";
import {
  ActionIcon,
  Box,
  Drawer,
  ScrollArea,
  Tooltip,
  rem,
} from "@mantine/core";
import { useDisclosure } from "@mantine/hooks";
import React, { useContext, useState } from "react";

import { IconArrowsDiagonalMinimize } from "@tabler/icons-react";

import dynamic from "next/dynamic";

import ReaderContext from "~/context/Reader";

const PDF = dynamic(() => import("../Pdf"), { ssr: false });

const CustomReader = (props: any) => {
  const { page, full, setFull, showSidebar, zoom, setShowSidebar } =
    useContext(ReaderContext);
  const [researcherOpened, { open, close }] = useDisclosure(false);
  const [pointer, setPointer] = useState<{ x: number; y: number }>();
  const { id, type, token } = props;

  return (
    <>
      <Drawer
        opened={showSidebar}
        onClose={() => setShowSidebar(false)}
        zIndex={51}
        withCloseButton={false}
        styles={{
          body: {
            padding: 0,
          },
        }}
        position="right"
      >
        <Researcher open={open} close={close} type="pdf" />
      </Drawer>

      <Box
        h={full ? "100vh" : "calc(100vh - 170px)"}
        mt={full ? "0" : "70px"}
        sx={{ overflow: "hidden" }}
        bg={"#fff"}
      >
        {full && (
          <Box
            sx={{
              position: "absolute",
              right: rem(32),
              top: rem(16),
              zIndex: 99,
            }}
          >
            <Tooltip label="Бүтэн дэлгэцээс гарах">
              <ActionIcon onClick={() => setFull(!full)}>
                <IconArrowsDiagonalMinimize />
              </ActionIcon>
            </Tooltip>
          </Box>
        )}
        {!full && <Header {...props} />}

        {/* Reader component */}

        <ScrollArea type="auto" h={full ? "100vh" : "calc(100vh - 170px)"}>
          <Box>
            <PDF
              id={id}
              itemId={""}
              isPreview={""}
              fileName={""}
              contentSlug={""}
              onContextMenu={(pointer) => {
                // setPointer(pointer);
              }}
              page={0}
              token={token}
            />
          </Box>
        </ScrollArea>

        {!full && <Footer {...props} />}
      </Box>
    </>
  );
};

export default CustomReader;
