import { useEffect } from "react";

const useClickOutside = (target: any, cb: any) => {
  useEffect(() => {
    const clickOutside = (e: any) => {
      const withinBoundaries = e.composedPath().includes(target);
      if (!withinBoundaries) {
        cb();
      }
    };
    document.addEventListener("click", clickOutside);

    return () => {};
  }, [cb, target]);
  return;
};

export default useClickOutside;
