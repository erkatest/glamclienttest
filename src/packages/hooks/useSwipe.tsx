import { useEffect } from "react";

const useSwipe = (cb: any, target?: any, getSelection?: any) => {
  useEffect(() => {
    let startX = 0;
    let endX = 0;

    (target || document).addEventListener("mousedown", handleTouchStart);
    (target || document).addEventListener("touchstart", handleTouchStart);

    (target || document).addEventListener("mouseup", handleTouchEnd);
    (target || document).addEventListener("touchend", handleTouchEnd);

    function handleTouchStart(event: any) {
      startX = event.clientX || event.touches[0].clientX;
    }

    function handleTouchEnd(event: any) {
      endX = event.clientX || event.changedTouches[0].clientX;
      detectSwipeDirection();
    }

    function detectSwipeDirection() {
      const deltaX = endX - startX;
      const selection = getSelection();
      if (deltaX > window.innerWidth / 2) {
        if (!selection || selection?.trim() === "") {
          cb("right");
        }
      } else if (deltaX < -(window.innerWidth / 2)) {
        if (!selection || selection?.trim() === "") {
          cb("left");
        }
      } else {
        cb("no");
      }
    }
  }, [target, cb]);
  return;
};

export default useSwipe;
