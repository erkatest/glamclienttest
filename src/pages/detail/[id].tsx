import { GetStaticPaths, GetStaticProps } from "next";
import React from "react";
import apiLibrary from "~/api/library";
import DefaultLayout from "~/components/Layout";
import ItemDetailContainer from "~/containers/ItemDetail";
import { TabContext } from "~/context/tab";

const ItemDetail = ({ content }: any) => {
  const { activeTabProps, tabs, active } = React.useContext(TabContext);

  return (
    <DefaultLayout>
      <ItemDetailContainer content={content} />
    </DefaultLayout>
  );
};

export default ItemDetail;

export const getStaticProps: GetStaticProps = async ({ params }: any) => {
  const { id, type } = params;
  const { data }: any = await apiLibrary.findBookById(id);

  if (!data) {
    return {
      notFound: true,
    };
  }

  const content = data;
  return {
    props: {
      content,
    },
    revalidate: 15,
  };
};
export const getStaticPaths: GetStaticPaths = async () => {
  return {
    paths: [],
    fallback: true,
  };
};
