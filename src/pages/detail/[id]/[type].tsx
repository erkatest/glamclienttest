import React, { useState } from "react";
import dayjs from "dayjs";
import { useRouter } from "next/router";
import { useQuery } from "react-query";
import apiItem from "~/api/item";
import DetailSidebar from "~/components/Detail/DetailSidebar";
import { TabContext } from "~/context/tab";
import BreadCrumb from "~/components/BreadCrumb";
import ImageSwiper from "~/components/Detail/ImageSwiper";
import useMessage from "~/hooks/useMessage";
import DefaultLayout from "~/components/Layout";
import HeritageDetail from "~/components/Heritage/HeritageDetail";
import ArchiveDetail from "~/components/Archive/ArchiveDetail";

import { TransformWrapper, TransformComponent } from "react-zoom-pan-pinch";
import { IconX } from "@tabler/icons-react";
import Controls from "../../../components/Detail/Controls";

import "swiper/css";
import "swiper/css/effect-coverflow";
import "swiper/css/navigation";
import "swiper/css/pagination";
import "swiper/css/scrollbar";

import {
  IoIosArrowDropdownCircle,
  IoIosArrowDropupCircle,
} from "react-icons/io";
import CustomSkeleton from "~/components/Skeletons/CustomSkeleton";
import Link from "next/link";
import apiLibrary from "~/api/library";
import Head from "next/head";

const ItemDetailContainers = () => {
  const f = useMessage();
  const router = useRouter();
  const { pathname } = router;
  const { activeTabProps, tabs, active } = React.useContext(TabContext);
  const [activeImage, setActiveImage] = React.useState<string>("");
  const { heritage_type, id, type } = router.query;

  const { data, isLoading } = useQuery(
    ["item-by-id_items", heritage_type, type, id],
    () =>
      // heritage_type &&
      id &&
      type &&
      ((type === "heritage"
        ? apiItem.heritageFindById(
            id,
            heritage_type === "Хөдлөх өв"
              ? "movable"
              : heritage_type === "Үл хөдлөх өв"
              ? "immovable"
              : heritage_type === "Биет бус өв"
              ? "intangible"
              : heritage_type === "movable"
              ? "movable"
              : heritage_type === "immovable"
              ? "immovable"
              : heritage_type === "intangible"
              ? "intangible"
              : ""
          )
        : type  === "koha" || "artwork" || "archive"
        ? apiItem.findById(id, type)
        : apiLibrary.findBookById(id)) as any),
    {
      refetchOnWindowFocus: false,
      refetchInterval: false,
    }
  );

  const { data: relatedData, isLoading: isLoadingRelated } = useQuery(
    ["related-item", data?.data?._id],
    async () =>
      typeof data?.data?._id === "number"
        ? await apiItem.relatedItems(data?.data?._id)
        : Promise.resolve()
  );

  const [wrap, setWrap] = useState(false);
  const handleWrap = () => {
    setWrap(!wrap);
  };

  const pathsWithDynamicBackground = ["/"];

  const buttonColor = `${
    pathsWithDynamicBackground.includes(pathname)
      ? "bg-default"
      : tabs?.find((tab: any) => tab.type === active)?.backgroundColor
  }`;

  return (
    <DefaultLayout>
      {active == "museum" && data?.data?.heritage_type ? (
        <HeritageDetail
          data={data}
          isLoading={isLoading}
          activeTabProps={activeTabProps}
          handleWrap={handleWrap}
          isLoadingRelated={isLoadingRelated}
          relatedData={relatedData}
          wrap={wrap}
          type={router.query.type}
        />
      ) : active == "archive" && data?.data?.heritage_type == "Баримтат өв" ? (
        <ArchiveDetail
          data={data}
          isLoading={isLoading}
          activeTabProps={activeTabProps}
          handleWrap={handleWrap}
          isLoadingRelated={isLoadingRelated}
          relatedData={relatedData}
          wrap={wrap}
          type={router.query.type}
        />
      ) : (
        <>
          <Head>
            <title>Монгол Улсын Үндэсний Номын Сан</title>
            <meta
              name="description"
              content="Монгол Улсын Үндэсний Номын Сан"
            />
            <meta property="og:title" content={data?.data?.name} />
            <meta property="og:description" content={data?.data?.description} />
            <meta property="og:image" content={data?.data?.mainImg} />
          </Head>
          <div className="mb-[4rem]">
            {/* Image swiper */}

            {/* Artwork name */}
            <div className={`${activeTabProps?.backgroundColor}`}>
              <div className="container py-[24px]">
                {isLoading ? (
                  <>
                    <h1 className="animate-pulse  rounded-md w-2/3 h-[40px] bg-slate-300" />
                    <p className="animate-pulse mt-4 w-[100px] h-[21px] bg-slate-300 rounded-md" />
                  </>
                ) : (
                  <>
                    <h1 className="text-white">{data?.data?.name}</h1>
                    <p className="text-white md:text-[14px]">
                      {data?.data?.itemInf?.artist?.name}
                    </p>
                  </>
                )}
              </div>
            </div>
            {isLoading ? (
              <div className="flex flex-col animate-pulse space-y-4 h-72 bg-slate-300" />
            ) : (
              <>
                {data?.data?.type === "artwork" &&
                data?.data?.images.length >= 2 ? (
                  <ImageSwiper
                    data={data?.data?.images || data?.data?.mainImg || ""}
                    isLoading={isLoading}
                    type={data?.data?.type}
                    pathname={pathname}
                    tabs={tabs}
                    active={active}
                  />
                ) : (
                  <div className="flex items-center justify-center w-full bg-[#F9FBFC]">
                    <img
                      src={data?.data?.mainImg || "/images/logo-blue.png"}
                      alt={`key-${data?.data?.mainImg}`}
                      className="cursor-pointer h-[30rem]"
                      onClick={() => {
                        setActiveImage(
                          data?.data?.mainImg || "/images/logo-blue.png"
                        );
                      }}
                      onError={({ currentTarget }) => {
                        currentTarget.onerror = null;
                        currentTarget.src = "/images/logo-blue.png";
                      }}
                    />
                  </div>
                )}
              </>
            )}

            {activeImage && (
              <div className="w-screen h-screen absolute top-0 bottom-0 right-0 left-0 z-[9999999] backdrop-blur-lg py-[1rem] flex items-center justify-center">
                <TransformWrapper>
                  {({ zoomIn, zoomOut, resetTransform, ...rest }) => (
                    <div className="flex flex-col bg-[#fff] p-[10px] gap-[10px] rounded-md">
                      <TransformComponent>
                        <img
                          id="fullscreen-image"
                          src={activeImage}
                          alt=""
                          className="h-[600px] w-full mx-auto cursor-pointer object-contain rounded-md"
                          onError={({ currentTarget }) => {
                            currentTarget.onerror = null;
                            currentTarget.src = "/images/logo-blue.png";
                          }}
                        />
                      </TransformComponent>
                      <div className="flex justify-between gap-[15px]">
                        <Controls
                          pathname={pathname}
                          tabs={tabs}
                          active={active}
                        />
                        <button
                          className={`${buttonColor} w-[60px] h-[31px] rounded-md`}
                          onClick={() => {
                            setActiveImage("");
                          }}
                        >
                          <IconX
                            className="m-auto"
                            color="#fff"
                            width={24}
                            height={24}
                          />
                        </button>
                      </div>
                    </div>
                  )}
                </TransformWrapper>
              </div>
            )}
            <div className="md:grid md:grid-cols-4 gap-[2rem] container pt-5 min-h-96">
              {/* Main section */}
              <div className="col-span-3 space-y-5">
                {/* Breadcrumb */}
                {data && <BreadCrumb type={router.query.type} data={data} />}
                {/* Tags */}
                {/* {data?.data?.tags?.length !== 0 && (
                  <div className="flex flex-wrap w-full justify-start space-x-3">
                    {data?.data?.tags?.map((item: any, i: number) => {
                      return (
                        <div
                          key={`tags-${i}`}
                          onClick={() => {
                            router.push({
                              pathname: "/search",
                              query: { tag: item?._id },
                            });
                          }}
                          className={`w-max h-max border-[1px] border-c1 rounded-full py-1 px-3 mb-2 space-x-2 cursor-pointer`}
                        >
                          <p className="md:text-[14px]">{item?.name}</p>
                        </div>
                      );
                    })}
                  </div>
                )} */}
                {/* Description */}
                {isLoading ? (
                  <>
                    <div className="animate-pulse py-3 bg-slate-300 rounded-md w-[120px]" />
                    <div className="animate-pulse py-14 bg-slate-300 rounded-md " />
                  </>
                ) : (
                  <>
                    {data?.data.description ? (
                      <div className="space-y-3 min-h-[160px]">
                        <h1>{f({ id: "culture-detail-menu-1" })}</h1>
                        <div
                          className={`${
                            wrap ? "line-clamp-none" : "line-clamp-6"
                          } transition-all ease-in-out duration-150`}
                        >
                          <p className={` text-justify text-[16px] leading-7`}>
                            {data?.data.description}
                          </p>
                        </div>
                        {data?.data?.description?.length > 500 ? (
                          <div className="flex w-full justify-end">
                            <button
                              type="button"
                              onClick={handleWrap}
                              className="flex items-center rounded-[1rem] px-3 py-1 border border-1 border-gray-700 grow-0"
                            >
                              <div className="items-center flex">
                                {wrap ? (
                                  <IoIosArrowDropupCircle
                                    size={18}
                                    className="mr-1"
                                  />
                                ) : (
                                  <IoIosArrowDropdownCircle
                                    size={18}
                                    className="mr-1"
                                  />
                                )}
                              </div>

                              {!wrap ? "Цааш үзэх" : "Хураах"}
                            </button>
                          </div>
                        ) : null}
                      </div>
                    ) : (
                      ""
                    )}
                  </>
                )}
                {/* Additional information */}
                {isLoading ? (
                  <>
                    <div className="animate-pulse py-4 bg-slate-300 rounded-md w-[200px]" />
                    <div className="animate-pulse h-[400px] bg-slate-300 rounded-md " />
                  </>
                ) : (
                  <>
                    {data?.data?.fields.length > 0 ? (
                      <div className="space-y-3">
                        <h1>{f({ id: "additional information" })}</h1>
                        <div
                          className={`overflow-hidden overflow-y-hidden overflow-x-hidden`}
                        >
                          {data?.data?.fields?.map((item: any, i: number) => {
                            return (
                              <div
                                key={`fields-${i}`}
                                className={`w-full h-12 overflow-hidden grid grid-cols-2 ${
                                  i % 2 === 1 ? "bg-white" : "bg-[#F4F7FA]"
                                }`}
                              >
                                <p className="flex pl-2 items-center text-sm text-[#767676]">
                                  {item?.name}:
                                </p>
                                {router.query.type === "artwork" ? (
                                  <p className="flex items-center font-medium text-sm">
                                    {item?.type === "text"
                                      ? item?.value
                                      : item?.type === "date"
                                      ? dayjs(item?.d_value).format(
                                          "YYYY/MM/DD"
                                        )
                                      : item?.type === "number"
                                      ? item?.value
                                      : "-"}
                                  </p>
                                ) : (
                                  <>
                                    {router.query.type === "koha" ? (
                                      <p className="flex items-center font-medium text-sm">
                                        {item?.value ? item?.value : "-"}
                                      </p>
                                    ) : (
                                      <p className="flex items-center font-medium text-sm">
                                        {item?.value ? item?.value : "-"}
                                      </p>
                                    )}
                                  </>
                                )}
                              </div>
                            );
                          })}
                        </div>
                      </div>
                    ) : (
                      ""
                    )}
                  </>
                )}
              </div>

              {/* Sidebar */}
              {isLoading ? (
                <div className="w-[288px] h-[300px] animate-pulse bg-slate-300 rounded-md"></div>
              ) : (
                <div className="md:col-span-1 md:w-full">
                  <DetailSidebar data={data} />
                </div>
              )}
            </div>

            {!data?.data?.type || data?.data?.type !== "artwork" ? (
              <div className="container ">
                {!isLoadingRelated ? (
                  <h1 className="animate-pulse h-[30px] bg-slate-300 rounded-md w-[0px] space-y-8 mt-12 mb-4" />
                ) : (
                  <h1 className="space-y-8 mt-12 mb-4">
                    {f({ id: "similar-contents" })}
                  </h1>
                )}
                {isLoadingRelated ? (
                  <>
                    <div className="grid grid-cols-4 gap-8 h-72">
                      <CustomSkeleton />
                      <CustomSkeleton />
                      <CustomSkeleton />
                      <CustomSkeleton />
                      {/* {type === "featured" ? <></> : <CustomSkeleton />} */}
                    </div>
                  </>
                ) : (
                  <div className="grid grid-cols-4 gap-8">
                    {(relatedData?.data || []).map((item: any, i: number) => {
                      return (
                        <div key={`item-${i}`}>
                          <Link href={`/detail/${item?._id}`}>
                            <img
                              src={item?.mainImg || "/images/empty.svg"}
                              className="w-200 h-72 object-contain"
                              onError={(e: any) => {
                                e.target.src = "/images/empty.svg";
                              }}
                            />
                            <h1 className="text-black font-bold text-sm overflow-hidden mb-2 line-clamp-1">
                              {item?.name}
                            </h1>

                            <p className="font-[500] text-gray text-sm overflow-hidden line-clamp-2 text-gray-500">
                              {item.description}
                            </p>
                          </Link>
                        </div>
                      );
                    })}
                  </div>
                )}
              </div>
            ) : null}
          </div>
        </>
      )}
    </DefaultLayout>
  );
};
export default ItemDetailContainers;
