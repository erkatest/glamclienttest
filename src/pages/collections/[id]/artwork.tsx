import React from "react";
import { useQuery } from "react-query";
import apiCollections from "~/api/collections";
import { TabContext } from "~/context/tab";
import { useRouter } from "next/router";
import Link from "next/link";
import { Divider } from "@mantine/core";

import DefaultLayout from "~/components/Layout";

const Collections = () => {
  const router = useRouter();
  const { activeTabProps } = React.useContext(TabContext);
  const { id } = router.query;

  const { data, isLoading } = useQuery(
    ["collections-detail", id],
    () => id && apiCollections.getByIdCollections(id),
    {
      refetchOnWindowFocus: false,
      refetchInterval: false,
    }
  );

  const reversedData = data?.data?.reverse();

  return (
    <DefaultLayout>
      <div className={`${activeTabProps?.backgroundColor}`}>
        <div className="container py-[24px]">
          {isLoading ? (
            <>
              <h1 className="animate-pulse  rounded-md w-2/3 h-[40px] bg-slate-300" />
              <p className="animate-pulse mt-4 w-[100px] h-[21px] bg-slate-300 rounded-md" />
            </>
          ) : (
            <>
              <h1 className="text-white">
                {data?.data[0]?.collections[0]?.name}
              </h1>
            </>
          )}
        </div>
      </div>
      <div className="container">
        {reversedData?.map((item: any, index: any) => {
            return <div key={`collection-details-${index}`}>
            <div className="grid grid-cols-2 py-[30px] gap-x-[40px]">
              <div className="w-full flex items-center justify-center">
                <div className="w-[500px] h-[300px] overflow-hidden flex items-center justify-center">
                  <img
                    src={item?.mainImg}
                    className="h-full scale-[1.15] hover:scale-[1] transition ease-out duration-500"
                  />
                </div>
              </div>
              <div>
                <div className="font-[700] text-black mb-[10px]">
                  {item?.name}
                </div>
                <div className="line-clamp-6 mb-[10px] text-justify">
                  {item?.description}
                </div>
                <Link
                  href={`/detail/${item._id}/artwork`}
                >
                  <p className="font-[600] text-[16px] text-right">Дэлгэрэнгүй...</p>
                </Link>
              </div>
            </div>
            <Divider my="md" />
          </div>
        })}
      </div>
    </DefaultLayout>
  );
};

export default Collections;
