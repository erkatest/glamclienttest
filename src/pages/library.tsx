import Link from "next/link";
import { useRouter } from "next/router";
import React from "react";
import { FiSearch } from "react-icons/fi";
import { MdArrowForwardIos } from "react-icons/md";
import { useQuery } from "react-query";
import apiLibrary from "~/api/library";
import apiMap from "~/api/map";
import BookSwiper from "~/components/Koha/BookSwiper";
import DefaultLayout from "~/components/Layout";
import LocationSwiper from "~/components/LocationSwiper";
import { TabContext } from "~/context/tab";
import useMessage from "~/hooks/useMessage";

const Koha = () => {
  const f = useMessage();
  const router = useRouter();
  const { tabs, setActive, active } = React.useContext(TabContext);
  const { activeTabProps } = React.useContext(TabContext);

  const [q, setQ] = React.useState("");

  const { data: featuredBooks, isLoading: isFeaturedBooksLoading } = useQuery(
    ["specialBooks"],
    () => apiLibrary.findSpecialBooks(),
    {
      refetchOnWindowFocus: false,
      refetchInterval: false,
    }
  );

  const { data: newBooks, isLoading: isNewBooksLoading } = useQuery(
    ["newBooks"],
    () => apiLibrary.findNewBooks(),
    {
      refetchOnWindowFocus: false,
      refetchInterval: false,
    }
  );

  const { data: books, isLoading: isBooksLoading } = useQuery(
    ["books"],
    () => apiLibrary.findBooks(1, 10, ""),
    {
      refetchOnWindowFocus: false,
      refetchInterval: false,
    }
  );

  const { data: libraryData, isLoading: isLibraryDataLoading } = useQuery(
    ["location-library"],
    () => apiMap.find("library", ""),
    {
      refetchOnWindowFocus: false,
      refetchInterval: false,
    }
  );

  return (
    <DefaultLayout>
      <div className="space-y-10 mb-[3rem]">
        <div
          className={`relative ${activeTabProps?.backgroundColor} dark:text-[#BE8100] z-90 h-[300px] bg-gradient-to-b from-red`}
          style={{
            backgroundImage:
              "linear-gradient(to bottom, rgba(0, 0, 0, 0.3), rgba(0, 0, 0, 0.1))",
          }}
        >
          <div className="container flex items-center justify-center flex-col h-full">
            <div className="uppercase text-[40px] font-semibold text-white mb-5">
              {f({ id: "header-menu-2" })}
            </div>
            <div
              className={
                "bg-white w-full rounded-full relative overflow-hidden"
              }
            >
              <input
                placeholder={f({ id: "koha-search-placeholder" })}
                className="h-[50px] lg:h-[60px] w-full p-5 pl-16 focus:outline-none"
                value={q}
                onChange={(e) => {
                  setQ(e.target.value);
                }}
                onKeyDown={(e) => {
                  if (e.key === "Enter") {
                    router.push({
                      pathname: "/search",
                      search: `?q=${q || ""}`,
                    });
                    setActive("library");
                  }
                }}
              />
              <div className="absolute top-1/2 left-5 transform -translate-y-1/2">
                <FiSearch size={28} />
              </div>
            </div>
          </div>
        </div>

        <div className="container md:space-y-[4rem] space-y-[2rem] md:mb-[4rem] mb-[2rem]">
          <div className="space-y-[1rem] md:space-y-[2rem]">
            <div className="flex items-center gap-[1rem]">
              <h1 className="uppercase font-[600] text-2xl">
                {f({ id: "featured-books" })}
              </h1>
            </div>
            <BookSwiper
              isLoading={isFeaturedBooksLoading}
              data={featuredBooks?.data}
            />
          </div>

          <div className="space-y-[1rem] md:space-y-[2rem]">
            <h1 className="uppercase font-[600] text-2xl">
              {f({ id: "newly-added" })}
            </h1>
            <BookSwiper isLoading={isNewBooksLoading} data={newBooks?.data} />
          </div>

          <div className="space-y-[1rem] md:space-y-[2rem]">
            <h1 className="uppercase font-[600] text-2xl">
              {f({ id: "digital-books" })}
            </h1>
            <BookSwiper isLoading={isBooksLoading} data={books?.data} />
          </div>

          <div className="space-y-[1rem] md:space-y-[2rem]">
            <div className="space-y-[1rem] md:space-y-[2rem]">
              <div className="flex justify-between items-center">
                <h2 className="uppercase">{f({ id: "library-locations" })}</h2>
                <Link href={"/map?type=library"}>
                  <div className="flex items-center gap-[1rem] hover:opacity-80">
                    <p className={`font-[700] text-[16px]`}>
                      {f({ id: "see-all" })}
                    </p>

                    <MdArrowForwardIos />
                  </div>
                </Link>
              </div>
            </div>

            <LocationSwiper locations={libraryData?.data} />
          </div>
        </div>
      </div>
    </DefaultLayout>
  );
};

export default Koha;
