import React, { useState } from "react";
import Arrow from "~/assets/Arrow";
import EditIcon from "~/assets/EditIcon";
import FolderIcon from "~/assets/FolderIcon";
import LogSearchTimeIcon from "~/assets/LogSearchTimeIcon";
import DefaultLayout from "~/components/Layout";
import ProfileLayout from "~/components/Layout/ProfileLayout";
import { IoIosCloseCircleOutline } from "react-icons/io";
const SearchLog = () => {
  return (
    <DefaultLayout>
      <div className="bg-[#112437] py-[24px] mb-[40px]">
        <div className="text-[40px] font-[600] text-white  container mx-auto px-[16px]">
          Хүсэлт илгээх
        </div>
      </div>
      <ProfileLayout>
        <div className="space-y-[36px] px-[64px]"></div>
      </ProfileLayout>
    </DefaultLayout>
  );
};

export default SearchLog;
