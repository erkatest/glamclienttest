import React, { useState } from "react";
import Arrow from "~/assets/Arrow";
import EditIcon from "~/assets/EditIcon";
import FolderIcon from "~/assets/FolderIcon";
import LogSearchTimeIcon from "~/assets/LogSearchTimeIcon";
import DefaultLayout from "~/components/Layout";
import ProfileLayout from "~/components/Layout/ProfileLayout";
import { IoIosCloseCircleOutline } from "react-icons/io";
import { useMutation, useQuery } from "react-query";
import apiItem from "~/api/item";
import { useSearchParams } from "next/navigation";
import dayjs from "dayjs";
import { toast } from "react-toastify";
import useMessage from "~/hooks/useMessage";
import ConfirmModal from "~/components/Modals/ConfirmModal";

import { useRouter } from "next/router";
import Pagination from "~/components/pagination";
import { useUser } from "~/context/auth";
import Link from "next/link";
import store from "~/utils/store";
import { IconTrash } from "@tabler/icons-react";
const SearchLog = () => {
  const router = useRouter();
  const [modal, setModal] = useState(false);
  const f = useMessage();
  const searchParams: any = useSearchParams();
  const currentPage = parseInt(searchParams.get("page") || 1);
  const { user, checkUserMe } = useUser();

  React.useEffect(() => {
    if (store?.get("user") !== null) {
      checkUserMe();
    } else {
      router.push("/");
    }
  }, []);
  const { data, refetch, isLoading } = useQuery(
    ["history-search", currentPage],
    () => apiItem.searchHistory(currentPage, 10),
    {
      refetchOnWindowFocus: false,
      refetchInterval: false,
    }
  );
  const deleteMutation = useMutation(
    (id: string) => apiItem.deleteHistory(id),
    {
      onSuccess() {
        refetch();
      },
    }
  );
  const deleteAllMutation = useMutation(() => apiItem.deleteAllHistory(), {
    onSuccess() {
      refetch();
      setModal(false);
      toast.success(f({ id: "success" }));
    },
    onError(error: any) {
      toast.error(error);
    },
  });

  return (
    <DefaultLayout>
      {modal && (
        <ConfirmModal
          isOpen={modal}
          onClose={() => setModal(false)}
          onChange={deleteAllMutation}
        />
      )}
      <div className="bg-[#112437] py-[24px] mb-[15px] lg:mb-[40px]">
        <div className="text-[21px] font-[600] text-white  container mx-auto px-[16px]">
          Ашиглалтын түүх
        </div>
      </div>
      <ProfileLayout>
        <div className="space-y-[36px]  lg:px-[64px]">
          <div className="space-y-5 lg:space-y-0 lg:flex justify-between items-center">
            <div className="text-[18px] font-[600] text-[#112437] ">
              Хайлтын түүх
            </div>
            <button
              onClick={() => setModal(true)}
              className=" py-[8px] px-[12px] rounded-[1rem] flex  space-x-[4px] items-center text-[14px] -tracking-[0.28px] bg-red-500 text-white"
            >
              <IconTrash size={18} strokeWidth={1.5} />
              <div className="font-lora">Бүгдийг устгах</div>
            </button>
          </div>

          <div className="space-y-[12px]">
            {isLoading ? (
              [0, 1, 2, 3, 4, 5]?.map((index) => (
                <div
                  className="flex space-x-[16px] items-center"
                  key={`views-log-${index}`}
                >
                  <LogSearchTimeIcon />

                  <div className="w-full space-y-[5px]">
                    <div>
                      <div className="flex justify-between w-full items-center">
                        <div className="h-2.5 bg-gray-200 rounded-full dark:bg-gray-700 w-48 mb-4" />

                        <div className="flex space-x-[15px] items-center">
                          <div className="h-2.5 bg-gray-200 rounded-full dark:bg-gray-700 w-24 mb-4" />
                          <IoIosCloseCircleOutline size={24} />
                        </div>
                      </div>
                    </div>
                    <div className="border-t w-full" />
                  </div>
                </div>
              ))
            ) : data?.data?.length > 0 ? (
              data?.data?.map((item: any, index: number) => (
                <div
                  className="flex space-x-[16px] items-center"
                  key={`search-log-${index}`}
                >
                  <LogSearchTimeIcon />
                  <div className="w-full space-y-[5px]">
                    <div className="flex justify-between w-full items-center">
                      <Link href={`/search/?q=${item?.value}`}>
                        <div>{item?.value}</div>
                      </Link>
                      <div className="flex space-x-[15px] items-center">
                        <div className="text-[12px] text-gray-400">
                          {dayjs(item?.createdAt).format("YYYY-MM-DD HH:mm")}
                        </div>
                        <IoIosCloseCircleOutline
                          size={24}
                          onClick={async (e) => {
                            e.preventDefault();
                            e.stopPropagation();
                            await deleteMutation.mutateAsync(item?._id);
                          }}
                          className="cursor-pointer"
                        />
                      </div>
                    </div>
                    <div className="border-t w-full" />
                  </div>
                </div>
              ))
            ) : (
              <div className="flex items-center justify-center py-16 w-full bg-[#F9FBFC] space-y-10">
                <div>Танд одоогоор хайлтын түүх үүсээгүй байна.</div>
              </div>
            )}
          </div>
          {data?.data?.length > 9 && (
            <div className="flex mt-4 justify-center">
              <Pagination
                setPage={(page: any) =>
                  router?.push(`/search-log?page=${page}`)
                }
                limit={data?.pagination?.limit}
                page={currentPage}
                total={data?.pagination?.total}
              />
            </div>
          )}
          {/* <div className="border-t w-full border-[#A6B2C3]" /> */}
        </div>
      </ProfileLayout>
    </DefaultLayout>
  );
};

export default SearchLog;
