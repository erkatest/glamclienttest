import * as React from "react";
import dynamic from "next/dynamic";
import { useSearchParams } from "next/navigation";
import { LoadingOverlay } from "@mantine/core";

import CustomEpubReader from "~/packages/custom-epub-reader";
import { useQuery } from "react-query";
import apiContent from "~/api/content";
import { useRouter } from "next/router";
import { ReaderProvider } from "~/context/Reader";
import { toast } from "react-toastify";
import { useEffect, useState } from "react";
import forge from "node-forge";
import { AES, enc, lib, mode, pad } from "crypto-js";
import NodeRSA from "node-rsa";
import bookDecryption from "~/components/DecryptionBook";

const CustomReader: any = dynamic(() => import(`~/packages/custom-reader`), {
  ssr: false,
});

const ReaderPage = () => {
  const router = useRouter();
  const [pdfURL, setPdfURL] = useState<any>(null);

  const params = useSearchParams();
  const [id, type, t, mobile] = [
    params.get("id"),
    params.get("type"),
    params.get("t"),
    params.get("mobile"),
  ];

  return (
    <ReaderProvider>
      <div>
        {id ? (
          type === "pdf" ? (
            <CustomReader type="pdf" token={t} mobile={mobile} />
          ) : (
            <CustomEpubReader id={id} type={type} token={t} mobile={mobile} />
          )
        ) : (
          <LoadingOverlay visible={true} />
        )}
      </div>
    </ReaderProvider>
  );
};

ReaderPage.getLayout = (page: React.ReactElement) => {
  return page;
};

export default ReaderPage;
