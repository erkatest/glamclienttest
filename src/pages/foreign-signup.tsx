import * as React from "react";
import DefaultLayout from "~/components/Layout";
import ForeignContainer from "~/containers/ForeignContainer";

const SingIn = () => {
  return (
    <DefaultLayout>
      <ForeignContainer />
    </DefaultLayout>
  );
};

export default SingIn;
