import * as React from "react";
import DefaultLayout from "../components/Layout";
import useMessage from "../hooks/useMessage";
import { BsArrowLeft } from "react-icons/bs";
import Link from "next/link";
const NotFoundPage: React.FC = () => {
  const f = useMessage();
  return (
    <DefaultLayout>
      <div className="container relative mx-auto max-w-7xl px-8 h-[80vh] flex items-center justify-center">
        <img src={"/images/bg-404.png"} />
        <div className="absolute top-0 left-0 right-0 bottom-0 flex items-center justify-center space-y-8 flex-col">
          <img src={"/images/book.svg"} className="max-w-[50%]" />
          <h3 className=" font-bold text-4xl lg:text-7xl text-c1">404</h3>
          <p className="text-xl lg:text-3xl font-bold text-[#4d4d4d]">
            {f({ id: "not-found" })}
          </p>
          <Link
            href="/"
            className="w-[400px] flex items-center justify-center space-x-2 height-[60px] text-sm p-4 rounded-[5px] bg-c1 text-white"
          >
            <BsArrowLeft /> <span>{f({ id: "not-found-btn" })}</span>
          </Link>
        </div>
      </div>
    </DefaultLayout>
  );
};

export default NotFoundPage;
