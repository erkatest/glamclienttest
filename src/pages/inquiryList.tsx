import React, { Fragment, useState } from "react";
import { Dialog, Transition } from "@headlessui/react";
import { useForm } from "@mantine/form";
import { Box, TextInput, Select, Card, Group } from "@mantine/core";
import { useQuery, useMutation } from "react-query";
import SelectType from "../components/Heritage/SelectType";
import inquiryItem from "~/api/inquiry";
import DefaultLayout from "~/components/Layout";
import ProfileLayout from "~/components/Layout/ProfileLayout";
import { toast } from "react-toastify";
import useMessage from "~/hooks/useMessage";
import { IconSearch } from "@tabler/icons-react";
import { Grid, Col } from "@mantine/core";
import moment from "moment";
import InquirySkeleton from "~/components/ContentCard/InquirySkeleton";

const InquiryModal = (props: { isOpen: boolean; onClose: any }) => {
  const f = useMessage();
  const [referenceValue, setReferenceValue] = React.useState<Array<any>>([]);

  const form = useForm({
    initialValues: {
      registerNumber: "",
    },
  });

  const referenceListMutation = useMutation((item: any) =>
    inquiryItem.getReferenceList(item)
  );

  const handleSubmit = async (values: any, e: any) => {
    if (values.registerNumber == "") {
      toast.error("Регистрийн дугаараа оруулна уу.");
      return;
    }
    referenceListMutation
      .mutateAsync(values)
      .then((res) => {
        setReferenceValue(res?.data);
        form.reset();
      })
      .catch((e) => {
        console.log("error", e);
      });
  };

  return (
    <DefaultLayout>
      <div className="bg-[#112437] py-[24px] mb-[15px] lg:mb-[40px]">
      <div className="text-[21px] font-[600] text-white  container mx-auto px-[16px]">
          Лавлагааны жагсаалт
        </div>
      </div>
      <ProfileLayout>
        <div className="px-5 lg:px-[64px] font-lora">
          <div className="space-y-[8px] mb-[35px]">
            {/* <div>Регистрийн дугаар:</div> */}
            <form onSubmit={form.onSubmit(handleSubmit)}>
              <Grid>
                <Col span={10}>
                  <TextInput
                    radius="lg"
                    size={"md"}
                    className="font-lora"
                    withAsterisk
                    placeholder="Регистрийн дугаараа оруулна уу"
                    {...form.getInputProps("registerNumber")}
                  />
                </Col>
                <Col span={2}>
                  <button
                    type="submit"
                    className="w-full h-[43px] bg-[#112437] active:bg-c5 text-[16px] 2xl:text-[18px] text-white p-2.5 rounded-[17px] hover:bg-c15 font-sans flex items-center justify-center"
                  >
                    {/* <IconSearch size="1.5rem" stroke={1.5} /> */}
                    <div>Хайх</div>
                  </button>
                </Col>
              </Grid>
            </form>
          </div>

          {referenceListMutation.isLoading ? (
            <>
              {[0, 1, 2, 3]?.map((index) => (
                <InquirySkeleton key={index} />
              ))}
            </>
          ) : (
            <div className="space-y-[28px] font-lora">
              {referenceValue.length >= 1 &&
                referenceValue?.map((item: any, index: any) => {
                  let dateCreatedAt = moment(item?.created_at).format(
                    "YYYY/MM/DD"
                  );
                  return (
                    <Card
                      key={index}
                      bg="#f7f6fa"
                      shadow="sm"
                      padding="lg"
                      radius="md"
                      withBorder
                      mb={10}
                      className="space-y-[8px] font-lora"
                    >
                      <div>№ {item?.request_number}</div>
                      <div className="font-bold">{item?.type_name}</div>
                      <Box>
                        <div className="flex justify-between">
                          <Group>
                            <div className="">{item?.first_name}</div>
                            <div>{item?.last_name}</div>
                          </Group>
                          <div>{item?.register_number}</div>
                        </div>
                        <div className="flex justify-between">
                          <div>{dateCreatedAt}</div>
                          <div>{item?.status_name}</div>
                        </div>
                      </Box>
                    </Card>
                  );
                })}
            </div>
          )}
        </div>
      </ProfileLayout>
    </DefaultLayout>
  );
};

export default InquiryModal;
