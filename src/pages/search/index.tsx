import * as React from "react";
import DefaultLayout from "../../components/Layout";
import { FiSearch } from "react-icons/fi";
import useMessage from "../../hooks/useMessage";
import { useRouter } from "next/router";
import CategoryBundle from "~/components/Category";
import { useMutation, useQuery } from "react-query";
import apiItem from "~/api/item";
import apiArtwork from "~/api/artwork";
import apiHeritage from "~/api/heritage";
import useParams from "~/hooks/useParams";
import { TabContext } from "~/context/tab";
import CustomSelect from "~/components/libs/CustomSelect";
import { IoIosCloseCircleOutline } from "react-icons/io";
import Pagination from "~/components/pagination";
import Spinner from "~/components/libs/Spinner";
import { VscChromeClose } from "react-icons/vsc";
import { useUser } from "~/context/auth";
import SelectDate from "~/components/SelectDate";
import { useEffect, useState } from "react";
import { Input, TextInput, Grid } from "@mantine/core";
import apiLibrary from "~/api/library";
import DigitalLibrary from "../../components/Koha/DigitalLibrary";
import { IconSearch } from "@tabler/icons-react";
import moment from "moment";

import {
  IconArrowsSort,
  IconChevronDown,
  IconChevronUp,
} from "@tabler/icons-react";

const SearchPage: React.FC = () => {
  const { setActive, active, activeTabProps, tabs } =
    React.useContext(TabContext);

  const today = new Date();
  const beforeTenYear = today.getFullYear() - 20;
  today.setFullYear(beforeTenYear);

  const [page, setPage] = React.useState<number>(1);
  const [focus, setFocus] = React.useState(false);
  const [loading, setLoading] = React.useState(false);
  const f = useMessage();
  const router = useRouter();
  const params = useParams();
  const { user } = useUser();
  const [q, setQ] = React.useState("");
  const { token } = useUser();
  const pageNum = Number(page);

  let date_A = moment(today).format("YYYY-MM-DD");
  let date_B = moment(new Date()).format("YYYY-MM-DD");

  const [startDateValue, setStartDateValue] = useState<string | Date>(date_A);
  const [endDateValue, setEndDateValue] = useState<string | Date>(date_B);

  const [artWorkCategoryItemsArr, setArtworkCategoryItemsArr] = React.useState<
    Array<any>
  >([]);
  const [artWorkId, setArtWorkId] = React.useState<string>("");

  const [provinceCategoryItems, setProvinceCategoryItems] = React.useState<
    Array<any>
  >([]);

  const [provinceId, setProvinceId] = React.useState<Array<string>>([]);
  const [idForRegion, setIdForRegion] = React.useState<any>(null);

  const [regionCategoryItems, setRegionCategoryItems] = React.useState<
    Array<any>
  >([]);
  const [regionId, setRegionId] = React.useState<Array<string>>([]);
  const [checkedValues, setValue] = React.useState<Array<string>>([]);
  const [enabled, setEnabled] = React.useState<boolean>(false);

  const [categoryId, setCategoryId] = React.useState<string>("");
  const [categoryItemsArr, setCategoryItemsArr] = React.useState<Array<any>>(
    []
  );
  const [organizationsItemsArr, setOrganizationsItemsArr] = React.useState<
    Array<any>
  >([]);
  const [organizationId, setOrganizationId] = React.useState<string>("");
  const [heritageTypeMain, setHeritageType] = React.useState<Array<any>>([
    {
      label: "Хөдлөх өв",
      value: "Хөдлөх өв",
      type: "heritage",
      selected: false,
      index: 0,
    },
    {
      label: "Биет бус өв",
      value: "Биет бус өв",
      type: "heritage",
      selected: false,
      index: 1,
    },
    {
      label: "Үл хөдлөх өв",
      value: "Үл хөдлөх өв",
      type: "heritage",
      selected: false,
      index: 2,
    },
  ]);

  const [getMainActive, setMainActive] = React.useState("artwork");

  const [immovableSpeciesCategory, setImmovableSpeciesCategory] =
    React.useState<Array<any>>([]);
  const [movableSpeciesCategory, setMovableSpeciesCategory] = React.useState<
    Array<any>
  >([]);
  const [intangibleSpeciesCategory, setIntangibleSpeciesCategory] =
    React.useState<Array<any>>([]);

  const [centuryCategory, setCenturyCategory] = React.useState<Array<any>>([]);

  const [sort, setSort] = React.useState<string>("name");
  const [sortOrder, setSortOrder] = React.useState<string>("asc");

  const [speciesId, setSpeciesId] = React.useState<string>("");
  const [centuryId, setCenturyId] = React.useState<string>("");
  const [keeperName, setKeeperName] = React.useState<string>("");

  const [checkSelected, setCheckSelected] = React.useState<Boolean>(false);

  const { data: commonData, isLoading: commonLoading } = useQuery(
    ["common-search"],
    () => apiItem.commonSearch(8)
  );

  const { data, isLoading } = useQuery(["data", params.q], () =>
    apiItem.find("", 1, 1, params.q || "")
  );

  // React.useEffect(() => {
  //   const selectedArtworks =
  //     artWorkCategoryItemsArr &&
  //     artWorkCategoryItemsArr?.filter((item: any) => item.selected);

  //   const getIds = selectedArtworks
  //     ? selectedArtworks.map((i: any) => {
  //         return i?.value;
  //       })
  //     : [];

  //   setArtWorkId(getIds.length > 0 ? getIds : []);

  //   if (active === "gallery") {
  //     setArtWorkId(getIds.length > 0 ? getIds : [activeArtwork]);
  //   }
  // }, [artWorkCategoryItemsArr]);

  React.useEffect(() => {
    const selected = heritageTypeMain?.filter((item: any) => item.selected);
    const types =
      selected.length > 0
        ? selected.map((i: any) => {
            return i?.label;
          })
        : [];
    if (types?.length === 1) {
      setCheckSelected(true);
    } else {
      setCheckSelected(false);
    }

    setValue(types);
  }, [heritageTypeMain]);

  React.useEffect(() => {
    active === "gallery"
      ? setMainActive("artwork")
      : active === "library"
      ? setMainActive("koha")
      : active === "archive"
      ? setMainActive("archive")
      : active === "museum"
      ? setMainActive("heritage")
      : "";
  }, [active]);

  const { data: activeData, isLoading: activeDataIsLoading } = useQuery(
    [
      "activeData",
      params.q,
      getMainActive,
      page,
      checkedValues,
      artWorkId,
      categoryId,
      provinceId,
      regionId,
      sort,
      sortOrder,
      speciesId,
      startDateValue,
      endDateValue,
      centuryId,
      keeperName,
      organizationId,
    ],
    () =>
      getMainActive &&
      (apiItem.find(getMainActive, page, 12, params.q || q, sort, sortOrder, {
        heritage: active === "museum" && {
          heritage_type:
            active == "museum" && checkedValues.length > 0
              ? checkedValues
              : ["Хөдлөх өв", "Биет бус өв", "Үл хөдлөх өв"],
          province: provinceId.length > 0 ? provinceId : undefined,
          region: regionId.length > 0 ? regionId : undefined,
          species_id: speciesId,
          heritage_century_id: centuryId || "",
          date_range_a: startDateValue,
          date_range_b: endDateValue,
          keeper_name: keeperName,
        },
        artwork: {
          parentCategory: artWorkId ? artWorkId : "",
          category: categoryId ? categoryId : "",
          organization: organizationId ? organizationId : "",
        },
      }) as any)
  );

  const { data: artworkCategory, isLoading: artworkCategoryIsLoading } =
    useQuery(["artworkCategory"], () => apiItem.category(), {
      refetchOnWindowFocus: false,
      refetchInterval: false,
    });

  const { data: category, isLoading: categoryIsLoading } = useQuery(
    ["childcategory", artWorkId],
    () => artWorkId && (apiItem.childCategory(artWorkId) as any),
    {
      refetchOnWindowFocus: false,
      refetchInterval: false,
    }
  );

  const { data: organizations, isLoading: organizationsIsLoading } = useQuery(
    ["organizations"],
    () => apiArtwork.organizations(),
    {
      refetchOnWindowFocus: false,
      refetchInterval: false,
    }
  );

  React.useEffect(() => {
    setOrganizationsItemsArr(
      organizations?.data.length > 0 &&
        organizations?.data?.map((e: any, index: any) => {
          return {
            label: e.name,
            value: e._id,
            selected: false,
            index,
          };
        })
    );
  }, [organizations]);

  React.useEffect(() => {
    setArtworkCategoryItemsArr(
      artworkCategory?.data.length > 0 &&
        artworkCategory?.data?.map((e: any, index: any) => {
          return {
            label: e.name,
            value: e._id,
            type: "artwork",
            selected: false,
            index,
          };
        })
    );
  }, [artworkCategory]);

  React.useEffect(() => {
    setCategoryItemsArr(
      category?.data?.length > 0 &&
        category?.data?.map((e: any, index: any) => {
          return {
            label: e.name,
            value: e._id,
            type: "artwork",
            selected: false,
            index,
          };
        })
    );
  }, [category]);

  const { data: heritageProvinces, isLoading: heritageProvincesIsLoading } =
    useQuery(["provinces"], () => apiItem.heritageProvince());

  const { data: heritageRegions, isLoading: heritageRegionsIsLoading } =
    useQuery(
      ["regions", idForRegion],
      () => idForRegion && apiItem.heritageRegion(idForRegion)
    );

  const { data: immovableSpecies, isLoading: immovableSpeciesLoading } =
    useQuery(["immovableSpecies"], () => apiHeritage.findImmovableSpecies());

  const { data: movableSpecies, isLoading: movableSpeciesLoading } = useQuery(
    ["movableSpecies"],
    () => apiHeritage.findMovableSpecies()
  );

  const { data: intangibleSpecies, isLoading: intangibleSpeciesLoading } =
    useQuery(["intangibleSpecies"], () => apiHeritage.intangibleSpecies());

  const { data: century, isLoading: centuryIsLoading } = useQuery(
    ["century"],
    () => apiHeritage.century()
  );

  const { data: books, isLoading: isBooksLoading } = useQuery(
    ["books", params.q, pageNum],
    () => apiLibrary.findBooks(pageNum || 1, 12, params.q, sort, "desc"),
    {
      refetchOnWindowFocus: false,
      refetchInterval: false,
    }
  );

  const heritageTypeHandler = (index: any) => {
    const updatedHeritageType = heritageTypeMain.map((item, i) => {
      if (index == i) {
        item.selected = !item.selected;
      }
      return item;
    });

    if (index === 4) {
      heritageTypeMain.map((item, i) => {
        setValue([]);
        item.selected = false;
        return item;
      });
      setCheckSelected(false);
    }
    setHeritageType(updatedHeritageType);
  };

  React.useEffect(() => {
    const province = params.province_type && params.province_type?.split(",");
    setProvinceCategoryItems(
      heritageProvinces?.data.length > 0 &&
        heritageProvinces?.data?.map((e: any, index: any) => {
          return {
            label: e.province_name,
            value: e.id,
            type: "heritage",
            selected: province?.includes(e.id),
            index,
          };
        })
    );
  }, [heritageProvinces]);

  // Main province handler

  const provinceHandler = (index: any) => {
    const updatedProvinceCategoryItems = provinceCategoryItems.map(
      (item, i) => {
        if (index == i) {
          item.selected = !item.selected;
        }
        return item;
      }
    );
    setProvinceId(getSelectedProvinceIds(updatedProvinceCategoryItems));

    if (index === 22) {
      return provinceFalseHandler();
    }
  };

  // Get selected ids

  const getSelectedProvinceIds = (provinceItems: any) => {
    const selectedProvinces = provinceItems?.filter(
      (item: any) => item.selected
    );
    if (selectedProvinces.length > 1) {
      setRegionId([]);
    }
    return selectedProvinces?.length > 0
      ? selectedProvinces.map((i: any) => {
          return i?.value;
        })
      : [];
  };

  const updateProvinceCategoryItems = () => {
    const updatedProvinceCategoryItems =
      provinceCategoryItems.length > 0
        ? provinceCategoryItems?.map((item, i: any) => {
            item.selected = provinceId?.includes(item.value);
            return item;
          })
        : [];
    setProvinceCategoryItems(updatedProvinceCategoryItems);
  };

  React.useEffect(() => {
    const region = params.region_type && params.region_type?.split(",");
    setRegionCategoryItems(
      heritageRegions?.data?.length > 0 &&
        heritageRegions?.data?.map((e: any, index: any) => {
          return {
            label: e.region_name,
            value: e.id,
            type: "heritage",
            selected: region?.includes(e.id),
            index,
          };
        })
    );
  }, [heritageRegions]);

  const regionHandler = (index: any) => {
    const updatedRegionCategoryItems = regionCategoryItems.map((item, i) => {
      if (index == i) {
        item.selected = !item.selected;
      }
      return item;
    });

    setRegionId(getSelectedRegionIds(updatedRegionCategoryItems));

    if (index === 100) {
      return regionFalseHandler();
    }
  };

  const getSelectedRegionIds = (region: any) => {
    const selectedRegions = regionCategoryItems?.filter(
      (item: any) => item.selected
    );

    return selectedRegions
      ? selectedRegions.map((i: any) => {
          return i?.value;
        })
      : [];
  };

  const updatedRegionCategoryItems = () => {
    const updatedRegionCategoryItems =
      regionCategoryItems.length > 0
        ? regionCategoryItems?.map((item, i: any) => {
            item.selected = regionId?.includes(item.value);
            return item;
          })
        : [];

    setRegionCategoryItems(updatedRegionCategoryItems);
  };

  const movableSpeciesHandler = (item: any) => {
    if (item?.index === 999) {
      setSpeciesId("");
    }
  };

  React.useEffect(() => {
    if (params.q) {
      setQ(params.q);
    }
  }, [params.q]);

  React.useEffect(() => {
    if (params.type) {
      setActive(params.type);
    }
  }, [params.type]);

  React.useEffect(() => {
    if (params.page) {
      const num = Number(params.page);
      setPage(num);
    }
  }, [params.page]);

  React.useEffect(() => {
    if (params.sort) {
      setSort(params.sort);
    }
  }, [params.sort]);

  React.useEffect(() => {
    if (params.category) {
      setArtWorkId(params.category);
    }
  }, [params.category]);

  React.useEffect(() => {
    if (params.sub_category) {
      setCategoryId(params.sub_category || "");
    }
  }, [params.sub_category]);

  React.useEffect(() => {
    if (params.museum_century) {
      setCenturyId(params.museum_century);
    }
  }, [params.museum_century]);

  React.useEffect(() => {
    if (params.sort_order) {
      setSortOrder(params.sort_order);
    }
  }, [params.sort_order]);

  React.useEffect(() => {
    if (params.digital_library === "true") {
      setEnabled(params.digital_library);
    } else if (params.digital_library === "false") {
      setEnabled(params.digital_library);
    }
  }, [params.digital_library]);

  React.useEffect(() => {
    if (params.keeper_name) {
      setKeeperName(params.keeper_name || "");
    }
  }, [params.keeper_name]);

  React.useEffect(() => {
    if (params.start_date !== "undefined") {
      setStartDateValue(params.start_date || today);
    }
  }, [params.start_date]);

  React.useEffect(() => {
    if (params.end_date !== "undefined") {
      setEndDateValue(params.end_date || new Date());
    }
  }, [params.end_date]);

  const buttonArray = [
    {
      type: "gallery",
      title: "Урын сан",
      backgroundColor: "bg-[#BE3455]",
      pagination: {
        total:
          activeData?.pagination?.artwork >= 0
            ? activeData?.pagination?.artwork
            : data?.pagination?.artwork,
      },
    },
    {
      type: "library",
      title: "Номын сан",
      backgroundColor: "bg-[#01417A]",
      pagination: {
        total: enabled
          ? books?.pagination?.total
          : activeData?.pagination?.koha >= 0
          ? activeData?.pagination?.koha
          : data?.pagination?.koha,
      },
    },
    {
      type: "archive",
      title: "Сан хөмрөг",
      backgroundColor: "bg-[#2E294E]",
      pagination: {
        total:
          activeData?.pagination?.archive >= 0
            ? activeData?.pagination?.archive
            : data?.pagination?.archive,
      },
    },
    {
      type: "museum",
      title: "Соёлын өв",
      backgroundColor: "bg-[#13452F]",
      pagination: {
        total:
          activeData?.pagination?.heritage >= 0
            ? activeData?.pagination?.heritage
            : data?.pagination?.heritage,
      },
    },
  ];

  const {
    data: searchHistory,
    isLoading: LoadingHistory,
    refetch: refetchHistory,
  } = useQuery(
    ["history-search", token],
    () => token && apiItem.searchHistory(0, 8)
  );

  const deleteMutation = useMutation(
    (id: string) => apiItem.deleteHistory(id),
    {
      onSuccess() {
        refetchHistory();
      },
    }
  );

  React.useEffect(() => {
    setImmovableSpeciesCategory(
      immovableSpecies?.data?.length > 0 &&
        immovableSpecies?.data?.map((e: any, index: any) => {
          return {
            label: e.name,
            value: e.id,
            type: "name",
            selected: false,
            index,
          };
        })
    );
  }, [immovableSpecies]);

  React.useEffect(() => {
    setMovableSpeciesCategory(
      movableSpecies?.data?.length > 0 &&
        movableSpecies?.data?.map((e: any, index: any) => {
          return {
            label: e.name,
            value: e.id,
            type: "name",
            selected: false,
            index,
          };
        })
    );
  }, [movableSpecies]);

  React.useEffect(() => {
    setIntangibleSpeciesCategory(
      intangibleSpecies?.data?.data?.map((e: any, index: any) => {
        return {
          label: e.name,
          value: e.id,
          type: "name",
          selected: false,
          index,
        };
      })
    );
  }, [immovableSpecies]);

  React.useEffect(() => {
    setCenturyCategory(
      century?.data?.map((e: any, index: any) => {
        return {
          label: e.name,
          value: e.id,
          selected: false,
          index,
          type: "heritage",
        };
      })
    );
  }, [century]);

  const startFunc = (a: object, b: object) => {
    let date_A = moment(a).format("YYYY-MM-DD");
    setStartDateValue(date_A);
  };

  const endFunc = (b: object) => {
    let date_B = moment(b).format("YYYY-MM-DD");
    setEndDateValue(date_B);
  };

  // Parameter

  React.useEffect(() => {
    if (enabled && active == "library") {
      routerPush_3({
        active: active,
        page: page,
        digital_library: enabled,
      });
    } else if (!enabled && active == "library") {
      routerPush_3({
        active: active,
        page: params.page,
      });
    }
  }, [enabled]);

  React.useEffect(() => {
    if (active === "gallery") {
      routerPush_2({
        active: active,
        page: page,
        artWorkId: artWorkId.toString(),
        sort: sort,
        sortOrder: sortOrder,
        categoryId: categoryId,
      });
    }
  }, [artWorkId, categoryId, sort, sortOrder]);

  React.useEffect(() => {
    setCenturyId("");
    heritageTypeFalseHandler();
    provinceFalseHandler();

    if (active === "gallery") {
      setSort("name");
      setArtWorkId("");
      setCategoryId("");
      routerPush_2({
        active: active,
        page: page,
        artWorkId: params.category,
        sort: params.sort,
        sortOrder: params.sort_order,
        categoryId: params.sub_category || "",
      });
    }

    if (active === "library") {
      routerPush_3({
        active: active,
        page: page,
      });
    }
    if (active === "archive") {
      routerPush_4({
        active: active,
        page: params.page,
      });
    }
    if (active === "museum") {
      setKeeperName("");
      setStartDateValue(date_A);
      setEndDateValue(date_B);
      setSort("heritage_name");
    }
    setPage(1);
    setEnabled(false);
  }, [active]);

  React.useEffect(() => {
    if (active === "gallery") {
      routerPush_2({
        active: active,
        page: page,
        artWorkId: params.category,
        sort: params.sort,
        sortOrder: params.sort_order,
        categoryId: params.sub_category || "",
      });
    }
    if (!enabled && active == "library") {
      routerPush_3({
        active: active,
        page: page,
      });
    } else if (enabled && active == "library") {
      routerPush_3({
        active: active,
        page: page,
        digital_library: enabled,
      });
    }
    if (active == "archive") {
      routerPush_4({
        active: active,
        page: page,
      });
    }
    if (active === "museum") {
      routerPush({
        q: q,
        keeper_name: params.keeper_name || "",
        startDateValue: params.start_date,
        endDateValue: params.end_date,
        museum_century: params.museum_century,
        museum_type: params.museum_type || "",
        province_type: params.province_type,
        region_type: params.region_type || "",
        century_id: params.century,
        page: page,
        sort: params.sort,
        sortOrder: params.sort_order,
      });
    }
  }, [page, q]);

  const routerPush_4 = (p: any) => {
    active == "archive" &&
      router.push({
        search: `?q=${q || ""}&type=${p.active || ""}&page=${p.page || ""}`,
      });
  };

  const routerPush_3 = (p: any) => {
    if (!enabled && active == "library") {
      router.push({
        search: `?q=${q || ""}&type=${p.active || ""}&page=${p.page || ""}`,
      });
    } else if (enabled && active == "library") {
      router.push({
        search: `?q=${q || ""}&type=${p.active || ""}&digital_library=${
          p.digital_library || ""
        }&page=${p.page || ""}`,
      });
    }
  };

  const routerPush_2 = (p: any) => {
    active == "gallery" &&
      router.push({
        search: `?q=${q || ""}&type=${p.active || ""}&category=${
          p.artWorkId || ""
        }&sub_category=${p.categoryId || ""}&page=${p.page || ""}&sort=${
          sort || p.sort || ""
        }&sort_order=${sortOrder || p.sortOrder || ""}`,
      });
  };

  React.useEffect(() => {
    updatedRegionCategoryItems();
    if (active === "museum") {
      routerPush({
        q: q,
        keeper_name: params.keeper_name || "",
        startDateValue: params.start_date,
        endDateValue: params.end_date,
        museum_century: centuryId,
        museum_type: params.museum_type || "",
        province_type: provinceId?.toString(),
        region_type: regionId?.toString() || "",
        century_id: params.century,
        page: page,
        sort: sort,
        sortOrder: params.sort_order,
      });
    }
  }, [regionId]);

  React.useEffect(() => {
    updateProvinceCategoryItems();
    if (active === "museum") {
      routerPush({
        q: q,
        keeper_name: params.keeper_name || "",
        startDateValue: params.start_date,
        endDateValue: params.end_date,
        museum_century: params.museum_century,
        museum_type: params.museum_type || "",
        province_type: provinceId?.toString(),
        region_type: regionId?.toString() || "",
        century_id: params.century,
        page: page,
        sort: params.sort,
      sortOrder: params.sort_order,
      });
    }

    if (active === "museum" && provinceId.length === 0) {
      setRegionId([]);
    }
  }, [provinceId]);

  React.useEffect(() => {
    if (active === "museum") {
      routerPush({
        q: q,
        keeper_name: keeperName || "",
        startDateValue: startDateValue,
        endDateValue: endDateValue,
        museum_century: centuryId,
        museum_type: checkedValues.toString() || "",
        province_type: params.province_type,
        region_type: params.region_type || "",
        century_id: params.century,
        page: page,
        sort: sort,
        sortOrder: sortOrder,
      });
    }
  }, [
    keeperName,
    startDateValue,
    endDateValue,
    centuryId,
    checkedValues,
    sortOrder,
    sort,
  ]);

  const routerPush = (p: any) => {
    active === "museum" &&
      router.push({
        search: `?q=${p.q || ""}&type=${active}&page=${
          p.page || ""
        }&keeper_name=${p.keeper_name}&start_date=${
          p.startDateValue
        }&end_date=${p.endDateValue}&museum_century=${
          p.museum_century || ""
        }&museum_type=${p.museum_type}&province_type=${
          p.province_type
        }&region_type=${p.region_type}&sort=${
          sort || p.sort || ""
        }&sort_order=${sortOrder || p.sortOrder || ""}`,
      });
  };

  React.useEffect(() => {
    const array = params.museum_type ? params.museum_type.split(",") : [];

    if (array !== "undefined") {
      if (array) {
        setValue(array);
      }
      heritageTypeMain.map((item, i) => {
        array &&
          array?.map((i: any) => {
            if (item.label == i) {
              item.selected = true;
            }
            return item;
          });
      });
    } else {
      setValue([]);
    }
  }, [params.museum_type]);

  React.useEffect(() => {
    setIdForRegion(params.province_type);
    if (params.province_type !== "undefined") {
      const province = params.province_type && params.province_type?.split(",");

      setProvinceId(province ? province : []);
    }
  }, [params.province_type]);

  React.useEffect(() => {
    if (params.region_type !== "") {
      const region = params.region_type && params.region_type?.split(",");
      setRegionId(region ? region : []);
    }
  }, [params.region_type]);

  const heritageTypeFalseHandler = () => {
    heritageTypeMain?.map((item, i) => {
      item.selected = false;
      return item;
    });
  };

  const provinceFalseHandler = () => {
    setProvinceId([]);
    provinceCategoryItems.map((item, i) => {
      item.selected = false;
      return item;
    });
  };

  const regionFalseHandler = () => {
    setRegionId([]);
    regionCategoryItems?.map((item, i) => {
      item.selected = false;
      return item;
    });
  };

  const sortArray = [
    {
      label: "Нэр",
      value: "name",
      type: "artwork",
      selected: false,
    },
    {
      label: "Хугацаа",
      value: "createdAt",
      type: "artwork",
      selected: false,
    },
  ];

  const firstValue = checkedValues[checkedValues.length - 1];
  const provinceValue = provinceId[provinceId.length - 1];
  const regionValue = regionId[regionId.length - 1];

  return (
    <DefaultLayout>
      <div className="flex justify-center flex-col relative pb-[100px]">
        <div className="">
          <div
            style={{
              background: `linear-gradient(180deg, rgba(0, 0, 0, 0.20) 0%, rgba(0, 0, 0, 0.04) 100%), var(--Magenta, ${activeTabProps?.color})`,
            }}
            className={`h-[300px] relative mb-[2rem] transition-all duration-300 ease-in-out`}
          >
            <div
              style={{ zIndex: 1 }}
              className="absolute top-[40%] w-full  -translate-y-[50%] right-[50%] translate-x-[50%] space-y-4"
            >
              <h1 className="uppercase text-white text-center">
                {f({ id: "search" })}
              </h1>
              <div
                className={`relative container mx-auto bg-white rounded-full overflow-hidden`}
              >
                <input
                  placeholder={f({ id: "search-placeholder" })}
                  className="lg:h-[60px] w-full p-5 pl-16 focus:outline-none text-[12px] sm:text-[14px] md:text-[16px]"
                  value={q}
                  onFocus={() => setFocus(true)}
                  onBlur={() => setFocus(false)}
                  onChange={(e) => {
                    setQ(e.target.value);
                  }}
                  onKeyDown={(e) => {
                    if (e.key === "Enter") {
                      router.push({
                        search: `?q=${q || ""}&type=${active}`,
                        // search:
                        //   active === "museum"
                        //     ? `?q=${
                        //         q || ""
                        //       }&museum_type=${checkedValues.toString()}`
                        //     : `?q=${q || ""}`,
                      });
                      setFocus(false);
                      setPage(1);
                    }
                  }}
                />
                <div className="absolute top-1/2 left-5 transform -translate-y-1/2">
                  <FiSearch size={28} />
                </div>
              </div>
              <div
                className={`absolute container mx-auto top-[100%] right-0 left-0 bg-[#F9F9F9] transition-all border-t border-[#E6EAF4] rounded-[1.25rem] w-full min-h-[72px] py-4 flex-wrap items-center px-4 ${
                  focus
                    ? "visible opacity-100 mt-[1rem]"
                    : "invisible opacity-0"
                }`}
              >
                {loading || commonLoading || LoadingHistory ? (
                  <div className="flex justify-center">
                    <Spinner />
                  </div>
                ) : (
                  <>
                    {user && (
                      <div className="space-y-3 mb-6">
                        <h3 className="text-[14px] mb-1 text-[#767676]">
                          {f({ id: "search-history-title" })}
                        </h3>
                        <div className="flex flex-wrap gap-[1rem]">
                          {searchHistory?.data.map((txt: any, i: number) => (
                            <div
                              key={`text-${i}`}
                              className="flex cursor-pointers whitespace-nowrap items-center space-x-1 bg-[#EEEEEE] text-[#55606B] hover:bg-gray-400 hover:text-white  rounded-[5px]"
                            >
                              <button
                                onClick={() => {
                                  setLoading(true);
                                  router.push({
                                    pathname: "/search",
                                    search: `?q=${txt.value || ""}`,
                                  });
                                  setActive("artwork");
                                }}
                                className="flex items-center"
                              >
                                <div className="flex p-2 py-1 items-center text-sm">
                                  <span className="mx-2 font-lora">
                                    {txt?.value}
                                  </span>
                                </div>
                              </button>

                              <button
                                onClick={async (e) => {
                                  e.preventDefault();
                                  e.stopPropagation();
                                  await deleteMutation.mutateAsync(txt?._id);
                                }}
                              >
                                <VscChromeClose
                                  size={16}
                                  className="mr-2 hover:text-black"
                                />
                              </button>
                            </div>
                          ))}
                        </div>
                      </div>
                    )}

                    <div className="space-y-3">
                      <h3 className="text-[14px] text-[#767676]">
                        {f({ id: "common-search" })}
                      </h3>
                      <div className="flex flex-wrap gap-[1rem]">
                        {commonData?.data.map((txt: any, i: number) => (
                          <div
                            key={`text-${i}`}
                            className="flex cursor-pointer whitespace-nowrap text-[14px] items-center space-x-1 p-2 py-1 bg-[#EEEEEE] text-[#55606B] hover:bg-gray-400 hover:text-white rounded-[5px]"
                            onClick={() => {
                              setLoading(true);
                              setQ(txt?.value);
                              router.push({
                                pathname: "/search",
                                search: `?q=${txt?.value}&type=${active}`,
                              });
                            }}
                          >
                            <span className="mx-4 ">{txt?.value}</span>
                          </div>
                        ))}
                      </div>
                    </div>
                  </>
                )}
              </div>
            </div>

            <div className="absolute bottom-0 md:left-[50%] md:-translate-x-[50%]">
              <div className="md:flex grid grid-cols-2">
                {buttonArray.map((e: any) => {
                  return (
                    <button
                      key={e.type}
                      className={`${
                        active === e.type ? "bg-white" : ""
                      } py-[0.5rem] md:py-[1rem] px-[1rem] md:px-[2rem] md:rounded-tl-3xl md:rounded-tr-3xl font-lora hover:${
                        active !== e.type
                          ? e?.backgroundColor + " text-white"
                          : "bg-white"
                      }`}
                      onClick={() => {
                        setActive(e.type);
                        setPage(1);
                        e.type === "museum"
                          ? router.push({
                              pathname: "/search",
                              search: `?q=${q || ""}&type=${
                                e.type || ""
                              }&museum_type=${
                                checkedValues.toString() || ""
                              }&province_type=${
                                provinceId?.toString() || ""
                              }&page=${page || ""}`,
                            })
                          : router.push({
                              pathname: "/search",
                              search: `?q=${q || ""}&type=${
                                e.type || ""
                              }&page=${page || ""}`,
                            });
                      }}
                    >
                      <div className="flex w-full md:w-max items-center gap-3">
                        <p>{e.title}</p>
                        {!isLoading ? (
                          <p>
                            (
                            {e.pagination.total > 0
                              ? e.pagination.total
                              : e.pagination.total == undefined
                              ? "0"
                              : "0"}
                            )
                          </p>
                        ) : (
                          <span className="relative flex h-3 w-3">
                            <span
                              className={`animate-ping absolute inline-flex h-full w-full rounded-full opacity-75 ${
                                active !== e.type ? "bg-white" : "bg-gray-700"
                              } hover:${
                                active === e.type
                                  ? e?.backgroundColor + " text-white"
                                  : "bg-white"
                              }`}
                            ></span>
                          </span>
                        )}
                      </div>
                    </button>
                  );
                })}
              </div>
            </div>
          </div>

          <div className="container mx-auto">
            {active == "museum" && (
              <div className="flex flex-col lg:flex-row gap-y-[20px] mb-[20px] gap-x-[20px]">
                <div className="flex-1">
                  <TextInput
                    radius="lg"
                    size="md"
                    placeholder="Байршлаар хайх"
                    value={keeperName}
                    className="w-full"
                    onChange={(e) => {
                      // setSpeciesId("");
                      // e.target.value.length > 0 && setCenturyId("");
                      setKeeperName(e.target.value);
                    }}
                    onKeyDown={(e) => {
                      if (e.key === "Enter") {
                        router.push({
                          search: `?q=${
                            q || ""
                          }&keeper_name=${keeperName}&type=${active}`,
                        });
                      }
                    }}
                    icon={<IconSearch size="1.2rem" stroke={1.5} />}
                  />
                </div>
                <div className="flex-1">
                  <div className="">
                    <SelectDate
                      startDateDefaultValue={startDateValue}
                      endDateDefaultValue={endDateValue}
                      startFunc={startFunc}
                      endFunc={endFunc}
                    />
                  </div>
                </div>
              </div>
            )}

            <div className="grid grid-cols-1 md:grid-cols-2 lg:grid-cols-3 gap-x-[40px] gap-y-[20px] w-full">
              {active == "museum" && (
                <div className="">
                  {centuryIsLoading ? (
                    <Spinner />
                  ) : (
                    centuryCategory && (
                      <div className="flex items-center md:justify-between gap-[20px]">
                        <div className="">Холбогдох он: </div>
                        <div className="w-[260px]">
                          <CustomSelect
                            defaultValue={centuryId}
                            items={[
                              { label: "Бүгд", value: "" },
                              ...centuryCategory,
                            ]}
                            onChange={(item) => {
                              setCenturyId(item.value);
                            }}
                          />
                        </div>
                      </div>
                    )
                  )}
                </div>
              )}
              {active == "museum" && (
                <div className="">
                  {heritageTypeMain && (
                    <div className="flex items-center md:justify-between gap-[20px]">
                      <div className="">Төрөл: </div>
                      <div className="w-[260px]">
                        <CustomSelect
                          defaultValue={firstValue}
                          items={[
                            { label: "Бүгд", value: "", index: 4 },
                            ...heritageTypeMain.filter(
                              (item) => !item.selected
                            ),
                          ]}
                          items_all={[
                            { label: "Бүгд", value: "", index: 4 },
                            ...heritageTypeMain,
                          ]}
                          onChange={(item) => {
                            return heritageTypeHandler(item.index);
                          }}
                        />
                      </div>
                    </div>
                  )}
                </div>
              )}
              {active == "gallery" && (
                <div className="">
                  {artworkCategoryIsLoading ? (
                    <Spinner />
                  ) : (
                    artWorkCategoryItemsArr && (
                      <div className="flex items-center md:justify-between gap-[20px]">
                        <div className="">Ангилал: </div>
                        <div className="w-[250px]">
                          <CustomSelect
                            defaultValue={artWorkId}
                            items={[
                              { label: "Бүгд", value: "", index: 12 },
                              ...artWorkCategoryItemsArr.filter(
                                (item) => !item.selected
                              ),
                            ]}
                            items_all={[
                              { label: "Бүгд", value: "", index: 12 },
                              ...artWorkCategoryItemsArr.filter(
                                (item) => !item.selected
                              ),
                            ]}
                            onChange={(item) => {
                              setCategoryId("");
                              setArtWorkId(item.value);
                            }}
                          />
                        </div>
                      </div>
                    )
                  )}
                </div>
              )}

              {artWorkId && categoryItemsArr && active == "gallery" && (
                <div className="">
                  {categoryIsLoading ? (
                    <Spinner />
                  ) : (
                    <div className="flex items-center md:justify-between gap-[20px]">
                      <div className="">Дэд ангилал: </div>
                      <div className="w-[250px]">
                        <CustomSelect
                          defaultValue={categoryId}
                          items={[
                            { label: "Бүгд", value: "", index: 12 },
                            ...categoryItemsArr.filter(
                              (item) => !item.selected
                            ),
                          ]}
                          onChange={(item) => {
                            setCategoryId(item.value);
                          }}
                        />
                      </div>
                    </div>
                  )}
                </div>
              )}

              {active == "gallery" && (
                <div className="">
                  {organizationsIsLoading ? (
                    <Spinner />
                  ) : (
                    organizationsItemsArr && (
                      <div className="flex items-center md:justify-between gap-[20px]">
                        <div className="">Байгууллага: </div>
                        <div className="w-[250px]">
                          <CustomSelect
                            defaultValue={organizationId}
                            items={[
                              { label: "Бүгд", value: "", index: 12 },
                              ...organizationsItemsArr.filter(
                                (item) => !item.selected
                              ),
                            ]}
                            onChange={(item) => {
                              setOrganizationId(item.value);
                            }}
                          />
                        </div>
                      </div>
                    )
                  )}
                </div>
              )}

              {active == "museum" && (
                <div className="">
                  {heritageProvincesIsLoading ? (
                    <Spinner />
                  ) : (
                    provinceCategoryItems.length > 1 && (
                      <div className="flex items-center md:justify-between gap-[20px]">
                        <div className="">Аймаг: </div>
                        <div className="w-[260px]">
                          <CustomSelect
                            defaultValue={provinceValue}
                            items={[
                              { label: "Бүгд", value: "", index: 22 },
                              ...provinceCategoryItems.filter(
                                (item) => !item.selected
                              ),
                            ]}
                            items_all={[
                              { label: "Бүгд", value: "" },
                              ...provinceCategoryItems,
                            ]}
                            onChange={(item) => {
                              setIdForRegion(item.value);
                              setKeeperName("");
                              return provinceHandler(item.index);
                            }}
                          />
                        </div>
                      </div>
                    )
                  )}
                </div>
              )}

              {heritageRegionsIsLoading ? (
                <Spinner />
              ) : (
                active == "museum" &&
                regionCategoryItems.length > 1 && (
                  <div className="">
                    <div className="flex items-center md:justify-between gap-[20px]">
                      <div className="">Сум: </div>
                      <div className="w-[260px]">
                        <CustomSelect
                          defaultValue={regionValue}
                          items={[
                            { label: "Бүгд", value: "", index: 100 },
                            ...regionCategoryItems.filter(
                              (item) => !item.selected
                            ),
                          ]}
                          items_all={[
                            { label: "Бүгд", value: "", index: 100 },
                            ...regionCategoryItems,
                          ]}
                          onChange={(item) => regionHandler(item.index)}
                        />
                      </div>
                    </div>
                  </div>
                )
              )}
              {active === "gallery" && (
                <div className="">
                  {sortArray && (
                    <div className="flex items-center md:justify-between gap-[20px]">
                      <div className="">Эрэмбэлэх: </div>
                      <div className="flex w-[250px] gap-[10px]">
                        <CustomSelect
                          defaultValue={sort}
                          items={sortArray}
                          onChange={(item: any) => {
                            setSort(item?.value);
                          }}
                        />
                        <div
                          className={`flex flex-col items-center justify-center rounded-[1rem] bg-[white] border-2 border cursor-pointer h-[40px] w-[45px] ${
                            tabs.find((tab: any) => tab.type === active)
                              ?.borderColor
                          } ${
                            tabs.find((tab: any) => tab.type === active)
                              ?.textColor
                          }`}
                          onClick={() => {
                            sortOrder === "asc"
                              ? setSortOrder("desc")
                              : setSortOrder("asc");
                          }}
                        >
                          <IconChevronUp
                            size={"1rem"}
                            color={
                              sortOrder === "desc"
                                ? "#0000005a"
                                : `${
                                    tabs.find((tab: any) => tab.type === active)
                                      ?.color
                                  }`
                            }
                          />
                          <IconChevronDown
                            size={"1rem"}
                            color={
                              sortOrder === "desc"
                                ? `${
                                    tabs.find((tab: any) => tab.type === active)
                                      ?.color
                                  }`
                                : "#0000005a"
                            }
                          />
                        </div>
                      </div>
                    </div>
                  )}
                </div>
              )}

              {active === "museum" && (
                <div className="flex items-center md:justify-between gap-[20px]">
                  <div className="">Эрэмбэлэх: </div>
                  <div className="flex w-[260px] gap-[10px]">
                    <CustomSelect
                      defaultValue={sort}
                      items={[
                        {
                          label: "Нэр",
                          value: "heritage_name",
                        },
                        {
                          label: "Хугацаа",
                          value: "created_at",
                        },
                      ]}
                      onChange={(item: any) => {
                        setSort(item?.value);
                      }}
                    />
                    <div
                      className={`flex flex-col items-center justify-center rounded-[1rem] bg-[white] border-2 border cursor-pointer h-[40px] w-[45px] ${
                        tabs.find((tab: any) => tab.type === active)
                          ?.borderColor
                      } ${
                        tabs.find((tab: any) => tab.type === active)?.textColor
                      }`}
                      onClick={() => {
                        sortOrder === "asc"
                          ? setSortOrder("desc")
                          : setSortOrder("asc");
                      }}
                    >
                      <IconChevronUp
                        size={"1rem"}
                        color={
                          sortOrder === "desc"
                            ? "#0000005a"
                            : `${
                                tabs.find((tab: any) => tab.type === active)
                                  ?.color
                              }`
                        }
                      />
                      <IconChevronDown
                        size={"1rem"}
                        color={
                          sortOrder === "desc"
                            ? `${
                                tabs.find((tab: any) => tab.type === active)
                                  ?.color
                              }`
                            : "#0000005a"
                        }
                      />
                    </div>
                  </div>
                </div>
              )}
              {heritageTypeMain?.map((item, i) => {
                if (
                  item.selected === true &&
                  item.label == "Үл хөдлөх өв" &&
                  checkSelected
                ) {
                  return (
                    <>
                      {active == "museum" && immovableSpeciesCategory && (
                        <div className="w-[389.3px]">
                          <div className="flex items-center gap-[10px]">
                            <div className="w-[35%]">Холбогдох төрөл: </div>
                            <CustomSelect
                              defaultValue=""
                              items={[
                                { label: "Бүгд", value: "" },
                                ...immovableSpeciesCategory,
                              ]}
                              onChange={(item) => {
                                setSpeciesId(item.value);
                              }}
                            />
                          </div>
                        </div>
                      )}
                    </>
                  );
                }
              })}

              {heritageTypeMain?.map((item, i) => {
                if (
                  item.selected === true &&
                  item.label == "Хөдлөх өв" &&
                  checkSelected
                ) {
                  return (
                    <>
                      {active == "museum" && (
                        <div className="w-[389.3px]">
                          {movableSpeciesCategory && (
                            <div className="flex items-center gap-[10px]">
                              <div className="w-[35%]">Холбогдох төрөл: </div>
                              <CustomSelect
                                defaultValue=""
                                items={[
                                  { label: "Бүгд", value: "", index: 999 },
                                  ...movableSpeciesCategory,
                                ]}
                                onChange={(item) => {
                                  setSpeciesId(item.value);
                                  movableSpeciesHandler(item);
                                }}
                              />
                            </div>
                          )}
                        </div>
                      )}
                    </>
                  );
                }
              })}

              {heritageTypeMain?.map((item, i) => {
                if (
                  item.selected === true &&
                  item.label == "Биет бус өв" &&
                  checkSelected
                ) {
                  return (
                    <>
                      {active == "museum" && (
                        <div className="w-[389.3px]">
                          {intangibleSpeciesCategory && (
                            <div className="flex items-center gap-[10px]">
                              <div className="w-[35%]">Холбогдох төрөл: </div>
                              <CustomSelect
                                defaultValue=""
                                items={[
                                  { label: "Бүгд", value: "" },
                                  ...intangibleSpeciesCategory,
                                ]}
                                onChange={(item) => {
                                  setSpeciesId(item.value);
                                }}
                              />
                            </div>
                          )}
                        </div>
                      )}
                    </>
                  );
                }
              })}
            </div>

            <div>
              {active == "museum" && (
                <div className="flex items-center gap-3">
                  {heritageTypeMain?.map((item: any, index: any) => {
                    if (item?.selected == true) {
                      return (
                        <ul
                          key={`ul-${index}`}
                          className="flex mt-5 items-center gap-2 border-2 border-c11 rounded-3xl py-1 px-3 bg-c14 text-c11 w-fit"
                        >
                          <li className="font-semibold" key={index}>
                            {item.label}
                          </li>
                          <IoIosCloseCircleOutline
                            className="cursor-pointer"
                            onClick={() => {
                              setSpeciesId("");
                              heritageTypeHandler(item.index);
                            }}
                          />
                        </ul>
                      );
                    }
                  })}
                </div>
              )}
              {active == "museum" && (
                <div className="flex items-center gap-3">
                  {provinceCategoryItems.length > 0 &&
                    provinceCategoryItems?.map((item: any, index: any) => {
                      if (item?.selected == true) {
                        return (
                          <ul
                            key={`ul-${index}`}
                            className="flex mt-5 items-center gap-2 border-2 border-c11 rounded-3xl py-1 px-3 bg-c14 text-c11 w-fit"
                          >
                            <li className="font-semibold" key={index}>
                              {item.label}
                            </li>
                            <IoIosCloseCircleOutline
                              className="cursor-pointer"
                              onClick={() => {
                                provinceHandler(item.index);
                              }}
                            />
                          </ul>
                        );
                      }
                    })}
                </div>
              )}
              {active == "museum" && (
                <div className="flex items-center gap-3">
                  {regionCategoryItems.length > 0 &&
                    regionCategoryItems?.map((item: any, index: any) => {
                      if (item?.selected == true) {
                        return (
                          <ul
                            key={`ul-${index}`}
                            className="flex mt-5 items-center gap-2 border-2 border-c11 rounded-3xl py-1 px-3 bg-c14 text-c11 w-fit"
                          >
                            <li className="font-semibold" key={index}>
                              {item.label}
                            </li>
                            <IoIosCloseCircleOutline
                              className="cursor-pointer"
                              onClick={() => regionHandler(item.index)}
                            />
                          </ul>
                        );
                      }
                    })}
                </div>
              )}
            </div>
          </div>

          {active === "library" && (
            <DigitalLibrary
              enabled={enabled}
              setEnabled={setEnabled}
              setPage={setPage}
            />
          )}
          {enabled ? (
            <CategoryBundle
              key={active}
              data={books}
              type={active}
              title={active}
              isLoading={isBooksLoading}
              enabled={enabled}
            />
          ) : (
            <CategoryBundle
              key={active}
              data={activeData}
              type={active}
              title={active}
              isLoading={activeDataIsLoading}
            />
          )}

          {!enabled &&
            activeData?.data?.length !== 0 &&
            activeData?.pagination?.total > 20 &&
            !isLoading &&
            !activeDataIsLoading && (
              <div className="mt-[4rem]">
                <Pagination
                  setPage={(page) => setPage(page)}
                  page={page}
                  total={activeData?.pagination?.total}
                  limit={activeData?.pagination?.limit}
                />
              </div>
            )}

          {enabled &&
            active === "library" &&
            books?.data?.length !== 0 &&
            books?.pagination?.total > 20 &&
            !isBooksLoading && (
              <div className="mt-[4rem]">
                <Pagination
                  setPage={(page) => setPage(page)}
                  page={page}
                  total={books?.pagination?.total}
                  limit={books?.pagination?.limit}
                />
              </div>
            )}
        </div>
      </div>
    </DefaultLayout>
  );
};

export default SearchPage;
