import * as React from "react";
import DefaultLayout from "../../../components/Layout";
import { FiSearch } from "react-icons/fi";
import useMessage from "../../../hooks/useMessage";
import ResultHeader from "~/components/ResultHeader";
import ResultSidebar from "~/components/ResultSidebar";
import ResultGrid from "~/components/ResultGrid";
import { useQuery } from "react-query";
import apiItem from "~/api/item";
import Pagination from "~/components/pagination";
import useParams from "~/hooks/useParams";
import LoadingOverly from "~/components/libs/LoadingOverly";
import { useRouter } from "next/router";
import ResultList from "~/components/ResultList";
import AddItemModal from "~/components/Modals/AddItemModal";
import SkeletonGrid from "~/components/Skeletons/SkeletonGrid";
import SkeletonList from "~/components/Skeletons/SkeletonList";
import { NotFoundData } from "~/components/libs";

const ItemPage: React.FC = () => {
  const f = useMessage();
  const params = useParams();
  const router = useRouter();
  const [q, setQ] = React.useState("");
  const [page, setPage] = React.useState(1);
  const [view, setView] = React.useState("grid");
  const [modal, setModal] = React.useState(false);
  const [currentData, setCurrentData] = React.useState<any>();

  const { data, isLoading } = useQuery(
    ["item", page, params.q, params.type],
    () => apiItem.find(params.type || "", page, 12, params.q || ""),
    {
      refetchOnWindowFocus: false,
      refetchInterval: false,
    }
  );

  return (
    <DefaultLayout>
      <div className="container mx-auto my-10 px-8 max-w-7xl flex justify-center flex-col dark:text-[#BE8100]">
        <div className="bg-[#F9F9F9] mb-5 w-full relative rounded-[5px] overflow-hidden">
          <input
            placeholder={f({ id: "search-placeholder" })}
            className="h-[50px] bg-[#F9F9F9] lg:h-[80px] w-full p-5 pl-16 focus:outline-none"
            defaultValue={params.q}
            onChange={(e) => {
              setQ(e.target.value);
            }}
            onKeyDown={(e) => {
              if (e.key === "Enter") {
                router.push({
                  pathname: "/search/item",
                  search: `?q=${q || ""}&type=${params.type || ""}`,
                });
              }
            }}
          />
          <div className="absolute top-1/2 left-5 transform -translate-y-1/2">
            <FiSearch size={28} />
          </div>
        </div>

        <ResultHeader
          setView={setView}
          view={view}
          total={data?.pagination?.total}
        />

        <div className="flex flex-col lg:flex-row">
          <div className="w-full lg:w-[368px]">
            <ResultSidebar type={params.type} />
          </div>
          {view === "grid" ? (
            <div className="relative flex-1 grid grid-cols-2 lg:grid-cols-3 gap-10 lg:border-l p-[30px] border-c1">
              <LoadingOverly visible={isLoading} />
              {(data?.data || []).map((item: any, index: number) => (
                <div className="group relative" key={`result-${index}`}>
                  <div
                    onClick={() => {
                      router.push({
                        pathname: `/detail/${item?._id || ""}/${
                          item?.type || ""
                        }`,
                      });
                    }}
                    className="cursor-pointer"
                  >
                    <ResultGrid data={item} key={`item-${index}`} />
                  </div>
                  <img
                    src="/images/detail-love.svg"
                    className="absolute top-2 right-3 w-9 h-9 border rounded-full bg-[#F6F6F6] transform translate-y-[10px] transition-all opacity-0 group-hover:opacity-100 duration-500 group-hover:translate-x-0 hover:transition hover:duration-150 cursor-pointer p-1"
                    onClick={() => {
                      setModal(true), setCurrentData(item);
                    }}
                  />
                </div>
              ))}
              {isLoading && <SkeletonGrid />}
              {isLoading && <SkeletonGrid />}
              {isLoading && <SkeletonGrid />}
              {isLoading && <SkeletonGrid />}
              {isLoading && <SkeletonGrid />}
              {isLoading && <SkeletonGrid />}
              {data?.data.length <= 0 && (
                <div className="col-start-1 flex flex-col items-center lg:col-start-2">
                  <img src="/images/no-data.png" className="w-44" />
                  <p
                    className="font-bold text-center"
                    dangerouslySetInnerHTML={{ __html: f({ id: "no-data" }) }}
                  ></p>
                </div>
              )}
            </div>
          ) : (
            <div className="relative flex-1 lg:border-l p-10 border-c1">
              <LoadingOverly visible={isLoading} />
              {(data?.data || []).map((item: any, index: number) => (
                <div
                  key={`result-list-${index}`}
                  onClick={() => {
                    router.push({
                      pathname: `/detail/${item?._id || ""}/${
                        item?.type || ""
                      }`,
                    });
                  }}
                  className="cursor-pointer"
                >
                  <ResultList data={item} key={`item-list-${index}`} />
                </div>
              ))}
              <div className="space-y-6">
                {isLoading && <SkeletonList />}
                {isLoading && <SkeletonList />}
                {isLoading && <SkeletonList />}
                {isLoading && <SkeletonList />}
              </div>
              {data?.data.length <= 0 && <NotFoundData />}
            </div>
          )}
        </div>
        <div className="flex">
          <div className="w-1/3"></div>
          <div className="w-2/3">
            {data?.data.length > 0 && (
              <div className="flex justify-center">
                <Pagination
                  setPage={(page) => setPage(page)}
                  page={page}
                  total={data?.pagination?.total}
                  limit={data?.pagination?.limit}
                />
              </div>
            )}
          </div>
        </div>
      </div>
      <AddItemModal
        isOpen={modal}
        onClose={() => setModal(false)}
        item={currentData}
      />
    </DefaultLayout>
  );
};

export default ItemPage;
