import * as React from "react";
import DefaultLayout from "../../../components/Layout";
import { FiSearch } from "react-icons/fi";
import useMessage from "../../../hooks/useMessage";
import ResultHeader from "~/components/ResultHeader";
import { useQuery } from "react-query";
import apiArtist from "~/api/artist";
import Pagination from "~/components/pagination";
import useParams from "~/hooks/useParams";
import LoadingOverly from "~/components/libs/LoadingOverly";
import { useRouter } from "next/router";
import AddItemModal from "~/components/Modals/AddItemModal";
import SkeletonGrid from "~/components/Skeletons/SkeletonGrid";
import CustomFilter from "~/components/libs/CutsomFilter";
import SkeletonList from "~/components/Skeletons/SkeletonList";
import { NotFoundData } from "~/components/libs";

const ArtistPage: React.FC = () => {
  const f = useMessage();
  const params = useParams();
  const router = useRouter();
  const [q, setQ] = React.useState("");
  const [page, setPage] = React.useState(1);
  const [view, setView] = React.useState("grid");
  const [modal, setModal] = React.useState(false);
  const [currentData, setCurrentData] = React.useState<any>();

  const { data, isLoading } = useQuery(
    ["item", page, params.q],
    () => apiArtist.find(page, 24, params.q || ""),
    {
      refetchOnWindowFocus: false,
      refetchInterval: false,
    }
  );
  const { data: category, isLoading: loadingCat } = useQuery(
    ["artist-category"],
    () => apiArtist.category(),
    {
      refetchInterval: false,
      refetchOnWindowFocus: false,
    }
  );

  return (
    <DefaultLayout>
      <div className="container mx-auto my-10 px-8 max-w-7xl flex justify-center flex-col dark:text-[#BE8100]">
        <h1 className="font-bold text-c1 text-2xl mb-6">Уран бүтээлчид</h1>
        <div className="bg-[#F9F9F9] mb-5 w-full relative rounded-[5px] overflow-hidden">
          <input
            placeholder={f({ id: "search-placeholder" })}
            className="h-[50px] bg-[#F9F9F9] lg:h-[80px] w-full p-5 pl-16 focus:outline-none"
            defaultValue={params.q}
            onChange={(e) => {
              setQ(e.target.value);
            }}
            onKeyDown={(e) => {
              if (e.key === "Enter") {
                router.push({
                  pathname: "/search/artist",
                  search: `?q=${q || ""}`,
                });
              }
              
            }}
          />
          <div className="absolute top-1/2 left-5 transform -translate-y-1/2">
            <FiSearch size={28} />
          </div>
        </div>

        <ResultHeader
          setView={setView}
          view={view}
          total={data?.pagination?.total}
        />

        {/* <CustomSelect items={[]} defaultValue={""} /> */}

        <div className="flex flex-col lg:flex-row">
          <div className="w-full lg:w-[368px] relative py-4 px-[26px]">
            <div className="absolute -top-[17px] right-1/2 transform translate-x-[17px] lg:translate-x-0 lg:-right-[17px]">
              <img src={"/images/logo-small.svg"} className="w-[34px]" />
            </div>
            <div className="relative">
              <LoadingOverly visible={loadingCat} />
              <CustomFilter
                items={(category?.data || [])?.map((item: any) => ({
                  label: `${item?.name}`,
                  value: item?._id,
                }))}
                defaultValue={"Уран бүтээлчийн ангилал"}
              />
            </div>
          </div>
          {view === "grid" ? (
            <div className="relative flex-1 grid grid-cols-2 lg:grid-cols-4 gap-8 lg:border-l p-[30px] border-c1">
              <LoadingOverly visible={isLoading} />
              {(data?.data || []).map((item: any, index: number) => (
                <div className="group relative" key={`result-${index}`}>
                  <div
                    onClick={() => {
                      router.push({
                        pathname: `/search/artist/detail/${item?._id || ""}`,
                      });
                    }}
                    className="cursor-pointer rounded-2xl shadow-[rgba(0,_0,_0,_0.2)_0px_3px_8px] p-2 h-[250px] overflow-hidden"
                  >
                    <div
                      className="rounded-lg mb-4 h-40"
                      // style={{
                      //   aspectRatio: "1 / 1",
                      // }}
                    >
                      <img
                        src={item?.mainImg || "/images/empty.svg"}
                        className="h-full pointer-events-none rounded-md mx-auto bg-contain"
                      />
                    </div>
                    <div className="space-y-2">
                      <p className="text-xs ">{item?.category?.name}</p>
                      <p className="text-sm font-semibold">{item?.name}</p>
                    </div>
                  </div>
                  <img
                    src="/images/detail-love.svg"
                    className="absolute top-2 right-3 w-9 h-9 border rounded-full bg-[#F6F6F6] transform translate-y-[10px] transition-all opacity-0 group-hover:opacity-100 duration-500 group-hover:translate-x-0 hover:transition hover:duration-150 cursor-pointer p-0.5"
                    onClick={() => {
                      setModal(true), setCurrentData(item);
                    }}
                  />
                </div>
              ))}
              {isLoading && <SkeletonGrid />}
              {isLoading && <SkeletonGrid />}
              {isLoading && <SkeletonGrid />}
              {isLoading && <SkeletonGrid />}
              {isLoading && <SkeletonGrid />}
              {isLoading && <SkeletonGrid />}
              {isLoading && <SkeletonGrid />}
              {isLoading && <SkeletonGrid />}

              {data?.data.length <= 0 && (
                <div className=" flex flex-col items-center col-span-4">
                  <img src="/images/no-data.png" className="w-44" />
                  <p
                    className="font-bold text-center"
                    dangerouslySetInnerHTML={{ __html: f({ id: "no-data" }) }}
                  ></p>
                </div>
              )}
            </div>
          ) : (
            <div className="relative flex-1 lg:border-l p-10 border-c1">
              <LoadingOverly visible={isLoading} />
              {(data?.data || []).map((item: any, index: number) => (
                <div
                  key={`result-list-${index}`}
                  onClick={() => {
                    router.push({
                      pathname: `/search/artist/detail/${item?._id || ""}
                      `,
                    });
                  }}
                  className="cursor-pointer"
                >
                  <section className="relative flex w-full h-20 rounded-2xl mb-8 space-x-6 shadow-[rgba(0,_0,_0,_0.24)_0px_3px_8px] dark:text-[#BE8100]">
                    <div
                      className="rounded-xl overflow-hidden "
                      style={{
                        aspectRatio: "1 / 1",
                      }}
                    >
                      <img
                        src={item?.mainImg || "/images/empty.svg"}
                        className="pointer-events-none rounded-2xl mx-auto bg-contain p-4 h-full"
                      />
                    </div>

                    <div className="space-y-2 mt-4 w-2/3">
                      <p className="font-bold text-sm leading-5">
                        {item?.name}
                      </p>
                      <p className="text-xs overflow-auto h-4">
                        {item?.description}
                      </p>
                    </div>
                    <button
                      onClick={(e) => {
                        e.preventDefault();
                        e.stopPropagation();
                        setModal(true);
                      }}
                    >
                      <img
                        src="/images/detail-love.svg"
                        className="absolute top-0 right-4 w-9 border rounded-full bg-[#F6F6F6] h-9 p-1 mt-4"
                      />
                    </button>
                    <button
                      onClick={(e) => {
                        e.preventDefault();
                        e.stopPropagation();
                        router.push({
                          pathname: `/search/artist/detail/${item?._id || ""}/${
                            item?.type || ""
                          }`,
                        });
                      }}
                    >
                      <img
                        src="/images/eye.svg"
                        className="absolute top-0 right-14 w-7 border rounded-full bg-[#F6F6F6] h-7 p-1 mt-4"
                      />
                    </button>
                    {modal && (
                      <AddItemModal
                        isOpen={modal}
                        onClose={() => setModal(false)}
                        item={data}
                      />
                    )}
                  </section>
                </div>
              ))}
              <div className="space-y-6">
                {isLoading && <SkeletonList />}
                {isLoading && <SkeletonList />}
                {isLoading && <SkeletonList />}
                {isLoading && <SkeletonList />}
              </div>
              {data?.data.length <= 0 && <NotFoundData />}
            </div>
          )}
        </div>
        <div className="flex">
          <div className="w-1/3"></div>
          <div className="w-2/3">
            {data?.data.length > 0 && (
              <div className="flex justify-center">
                <Pagination
                  setPage={(page) => setPage(page)}
                  page={page}
                  total={data?.pagination?.total}
                  limit={data?.pagination?.limit}
                />
              </div>
            )}
          </div>
        </div>
      </div>
      <AddItemModal
        isOpen={modal}
        onClose={() => setModal(false)}
        item={currentData}
      />
    </DefaultLayout>
  );
};

export default ArtistPage;
