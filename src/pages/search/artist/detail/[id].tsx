import React from "react";
import DefaultLayout from "~/components/Layout";
import ItemDetailsContainers from "~/containers/ArtistDetail";

const ArtistDetail = () => {
  return (
    <DefaultLayout>
      <ItemDetailsContainers />
    </DefaultLayout>
  );
};

export default ArtistDetail;
