import React, { Fragment, useState } from "react";
import { Dialog, Transition } from "@headlessui/react";
import { useForm, yupResolver } from "@mantine/form";
import { Box, TextInput, Select, Grid, Textarea } from "@mantine/core";
import { useQuery, useMutation } from "react-query";
import SelectType from "../components/Heritage/SelectType";
import inquiryItem from "~/api/inquiry";
import apiItem from "~/api/item";
import Upload from "../components/Upload/Upload";
import DefaultLayout from "~/components/Layout";
import ProfileLayout from "~/components/Layout/ProfileLayout";
import { toast } from "react-toastify";
import useMessage from "~/hooks/useMessage";
import * as Yup from "yup";

const InquiryModal = (props: { isOpen: boolean; onClose: any }) => {
  const f = useMessage();
  const { reset } = useForm();
  const [selectedCategory, setSelectedCategory] = React.useState<any>(null);
  const [heritageTypeCategory, setHeritageTypeCategory] = React.useState<
    Array<string>
  >([]);
  const [subHeritageTypeCategory, setSubHeritageTypeCategory] = React.useState<
    Array<string>
  >([]);
  const { isOpen, onClose } = props;
  const filesRef = React.useRef<any>({});
  const [idForRegion, setIdForRegion] = React.useState<any>(null);
  const [clearFile, setClearFile] = React.useState<boolean>(false);
  const [provinceCategory, setProvinceCategory] = React.useState<Array<string>>(
    []
  );
  const [regionCategory, setRegionCategory] = React.useState<Array<string>>([]);

  const createMutation = useMutation((item: any) =>
    inquiryItem.createReference(item)
  );

  const form = useForm({
    initialValues: {
      firstName: "",
      lastName: "",
      registerNumber: "",
      province: "",
      region: "",
      address: "",
      type: "",
      subtype: "",
      content: "",
      image: "",
    },
    validate: yupResolver(
      Yup.object().shape({
        firstName: Yup.string().required("Бөглөх шаардлагатай."),
        lastName: Yup.string().required("Бөглөх шаардлагатай."),
        registerNumber: Yup.string().required("Бөглөх шаардлагатай."),
        province: Yup.string().required("Бөглөх шаардлагатай."),
        region: Yup.string().required("Бөглөх шаардлагатай."),
        address: Yup.string().required("Бөглөх шаардлагатай."),
        type: Yup.string().required("Бөглөх шаардлагатай."),
        subtype: Yup.string().required("Бөглөх шаардлагатай."),
        content: Yup.string().required("Бөглөх шаардлагатай."),
        // image: Yup.object().required("Бөглөх шаардлагатай."),
      })
    ),
  });

  const { data: heritageTypes, isLoading } = useQuery(
    ["heritage_type", isOpen],
    () => inquiryItem.heritageTypes()
  );

  const { data: heritageProvinces, isLoading: heritageProvincesIsLoading } =
    useQuery(["provinces"], () => apiItem.heritageProvince());

  const { data: heritageRegions, isLoading: heritageRegionsLoading } = useQuery(
    ["regions", idForRegion],
    () => idForRegion && apiItem.heritageRegion(idForRegion)
  );

  React.useEffect(() => {
    setHeritageTypeCategory(
      heritageTypes?.data?.length > 1
        ? heritageTypes?.data?.map((item: any) => {
            return {
              label: item?.name,
              value: item?.id,
            };
          })
        : []
    );
  }, [heritageTypes]);

  React.useEffect(() => {
    setProvinceCategory(
      heritageProvinces?.data?.length > 1
        ? heritageProvinces?.data?.map((item: any) => {
            return {
              label: item?.province_name,
              value: item?.id,
            };
          })
        : []
    );
  }, [heritageProvinces]);

  React.useEffect(() => {
    setRegionCategory(
      heritageRegions?.data?.length > 1
        ? heritageRegions?.data?.map((item: any) => {
            return {
              label: item?.region_name,
              value: item?.id,
            };
          })
        : []
    );
  }, [heritageRegions]);

  const { data: heritageSub, isLoading: heritageSubIsLoading } = useQuery(
    ["heritage_sub", form.values.type],
    () => inquiryItem.heritageSubTypes(form.values.type)
  );

  React.useEffect(() => {
    setSubHeritageTypeCategory(
      heritageSub?.data?.length > 1
        ? heritageSub?.data?.map((item: any) => {
            return {
              label: item?.name,
              value: item?.id,
            };
          })
        : []
    );
  }, [heritageSub]);

  const handleSubmit = async (values: any, e: any) => {
    const formData = new FormData();

    formData.append("firstName", values.firstName);
    formData.append("lastName", values.lastName);
    formData.append("registerNumber", values.registerNumber);
    formData.append("province", values.province);
    formData.append("region", values.region);
    formData.append("address", values.address);
    formData.append("type", values.type);
    formData.append("subtype", values.subtype);
    formData.append("content", values.content);
    formData.append("image", filesRef.current["current_image"]);

    createMutation
      .mutateAsync(formData)
      .then((res) => {
        toast.success(f({ id: "success-add-item" }));
        form.reset();
        formData.delete("image");
        setClearFile(true);
      })
      .catch((e) => {
        toast.error(
          e == "Cannot read properties of null (reading 'image')"
            ? "Зураг оруулна уу."
            : e
        );
      });
  };

  return (
    <DefaultLayout>
      <div className="bg-[#112437] py-[24px] mb-[15px] lg:mb-[40px]">
        <div className="text-[21px] font-[600] text-white  container mx-auto px-[16px]">
          Лавлагаа
        </div>
      </div>
      <ProfileLayout>
        <div className="px-5 lg:px-[64px] font-lora">
          {/* <div className="space-y-[60px] px-5 lg:px-[64px] font-lora"> */}
          <form onSubmit={form.onSubmit(handleSubmit)} className="font-lora">
            <div className="md:flex gap-[20px]">
              <TextInput
                radius="lg"
                className="flex-1"
                // className="flex-1 border border-[1px] rounded-[17px]"
                size={"md"}
                withAsterisk
                placeholder="Нэр"
                {...form.getInputProps("firstName")}
                mb="30px"
              />

              <TextInput
                radius="lg"
                className="flex-1"
                size={"md"}
                withAsterisk
                placeholder="Овог"
                {...form.getInputProps("lastName")}
                mb="30px"
              />
            </div>
            <div className="md:flex gap-[20px]">
              <TextInput
                radius="lg"
                className="flex-1"
                size={"md"}
                withAsterisk
                placeholder="Регистрийн дугаар"
                {...form.getInputProps("registerNumber")}
                mb="30px"
              />
              <TextInput
                radius="lg"
                className="flex-1"
                size={"md"}
                withAsterisk
                placeholder="Хаяг"
                {...form.getInputProps("address")}
                mb="30px"
              />
            </div>
            <div className="md:flex gap-[20px]">
              <Select
                radius="lg"
                className="flex-1"
                size={"md"}
                defaultValue=""
                placeholder="Аймаг"
                data={provinceCategory}
                required
                {...form.getInputProps("province")}
                onChange={(item: string) => {
                  if (form.getInputProps("province").onChange) {
                    form.getInputProps("province").onChange(item);
                  }
                  setIdForRegion(item);
                }}
                mb="30px"
              />
              <Select
                radius="lg"
                className="flex-1"
                size={"md"}
                placeholder="Сум"
                data={regionCategory}
                required
                {...form.getInputProps("region")}
                mb="30px"
              />
            </div>
            <div className="md:flex gap-[20px]">
              <Select
                radius="lg"
                className="flex-1"
                size={"md"}
                placeholder="Соёлын өвийн төрлөөс сонгоно уу"
                data={heritageTypeCategory}
                required
                {...form.getInputProps("type")}
                mb="lg"
              />
              {subHeritageTypeCategory.length > 1 && (
                <Select
                  radius="lg"
                  className="flex-1"
                  size={"md"}
                  placeholder="Соёлын өвийн дэд төрлөөс сонгоно уу"
                  data={subHeritageTypeCategory}
                  required
                  {...form.getInputProps("subtype")}
                  mb="30px"
                />
              )}
            </div>
            <Textarea
              radius="lg"
              size={"md"}
              withAsterisk
              placeholder="Контент"
              {...form.getInputProps("content")}
              mb="30px"
            />
            <Upload
              {...form.getInputProps("image")}
              onChange={(value: any) => {}}
              handleFile={(file: any) => {
                filesRef.current["current_image"] = file;
              }}
              previewStyle={{
                width: "auto",
                height: "70px",
                margin: 0,
              }}
              w={400}
              clearFile={clearFile}
              setClearFile={setClearFile}
            />

            <Box className="flex items-center justify-end w-full mt-4">
              <button
                type="submit"
                className="w-[150px] bg-[#112437] active:bg-c5 text-[16px] 2xl:text-[18px] text-white p-2.5 rounded-[17px] hover:bg-c15 font-sans"
              >
                Хадгалах
              </button>
            </Box>
          </form>
        </div>
      </ProfileLayout>
    </DefaultLayout>
  );
};

export default InquiryModal;
