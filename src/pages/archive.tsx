import React, { useState } from "react";
import DefaultLayout from "~/components/Layout";
import TreasuryProduct from "~/components/Treasury/TreasuryProduct";
import TeasurySearch from "~/components/Treasury/TreasurySearch";

const Culture = () => {
  return (
    <DefaultLayout>
      <TeasurySearch />
      <TreasuryProduct />
    </DefaultLayout>
  );
};

export default Culture;
