import { useRouter } from "next/router";
import useMessage from "~/hooks/useMessage";

import { GoShareAndroid } from "react-icons/go";
import { HiOutlineMegaphone } from "react-icons/hi2";

import DefaultLayout from "~/components/Layout";

const HeritageContent = () => {
  const f = useMessage();

  const router = useRouter();
  const page = router.query.heritageId;

  const items = [
    {
      title: f({ id: "culture-detail-title" }),
      firstName: f({ id: "culture-detail-name" }),
      description: f({ id: "culture-detail-desc-0" }),
      image: "/culture/c-0.png",
      url: "1",
      year: f({ id: "culture-detail-information-value-1" }),
      size: f({ id: "culture-detail-information-value-2" }),
      number: f({ id: "culture-detail-information-value-3" }),
      feature: f({ id: "culture-detail-information-value-4" }),
      place: f({ id: "culture-detail-information-value-5" }),
      whoFound: f({ id: "culture-detail-information-value-6" }),
    },
  ];

  return (
    <DefaultLayout>
      <section>
        {items.map((item, index) => (
          <div key={index}>
            <div className="grid grid-cols-3">
              <img src={item.image} />
              <img src={item.image} />
              <img src={item.image} />
            </div>
            <h4 className="flex justify-center font-bold lg:text-[38px]">
              {item.title}
            </h4>
            <div className="flex justify-center text-c12 mt-2">
              {item.firstName}
            </div>
            <div className="grid grid-cols-1 place-content-center lg:grid-cols-3">
              <div className="mx-auto text-c7 leading-10">
                <div className="hover:text-c13">
                  {f({ id: "culture-detail-menu-1" })}
                </div>
                <div className="hover:text-c13">
                  {f({ id: "culture-detail-menu-2" })}
                </div>
                <div className="hover:text-c13">
                  {f({ id: "culture-detail-menu-3" })}
                </div>
                <div className="hover:text-c13">
                  {f({ id: "culture-detail-menu-4" })}
                </div>
              </div>
              <div className="mx-auto mt-6">
                <div className="flex text-c11 gap-3">
                  <div className="border border-c11 rounded-[20px] px-2 py-0.5">
                    {f({ id: "culture-detail-type-1" })}
                  </div>
                  <div className="border border-c11 rounded-[20px] px-2 py-0.5">
                    {f({ id: "culture-detail-type-2" })}
                  </div>
                  <div className="border border-c11 rounded-[20px] px-2 py-0.5">
                    {f({ id: "culture-detail-type-3" })}
                  </div>
                </div>
                <h4 className="font-bold lg:text-[28px] mt-6">
                  {f({ id: "culture-detail-menu-1" })}
                </h4>
                <div className="text-justify leading-7.5 mt-6">
                  {item.description}
                </div>
                <div className="text-c11 underline underline-offset-2">
                {f({ id: "culture-detail-continue" })}
                </div>
              </div>
              <div className="mx-auto">
                <div className="flex items-center gap-2 w-64 border border-c2 p-2 mb-2">
                  <GoShareAndroid />
                  {f({ id: "culture-detail-share" })}
                  <div />
                </div>
                <div className="flex items-center gap-2 w-64 border border-c2 p-2">
                  <HiOutlineMegaphone />
                  <div>{f({ id: "culture-detail-request" })}</div>
                </div>
              </div>
            </div>
          </div>
        ))}
      </section>
    </DefaultLayout>
  );
};

export default HeritageContent;
