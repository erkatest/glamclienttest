import React from "react";
import DefaultLayout from "~/components/Layout";
import useMessage from "~/hooks/useMessage";

const ContactUs = () => {
  const f = useMessage();
  return (
    <DefaultLayout>
      <div className="contain mx-auto px-8 max-w-7xl space-y-6">
        <h1 className="font-bold text-2xl text-[#011C5A]">
          {f({ id: "contact-us-title" })}
        </h1>
        <p>
          Lorem ipsum dolor sit amet consectetur. Ornare a sit ac risus
          pellentesque maecenas risus phasellus quam. Ac sem enim dictum et
          amet. Consectetur elit justo egestas nibh vulputate neque. Tellus orci
          risus id egestas habitasse cum vitae. Malesuada gravida etiam at vel
          nunc scelerisque at nisi congue. Arcu vitae tristique pharetra
          pulvinar egestas habitasse. Nisi at amet eu eu vitae interdum integer
          ipsum. Enim scelerisque nec et quam eu porta amet aenean nec. Felis
          euismod tristique vel est.
        </p>
        <p>
          Lorem ipsum dolor sit amet consectetur. Ornare a sit ac risus
          pellentesque maecenas risus phasellus quam. Ac sem enim dictum et
          amet. Consectetur elit justo egestas nibh vulputate neque. Tellus orci
          risus id egestas habitasse cum vitae. Malesuada gravida etiam at vel
          nunc scelerisque at nisi congue. Arcu vitae tristique pharetra
          pulvinar egestas habitasse. Nisi at amet eu eu vitae interdum integer
          ipsum. Enim scelerisque nec et quam eu porta amet aenean nec. Felis
          euismod tristique vel est.
        </p>
        <p>
          Lorem ipsum dolor sit amet consectetur. Ornare a sit ac risus
          pellentesque maecenas risus phasellus quam. Ac sem enim dictum et
          amet. Consectetur elit justo egestas nibh vulputate neque. Tellus orci
          risus id egestas habitasse cum vitae. Malesuada gravida etiam at vel
          nunc scelerisque at nisi congue. Arcu vitae tristique pharetra
          pulvinar egestas habitasse. Nisi at amet eu eu vitae interdum integer
          ipsum. Enim scelerisque nec et quam eu porta amet aenean nec. Felis
          euismod tristique vel est.
        </p>
      </div>
    </DefaultLayout>
  );
};

export default ContactUs;
