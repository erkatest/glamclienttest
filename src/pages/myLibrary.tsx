import React, { useEffect, useState } from "react";
import FolderIcon from "~/assets/FolderIcon";
import DefaultLayout from "~/components/Layout";
import ProfileLayout from "~/components/Layout/ProfileLayout";
import Slider from "react-slick";
import { useQuery } from "react-query";
import apiSavedItem from "~/api/savedItem";
import { FiArrowLeftCircle, FiArrowRightCircle } from "react-icons/fi";
import { useSearchParams } from "next/navigation";
import SkeletonGrid from "~/components/Skeletons/SkeletonGrid";
import { useUser } from "~/context/auth";
import { useRouter } from "next/router";
import CreateFolderModal from "~/components/Modals/CreateFolderModal";
import { FaEllipsisV } from "react-icons/fa";
import { ActionIcon, Menu } from "@mantine/core";
import { MdDelete } from "react-icons/md";
import { CiEdit } from "react-icons/ci";
import FolderDeleteModal from "~/components/Modals/FolderDeleteModal";
import FolderUpdateModal from "~/components/Modals/FolderUpdateModal";
import useMessage from "~/hooks/useMessage";
import store from "~/utils/store";
import ContentSkeleton from "~/components/ContentCard/Skeleton";
import ContentCard from "~/components/ContentCard";
import Pagination from "~/components/pagination";

const MyLibrary = () => {
  const router = useRouter();
  const searchParams: any = useSearchParams();
  const f = useMessage();
  const [deleteFolderModal, setDeleteFolderModal] = useState(false);
  const [editFolderModal, setEditFolderModal] = useState(false);
  const currentPage = parseInt(searchParams.get("page") || 1);
  const [createFolder, setCreateFolder] = useState(false);
  const folder_id = searchParams.get("folder_id");
  const [folderId, setFolderId] = useState("");
  const [page, setPage] = React.useState(1);

  const { user, checkUserMe } = useUser();

  const savedItems = useQuery(
    ["savedItems", page, user, router.query.page],
    () => user && apiSavedItem.getSavedItem(12, router?.query?.page),
    {
      refetchOnWindowFocus: false,
      refetchInterval: false,
    }
  );

  React.useEffect(() => {
    if (store?.get("user") !== null) {
      checkUserMe();
    } else {
      router.push("/");
    }
  }, []);

  const sliderRef = React.useRef<any>();
  const settings = {
    className: "px-[16px]",
    infinite: true,
    slidesToShow: 3,
    swipeToSlide: true,
    autoplay: folderId ? false : true,
    nextArrow: <FiArrowRightCircle color="black" />,
    prevArrow: <FiArrowLeftCircle color="black" />,
    responsive: [
      {
        breakpoint: 1920,
        settings: {
          slidesToShow: 3,
          slidesToScroll: 1,
          infinite: true,
        },
      },
      {
        breakpoint: 1080,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 1,
          initialSlide: 2,
        },
      },
      {
        breakpoint: 900,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 1,
        },
      },
      {
        breakpoint: 600,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 1,
        },
      },
    ],
  };
  const typeReturnName = (type: any) => {
    if (type === "koha") {
      return "Номын сан";
    } else if (type === "atwork") {
      return "Урын сан";
    } else if (type === "archive") {
      return "Сан хөмрөг";
    } else if (type === "heritage") {
      return "Соёлын өв";
    }
  };

  return (
    <DefaultLayout>
      {/* <FolderUpdateModal
        isOpen={editFolderModal}
        onClose={() => setEditFolderModal(false)}
        refetch={refetch}
        foldereRefetch={folderData?.refetch}
        id={folderId}
        data={folderData?.data?.data?.folder}
      />
      <FolderDeleteModal
        isOpen={deleteFolderModal}
        onClose={() => setDeleteFolderModal(false)}
        refetch={refetch}
        id={folderId}
      />
      <CreateFolderModal
        isOpen={createFolder}
        onClose={() => setCreateFolder(false)}
        refetch={refetch}
      /> */}
      <div className="bg-[#112437] py-[24px] mb-[15px] lg:mb-[40px]">
        <div className="text-[21px] font-[600] text-white  container mx-auto px-[16px]">
          Миний булан
        </div>
      </div>
      <ProfileLayout>
        <div className="space-y-[36px] px-5 lg:px-[64px]">
          <div className="flex justify-between items-center">
            <div className="relative w-full lg:w-3/5">
              {/* {data?.data?.length > 3 ? (
                <Slider ref={(c) => (sliderRef.current = c)} {...settings}>
                  {data?.data?.map((item: any, index: any) => (
                    <div
                      onClick={() => setFolderId(item?._id)}
                      key={`folders-${index}`}
                      className="flex" // Ensure this is a flex container
                    >
                      <div
                        className={`flex space-x-[16px] items-center px-[14px] py-[7px] cursor-pointer rounded-[39px] ${
                          folderId === item?._id ? "bg-[#edf0f3]" : ""
                        }`}
                        style={{ width: "fit-content" }} // Adjust width to fit content
                      >
                        <FolderIcon color={`#${item?.color}`} size={true} />
                        <div className="text-[14px] text-[#112437] -tracking-[0.28px] ">
                          {item?.name}
                        </div>
                      </div>
                    </div>
                  ))}
                </Slider>
              ) : (
                <div className="flex space-x-4">
                  {data?.data?.map((item: any, index: any) => (
                    <div
                      onClick={() => setFolderId(item?._id)}
                      key={`folders-${index}`}
                      className="flex" // Ensure this is a flex container
                    >
                      <div
                        className={`flex space-x-[8px] items-center px-[14px] py-[7px] cursor-pointer rounded-[16px] ${
                          folderId === item?._id ? "bg-[#edf0f3]" : ""
                        }`}
                        style={{ width: "fit-content" }} // Adjust width to fit content
                      >
                        <FolderIcon color={`#${item?.color}`} size={true} />
                        <div className="text-[14px] text-[#112437] font-[500]">
                          {item?.name}
                        </div>
                      </div>
                    </div>
                  ))}
                </div>
              )} */}
            </div>

            {/* <div
              onClick={() => setCreateFolder(true)}
              className="cursor-pointer flex items-center space-x-[8px] px-[14px] py-[7px] border border-[#A6B2C3] rounded-[1rem] text-[14px] "
            >
              <svg
                xmlns="http://www.w3.org/2000/svg"
                width="24"
                height="24"
                viewBox="0 0 24 24"
                fill="none"
              >
                <path
                  d="M14.7413 4.5H20.7143C21.1312 4.49996 21.5435 4.5868 21.9249 4.75499C22.3064 4.92317 22.6486 5.16902 22.9297 5.47683C23.2109 5.78465 23.4248 6.14768 23.5578 6.54276C23.6908 6.93785 23.74 7.35633 23.7023 7.7715L22.7468 18.2715C22.679 19.0169 22.3351 19.71 21.7826 20.2148C21.23 20.7197 20.5087 20.9997 19.7603 21H4.23678C3.48834 20.9997 2.76704 20.7197 2.21448 20.2148C1.66192 19.71 1.31802 19.0169 1.25028 18.2715L0.294783 7.7715C0.230862 7.07667 0.412374 6.38141 0.807783 5.8065L0.749283 4.5C0.749283 3.70435 1.06535 2.94129 1.62796 2.37868C2.19057 1.81607 2.95363 1.5 3.74928 1.5H9.25728C10.0529 1.50017 10.8158 1.81635 11.3783 2.379L12.6203 3.621C13.1828 4.18365 13.9457 4.49983 14.7413 4.5ZM2.25828 4.68C2.57928 4.563 2.92428 4.5 3.28428 4.5H11.3783L10.3178 3.4395C10.0365 3.15818 9.65508 3.00008 9.25728 3H3.74928C3.35635 2.99993 2.97907 3.15405 2.69857 3.42922C2.41807 3.70439 2.25675 4.07864 2.24928 4.4715L2.25828 4.68Z"
                  fill="#A6B2C3"
                />
                <path
                  d="M15.5003 14.0827H12.5837V16.9993C12.5837 17.1541 12.5222 17.3024 12.4128 17.4118C12.3034 17.5212 12.155 17.5827 12.0003 17.5827C11.8456 17.5827 11.6972 17.5212 11.5878 17.4118C11.4785 17.3024 11.417 17.1541 11.417 16.9993V14.0827H8.50033C8.34562 14.0827 8.19724 14.0212 8.08785 13.9118C7.97845 13.8024 7.91699 13.6541 7.91699 13.4993C7.91699 13.3446 7.97845 13.1963 8.08785 13.0869C8.19724 12.9775 8.34562 12.916 8.50033 12.916H11.417V9.99935C11.417 9.84464 11.4785 9.69627 11.5878 9.58687C11.6972 9.47747 11.8456 9.41602 12.0003 9.41602C12.155 9.41602 12.3034 9.47747 12.4128 9.58687C12.5222 9.69627 12.5837 9.84464 12.5837 9.99935V12.916H15.5003C15.655 12.916 15.8034 12.9775 15.9128 13.0869C16.0222 13.1963 16.0837 13.3446 16.0837 13.4993C16.0837 13.6541 16.0222 13.8024 15.9128 13.9118C15.8034 14.0212 15.655 14.0827 15.5003 14.0827Z"
                  fill="white"
                />
              </svg>
              <div className="tracking-wide">Хавтас үүсгэх</div>
            </div> */}
          </div>
          {/* {folderId && (
            <div className="container mx-auto flex justify-end items-center">
              <div>
                <Menu width={200} shadow="md" position="bottom-end">
                  <Menu.Target>
                    <ActionIcon>
                      <FaEllipsisV className="cursor-pointer" />
                    </ActionIcon>
                  </Menu.Target>

                  <Menu.Dropdown>
                    <Menu.Item onClick={() => setEditFolderModal(true)}>
                      <div className="flex gap-2 items-center font-lora">
                        <CiEdit size={16} />
                        <div>Хавтас засварлах</div>
                      </div>
                    </Menu.Item>
                    <Menu.Item onClick={() => setDeleteFolderModal(true)}>
                      <div className="flex gap-2 items-center font-lora">
                        <MdDelete size={16} />
                        <div>Хавтас устгах</div>
                      </div>
                    </Menu.Item>
                  </Menu.Dropdown>
                </Menu>
              </div>
            </div>
          )} */}
          <div
            className={
              savedItems?.data?.data.length > 0 || savedItems?.isLoading
                ? "grid grid-cols-2 lg:grid-cols-3 gap-[20px] lg:gap-[35px]"
                : "w-full"
            }
          >
            {savedItems?.isLoading ? (
              [0, 1, 2, 3, 4, 5]?.map((index) => (
                <ContentSkeleton key={index} />
              ))
            ) : savedItems?.data?.data.length > 0 ? (
              savedItems?.data?.data.map((item: any, index: number) => {
                return (
                  <ContentCard
                    deletable
                    key={index}
                    title={item?.item_name}
                    url={
                      item?.heritage_type
                        ? `/detail/${item?.item_id || ""}/${
                            item?.type || ""
                          }?heritage_type=${item?.heritage_type}`
                        : item.item_id.length === 4
                        ? `/detail/${item.item_id}`
                        : `/detail/${item?.item_id || ""}/${item?.type || ""}`
                    }
                    imageUrl={
                      item?.main_img ? item?.main_img : "/images/logo-blue.png"
                    }
                    description={item?.description}
                    id={item?._id}
                    type={item?.type}
                    // selectedFolders={folderId}
                    heritageType={item?.heritage_type} // not yet
                    refetch={() => savedItems.refetch()}
                  />
                );
              })
            ) : (
              <div className="col-start-1 md:gtid-cols-2 flex my-20 flex-col items-center lg:col-start-3">
                <img alt="" src="/images/no-data.png" className="w-44" />
                <p
                  className="font-bold text-center"
                  dangerouslySetInnerHTML={{
                    __html: f({ id: "no-data-found" }),
                  }}
                ></p>
              </div>
            )}
          </div>
          {savedItems?.data?.data.length !== 0 &&
            savedItems?.data?.pagination?.total > 10 && (
              <Pagination
                setPage={(page: any) => {
                  router?.push(`/myLibrary?page=${page}`);
                  setPage(page);
                }}
                page={page}
                total={savedItems?.data?.pagination?.total}
                limit={savedItems?.data?.pagination?.limit}
              />
            )}
        </div>
      </ProfileLayout>
    </DefaultLayout>
  );
};

export default MyLibrary;
