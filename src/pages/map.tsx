import React from "react";
import DefaultLayout from "~/components/Layout";
import dynamic from "next/dynamic";

const MapContainers = dynamic(() => import("~/containers/MapContainer"), {
  ssr: false,
});
const Map = () => {
  return (
    <DefaultLayout>
      <MapContainers />
    </DefaultLayout>
  );
};

export default Map;
