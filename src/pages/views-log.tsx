import React, { useState } from "react";
import Arrow from "~/assets/Arrow";
import EditIcon from "~/assets/EditIcon";
import FolderIcon from "~/assets/FolderIcon";
import LogSearchTimeIcon from "~/assets/LogSearchTimeIcon";
import DefaultLayout from "~/components/Layout";
import ProfileLayout from "~/components/Layout/ProfileLayout";
import { IoIosCloseCircleOutline } from "react-icons/io";
import apiItem from "~/api/item";
import { useSearchParams } from "next/navigation";
import { useQuery } from "react-query";
import dayjs from "dayjs";
import Pagination from "~/components/pagination";
import { useRouter } from "next/router";
import SkeletonList from "~/components/Skeletons/SkeletonList";
import { useUser } from "~/context/auth";
import Link from "next/link";
import store from "~/utils/store";
const SearchLog = () => {
  const router = useRouter();
  const searchParams: any = useSearchParams();
  const currentPage = parseInt(searchParams.get("page") || 1);
  const { user, checkUserMe } = useUser();

  React.useEffect(() => {
    if (store?.get("user") !== null) {
      checkUserMe();
    } else {
      router.push("/");
    }
  }, []);
  const { data, refetch, isLoading }: any = useQuery(
    ["viewed-log", currentPage],
    () => apiItem.findViewedItemsAll(currentPage, 10),
    {
      refetchOnWindowFocus: false,
      refetchInterval: false,
    }
  );

  return (
    <DefaultLayout>
      <div className="bg-[#112437] py-[24px] mb-[15px] lg:mb-[40px]">
        <div className="text-[21px] font-[600] text-white  container mx-auto px-[16px]">
          Ашиглалтын түүх
        </div>
      </div>

      <ProfileLayout>
        <div className="space-y-[36px] lg:px-[64px]">
          <div className="text-[18px] font-[600] text-[#112437] ">
            Үзэлтийн түүх
          </div>

          <div className="space-y-[12px]">
            {isLoading ? (
              [0, 1, 2, 3, 4, 5]?.map((index) => (
                <div
                  className="flex space-x-[16px] items-center"
                  key={`views-log-${index}`}
                >
                  <LogSearchTimeIcon />
                  <div className="w-full space-y-[5px]">
                    <div className="flex justify-between w-full items-center">
                      <div className="flex items-center space-x-[16px]">
                        <div className="aspect-square">
                          <svg
                            className="w-[64px] h-[64px] text-gray-200 dark:text-gray-600"
                            aria-hidden="true"
                            xmlns="http://www.w3.org/2000/svg"
                            fill="currentColor"
                            viewBox="0 0 20 18"
                          >
                            <path d="M18 0H2a2 2 0 0 0-2 2v14a2 2 0 0 0 2 2h16a2 2 0 0 0 2-2V2a2 2 0 0 0-2-2Zm-5.5 4a1.5 1.5 0 1 1 0 3 1.5 1.5 0 0 1 0-3Zm4.376 10.481A1 1 0 0 1 16 15H4a1 1 0 0 1-.895-1.447l3.5-7A1 1 0 0 1 7.468 6a.965.965 0 0 1 .9.5l2.775 4.757 1.546-1.887a1 1 0 0 1 1.618.1l2.541 4a1 1 0 0 1 .028 1.011Z" />
                          </svg>
                        </div>
                        <div className="h-2.5 bg-gray-200 rounded-full dark:bg-gray-700 w-48 mb-4" />
                      </div>
                      <div className="h-2.5 bg-gray-200 rounded-full dark:bg-gray-700 w-24 mb-4" />

                      {/* <IoIosCloseCircleOutline size={24} /> */}
                    </div>
                    <div className="border-t w-full" />
                  </div>
                </div>
              ))
            ) : data?.data?.length > 0 ? (
              data?.data?.map((item: any, index: number) => (
                <div
                  className="flex space-x-[16px] items-center"
                  key={`views-log-${index}`}
                >
                  <LogSearchTimeIcon />
                  <div className="w-full space-y-[5px]">
                    <div className="flex justify-between w-full items-center">
                      <div className="flex items-center space-x-[16px]">
                        <Link href={`/detail/${item?._id}/${item?.type}`}>
                          <div className="aspect-square">
                            <img
                              src={
                                item?.mainImg
                                  ? item?.mainImg
                                  : "/images/profileDef.png"
                              }
                              className="h-[64px] w-[64px] object-cover"
                            />
                          </div>
                        </Link>
                        <Link href={`/detail/${item?._id}/${item?.type}`}>
                          <div>{item?.name}</div>
                        </Link>
                      </div>
                      <div className="text-[12px] text-gray-400">
                        {dayjs(item?.createdAt).format("YYYY-MM-DD HH:mm")}
                      </div>
                      {/* <IoIosCloseCircleOutline size={24} /> */}
                    </div>
                    <div className="border-t w-full" />
                  </div>
                </div>
              ))
            ) : (
              <div className="flex items-center justify-center py-16 w-full bg-[#F9FBFC] space-y-10">
                <div>Танд одоогоор үзэлтийн түүх үүсээгүй байна.</div>
              </div>
            )}
          </div>
          {data?.data?.length > 9 && (
            <div className="flex mt-4 justify-center">
              <Pagination
                setPage={(page: any) =>
                  router?.push(`/search-log?page=${page}`)
                }
                page={currentPage}
                total={data?.pagination?.total}
                limit={data?.pagination?.limit}
              />
            </div>
          )}
        </div>
      </ProfileLayout>
    </DefaultLayout>
  );
};

export default SearchLog;
