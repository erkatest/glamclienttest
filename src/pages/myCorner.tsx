import React from "react";
import DefaultLayout from "~/components/Layout";
import MyCornerContainer from "~/containers/MyCornerContainer";

const MyCorner = () => {
  return (
    <DefaultLayout>
      <MyCornerContainer />
    </DefaultLayout>
  );
};

export default MyCorner;
