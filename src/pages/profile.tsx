import React, { useEffect, useState } from "react";
import Arrow from "~/assets/Arrow";
import EditIcon from "~/assets/EditIcon";
import FolderIcon from "~/assets/FolderIcon";
import DefaultLayout from "~/components/Layout";
import ProfileLayout from "~/components/Layout/ProfileLayout";
import { Formik, Form, Field } from "formik";
import apiUsers from "~/api/users";
import { useUser } from "~/context/auth";
import { toast } from "react-toastify";
import useMessage from "~/hooks/useMessage";
import { useQuery } from "react-query";
import apiFolder from "~/api/folder";
import Link from "next/link";
import { useRouter } from "next/router";
import PasswordIcon from "~/assets/PasswordIcon";
import ChangePasswordModal from "~/components/Modals/ChangePasswordModal";
import store from "~/utils/store";
import ContentSkeleton from "~/components/ContentCard/Skeleton";
import ContentCard from "~/components/ContentCard";

const Profile = () => {
  const router = useRouter();
  const [edit, setEdit] = useState(false);
  const [passwordChangeModal, setPasswordChangeModal] = useState(false);
  const [loading, setLoading] = useState(false);
  const { user, checkUserMe } = useUser();
  const f = useMessage();
  React.useEffect(() => {
    if (store?.get("user") != null) {
      checkUserMe();
    } else {
      router.push("/");
    }
  }, []);
  const [profileImage, setProfileImage] = useState("");
  useEffect(() => {
    if (user) {
      setProfileImage(user?.img);
    }
  }, user);
  const handleImageChange = async (event: any) => {
    setLoading(true);
    const file = event.target.files[0];
    if (file) {
      const formData = new FormData();
      formData.append("image", file);
      const image = await apiUsers?.upload(formData);
      if (image.data && image.status === "success") {
        const res = await apiUsers.update({ img: image.data?.path });
        if (res.data && res.status === "success") {
          checkUserMe();
          setProfileImage(URL.createObjectURL(file));
          setLoading(false);
          toast.success(f({ id: "success-updated-message" }));
        }
      }
    }
    setLoading(false);
  };
  const { data, refetch, isLoading } = useQuery(["my-Folders"], () =>
    apiFolder.find()
  );

  const initialValues = {
    firstname: user?.firstname,
    lastname: user?.lastname,
    age: user?.age ? user?.age : "16",
  };
  const handleSubmit = async (values: any) => {
    const res = await apiUsers.update(values);

    if (res.data && res.status === "success") {
      checkUserMe();

      toast.success(f({ id: "success-updated-message" }));
    } else {
      toast.error(res.e);
      return { success: false };
    }
    setEdit(false); // Switch back to view mode
  };
  const typeReturnName = (type: any) => {
    if (type === "koha") {
      return "Номын сан";
    } else if (type === "atwork") {
      return "Урын сан";
    } else if (type === "archive") {
      return "Сан хөмрөг";
    } else if (type === "heritage") {
      return "Соёлын өв";
    }
  };

  return (
    <DefaultLayout>
      <ChangePasswordModal
        isOpen={passwordChangeModal}
        onClose={() => setPasswordChangeModal(false)}
      />
      <div className="bg-[#112437] py-[24px] mb-[15px] lg:mb-[40px]">
        <div className="text-[21px] font-[600] text-white  container mx-auto px-[16px]">
          Хувийн мэдээлэл
        </div>
      </div>
      <ProfileLayout>
        <div className="space-y-[20px] lg:px-[64px]">
          <Formik initialValues={initialValues} onSubmit={handleSubmit}>
            {({ values }) => (
              <Form className="space-y-[40px] lg:space-y-[20px]">
                <div className="lg:flex justify-between hidden ">
                  <div
                    onClick={() => setPasswordChangeModal(true)}
                    className="px-[16px] py-[8px] bg-[#112437] rounded-[1rem] flex items-center space-x-[10px] text-[14px] lg:text-[16px] text-white cursor-pointer"
                  >
                    <div className="cursor-pointer">
                      <PasswordIcon />
                    </div>
                    <div>Нууц үг солих</div>
                  </div>
                  <div>
                    {edit ? (
                      <>
                        <button
                          className="text-[14px] lg:text-[16px] bg-[#112437] px-[20px] py-[11px] rounded-[1rem] text-white font-lora mr-2"
                          onClick={() => setEdit(false)}
                        >
                          Болих
                        </button>
                        <button
                          className="text-[14px] lg:text-[16px] bg-[#112437] px-[20px] py-[11px] rounded-[1rem] text-white font-lora"
                          type="submit"
                        >
                          Хадгалах
                        </button>
                      </>
                    ) : (
                      <div
                        className="px-[16px] py-[8px] bg-[#112437] rounded-[1rem] flex items-center space-x-[10px] text-[14px] lg:text-[16px] text-white cursor-pointer"
                        onClick={() => setEdit(!edit)}
                      >
                        <div>Мэдээлэл засварлах</div>
                        <div className="cursor-pointer">
                          <EditIcon />
                        </div>
                      </div>
                    )}
                  </div>
                </div>
                <div className="relative pt-5 lg:pt-0">
                  <div
                    onClick={() => setPasswordChangeModal(true)}
                    className="lg:hidden cursor-pointer absolute -top-10 lg:top-0 left-0 px-[16px] py-[8px] bg-[#112437] rounded-full flex items-center space-x-[10px] text-[14px] lg:text-[16px] text-white cursor-pointer"
                  >
                    <div className="cursor-pointer">
                      <PasswordIcon />
                    </div>
                    <div>Нууц үг солих</div>
                  </div>
                  <div className="space-y-[5px] text-center">
                    <div className="relative mx-auto w-[100px] h-[100px] cursor-pointer">
                      {loading ? (
                        <svg
                          aria-hidden="true"
                          className="w-[100px] h-[100px] rounded-full border-[6px] border-[#112437] text-gray-200 animate-spin dark:text-gray-600 fill-blue-600"
                          viewBox="0 0 100 101"
                          fill="none"
                          xmlns="http://www.w3.org/2000/svg"
                        >
                          <path
                            d="M100 50.5908C100 78.2051 77.6142 100.591 50 100.591C22.3858 100.591 0 78.2051 0 50.5908C0 22.9766 22.3858 0.59082 50 0.59082C77.6142 0.59082 100 22.9766 100 50.5908ZM9.08144 50.5908C9.08144 73.1895 27.4013 91.5094 50 91.5094C72.5987 91.5094 90.9186 73.1895 90.9186 50.5908C90.9186 27.9921 72.5987 9.67226 50 9.67226C27.4013 9.67226 9.08144 27.9921 9.08144 50.5908Z"
                            fill="currentColor"
                          />
                          <path
                            d="M93.9676 39.0409C96.393 38.4038 97.8624 35.9116 97.0079 33.5539C95.2932 28.8227 92.871 24.3692 89.8167 20.348C85.8452 15.1192 80.8826 10.7238 75.2124 7.41289C69.5422 4.10194 63.2754 1.94025 56.7698 1.05124C51.7666 0.367541 46.6976 0.446843 41.7345 1.27873C39.2613 1.69328 37.813 4.19778 38.4501 6.62326C39.0873 9.04874 41.5694 10.4717 44.0505 10.1071C47.8511 9.54855 51.7191 9.52689 55.5402 10.0491C60.8642 10.7766 65.9928 12.5457 70.6331 15.2552C75.2735 17.9648 79.3347 21.5619 82.5849 25.841C84.9175 28.9121 86.7997 32.2913 88.1811 35.8758C89.083 38.2158 91.5421 39.6781 93.9676 39.0409Z"
                            fill="currentFill"
                          />
                        </svg>
                      ) : (
                        <img
                          src={profileImage || "/images/logo-black.svg"}
                          className="w-[100px] h-[100px] object-cover rounded-full border-[6px] border-[#112437]"
                        />
                      )}
                      <div className="absolute rounded-full inset-0 flex items-center justify-center bg-black bg-opacity-50 opacity-0 hover:opacity-100">
                        <label htmlFor="imageUpload" className="cursor-pointer">
                          <EditIcon />
                        </label>
                        <input
                          id="imageUpload"
                          type="file"
                          accept="image/*"
                          onChange={handleImageChange}
                          className="hidden"
                        />
                      </div>
                    </div>
                    <div className="text-[#A6B2C3] text-[16px] ">
                      {user?.email}
                    </div>

                    {/* <div className="text-[14px] font-[700] lg:text-[16px] text-[#112437]">
                      Нас:
                      {edit ? (
                        <Field
                          name="age"
                          type="text"
                          placeholder="Нас"
                          className="border rounded-md px-2 py-2 ml-2 font-[400]"
                        />
                      ) : (
                        <span className="ml-2">{initialValues?.age}</span>
                      )}
                    </div> */}
                  </div>
                  <div className="lg:hidden absolute -top-10 lg:top-0 right-0">
                    {edit ? (
                      <button
                        className="text-[14px] lg:text-[16px] bg-[#112437] px-[20px] py-[8px] rounded-full text-white"
                        type="submit"
                      >
                        Хадгалах
                      </button>
                    ) : (
                      <div
                        className="px-[16px] py-[8px] bg-[#112437] rounded-full flex items-center space-x-[10px] text-[14px] lg:text-[16px] text-white cursor-pointer"
                        onClick={() => setEdit(!edit)}
                      >
                        <div>Мэдээлэл засварлах</div>
                        <div className="cursor-pointer">
                          <EditIcon />
                        </div>
                      </div>
                    )}
                  </div>
                </div>
                <div className="flex space-x-[48px] justify-center">
                  <div className="flex items-center gap-2">
                    <div className=" text-[#112437] font-[700]">Нэр:</div>
                    {edit ? (
                      <Field
                        className="border rounded-md p-2 w-full lg:w-auto"
                        name="firstname"
                        type="text"
                        placeholder="Овог"
                      />
                    ) : (
                      <div className="text-[20px] text-[#112437] font-[700]">
                        {initialValues.firstname}
                      </div>
                    )}
                  </div>

                  <div className="flex items-center gap-2">
                    <div className=" text-[#112437] font-[700]">Овог:</div>
                    {edit ? (
                      <Field
                        className="border rounded-md p-2 w-full lg:w-auto"
                        name="lastname"
                        type="text"
                        placeholder="Овог"
                      />
                    ) : (
                      <div className="text-[20px] capitalize text-[#112437] font-[700]">
                        {initialValues.lastname}
                      </div>
                    )}
                  </div>
                </div>
              </Form>
            )}
          </Formik>
        </div>
      </ProfileLayout>
    </DefaultLayout>
  );
};

export default Profile;
