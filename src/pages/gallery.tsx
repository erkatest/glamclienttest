import React from "react";

import { useQuery } from "react-query";
import apiItem from "~/api/item";
import DefaultLayout from "~/components/Layout";

import Link from "next/link";
import CategorySwiper from "~/components/Artwork/CategorySwiper";
import useMessage from "~/hooks/useMessage";
import { TabContext } from "~/context/tab";
import useParams from "~/hooks/useParams";
import { FiSearch } from "react-icons/fi";
import { useRouter } from "next/router";
import Creations from "~/components/Artwork/Creations";
import Collections from "~/components/Artwork/Collections";
import apiMap from "~/api/map";
import { MdArrowForwardIos } from "react-icons/md";
import LocationSwiper from "~/components/LocationSwiper";

const Artwork = () => {
  const f = useMessage();
  const [q, setQ] = React.useState("");
  const { tabs, setActive, active } = React.useContext(TabContext);
  const { activeTabProps } = React.useContext(TabContext);
  const params = useParams();
  const router = useRouter();

  const { data: artworkData, isLoading: isartworkDataLoading } = useQuery(
    ["location-artwork"],
    () => apiMap.find("artwork", " "),
    {
      refetchOnWindowFocus: false,
      refetchInterval: false,
    }
  );

  return (
    <DefaultLayout header={{ isIndex: true }}>
      <div className="space-y-10 mb-[2rem]">
        <div
          className={`relative ${activeTabProps?.backgroundColor} dark:text-[#BE8100] z-90 h-[300px] bg-gradient-to-b from-red`}
          style={{
            backgroundImage:
              "linear-gradient(to bottom, rgba(0, 0, 0, 0.3), rgba(0, 0, 0, 0.1))",
          }}
        >
          <div className="container flex items-center justify-center flex-col h-full">
            <div className="uppercase text-[40px] font-semibold text-white mb-5">
              {f({ id: "header-menu-1" })}
            </div>
            <div
              className={
                "bg-white w-full rounded-full relative overflow-hidden"
              }
            >
              <input
                placeholder={f({ id: "artwork-search-placeholder" })}
                className="h-[50px] lg:h-[60px] w-full p-5 pl-16 focus:outline-none"
                value={q}
                onChange={(e) => {
                  setQ(e.target.value);
                }}
                onKeyDown={(e) => {
                  if (e.key === "Enter") {
                    router.push({
                      pathname: "/search",
                      search: `?q=${q || ""}`,
                    });
                    setActive("gallery");
                  }
                }}
              />
              <div className="absolute top-1/2 left-5 transform -translate-y-1/2">
                <FiSearch size={28} />
              </div>
            </div>
          </div>
        </div>

        <h1 className="container uppercase">{f({ id: "categories" })}</h1>

        <CategorySwiper />
        <h1 className="container uppercase">
          {f({ id: "artwork-creations" })}
        </h1>

        <Creations />

        <h1 className="container uppercase">
          {f({ id: "artwork-collections" })}
        </h1>

        <Collections />

        <div className="flex">
          <button
            className={`font-lora font-[700] rounded-[1rem] text-white px-[1rem] py-[0.8rem] mx-auto ${activeTabProps?.backgroundColor}`}
          >
            <Link href={"/search/?q="} className="md:text-[16px]">
              {f({ id: "see-next" })}
            </Link>
          </button>
        </div>

        <div className="flex justify-between items-center container mx-auto">
          <h2 className="uppercase">
            {f({ id: "artwork-gallery-locations" })}
          </h2>
          <Link href={"/map/artwork"}>
            <div className="flex items-center gap-[1rem] hover:opacity-80">
              <p className={`font-[700] text-[16px]`}>{f({ id: "see-all" })}</p>

              <MdArrowForwardIos />
            </div>
          </Link>
        </div>
        <div className="container mx-auto">
          <LocationSwiper locations={artworkData?.data} />
        </div>
      </div>
    </DefaultLayout>
  );
};

export default Artwork;
