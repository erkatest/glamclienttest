import React, { useState } from "react";
import Arrow from "~/assets/Arrow";
import EditIcon from "~/assets/EditIcon";
import FolderIcon from "~/assets/FolderIcon";
import DefaultLayout from "~/components/Layout";
import ProfileLayout from "~/components/Layout/ProfileLayout";

const Notification = () => {
  const [edit, setEdit] = useState(false);
  const data = [
    {
      img: "https://scontent.fuln2-2.fna.fbcdn.net/v/t39.30808-6/248711625_4504081313007384_6532828299798955333_n.jpg?_nc_cat=111&ccb=1-7&_nc_sid=efb6e6&_nc_ohc=LT5DBbrTNKEAX-1zT97&_nc_ht=scontent.fuln2-2.fna&oh=00_AfBKa5cKFK8luPvLfNBE4WKacDjuvVa31jBG7WZMyOZyiQ&oe=657D6875",
      description:
        "А. Төгөлдөр танд найзын хүсэлт явууллаа. А. Төгөлдөр танд найзын хүсэлт явууллаа. А. Төгөлдөр танд найзын хүсэлт явууллаа. А. Төгөлдөр танд найзын хүсэлт явууллаа. А. Төгөлдөр танд найзын хүсэлт явууллаа. А. Төгөлдөр танд найзын хүсэлт явууллаа. А. Төгөлдөр танд найзын хүсэлт явууллаа. А. Төгөлдөр танд найзын хүсэлт явууллаа.",
    },
    {
      img: "https://scontent.fuln2-2.fna.fbcdn.net/v/t39.30808-6/248711625_4504081313007384_6532828299798955333_n.jpg?_nc_cat=111&ccb=1-7&_nc_sid=efb6e6&_nc_ohc=LT5DBbrTNKEAX-1zT97&_nc_ht=scontent.fuln2-2.fna&oh=00_AfBKa5cKFK8luPvLfNBE4WKacDjuvVa31jBG7WZMyOZyiQ&oe=657D6875",
      description:
        "А. Төгөлдөр танд найзын хүсэлт явууллаа. А. Төгөлдөр танд найзын хүсэлт явууллаа. А. Төгөлдөр танд найзын хүсэлт явууллаа. А. Төгөлдөр танд найзын хүсэлт явууллаа. А. Төгөлдөр танд найзын хүсэлт явууллаа. А. Төгөлдөр танд найзын хүсэлт явууллаа. А. Төгөлдөр танд найзын хүсэлт явууллаа. А. Төгөлдөр танд найзын хүсэлт явууллаа.",
    },
    {
      img: "https://scontent.fuln2-2.fna.fbcdn.net/v/t39.30808-6/248711625_4504081313007384_6532828299798955333_n.jpg?_nc_cat=111&ccb=1-7&_nc_sid=efb6e6&_nc_ohc=LT5DBbrTNKEAX-1zT97&_nc_ht=scontent.fuln2-2.fna&oh=00_AfBKa5cKFK8luPvLfNBE4WKacDjuvVa31jBG7WZMyOZyiQ&oe=657D6875",

      description:
        "А. Төгөлдөр танд найзын хүсэлт явууллаа. А. Төгөлдөр танд найзын хүсэлт явууллаа. А. Төгөлдөр танд найзын хүсэлт явууллаа. А. Төгөлдөр танд найзын хүсэлт явууллаа. А. Төгөлдөр танд найзын хүсэлт явууллаа. А. Төгөлдөр танд найзын хүсэлт явууллаа. А. Төгөлдөр танд найзын хүсэлт явууллаа. А. Төгөлдөр танд найзын хүсэлт явууллаа.",
    },
  ];
  return (
    <DefaultLayout>
      <div className="bg-[#112437] py-[24px] mb-[40px]">
        <div className="text-[40px] font-[600] text-white  container mx-auto px-[16px]">
          Мэдэгдэл
        </div>
      </div>
      <ProfileLayout>
        <div className="space-y-[20px] px-[64px]">
          <div className="text-[20px] text-[#112437] font-[500]">
            2023/12/10
          </div>
          <div className="space-y-[20px]">
            {data?.map((item: any, index: number) => (
              <div className="space-y-[20px]" key={`notification-${index}`}>
                <div className="p-[16px] bg-[#f6f7f9] flex space-x-[40px] items-start">
                  <img
                    src={item?.img}
                    className="w-[80px] h-[80px] object-cover rounded-full"
                  />
                  <div className="space-y-[28px]">
                    <div className="text-[14px] text-[#112437]">
                      {item?.description}
                    </div>
                    <div className="flex space-x-[16px]">
                      <button className="text-[12px] font-[500] leading-[20px] text-[#112437] bg-[#d3d9e1] rounded-full px-[40px] py-[5px]">
                        Татгалзах
                      </button>
                      <button className="text-[12px] font-[500] leading-[20px] text-white bg-[#112437] rounded-full px-[40px] py-[5px]">
                        Зөвшөөрөх
                      </button>
                    </div>
                  </div>
                </div>
                <div className="border-t w-full border-[#A6B2C3]" />
              </div>
            ))}
          </div>
          <div className="text-[20px] text-[#112437] font-[500]">
            2023/12/09
          </div>
          <div className="space-y-[20px]">
            {data?.map((item: any, index: number) => (
              <div className="space-y-[20px]" key={`notification-${index}`}>
                <div className="p-[16px] bg-[#f6f7f9] flex space-x-[40px] items-start">
                  <img
                    src={item?.img}
                    className="w-[80px] h-[80px] object-cover rounded-full"
                  />
                  <div className="space-y-[28px]">
                    <div className="text-[14px] text-[#112437]">
                      {item?.description}
                    </div>
                    <div className="flex space-x-[16px]">
                      <button className="text-[12px] font-[500] leading-[20px] text-[#112437] bg-[#d3d9e1] rounded-full px-[40px] py-[5px]">
                        Татгалзах
                      </button>
                      <button className="text-[12px] font-[500] leading-[20px] text-white bg-[#112437] rounded-full px-[40px] py-[5px]">
                        Зөвшөөрөх
                      </button>
                    </div>
                  </div>
                </div>
                <div className="border-t w-full border-[#A6B2C3]" />
              </div>
            ))}
          </div>
        </div>
      </ProfileLayout>
    </DefaultLayout>
  );
};

export default Notification;
