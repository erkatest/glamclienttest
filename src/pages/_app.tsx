import "~/styles/globals.css";
import type { AppProps } from "next/app";
import { useEffect, useMemo, useRef, useState } from "react";
import { IntlProvider } from "react-intl";
import mn from "../intl/mn.json";
import en from "../intl/en.json";
import { useRouter } from "next/router";
import Head from "next/head";

import AOS from "aos";
import "aos/dist/aos.css";

import { Lora } from "next/font/google";
import { QueryClient, QueryClientProvider } from "react-query";
import { AuthProvider } from "~/context/auth";
import { ToastContainer } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import { ThemeProvider } from "next-themes";
import TabProvider from "~/context/tab";
import { LocationPromptProvider } from "~/context/location";
import { LottiePlayer } from "lottie-web";
import { MantineProvider, createEmotionCache } from "@mantine/core";

const lora = Lora({
  subsets: ["cyrillic-ext"],
  variable: "--font-lora",
});

export default function App({ Component, pageProps }: AppProps) {
  const router = useRouter();

  const [loader, setLoader] = useState(false);
  const { locale, defaultLocale } = useRouter();
  const ref = useRef<HTMLDivElement>(null);
  const [lottie, setLottie] = useState<LottiePlayer | null>(null);
  const messages = useMemo(() => {
    switch (locale) {
      case "mn":
        return mn;
      case "en":
        return en;
      default:
        return mn;
    }
  }, [locale]);

  useEffect(() => {
    import("lottie-web").then(Lottie => setLottie(Lottie.default));
    AOS.init();
  }, []);

  useEffect(() => {
    if (lottie && ref.current) {
      const animation = lottie.loadAnimation({
        container: ref.current,
        renderer: "svg",
        loop: true,
        autoplay: true,
        path: "/loader.json",
      });

      return () => animation.destroy();
    }
  }, [lottie, ref]);

  useEffect(() => {
    const handleStart = () => {
      setLoader(true);
    };

    const handleComplete = () => {
      setLoader(false);
    };

    router.events.on("routeChangeStart", handleStart);
    router.events.on("routeChangeComplete", handleComplete);
    router.events.on("routeChangeError", handleComplete);

    return () => {
      router.events.off("routeChangeStart", handleStart);
      router.events.off("routeChangeComplete", handleComplete);
      router.events.off("routeChangeError", handleComplete);
    };
  }, []);

  const queryClient = new QueryClient({});
  createEmotionCache({ key: "mantine", prepend: false });
  return (
    <main className={`${lora.variable} font-lora`}>
      <Head>
        <title>{messages.title}</title>
        <link rel="icon" type="image/png" href="/fav.png?v=1.1" />
        <meta
          name="viewport"
          content="width=device-width, initial-scale = 1.0, maximum-scale = 1.0, user-scalable=no"
        />
        <link
          href="https://fonts.googleapis.com/css2?family=Montserrat:wght@400;700&display=swap"
          rel="stylesheet"
        />
      </Head>
      <MantineProvider
        withGlobalStyles
        withNormalizeCSS
        theme={{
          fontFamily: 'Lora, sans-serif',
        }}
      >
        <IntlProvider
          messages={messages}
          locale={locale || "mn"}
          defaultLocale={defaultLocale}
        >
          <LocationPromptProvider>
            <TabProvider>
              <ThemeProvider enableSystem={false} attribute="class">
                <AuthProvider>
                  <QueryClientProvider client={queryClient}>
                    <ToastContainer
                      position="bottom-right"
                      autoClose={8000}
                      limit={3}
                      hideProgressBar={true}
                      newestOnTop={false}
                      closeOnClick
                      rtl={false}
                      pauseOnFocusLoss
                      draggable
                      pauseOnHover
                      theme="light"
                      style={{ zIndex: 999999 }}
                    />
                    <div
                      className={`fixed insets-0 h-full overflow-hidden w-full  flex flex-col justify-center items-center transition-all z-40 ${
                        loader
                          ? "opacity-100 visible z-50"
                          : "invisible opacity-0 hidden"
                      }`}
                    >
                      <div className="w-[300px] z-50">
                        <div ref={ref} />
                      </div>
                    </div>

                    <div
                      className={` transition-all ${
                        loader ? "blur-sm z-50" : ""
                      }`}
                    >
                      <main className={`${lora.variable} `}>
                        <Component {...pageProps} />
                      </main>
                    </div>
                  </QueryClientProvider>
                </AuthProvider>
              </ThemeProvider>
            </TabProvider>
          </LocationPromptProvider>
        </IntlProvider>
      </MantineProvider>
    </main>
  );
}
