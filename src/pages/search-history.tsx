import React from "react";
import DefaultLayout from "~/components/Layout";
import SearchHistoryContainer from "~/containers/SearchHistory";

const SearchHistory = () => {
  return (
    <DefaultLayout>
      <SearchHistoryContainer />
    </DefaultLayout>
  );
};

export default SearchHistory;
