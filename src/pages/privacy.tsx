import React from "react";
import DefaultLayout from "~/components/Layout";
import { useRouter } from "next/router";
import useMessage from "../hooks/useMessage";

const Privacy = () => {
  const f = useMessage();

  const data = [
    {
      title: f({ id: "privacy-title-1" }),
      desc: f({ id: "privacy-desc-1" }),
    },
    {
      title: f({ id: "privacy-title-2" }),
      desc: f({ id: "privacy-desc-2" }),
    },
    {
      title: f({ id: "privacy-title-3" }),
      desc: f({ id: "privacy-desc-3" }),
    },
    {
      title: f({ id: "privacy-title-4" }),
      desc: f({ id: "privacy-desc-4" }),
    },
    {
      title: f({ id: "privacy-title-5" }),
      desc: f({ id: "privacy-desc-5" }),
    },
    {
      title: f({ id: "privacy-title-6" }),
      desc: f({ id: "privacy-desc-6" }),
    },
    {
      title: f({ id: "privacy-title-7" }),
      desc: f({ id: "privacy-desc-7" }),
    },
  ];

  return (
    <DefaultLayout>
      <main className="container text-[16px] mx-auto max-w-6xl px-8 py-0 text-justify lg:py-8">
        <h1 className="text-[16px] font-bold mb-4">
          {f({ id: "privacy-policy" })}
        </h1>
        <p className="text-[16px] mb-4">
          {f({ id: "privacy-policy-introduction-1" })}
        </p>
        <h3 className="text-[16px] font-bold mb-2">
          {f({ id: "privacy-policy-introduction-2" })}
        </h3>
        {data?.map((item) => {
          return (
            <>
              <p className="italic text-[16px]">{item.title}</p>
              <p
                className="text-[16px] mb-4"
                dangerouslySetInnerHTML={{
                  __html: item.desc,
                }}
              />
            </>
          );
        })}
      </main>
    </DefaultLayout>
  );
};

export default Privacy;
