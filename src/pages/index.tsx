import React, { useState, useEffect } from "react";

import DefaultLayout from "../components/Layout";
import Section1 from "../components/Index/Section1";
import Section2 from "../components/Index/Section2";
import Section5 from "../components/Index/Section5";
import LocationModal from "~/components/Modals/AskLocationModal";
import { useRouter } from "next/router";
import Section7 from "~/components/Index/Section7";
import { useUser } from "~/context/auth";
const IndexPage: React.FC = () => {
  const [loading, setLoading] = useState(false);
  const [error, setError] = useState("");
  const [showModal, setShowModal] = useState(false);
  const { user } = useUser();
  const router = useRouter();
  const handleLocationAccess = () => {
    setLoading(true);
    if (!navigator.geolocation) {
      setError("Geolocation is not supported by your browser");
      setLoading(false);
      return;
    }
    navigator.geolocation.getCurrentPosition(
      (position) => {
        const locationData = {
          lat: position.coords.latitude,
          lng: position.coords.longitude,
          timestamp: new Date().getTime(),
        };
        localStorage.setItem("locationData", JSON.stringify(locationData));
        setShowModal(false);
        setLoading(false);
        router.reload();
      },
      (error) => {
        setError(error.message);
        setShowModal(false);
        setLoading(false);
      }
    );
  };

  const handleDecline = () => {
    setError("Location access denied");
    setShowModal(false);
  };

  useEffect(() => {
    const storedData = localStorage.getItem("locationData");
    if (storedData) {
      const locationData = JSON.parse(storedData);
      const currentTime = new Date().getTime();
      const timeDifference = currentTime - locationData.timestamp;
      if (timeDifference > 5 * 24 * 60 * 60 * 1000) {
        localStorage.removeItem("locationData");
        setShowModal(true);
      }
    } else {
      setShowModal(true);
    }
  }, []);

  return (
    <DefaultLayout header={{ isIndex: true }}>
      <LocationModal
        isOpen={showModal}
        onClose={() => setShowModal(false)}
        onAccept={handleLocationAccess}
        onDecline={handleDecline}
        error={error}
        loading={loading}
      />
      <Section1 />
      <Section2 />
      <Section5 />
      {!user && <Section7 />}
    </DefaultLayout>
  );
};

export default IndexPage;
