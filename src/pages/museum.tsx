import React from "react";
import { useQuery } from "react-query";

import DefaultLayout from "~/components/Layout";
import CultureHeritageSearch from "~/components/Heritage/HeritageSearch";
import CultureMovable from "~/components/Heritage/HeritageMovable";
import CultureIntangible from "~/components/Heritage/HeritageIntangible";
import CultureInmovable from "~/components/Heritage/HeritageInmovable";
import Link from "next/link";
import { MdArrowForwardIos } from "react-icons/md";
import useMessage from "~/hooks/useMessage";
import apiMap from "~/api/map";
import LocationSwiper from "~/components/LocationSwiper";

const Culture = () => {
  const f = useMessage();
  const { data: museumData, isLoading: ismuseumDataLoading } = useQuery(
    ["museum-locations"],
    () => apiMap.find("museum", "  "),
    {
      refetchOnWindowFocus: false,
      refetchInterval: false,
    }
  );

  return (
    <DefaultLayout>
      <CultureHeritageSearch />
      <CultureMovable />
      <CultureIntangible />
      <CultureInmovable />
      <div className="space-y-[1rem] md:space-y-[2rem] container mx-auto">
        <div className="space-y-[1rem] md:space-y-[2rem]">
          <div className="flex justify-between items-center">
            <h2 className="uppercase">{f({ id: "museum-locations" })}</h2>
            <Link href={"/map?type=museum"}>
              <div className="flex items-center gap-[1rem] hover:opacity-80">
                <p className={`font-[700] text-[16px]`}>
                  {f({ id: "see-all" })}
                </p>
                <MdArrowForwardIos />
              </div>
            </Link>
          </div>
        </div>
        <LocationSwiper locations={museumData?.data} />
      </div>
    </DefaultLayout>
  );
};

export default Culture;
