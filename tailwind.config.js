const { fontFamily } = require("tailwindcss/defaultTheme");
/** @type {import('tailwindcss').Config} */
module.exports = {
  darkMode: "class",
  content: ["./src/**/*.{js,ts,jsx,tsx}"],
  theme: {
    fontFamily: {
      body: ["var(--font-lora)", ...fontFamily.sans],
      lora: ["var(--font-lora)", ...fontFamily.sans],
      display: ["var(--font-lora)", ...fontFamily.sans],
    },
    extend: {
      width: {
        '80': "380px"
      },
      fontFamily: {
        montserrat: ["Montserrat"],
        sans: ["var(--font-lora)", ...fontFamily.sans],
        body: ["var(--font-lora)", ...fontFamily.sans],
      },
      colors: {
        c1: "#011C5A",
        c2: "#E6EAF4",
        c3: "#012E93",
        c4: "#051332", // signin title
        c5: "#6C86C0",
        c6: "#012E93",
        c7: "#A6B2C3", // silver
        c8: "#464B13", // green
        c9: "#312C59", // space cadet
        c10: "rgba(0, 0, 0, 0.6)",
        c11: "#14452F", // Heritage detail green
        c12: "#4D4D4D", // Heritage detail silver
        c13: "#112437", // base black
        c14: "rgba(20, 69, 47, 0.2)",
        c15: "#213951",
        c16: "#D0D5DD",
        c17: "#667085",
        c18: "#101828",
        black: "#000000",
        default: "#112437",
        artwork: "#BE3455",
        koha: "#01417A",
        education: "#2E294E",
        heritage: "#13452F",
      },
      boxShadow: {
        "3xl": "8px 8px 15px 0px rgba(0, 0, 0, 0.1)",
      },
    },
    container: {
      center: true,
      padding: {
        DEFAULT: "1rem",
      },
    },
    screens: {
      sm: "640px",
      md: "768px",
      lg: "1024px",
      xl: "1280px",
      "1xl": "1416px",
      "2xl": "1536px",
      "3xl": "1578px",
      "4xl": "1636px",
    },
  },
  plugins: [],
};
